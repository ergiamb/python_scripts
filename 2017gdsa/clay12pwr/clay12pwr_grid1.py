#make regions, strata, initial condition, and source sink cards to go with 2017gdsa, clay, grid1
#not making boundary condition cards in here, or flow and transport conditions, or actual materials
#emily 10.29.16

import wfg_block as wfg

#hardwire some names
mat = ['shale','drz','siltstone','silt_drz','sandstone','sand_drz','limestone','lime_drz','buffer','wp']
flow = ['initial','wp_heatsource']
trans = ['initial']

#define dimensions (for the most part)
small = 5./3. #smallest division of material

num_drift = 42 #number of disposal drifts
num_wp = 50 #number of wp/drift

x0 = 460. #start of longhall, no drz, room for 5 m of drz behind it
x_shaft = 10.
x_shaft_drz = 5.
x_between = 40. #distance from shaft to start of repository
x_extra = small #necessary for grid1a drift0 drz
x1 = x0 + x_shaft + x_between + x_extra #west end of repository, no drz 
x_shorthall = 5. #width of short hall connecting long hall to panel
x_drift = 5.
x_drz = small
x_wp = small
x_pillar = 25.
x_repeat = x_drift + x_pillar
x_access = x_repeat*num_drift - x_pillar #no drz
x_longhall = 2.*x_shaft + 2.*x_between + x_repeat*num_drift - x_pillar + 5. #no drz in here 
x2 = x0 + x_longhall - x_shaft #start of east shaft, no drz
xwest = 0.
xeast = 6855.

y1 = 20. #south face of repository (long hall), no drz
y_drz = small
y_shaft = 5. #width of shaft (w/o drz)
y_longhall = 5. #width w/o drz
y_shorthall = 40. #distance between longhall and panel (of num_drift)
y_access = 5. #width of hall connecting panels
y2 = y1 + y_longhall + y_shorthall #south face of south access hall
y3 = y2 + y_access #start of drift including backfill/seal
y_drift = 1035. #including 25 m of backfill/seal
y_wp = 5.
y_space = 15. #distance between waste packages
y_backfill = 25. #each end of drift w/o waste package
#consider adding a seal that included drz excavation later
y_repeat = y_wp + y_space
ysouth = 0.
ynorth = 1575.

z1 = 690. - small #base of repository, no drz
z_hall = 5.
z_drz = small
z_wp = small
ztop = 1200. #top of domain which is siltstone
ztop_aq3 = ztop-105. #top of sandstone aquifer
ztop_shale3 = ztop_aq3 - 45. #top of shale/host rock
ztop_aq2 = ztop_shale3 - 600. #top of carbonate/chalk aquifer
ztop_shale2 = ztop_aq2 - 45. #top of second thinner shale
ztop_aq1 = ztop_shale2 - 90. #top of deep sandstone aquifer
ztop_shale1 = ztop_aq1 - 45. #top of basal confining unit
zbase = 0.

#write region, strata, source sink, and waste form blocks
#model domain, then DRZ, then buffer, then wp
rfile = file('regions.txt', 'w')
sfile = file('strata.txt', 'w')
ssfile = file('source_sink.txt','w')
wfgfile = file('wfg.txt','w')
wfg.gdsa_to_file(wfgfile) #right now this is inventory for 12-PWR 100 y OoR

#files need to be open already, and closed afterward!
def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

#Model Domain - cut and paste this first region into the main input deck
write_reg('all',xwest,ysouth,zbase,xeast,ynorth,ztop)
write_strata('all',mat[0])
write_reg_face('top',xwest,ysouth,ztop,xeast,ynorth,ztop)
write_reg_face('bottom',xwest,ysouth,zbase,xeast,ynorth,zbase)
write_reg_face('west',xwest,ysouth,zbase,xwest,ynorth,ztop)
write_reg_face('east',xeast,ysouth,zbase,xeast,ynorth,ztop)
write_reg_face('south',xwest,ysouth,zbase,xeast,ysouth,ztop)
write_reg_face('north',xwest,ynorth,zbase,xeast,ynorth,ztop)

#Other sedimentary strata above and below the repository horizon 
write_reg('siltstone',xwest,ysouth,ztop_aq3,xeast,ynorth,ztop)
write_strata('siltstone',mat[2])
write_reg('aq3',xwest,ysouth,ztop_shale3,xeast,ynorth,ztop_aq3)
write_strata('aq3',mat[4])
write_reg('aq2',xwest,ysouth,ztop_shale2,xeast,ynorth,ztop_aq2)
write_strata('aq2',mat[6])
write_reg('aq1',xwest,ysouth,ztop_shale1,xeast,ynorth,ztop_aq1)
write_strata('aq1',mat[4])

#DRZ, longhall
xmin = x0-x_shaft_drz
ymin = y1-y_drz
zmin = z1-z_drz
xmax = xmin + x_longhall + x_shaft_drz*2.
ymax = ymin + y_longhall + y_drz*2.
zmax = zmin + z_hall + z_drz*2.
write_reg('drz_longhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_longhall',mat[1])
#DRZ west shaft, drz=halite;adrz=aquifer;ssdrz=siltstone
xmin = x0-x_shaft_drz
ymin = y1-y_drz
zmin = z1-z_drz
xmax = xmin + x_shaft + x_shaft_drz*2.
ymax = ymin + y_shaft + y_drz*2.
zmax = ztop_shale3
write_reg('drz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_wshaft',mat[1])
zmin = ztop_shale3
zmax = ztop_aq3
write_reg('adrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('adrz_wshaft',mat[5])
zmin = ztop_aq3
zmax = ztop
write_reg('sdrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('sdrz_wshaft',mat[3])
#DRZ east shaft, drz=halite;adrz=aquifer;ssdrz=siltstone
xmin = x2 - x_shaft_drz
zmin = z1 - z_drz
xmax = xmin + x_shaft + x_shaft_drz*2.
zmax = ztop_shale3
write_reg('drz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_eshaft',mat[1])
zmin = ztop_shale3
zmax = ztop_aq3
write_reg('adrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('adrz_eshaft',mat[5])
zmin = ztop_aq3
zmax = ztop
write_reg('sdrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('sdrz_eshaft',mat[3])
#DRZ short hall, access halls, and disposal drifts
zmin = z1 - z_drz
zmax = z1 + z_hall + z_drz
#short hall connecting long hall to panel
ymin = 0.
ymax = y1 + y_longhall + y_shorthall
xmin = x1 - x_drz
xmax = x1 + x_shorthall + x_drz
write_reg('drz_shorthall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_shorthall',mat[1])
#south access hall of panel
ymin = y2 - y_drz
ymax = ymin + y_access + y_drz*2.
xmin = x1 - x_drz
xmax = x1 + x_access + x_drz
write_reg('drz_saccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_saccess',mat[1])
#north access hall of panel
ymin = y3 + y_drift - y_drz
ymax = ymin + y_access + y_drz*2.
xmin = x1 - x_drz
xmax = x1 + x_access + x_drz
write_reg('drz_naccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_naccess',mat[1])
#drifts
ymin = y3 - y_drz
ymax = y3 + y_drift + y_drz*2.
for j in range(num_drift):
  xmin = x1 - x_drz + j*x_repeat
  xmax = xmin + x_drift + x_drz*2.
  write_reg('drz_drift'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_drift'+str(j),mat[1])

#Buffer/Backfill
#buffer, longhall
xmin = x0
ymin = y1
zmin = z1
xmax = xmin + x_longhall
ymax = ymin + y_longhall
zmax = zmin + z_hall
write_reg('bf_longhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_longhall',mat[8])
#buffer west shaft
xmin = x0
ymin = y1
zmin = z1
xmax = xmin + x_shaft
ymax = ymin + y_shaft
zmax = ztop
write_reg('bf_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_wshaft',mat[8])
#buffer east shaft
xmin = x2
xmax = xmin + x_shaft
write_reg('bf_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_eshaft',mat[8])
#buffer short hall, access halls, and disposal drifts
zmin = z1
zmax = z1 + z_hall
#short hall connecting long hall to panel
ymin = 0.
ymax = y1 + + y_longhall + y_shorthall
xmin = x1
xmax = x1 + x_shorthall
write_reg('bf_shorthall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_shorthall',mat[8])
#south access hall of panel
ymin = y2
ymax = ymin + y_access
xmin = x1
xmax = x1 + x_access
write_reg('bf_saccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_saccess',mat[8])
#north access hall of panel
ymin = y2 + y_access + y_drift
ymax = ymin + y_access
xmin = x1
xmax = x1 + x_access
write_reg('bf_naccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_naccess',mat[8])
#drifts
ymin = y3
ymax = y3 + y_drift
for j in range(num_drift):
  xmin = x1 + j*x_repeat
  xmax = xmin + x_drift
  write_reg('bf_drift'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('bf_drift'+str(j),mat[8])
  
#Waste Packages (CSNF)
zmin = z1 + small #middle of drift
zmax = zmin + z_wp
zcenter = (zmin+zmax)/2.
for i in range(num_drift):
  xmin = x1 + small + i*x_repeat
  xmax = xmin + x_wp
  xcenter = (xmin+xmax)/2.
  for j in range(num_wp):
    ymin = y3 + y_backfill + j*y_repeat
    ymax = ymin + y_wp
    ycenter = (ymin+ymax)/2.
    name = 'wp'+str(i)+'_'+str(j)
    write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata(name,mat[9])
    write_ss(name,flow[1],trans[0],name)
    #wfg.csnf_to_file(wfgfile,xcenter,ycenter,zcenter) #update this later to new volume averaged option
    wfg.csnf_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

rfile.close
sfile.close
ssfile.close
wfgfile.close


