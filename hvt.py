#read in all_heat_2050.txt and write it over and over again for 2.738e-3 year increments (24 hours)
#this is not the best way to do it, it's the quickest way to do it.
#if it works, make a better method beginning with calculating desired interval in magic spreadsheet
#emily 2.29.16
#oops - no better way yet, using it again for dbh_triplets. 4.8.16

import numpy as np
#import wfg_block as wfg #someday add waste_form_general block with package breach

fin1 = file('./cs_heat_2050.txt', 'r')
fin2 = file('./sr_heat_2050.txt', 'r')

#throw away first four lines
for i in range(4):
  fin1.readline()
  fin2.readline()

#keep the rest in time array and heat array
firstline = 5 #wow this is an awkward way to index this.
lastline = 63
num_t = lastline-firstline+1 #needs to be the same in fin1 and fin2
num_i = 108 #waste packages, each holding 18 canisters
time1 = []
energy1 = []
time2 = []
energy2 = []
n_cs = 74
n_sr = num_i-n_cs

for i in range(num_t): 
  line = fin1.readline()
  time1.append(float(line.split()[0]))
  energy1.append(float(line.split()[1])/n_cs)
fin1.close()

for i in range(num_t): 
  line = fin2.readline()
  time2.append(float(line.split()[0]))
  energy2.append(float(line.split()[1])/n_sr)
fin2.close()

rfile = file('hvt_region.txt','w')
cfile = file('hvt_condition.txt','w')
ssfile = file('hvt_ss.txt', 'w')
sschem = file('hvt_ss_chem.txt','w')
sfile = file('hvt_strata.txt','w')
obsfile = file('hvt_obs_points.txt','w')
obsregf = file('hvt_obs_region.txt','w')
#wfgfile = file('hvt_wfg.txt','w')

z = 1000.0 #base of disposal zone
z_il = 0.7 #ht of impact limiter
z_wp = 3.76 #ht of waste package
half = z_wp/2. #to find center of waste package
z_st = 1.0 #ht of impact limiter + fishing neck
z_fn = 0.3 #ht of fishing neck
n_wp = 40 #number of wp in an interval
z_cement = 10. #length of cement plug
t = 0.0
dt = 2.738e-3 #years, one waste package per day I think

#write first impact limiter region
rfile.write('REGION impact_limiter\n  COORDINATES\n' +
            '    0.d0 0.d0 '+str(z)+'\n' +
            '    0.14 1.d0 '+str(z+z_il)+'\n  /\nEND\n\n')
sfile.write('STRATA\n  REGION impact_limiter\n  MATERIAL steel\nEND\n\n')
z += z_il

#for each waste package write a heat source file, region, etc.
for i in range(int(num_i)): 
#write heat input files, Cs followed by Sr
  if i < n_cs: #it's a Cs waste package
    string = 'heat'+str(i)+'.txt'
    fout = file (string, 'w')
    fout.write('#Radial grid, 18 capsules worth of heat, average over all Cs capsules\n'  +
               '  TIME_UNITS y\n  DATA_UNITS MW\n')
    if t == 0.0:
      for j in range(num_t):
        fout.write('  %.1f  %.4e\n' %(time1[j], energy1[j]))
    elif t > 0.0:
      fout.write('  %.1f  %.4e\n'%(0.0, 0.0))
      for j in range(num_t):
        fout.write('  %.4e  %.4e\n' %(time1[j]+dt*i, energy1[j]))
    fout.close()
  else: #it's a Sr waste package
    string = 'heat'+str(i)+'.txt'
    fout = file (string, 'w')
    fout.write('#Radial grid, 18 capsules worth of heat, average over all Sr capsules\n'  +
               '  TIME_UNITS y\n  DATA_UNITS MW\n')
    if t == 0.0: #t won't equal zero, but leave it anyway
      for j in range(num_t):
        fout.write('  %.1f  %.4e\n' %(time2[j], energy2[j]))
    elif t > 0.0:
      fout.write('  %.1f  %.4e\n'%(0.0, 0.0))
      for j in range(num_t):
        fout.write('  %.4e  %.4e\n' %(time2[j]+dt*i, energy2[j]))
    fout.close()
  #increment time
  t += dt
#write regions, etc. which depend on position in space
  if i == n_wp or i == n_wp*2: #if we're at wp 40 or 80 put cement plug in first
    rfile.write('REGION cement_plug'+str(i)+'\n  COORDINATES\n' +
                '    0.d0 0.d0 '+str(z)+'\n' +
                '    0.19 1.d0 '+str(z+z_cement)+'\n  /\nEND\n\n')
    rfile.write('REGION impact_limiter'+str(i)+'\n  COORDINATES\n' +
                '    0.d0 0.d0 '+str(z+z_cement)+'\n' +
                '    0.14 1.d0 '+str(z+z_cement+z_il)+'\n  /\nEND\n\n')
    sfile.write('STRATA\n  REGION cement_plug'+str(i)+'\n  MATERIAL cement\nEND\n\n')
    sfile.write('STRATA\n  REGION impact_limiter'+str(i)+'\n  MATERIAL steel\nEND\n\n')
    z += (z_cement+z_il)
  #write waste package region, strata depends on whether Cs or Sr
  rfile.write('REGION wp'+str(i)+'\n  COORDINATES\n' +
              '    0.d0 0.d0 '+str(z)+'\n' +
              '    0.14 1.d0 '+str(z+z_wp)+'\n  /\nEND\n\n')
  if i < n_cs:
    sfile.write('STRATA\n  REGION wp'+str(i)+'\n  MATERIAL Cs_waste_package\nEND\n\n')
  else:
    sfile.write('STRATA\n  REGION wp'+str(i)+'\n  MATERIAL Sr_waste_package\nEND\n\n')
  #then decide whether to write a fishing neck or a complete steel region
  if i == (n_wp-1) or i == (n_wp*2-1) or i == num_i-1: #if we're just below a cement plug or it's the last wp
    rfile.write('REGION fishing_neck'+str(i)+'\n  COORDINATES\n' +
                '    0.d0 0.d0 '+str(z+z_wp)+'\n' + 
                '    0.14 1.d0 '+str(z+z_wp+z_fn)+'\n  /\nEND\n\n')
    sfile.write('STRATA\n  REGION fishing_neck'+str(i)+'\n  MATERIAL steel\nEND\n\n')
    delta_z = z_wp+z_fn
  else: #a waste package somewhere in the middle of an interval
    rfile.write('REGION steel'+str(i)+'\n  COORDINATES\n' +
                '    0.d0 0.d0 '+str(z+z_wp)+'\n' +
                '    0.14 1.d0 '+str(z+z_wp+z_st)+'\n  /\nEND\n\n')
    sfile.write('STRATA\n  REGION steel'+str(i)+'\n  MATERIAL steel\nEND\n\n')
    delta_z = z_wp+z_st
  cfile.write('FLOW_CONDITION heat'+str(i)+'\n' +
              '  TYPE\n    RATE MASS_RATE\n    ENERGY_RATE SCALED_ENERGY_RATE VOLUME\n  /\n' +
              '  INTERPOLATION LINEAR\n  RATE 0.d0\n  ENERGY_RATE FILE ../split_heat/'+string+'\nEND\n\n')
  ssfile.write('SOURCE_SINK heat'+str(i)+'\n  FLOW_CONDITION heat'+str(i)+'\n  REGION wp'+str(i)+'\nEND\n\n')
  sschem.write('SOURCE_SINK heat'+str(i)+'\n  FLOW_CONDITION heat'+str(i)+
               '\n  TRANSPORT_CONDITION initial_Cs_wp\n  REGION wp'+str(i)+'\nEND\n\n')
  #wfg.capsules_to_file(wfgfile,0.07,0.5,zcenter)
  zcenter = z + half
  if i == 0 or i == 20 or i == n_cs or i == 60 or i == 107: #just hardwire these, because I can't think
    obsfile.write('OBSERVATION\n  REGION obswp'+str(i)+'\n  VELOCITY\nEND\n\n')
    obsfile.write('OBSERVATION\n  REGION annulus'+str(i)+'\n  VELOCITY\nEND\n\n')
    obsfile.write('OBSERVATION\n  REGION drz'+str(i)+'\n  VELOCITY\nEND\n\n')
    obsfile.write('OBSERVATION\n  REGION 1m'+str(i)+'\n  VELOCITY\nEND\n\n')
    obsregf.write('REGION obswp'+str(i)+'\n  COORDINATE 0.07 0.5 '+str(zcenter)+'\nEND\n\n')
    obsregf.write('REGION annulus'+str(i)+'\n  COORDINATE 0.165 0.5 '+str(zcenter)+'\nEND\n\n')
    obsregf.write('REGION drz'+str(i)+'\n  COORDINATE 0.35 0.5 '+str(zcenter)+'\nEND\n\n')
    obsregf.write('REGION 1m'+str(i)+'\n  COORDINATE 1.0 0.5 '+str(zcenter)+'\nEND\n\n')
  #increment z by wp plus whatever steel went on top of it.
  z += delta_z

#write uppermost cement and the seal region
rfile.write('REGION cement'+str(i)+'\n  COORDINATES\n' +
            '    0.d0 0.d0 '+str(z)+'\n' +
            '    0.19 1.d0 '+str(z+z_cement)+'\n  /\nEND\n\n')
sfile.write('STRATA\n  REGION cement'+str(i)+'\n  MATERIAL cement\nEND\n\n')
z += z_cement
rfile.write('REGION seal\n  COORDINATES\n' +
            '    0.d0 0.d0 '+str(z)+'\n' +
            '    0.19 1.d0 1.d20\n  /\nEND\n\n')
sfile.write('STRATA\n  REGION seal\n  MATERIAL ugseal\nEND\n\n')
#add an obs point above at base of seal region
zcenter = z + half #close enough to grab cell just above cement
obsfile.write('OBSERVATION\n  REGION obswp_seal\n  VELOCITY\nEND\n\n')
obsfile.write('OBSERVATION\n  REGION annulus_seal\n  VELOCITY\nEND\n\n')
obsfile.write('OBSERVATION\n  REGION drz_seal\n  VELOCITY\nEND\n\n')
obsfile.write('OBSERVATION\n  REGION 1m_seal\n  VELOCITY\nEND\n\n')
obsregf.write('REGION obswp_seal\n  COORDINATE 0.07 0.5 '+str(zcenter)+'\nEND\n\n')
obsregf.write('REGION annulus_seal\n  COORDINATE 0.165 0.5 '+str(zcenter)+'\nEND\n\n')
obsregf.write('REGION drz_seal\n  COORDINATE 0.35 0.5 '+str(zcenter)+'\nEND\n\n')
obsregf.write('REGION 1m_seal\n  COORDINATE 1.0 0.5 '+str(zcenter)+'\nEND\n\n')

sschem.write('#restart at %.4e years\n#max dt = %.4e years' %(t,dt))
print ('restart at %.4e years; max dt = %.4e years' %(t,dt))
print ('energy per linear meter Sr wp = %.4e MW' %(energy2[0]/z_wp))
print ('energy per linear meter Sr whole = %.4e MW' %(energy2[0]/(z_wp+z_st)))
rfile.close()
cfile.close()
ssfile.close()
sschem.close()
sfile.close()
obsfile.close()
obsregf.close()
#wfgfile.close()


