'''
functions to write XDMF (.xmf) file for use with HDF5 output of explicit unstructured grid (.uge)
1.9.17
Emily
'''

import h5py
import os
import sys

header_template = \
'''<?xml version="1.0" ?>
<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>
<Xdmf>
  <Domain>
    <Grid Name="Mesh">
'''
time_template= \
'''      <Time Value="%s" />
'''
topo_template = \
'''      <Topology Type="Mixed" NumberOfElements="%d" >
        <DataItem Format="HDF" NumberType="Int" Precision="4" Dimensions="%d 1">
        %s.h5:/Domain/Cells
        </DataItem>
      </Topology>
'''
geo_template = \
'''      <Geometry GeometryType="XYZ">
        <DataItem Format="HDF" NumberType="Float" Precision="8" Dimensions="%d 3">
        %s.h5:/Domain/Vertices
        </DataItem>
      </Geometry>
'''
XYZ_template = \
'''      <Attribute Name="%s" AttributeType="%s" Center="%s">
        <DataItem Dimensions="%d 1" Format="HDF">
        %s.h5:/%s/%s
        </DataItem>
      </Attribute>
'''
attribute_template = \
'''      <Attribute Name="%s" AttributeType="%s" Center="%s">
        <DataItem Dimensions="%d 1" Format="HDF">
        %s.h5:/%s/%s
        </DataItem>
      </Attribute>
'''
tail_template = \
'''    </Grid>
  </Domain>
</Xdmf>
'''
#don't need to specify NumberType and Precision, so don't!
#%s/%s needs to be the time string followed by the variable string

def write_xmf():
  pfloname = sys.argv[1]
  domainname = sys.argv[2]
  if len(sys.argv) != 3:
    print('ERROR: Command line arguments not provided.\n')
    print('Usage: python write_xmf.py pflotran grid-domain\n')
    sys.exit(0)
  #pfloname = 'pflotran'
  #domainname = 'pflotran-domain.h5'
  h5f = h5py.File(pfloname+".h5","r")
  domainf = h5py.File(domainname+".h5","r")
  times = h5f.keys()
  for tkey in times:
    if tkey.startswith('Domain'): #skip Domain key, if it exists
      continue
    xmf = open(pfloname+'-%s.xmf' % tkey.split()[0],'w') 
    xmf.write(header_template)
    xmf.write(time_template % tkey.split()[2])
    xmf.write(topo_template % (len(domainf['Domain/XC']),len(domainf['Domain/Cells']),domainname))
    xmf.write(geo_template % (len(domainf['Domain/Vertices']),domainname))
    xmf.write(XYZ_template % ('XC','Scalar','Cell',len(domainf['Domain/XC']),domainname,'Domain','XC'))
    xmf.write(XYZ_template % ('YC','Scalar','Cell',len(domainf['Domain/YC']),domainname,'Domain','YC'))
    xmf.write(XYZ_template % ('ZC','Scalar','Cell',len(domainf['Domain/ZC']),domainname,'Domain','ZC'))
    try:
      xmf.write(XYZ_template % ('Volume','Scalar','Cell',len(domainf['Domain/Volume']),domainname,'Domain','Volume'))
    except:
      print('No Volumes in this file')
    try:
      xmf.write(XYZ_template % ('Cell_Ids','Scalar','Cell',len(domainf['Domain/Cell_Ids']),domainname,'Domain','Cell_Ids'))
    except:
      print('No Cell_Ids in this file')
    attributes = h5f[tkey].keys()
    for akey in attributes:
      xmf.write(attribute_template % (akey,'Scalar','Cell',len(h5f[tkey][akey]),pfloname,tkey,akey))
    xmf.write(tail_template)
    xmf.close()
  h5f.close()
  domainf.close()

if __name__ == "__main__":
  write_xmf()
