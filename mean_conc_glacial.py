
import sys
import os
try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
import pflotran as pft
import pickle as p
from pylab import sort

dfile = file('dfile.dat','r')
dd = p.load(dfile)
dfile.close()
print(dd.keys())
print(dd[0].keys())
#print(dd[0]['glacial1'])

mpl.rcParams['font.size']=14
#mpl.rcParams['font.weight']='bold'

def make_subplots(nrow, ncol, nplots, filenames, columns, name, titles):
  for iplot in range(nplots):
    plt.subplot(nrow,ncol,iplot+1)
    plt.title(titles[iplot])
    plt.xlabel('Time (years)', fontsize=20, fontweight='bold')
    if iplot == 0:
      plt.ylabel('Concentration (M)', fontsize=20, fontweight='bold')

    plt.xlim(1.e-0,1.e6)
    plt.ylim(1.e-21,1.e-6)
    plt.grid(True)

    for icol in range(iplot*len(columns)/nplots,iplot*len(columns)/nplots+len(columns)/nplots):
      ifile = icol
      data = pft.Dataset(filenames[ifile],1,columns[icol])
    #label with column header
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.semilogx(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  string = data.get_name('yname').split()[3].split('_')[1]
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=string)
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.axis([0.1, 1.e6, 200., 0.]) #why is this here?
    #or label with file name
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=filenames[ifile])
    #or label with name assigned above
      string = name[icol]
      if icol == 50 or icol == 101 or icol == 152:
        plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],linewidth=3,label=string)
      else:
        plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol])
      #plt.semilogx(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)
      #plt.plot(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)

path = []
path.append('.')

titles = []
files = []
columns = []
name = []
linestyles = []
colors = []

nsim = 50
basestyles = ['-' for x in range(nsim)] #['-','--','-.',':']
basestyles.extend(['-'])
basecolors = ['grey' for x in range(nsim)]
basecolors.extend(['darkorange'])

#glacial1 (1012.5, 1012.5, 1252.5)
titles.append('a.) Observation point "glacial1"')
# loop through the realization and calculate the stats for each time
obs = 'glacial1'
ki = sort(dd.keys())[0];
#print(ki)
tt = dd[ki][obs]['Time [y]'].copy();
dt_Am241mean1=[];
dt_Am243mean1=[];
dt_Pu238mean1=[];
dt_Pu239mean1=[];
dt_Pu240mean1=[];
dt_Pu242mean1=[];
#    dt_953.append(np.percentile(dt,95.));
dt_Np237mean1=[];
dt_U233mean1=[];
dt_U234mean1=[];
dt_U236mean1=[];
dt_U238mean1=[];
dt_Th229mean1=[];
dt_Th230mean1=[];
dt_Tc99mean1=[];
dt_I129mean1=[];
dt_Cs135mean1=[];
dt_Cl36mean1=[];
for t in tt:
    dt_Am241=[];
    dt_Am243=[];
    dt_Pu238=[];
    dt_Pu239=[];
    dt_Pu240=[];
    dt_Pu242=[];
    dt_Np237=[];
    dt_U233=[];
    dt_U234=[];
#    dt_953.append(np.percentile(dt,95.));
    dt_U236=[];
    dt_U238=[];
    dt_Th229=[];
    dt_Th230=[];
    dt_Tc99=[];
    dt_I129=[];
    dt_Cs135=[];
    dt_Cl36=[];
    for j in sort(dd.keys()):
        dt_Am241.append(dd[j][obs]['Total Am241'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Am243.append(dd[j][obs]['Total Am243'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu238.append(dd[j][obs]['Total Pu238'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu239.append(dd[j][obs]['Total Pu239'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu240.append(dd[j][obs]['Total Pu240'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu242.append(dd[j][obs]['Total Pu242'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Np237.append(dd[j][obs]['Total Np237'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U233.append(dd[j][obs]['Total U233'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U236.append(dd[j][obs]['Total U236'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U238.append(dd[j][obs]['Total U238'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Th229.append(dd[j][obs]['Total Th229'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Th230.append(dd[j][obs]['Total Th230'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Tc99.append(dd[j][obs]['Total Tc99'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_I129.append(dd[j][obs]['Total I129'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Cs135.append(dd[j][obs]['Total Cs135'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Cl36.append(dd[j][obs]['Total Cl36'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
    dt_Am241mean1.append(np.mean(dt_Am241));
    dt_Am243mean1.append(np.mean(dt_Am243));
    dt_Pu238mean1.append(np.mean(dt_Pu238));
    dt_Pu239mean1.append(np.mean(dt_Pu239));
    dt_Pu240mean1.append(np.mean(dt_Pu240));
    dt_Pu242mean1.append(np.mean(dt_Pu242));
    dt_Np237mean1.append(np.mean(dt_Np237));
    dt_U233mean1.append(np.mean(dt_U233));
    dt_U234mean1.append(np.mean(dt_U234));
    dt_U236mean1.append(np.mean(dt_U236));
    dt_U238mean1.append(np.mean(dt_U238));
    dt_Th229mean1.append(np.mean(dt_Th229));
    dt_Th230mean1.append(np.mean(dt_Th230));
    dt_Tc99mean1.append(np.mean(dt_Tc99));
    dt_I129mean1.append(np.mean(dt_I129));
    dt_Cs135mean1.append(np.mean(dt_Cs135));
    dt_Cl36mean1.append(np.mean(dt_Cl36));

#linestyles.extend(basestyles)
#colors.extend(basecolors)


#glacial2 (1807.5,1012.5,1252.5)
titles.append('b.) Observation point "glacial2"')
# loop through the realization and calculate the stats for each time
obs = 'glacial2'
ki = sort(dd.keys())[0];
print(ki)
tt = dd[ki][obs]['Time [y]'].copy();
#    dt_953.append(np.percentile(dt,95.));
dt_Am241mean2=[];
dt_Am243mean2=[];
dt_Pu238mean2=[];
dt_Pu239mean2=[];
dt_Pu240mean2=[];
dt_Pu242mean2=[];
dt_Np237mean2=[];
dt_U233mean2=[];
dt_U234mean2=[];
dt_U236mean2=[];
dt_U238mean2=[];
dt_Th229mean2=[];
dt_Th230mean2=[];
dt_Tc99mean2=[];
dt_I129mean2=[];
dt_Cs135mean2=[];
dt_Cl36mean2=[];
for t in tt:
    dt_Am241=[];
    dt_Am243=[];
    dt_Pu238=[];
    dt_Pu239=[];
    dt_Pu240=[];
    dt_Pu242=[];
    dt_Np237=[];
    dt_U233=[];
    dt_U234=[];
    dt_U236=[];
    dt_U238=[];
    dt_Th229=[];
    dt_Th230=[];
    dt_Tc99=[];
    dt_I129=[];
    dt_Cs135=[];
    dt_Cl36=[];
    for j in sort(dd.keys()):
        dt_Am241.append(dd[j][obs]['Total Am241'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Am243.append(dd[j][obs]['Total Am243'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu238.append(dd[j][obs]['Total Pu238'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu239.append(dd[j][obs]['Total Pu239'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu240.append(dd[j][obs]['Total Pu240'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu242.append(dd[j][obs]['Total Pu242'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Np237.append(dd[j][obs]['Total Np237'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U233.append(dd[j][obs]['Total U233'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U236.append(dd[j][obs]['Total U236'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U238.append(dd[j][obs]['Total U238'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Th229.append(dd[j][obs]['Total Th229'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Th230.append(dd[j][obs]['Total Th230'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Tc99.append(dd[j][obs]['Total Tc99'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_I129.append(dd[j][obs]['Total I129'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Cs135.append(dd[j][obs]['Total Cs135'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Cl36.append(dd[j][obs]['Total Cl36'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
    dt_Am241mean2.append(np.mean(dt_Am241));
    dt_Am243mean2.append(np.mean(dt_Am243));
    dt_Pu238mean2.append(np.mean(dt_Pu238));
    dt_Pu239mean2.append(np.mean(dt_Pu239));
    dt_Pu240mean2.append(np.mean(dt_Pu240));
    dt_Pu242mean2.append(np.mean(dt_Pu242));
    dt_Np237mean2.append(np.mean(dt_Np237));
    dt_U233mean2.append(np.mean(dt_U233));
    dt_U234mean2.append(np.mean(dt_U234));
    dt_U236mean2.append(np.mean(dt_U236));
    dt_U238mean2.append(np.mean(dt_U238));
    dt_Th229mean2.append(np.mean(dt_Th229));
    dt_Th230mean2.append(np.mean(dt_Th230));
    dt_Tc99mean2.append(np.mean(dt_Tc99));
    dt_I129mean2.append(np.mean(dt_I129));
    dt_Cs135mean2.append(np.mean(dt_Cs135));
    dt_Cl36mean2.append(np.mean(dt_Cl36));

#linestyles.extend(basestyles)
#colors.extend(basecolors)


#glacial3 (2917.5,1012.5,1252.5)
titles.append('c.) Observation point "glacial3"')
# loop through the realization and calculate the stats for each time
obs = 'glacial3'
ki = sort(dd.keys())[0];
print(ki)
tt = dd[ki][obs]['Time [y]'].copy();
dt_Am241mean3=[];
dt_Am243mean3=[];
dt_Pu238mean3=[];
dt_Pu239mean3=[];
dt_Pu240mean3=[];
dt_Pu242mean3=[];
dt_Np237mean3=[];
dt_U233mean3=[];
dt_U234mean3=[];
dt_U236mean3=[];
dt_U238mean3=[];
dt_Th229mean3=[];
dt_Th230mean3=[];
dt_Tc99mean3=[];
dt_I129mean3=[];
dt_Cs135mean3=[];
dt_Cl36mean3=[];
for t in tt:
    dt_Am241=[];
    dt_Am243=[];
    dt_Pu238=[];
    dt_Pu239=[];
    dt_Pu240=[];
    dt_Pu242=[];
    dt_Np237=[];
    dt_U233=[];
    dt_U234=[];
    dt_U236=[];
    dt_U238=[];
    dt_Th229=[];
    dt_Th230=[];
    dt_Tc99=[];
    dt_I129=[];
    dt_Cs135=[];
    dt_Cl36=[];
    for j in sort(dd.keys()):
        dt_Am241.append(dd[j][obs]['Total Am241'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Am243.append(dd[j][obs]['Total Am243'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu238.append(dd[j][obs]['Total Pu238'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu239.append(dd[j][obs]['Total Pu239'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu240.append(dd[j][obs]['Total Pu240'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Pu242.append(dd[j][obs]['Total Pu242'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Np237.append(dd[j][obs]['Total Np237'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U233.append(dd[j][obs]['Total U233'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U236.append(dd[j][obs]['Total U236'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_U238.append(dd[j][obs]['Total U238'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Th229.append(dd[j][obs]['Total Th229'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Th230.append(dd[j][obs]['Total Th230'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Tc99.append(dd[j][obs]['Total Tc99'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_I129.append(dd[j][obs]['Total I129'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Cs135.append(dd[j][obs]['Total Cs135'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
        dt_Cl36.append(dd[j][obs]['Total Cl36'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
    dt_Am241mean3.append(np.mean(dt_Am241));
    dt_Am243mean3.append(np.mean(dt_Am243));
    dt_Pu238mean3.append(np.mean(dt_Pu238));
    dt_Pu239mean3.append(np.mean(dt_Pu239));
    dt_Pu240mean3.append(np.mean(dt_Pu240));
    dt_Pu242mean3.append(np.mean(dt_Pu242));
    dt_Np237mean3.append(np.mean(dt_Np237));
    dt_U233mean3.append(np.mean(dt_U233));
    dt_U234mean3.append(np.mean(dt_U234));
    dt_U236mean3.append(np.mean(dt_U236));
    dt_U238mean3.append(np.mean(dt_U238));
    dt_Th229mean3.append(np.mean(dt_Th229));
    dt_Th230mean3.append(np.mean(dt_Th230));
    dt_Tc99mean3.append(np.mean(dt_Tc99));
    dt_I129mean3.append(np.mean(dt_I129));
    dt_Cs135mean3.append(np.mean(dt_Cs135));
    dt_Cl36mean3.append(np.mean(dt_Cl36));

#linestyles.extend(basestyles)
#colors.extend(basecolors)

#filenames = pft.get_full_paths(path,files)



#Plot the Figures
f = plt.figure(figsize=(16,16)) #adjust size to fit plots (width, height)
#f.suptitle("Cement, line loads: Temperature (degrees C)",fontsize=16)

#make_subplots(2,2,3,filenames,columns,name,titles)
plt.subplot(2,2,1)
plt.xlabel('Time (years)', fontsize=20, fontweight='bold')
#    if iplot == 0:
plt.ylabel('Mean Concentration (M) at glacial1', fontsize=20, fontweight='bold')
plt.xlim(1.e-0,1.e6)
plt.ylim(1.e-21,1.e-6)
plt.loglog(tt,dt_Am241mean1,color='indigo',linestyle='-',label='Am-241',linewidth=3)
plt.loglog(tt,dt_Am243mean1,color='darkviolet',linestyle='--',label='Am-243',linewidth=3)
plt.loglog(tt,dt_Pu238mean1,color='mediumvioletred',linestyle=':',label='Pu-238',linewidth=3)
plt.loglog(tt,dt_Pu239mean1,color='red',linestyle='-',label='Pu-239',linewidth=3)
plt.loglog(tt,dt_Pu240mean1,color='crimson',linestyle='--',label='Pu-240',linewidth=3)
plt.loglog(tt,dt_Pu242mean1,color='darkorange',linestyle=':',label='Pu-242',linewidth=3)
plt.loglog(tt,dt_Np237mean1,color='gold',linestyle='-',label='Np-237',linewidth=3)
plt.loglog(tt,dt_U233mean1,color='yellowgreen',linestyle='--',label='U-233',linewidth=3)
plt.loglog(tt,dt_U234mean1,color='green',linestyle=':',label='U-234',linewidth=3)
plt.loglog(tt,dt_U236mean1,color='darkgreen',linestyle='-',label='U-236',linewidth=3)
plt.loglog(tt,dt_U238mean1,color='olivedrab',linestyle='--',label='U-238',linewidth=3)
plt.loglog(tt,dt_Th229mean1,color='teal',linestyle=':',label='Th-229',linewidth=3)
plt.loglog(tt,dt_Th230mean1,color='cadetblue',linestyle='-',label='Th-230',linewidth=3)
plt.loglog(tt,dt_Tc99mean1,color='darkturquoise',linestyle='--',label='Tc-99',linewidth=3)
plt.loglog(tt,dt_I129mean1,color='deepskyblue',linestyle=':',label='I-129',linewidth=3)
plt.loglog(tt,dt_Cs135mean1,color='mediumslateblue',linestyle='-',label='Cs-135',linewidth=3)
plt.loglog(tt,dt_Cl36mean1,color='navy',linestyle='--',label='Cl-36',linewidth=3)


plt.subplot(2,2,2)
plt.xlabel('Time (years)', fontsize=20, fontweight='bold')
#    if iplot == 0:
plt.ylabel('Mean Concentration (M) at glacial2', fontsize=20, fontweight='bold')
plt.xlim(1.e-0,1.e6)
plt.ylim(1.e-21,1.e-6)
plt.loglog(tt,dt_Am241mean2,color='indigo',linestyle='-',label='Am-241',linewidth=3)
plt.loglog(tt,dt_Am243mean2,color='darkviolet',linestyle='--',label='Am-243',linewidth=3)
plt.loglog(tt,dt_Pu238mean2,color='mediumvioletred',linestyle=':',label='Pu-238',linewidth=3)
plt.loglog(tt,dt_Pu239mean2,color='red',linestyle='-',label='Pu-239',linewidth=3)
plt.loglog(tt,dt_Pu240mean2,color='crimson',linestyle='--',label='Pu-240',linewidth=3)
plt.loglog(tt,dt_Pu242mean2,color='darkorange',linestyle=':',label='Pu-242',linewidth=3)
plt.loglog(tt,dt_Np237mean2,color='gold',linestyle='-',label='Np-237',linewidth=3)
plt.loglog(tt,dt_U233mean2,color='yellowgreen',linestyle='--',label='U-233',linewidth=3)
plt.loglog(tt,dt_U234mean2,color='green',linestyle=':',label='U-234',linewidth=3)
plt.loglog(tt,dt_U236mean2,color='darkgreen',linestyle='-',label='U-236',linewidth=3)
plt.loglog(tt,dt_U238mean2,color='olivedrab',linestyle='--',label='U-238',linewidth=3)
plt.loglog(tt,dt_Th229mean2,color='teal',linestyle=':',label='Th-229',linewidth=3)
plt.loglog(tt,dt_Th230mean2,color='cadetblue',linestyle='-',label='Th-230',linewidth=3)
plt.loglog(tt,dt_Tc99mean2,color='darkturquoise',linestyle='--',label='Tc-99',linewidth=3)
plt.loglog(tt,dt_I129mean2,color='deepskyblue',linestyle=':',label='I-129',linewidth=3)
plt.loglog(tt,dt_Cs135mean2,color='mediumslateblue',linestyle='-',label='Cs-135',linewidth=3)
plt.loglog(tt,dt_Cl36mean2,color='navy',linestyle='--',label='Cl-36',linewidth=3)


plt.subplot(2,2,3)
plt.xlabel('Time (years)', fontsize=20, fontweight='bold')
#    if iplot == 0:
plt.ylabel('Mean Concentration (M) at glacial3', fontsize=20, fontweight='bold')
plt.xlim(1.e-0,1.e6)
plt.ylim(1.e-21,1.e-6)
plt.loglog(tt,dt_Am241mean3,color='indigo',linestyle='-',label='Am-241',linewidth=3)
plt.loglog(tt,dt_Am243mean3,color='darkviolet',linestyle='--',label='Am-243',linewidth=3)
plt.loglog(tt,dt_Pu238mean3,color='mediumvioletred',linestyle=':',label='Pu-238',linewidth=3)
plt.loglog(tt,dt_Pu239mean3,color='red',linestyle='-',label='Pu-239',linewidth=3)
plt.loglog(tt,dt_Pu240mean3,color='crimson',linestyle='--',label='Pu-240',linewidth=3)
plt.loglog(tt,dt_Pu242mean3,color='darkorange',linestyle=':',label='Pu-242',linewidth=3)
plt.loglog(tt,dt_Np237mean3,color='gold',linestyle='-',label='Np-237',linewidth=3)
plt.loglog(tt,dt_U233mean3,color='yellowgreen',linestyle='--',label='U-233',linewidth=3)
plt.loglog(tt,dt_U234mean3,color='green',linestyle=':',label='U-234',linewidth=3)
plt.loglog(tt,dt_U236mean3,color='darkgreen',linestyle='-',label='U-236',linewidth=3)
plt.loglog(tt,dt_U238mean3,color='olivedrab',linestyle='--',label='U-238',linewidth=3)
plt.loglog(tt,dt_Th229mean3,color='teal',linestyle=':',label='Th-229',linewidth=3)
plt.loglog(tt,dt_Th230mean3,color='cadetblue',linestyle='-',label='Th-230',linewidth=3)
plt.loglog(tt,dt_Tc99mean3,color='darkturquoise',linestyle='--',label='Tc-99',linewidth=3)
plt.loglog(tt,dt_I129mean3,color='deepskyblue',linestyle=':',label='I-129',linewidth=3)
plt.loglog(tt,dt_Cs135mean3,color='mediumslateblue',linestyle='-',label='Cs-135',linewidth=3)
plt.loglog(tt,dt_Cl36mean3,color='navy',linestyle='--',label='Cl-36',linewidth=3)

#Choose a location for the legend. #This is associated with the last plot
#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
plt.legend(loc=(1.15,0.03),title='RADIONUCLIDE')
#plt.legend(loc=2)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

#adjust blank space and show/save the plot
f.subplots_adjust(hspace=0.2,wspace=0.2, bottom=.12,top=.9, left=.14,right=.9)
plt.savefig('mean_conc_glacial.png',bbox_inches='tight')
plt.show()
