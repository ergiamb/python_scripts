
import sys
import os
try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
import pflotran as pft
import pickle as p
from pylab import sort

dfile = file('dfile.dat','r')
dd = p.load(dfile)
dfile.close()
print(dd.keys())
print(dd[0].keys())
#print(dd[0]['glacial1'])

mpl.rcParams['font.size']=17
#mpl.rcParams['font.weight']='bold'

def make_subplots(nrow, ncol, nplots, filenames, columns, name, titles):
  for iplot in range(nplots):
    plt.subplot(nrow,ncol,iplot+1)
    plt.title(titles[iplot])
    plt.xlabel('Time (years)', fontsize=20, fontweight='bold')
   # if iplot == 0:
    plt.ylabel('[I-129] M', fontsize=20, fontweight='bold')

    plt.xlim(1.e-0,1.e6)
    plt.ylim(1.e-21,1.e-6)
    plt.grid(True)

    for icol in range(iplot*len(columns)/nplots,iplot*len(columns)/nplots+len(columns)/nplots):
      ifile = icol
      data = pft.Dataset(filenames[ifile],1,columns[icol])
    #label with column header
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.semilogx(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  string = data.get_name('yname').split()[3].split('_')[1]
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=string)
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.axis([0.1, 1.e6, 200., 0.]) #why is this here?
    #or label with file name
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=filenames[ifile])
    #or label with name assigned above
      string = name[icol]
      if icol == 50 or icol == 101 or icol == 152:
        plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],linewidth=3,label=string)
      else:
        plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol])
      #plt.semilogx(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)
      #plt.plot(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)

path = []
path.append('.')

titles = []
files = []
columns = []
name = []
linestyles = []
colors = []

nsim = 50
basestyles = ['-' for x in range(nsim)] #['-','--','-.',':']
basestyles.extend(['-'])
basecolors = ['grey' for x in range(nsim)]
basecolors.extend(['darkorange'])

#glacial1 (1012.5, 1012.5, 1252.5)
titles.append('a.) Observation point "glacial1"')
for i in range(nsim):
  files.append('./workdir.'+str(i+1)+'/pflotran-obs-16.tec')
  columns.append(22)
  name.append('workdir.'+str(i+1))
files.append('dfascit_domain6/pflotran-obs-16.tec')
#files.append('../gdsa/domain6/pflotran-obs-16.tec')
columns.append(23)
name.append('Deterministic')
# loop through the realization and calculate the stats for each time
obs = 'glacial1'
ki = sort(dd.keys())[0];
print(ki)
tt = dd[ki][obs]['Time [y]'].copy();
dt_mean1=[];
dt_median1=[];
dt_051=[];
dt_951=[];
for t in tt:
    dt=[];
    for j in sort(dd.keys()):
        dt.append(dd[j][obs]['Total I129'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
    dt_mean1.append(np.mean(dt));
    dt_median1.append(np.median(dt));
    dt_051.append(np.percentile(dt,5.));
    dt_951.append(np.percentile(dt,95.));

linestyles.extend(basestyles)
colors.extend(basecolors)

#glacial2 (1807.5,1012.5,1252.5)
titles.append('b.) Observation point "glacial2"')
for i in range(nsim):
  files.append('./workdir.'+str(i+1)+'/pflotran-obs-13.tec')
  columns.append(82)
  name.append('workdir.'+str(i+1))
files.append('dfascit_domain6/pflotran-obs-13.tec')
columns.append(86)
name.append('Deterministic')
# loop through the realization and calculate the stats for each time
obs = 'glacial2'
ki = sort(dd.keys())[0];
tt = dd[ki][obs]['Time [y]'].copy();
dt_mean2=[];
dt_median2=[];
dt_052=[];
dt_952=[];
for t in tt:
    dt=[];
    for j in sort(dd.keys()):
        dt.append(dd[j][obs]['Total I129'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
    dt_mean2.append(np.mean(dt));
    dt_median2.append(np.median(dt));
    dt_052.append(np.percentile(dt,5.));
    dt_952.append(np.percentile(dt,95.));

linestyles.extend(basestyles)
colors.extend(basecolors)

#glacial3 (2917.5,1012.5,1252.5)
titles.append('c.) Observation point "glacial3"')
for i in range(nsim):
  files.append('./workdir.'+str(i+1)+'/pflotran-obs-25.tec')
  columns.append(82)
  name.append('workdir.'+str(i+1))
files.append('dfascit_domain6/pflotran-obs-25.tec')
columns.append(86)
name.append('Deterministic')
# loop through the realization and calculate the stats for each time
obs = 'glacial3'
ki = sort(dd.keys())[0];
tt = dd[ki][obs]['Time [y]'].copy();
dt_mean3=[];
dt_median3=[];
dt_053=[];
dt_953=[];
for t in tt:
    dt=[];
    for j in sort(dd.keys()):
        dt.append(dd[j][obs]['Total I129'][np.argmin(abs(dd[j][obs]['Time [y]']-t))]);
    dt_mean3.append(np.mean(dt));
    dt_median3.append(np.median(dt));
    dt_053.append(np.percentile(dt,5.));
    dt_953.append(np.percentile(dt,95.));

linestyles.extend(basestyles)
colors.extend(basecolors)

filenames = pft.get_full_paths(path,files)

f = plt.figure(figsize=(16,16)) #adjust size to fit plots (width, height)
#f.suptitle("Cement, line loads: Temperature (degrees C)",fontsize=16)

make_subplots(2,2,3,filenames,columns,name,titles)
plt.subplot(2,2,1)
plt.loglog(tt,dt_mean1,color='red',linestyle='-',label='Mean',linewidth=3)
plt.loglog(tt,dt_median1,color='red',linestyle='--',label='Median',linewidth=3)
plt.loglog(tt,dt_051,color='green',linestyle='--',label='q = 5%',linewidth=3)
plt.loglog(tt,dt_951,color='blue',linestyle='--',label='q = 95%',linewidth=3)
plt.subplot(2,2,2)
plt.loglog(tt,dt_mean2,color='red',linestyle='-',label='Mean',linewidth=3)
plt.loglog(tt,dt_median2,color='red',linestyle='--',label='Median',linewidth=3)
plt.loglog(tt,dt_052,color='green',linestyle='--',label='q = 5%',linewidth=3)
plt.loglog(tt,dt_952,color='blue',linestyle='--',label='q = 95%',linewidth=3)
plt.subplot(2,2,3)
plt.loglog(tt,dt_mean3,color='red',linestyle='-',label='Mean',linewidth=3)
plt.loglog(tt,dt_median3,color='red',linestyle='--',label='Median',linewidth=3)
plt.loglog(tt,dt_053,color='green',linestyle='--',label='q = 5%',linewidth=3)
plt.loglog(tt,dt_953,color='blue',linestyle='--',label='q = 95%',linewidth=3)

#Choose a location for the legend. #This is associated with the last plot
#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
plt.legend(loc=(1.17,0.62),)
#plt.legend(loc=2)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

#adjust blank space and show/save the plot
f.subplots_adjust(hspace=0.2,wspace=0.2, bottom=.12,top=.9, left=.14,right=.9)
plt.savefig('glacial_prob_I129.png',bbox_inches='tight')
plt.show()
