#!/usr/bin/python

# Calculate mass balance across a boundary of a DFN mesh
# Make sure that you run PFLOTRAN with MASS_FLOWRATE under OUTPUT
# Satish Karra
# March 17, 2016

# Add Tracer mass balance 5.26.17
# Emily Stein
# Also should note I messed with some other stuff earlier

import numpy as np
import glob
import argparse
from StringIO import StringIO

__author__ = 'Satish Karra'
__email__ = 'satkarra@lanl.gov'

# Read commandline arguments
parser = argparse.ArgumentParser(
    description='Script to calculate the mass flow rate through a ' +
    'given boundary on a DFN mesh. Make sure that you run PFLOTRAN ' +
    'with MASS_FLOWRATE under OUTPUT')
parser.add_argument('-b', '--boundary_file',
                    help='Boundary filename [ex: pboundary_left_w.ex]',
                    required=True)
#parser.add_argument(
#    '-i', '--input_file', help='PFLOTRAN input filename [ex: dfn.in]',
#    required=True)
parser.add_argument('-d', '--darcy_vel_timestep',
                    help='Darcy vel filename timestep number [ex: 001]',
                    required=True)
args = parser.parse_args()

# show values
print("Boundary filename: %s" % args.boundary_file)
#print("Input filename: %s" % args.input_file)
print("Darcy vel timestep number: %s" % args.darcy_vel_timestep)
print("Tracer concentration from vtk file at same timestep number")

# set files
boundary_file = args.boundary_file
#input_file = args.input_file
darcy_vel_base = 'darcyvel-' + args.darcy_vel_timestep

# Read the darcy vel files for a given timestep
#base = input_file.strip('.in')
#darcy_vel_files = glob.glob(base + '-' + darcy_vel_base + '-rank*dat')
base = 'pflotran' #just hardwire this, it isn't working
darcy_vel_files = []
rank = 12 #number of cores you ran pflotran on
for i in range(rank):
    darcy_vel_files.append(base + '-' + darcy_vel_base + '-rank' + str(i) + '.dat')
print(darcy_vel_files)
vtk_file = base + '-' + args.darcy_vel_timestep + '.vtk'

'''
# Number of connections and cells
num_conns = int(
    [line for line in open(uge_file) if 'CONNECTIONS' in line][0].split()[1])
num_cells = int(
    [line for line in open(uge_file) if 'CELLS' in line][0].split()[1])
'''

# Read the cells from boundary file
dat_boundary = np.genfromtxt(boundary_file, skip_header=1)

# Read tracer concentrations from vtk file
f = file(vtk_file,'r')
wholething = f.read()
block = wholething.split('\n\n')[2]
tracer_conc = []
for line in block.split('\n')[3:]:
    for x in line.split():
        tracer_conc.append(float(x))
tracer_conc = np.array(tracer_conc).flatten()
f.close()

# Calculate the mass flow rate
mass_rate = 0.0 #kg/s
volume_rate = 0.0 #m^3/s
tracer_rate = 0.0 #mol/s
Lm3 = 1000. #L/m^3
for file in darcy_vel_files:
    dat = np.genfromtxt(file)
    for cell in dat_boundary[:, 0]:
        if (np.any(dat[:, 0] == int(cell))):
            ids = np.where(dat[:, 0] == int(cell))[0]
            for idx in ids:
                cell_up = int(dat[idx, 0])
                cell_down = int(dat[idx, 1])
                mass_flux = dat[idx, 2]  # in m/s , darcy flux, right? m3/m2/s
                density = dat[idx, 3]  # in kg/m3
                area = dat[idx, 4]  # in m^2
                tracer = tracer_conc[cell_up-1] #1 indexed to 0 indexed
                mass_rate = mass_flux * area * \
                    density + mass_rate  # in kg/s
                volume_rate = mass_flux * area + volume_rate #in m3/s
                tracer_rate = mass_flux * area * Lm3 * tracer + tracer_rate
        if (np.any(dat[:, 1] == int(cell))):
            ids = np.where(dat[:, 1] == int(cell))[0]
            for idx in ids:
                cell_up = int(dat[idx, 0])
                cell_down = int(dat[idx, 1])
                mass_flux = dat[idx, 2]  # in m/s
                density = dat[idx, 3]  # in kg/m3
                area = dat[idx, 4]  # in m^2
                tracer = tracer_conc[cell_up-1] #1 indexed to 0 indexed
                mass_rate = mass_flux * area * \
                    density + mass_rate  # in kg/s
                volume_rate = mass_flux * area + volume_rate #in m3/s
                tracer_rate = mass_flux * area * Lm3 * tracer + tracer_rate

# Print the calculated mass flow rate in kg/s
mu = 1.e-3 #dynamic viscosity of water at 20 degrees C, Pa*s
surface = 990.*990. #m^2
spery = 3600.*24.*365.25 #seconds per year
pgrad = 1. #Pa/m
print('The mass flow rate [kg/s]: ' + str(mass_rate))
print('The volume flow rate [m3/s]: ' + str(volume_rate))
q = volume_rate/surface #darcy flux over entire face m3/m2/s
print('The darcy flow rate over 990x990 m2 area [m3/m2/s]: ' + str(q))
print('The darcy flow rate over 990x990 m2 area [m3/m2/yr]: ' + str(spery*q))
print('The effective permeability of the domain [m2]: ' + str(q*mu/pgrad))
print('The tracer flux [mol/s]: ' + str(tracer_rate))
print('The tracer flux [mol/y]: ' + str(tracer_rate*spery))
