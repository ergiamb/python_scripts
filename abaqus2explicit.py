#-------------------------------------------------------------------------------
# Tool to convert an Abaqus formatted mesh to PFLOTRAN explicit unstructured grid (.uge text file)
# ???Usage: python abaqus2explicit.py abaqus_mesh.txt
# 
# Author: Emily Stein (ergiamb@sandia.gov)
#
# borrowing heavily from abaqus2pflotran.py by:
# Author: Glenn Hammond (gehammo@sandia.gov)
#
# Applied Systems Analysis and Research, 6224
# Sandia National Laboratories
# Date: 01.11.17
#-------------------------------------------------------------------------------
from h5py import * 
import numpy as np
import os
import sys
import time
import write_xmf as wxmf

# ASSUMPTIONS:
# 1. Single set of nodes and elements in file denoted by '*NODE' and '*ELEMENT'
# 2. Hex elements (8 nodes per elements)
# 3. Node and element IDs are in ascending order
# 4. Which means there is only one ELEMENT block, no materials assigned in .inp

def ComputeVolumeOfTetrahedron(point1,point2,point3,point4):
  '''computes the volume of a tetrahedron given 4 pnts
     translated from grid_unstructured_cell.F90
  '''
  vv = np.zeros((3,4),'f8')
  vv[0][0] = point1[0]
  vv[1][0] = point1[1]
  vv[2][0] = point1[2]
  vv[0][1] = point2[0]
  vv[1][1] = point2[1]
  vv[2][1] = point2[2]
  vv[0][2] = point3[0]
  vv[1][2] = point3[1]
  vv[2][2] = point3[2]
  vv[0][3] = point4[0]
  vv[1][3] = point4[1]
  vv[2][3] = point4[2]

#  V = |(a-d).((b-d)x(c-d))| / 6
  vv1_minus_vv4 = vv[:,0]-vv[:,3]
  vv2_minus_vv4 = vv[:,1]-vv[:,3]
  vv3_minus_vv4 = vv[:,2]-vv[:,3]
  cross_2_minus_4_X_3_minus_4 = np.cross(vv2_minus_vv4,vv3_minus_vv4)
  return np.absolute(np.dot(vv1_minus_vv4,cross_2_minus_4_X_3_minus_4)) / 6.

def ComputePlane(point1,point2,point3):
  '''Calculates the plane intersected by 3 points
   translated from grid_unstructured_cell.F90
  '''
  x1 = point1[0]
  y1 = point1[1]
  z1 = point1[2]
  x2 = point2[0]
  y2 = point2[1]
  z2 = point2[2]
  x3 = point3[0]
  y3 = point3[1]
  z3 = point3[2]

  plane = []
  plane.append( y1*(z2-z3)+y2*(z3-z1)+y3*(z1-z2) )
  plane.append( z1*(x2-x3)+z2*(x3-x1)+z3*(x1-x2) )
  plane.append( x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2) )
  plane.append( -1.*(x1*(y2*z3-y3*z2)+x2*(y3*z1-y1*z3)+x3*(y1*z2-y2*z1)) )
  plane = np.array(plane,'f8')
  return plane

def GetPlaneIntercept(plane,point1,point2):
  '''Calculates the intercept of a line with a plane
     translated from grid_unstructured_cell.F90
  '''
  x1 = point1[0]
  y1 = point1[1]
  z1 = point1[2]
  x2 = point2[0]
  y2 = point2[1]
  z2 = point2[2]

  u = ((plane[0]*x1 + plane[1]*y1 + plane[2]*z1 + plane[3]) /
      (plane[0]*(x1-x2) + plane[1]*(y1-y2) + plane[2]*(z1-z2)))

  intercept = []
  intercept.append( x1 + u*(x2-x1) )
  intercept.append( y1 + u*(y2-y1) )
  intercept.append( z1 + u*(z2-z1) )
  intercept = np.array(intercept,'f8')
  return intercept

def ComputeArea(point1,point2,point3):
  '''Calculates area of 3-point face
     translated from grid_unstructured_cell.F90, 
     more or less
  '''
  v1 = np.zeros(3,'f8')
  v2 = np.zeros(3,'f8')
  v1[0] = point3[0]-point2[0]
  v1[1] = point3[1]-point2[1]
  v1[2] = point3[2]-point2[2]
  v2[0] = point1[0]-point2[0]
  v2[1] = point1[1]-point2[1]
  v2[2] = point1[2]-point2[2]
  #geh: area = 0.5 * |v1 x v2|
  vcross = np.cross(v1,v2)
  area = 0.5*np.sqrt(np.dot(vcross,vcross))
  return area

def ProjectArea(point1,point2,point3,pointup,pointdn):
  '''Calculates area of 3-point face and projects it to intercept of vector between cell centers
     translated from grid_unstructured.F90, deep into UGridComputeInternConnect
  '''
  v1 = np.zeros(3,'f8')
  v2 = np.zeros(3,'f8')
  v3 = np.zeros(3,'f8')
  v1[0] = point3[0]-point2[0]
  v1[1] = point3[1]-point2[1]
  v1[2] = point3[2]-point2[2]
  v2[0] = point1[0]-point2[0]
  v2[1] = point1[1]-point2[1]
  v2[2] = point1[2]-point2[2]
  v3[0] = pointdn[0]-pointup[0]
  v3[1] = pointdn[1]-pointup[1]
  v3[2] = pointdn[2]-pointup[2]
  n_up_dn = v3/np.sqrt(np.dot(v3,v3))
  #geh: area = 0.5 * |v1 x v2|
  vcross = np.cross(v1,v2)
  magnitude = np.sqrt(np.dot(vcross,vcross))
  n1 = vcross/magnitude
  area = 0.5*magnitude #the same as the area calc'd by ComputeArea()
  area_proj = np.absolute(area*np.dot(n1,n_up_dn))
  return area_proj

def abaqus_to_uge():
  start_time = time.time()
  infilename = sys.argv[1]
#  infilename = '1dcompare.inp'
  outfilename = infilename.split('.')[0] + '.uge'
  domainname = infilename.split('.')[0] + '-domain.h5'
  xmfname = infilename.split('.')[0] + '-domain.xmf'
  if len(sys.argv) != 2:
    print("ERROR: Command line arguments not provided.\n")
    print("Usage: python abaqus2explicit.py abaqus_mesh.inp")
    sys.exit(0)

  print("Infile: %s Outfile: %s" %(infilename, outfilename))

  nodes = []
  elements = []
  # open Abaqus file and read in nodes and elements
  abaqus_file = open(infilename,'r')
  while(1):
    line = abaqus_file.readline()
    print(line)
    if line.startswith('*NODE'):
      while(True):
        line = abaqus_file.readline()
        if line.startswith('*'):
          break
        w = line.split(',')
        node_coordinates = [0.]*3
        for icoord in range(3):
          node_coordinates[icoord] = float(w[icoord+1])
        nodes.append(node_coordinates)
        if len(nodes) % 10000 == 0:
          print('Node %d' % len(nodes))
    elif line.startswith('*ELEMENT'):
      while(True):
        line = abaqus_file.readline()
        if line.startswith('*ELEMENT'):
          print(line)
          continue
        elif line.startswith('**'):
          break
        w = line.split(',')
        element_id = int(w[0])
        node_ids = [0.]*8
        for inode in range(8):
          node_ids[inode] = int(w[inode+1])
        #elements.append([element_id,node_ids])
        node_ids.insert(0,element_id)
        elements.append(node_ids)
        if len(elements) % 10000 == 0:
          print('Element %d' % len(elements))
    else:
      if len(nodes) > 0 and len(elements) > 0:
        break
  abaqus_file.close()

  # Open PFLOTRAN HDF5 domain file, for use with Paraview
  h5_file = File(domainname, mode='w')

  print('Write -domain.h5')
  # replace list with array and write node coordinates to -domain.h5
  print('Creating Vertices data set. %i vertices' % len(nodes))
  nodes = np.array(nodes,'f8')
  h5_file.create_dataset('Domain/Vertices',data=nodes)

  # replace list with array and write cell vertices to -domain.h5
  print('Creating Cells data set. %i cells' % len(elements))
  elements = np.array(elements,'i4')
  int_array = np.zeros((len(elements)*9),'i4')
  int_array[:] = 9 #tells Paraview/XDMF to expect hexes
  for i in range(len(elements)):
    int_array[(i*9+1):((i+1)*9)] = elements[i][1:9]-1 #zero-indexed for Paraview
  h5_file.create_dataset('Domain/Cells',data=int_array)
  del int_array
  
  # give Paraview cell ids, too, because tired of operating blind
  print('Creating Cell_Ids data set.')
  int_array = np.array(elements[:,0],'i4')
  h5_file.create_dataset('Domain/Cell_Ids',data=int_array)
  del int_array
  print('Creating Node_Ids data set.')
  int_array = np.zeros((len(nodes)),'i4')
  for i in range(len(int_array)):
    int_array[i] = i+1
  h5_file.create_dataset('Domain/Node_Ids',data=int_array)
  del int_array

  # compute cell centroid, write to .uge and to -domain.h5
  # method as in grid_unstructured_cell.F90
  print('Creating XC,YC,ZC data sets (cell centroids)')
  centroids = np.zeros((len(elements),3),'f8')
  count = 0
  for cell in elements:
    for i in range(1,9):
      centroids[count][0] += nodes[cell[i]-1][0] #x 
      centroids[count][1] += nodes[cell[i]-1][1] #y
      centroids[count][2] += nodes[cell[i]-1][2] #z
    centroids[count] /= 8.
    count += 1
  x_array = np.array(centroids[:,0],'f8')
  y_array = np.array(centroids[:,1],'f8')
  z_array = np.array(centroids[:,2],'f8')
  h5_file.create_dataset('Domain/XC',data=x_array)
  h5_file.create_dataset('Domain/YC',data=y_array)
  h5_file.create_dataset('Domain/ZC',data=z_array)
  del x_array
  del y_array
  del z_array
  
  # compute cell volumes, and add to h5 file for Paraview
  # method as in grid_unstructured_cell.F90
  print('Compute cell volumes')
  volumes = np.zeros((len(elements)),'f8')
  count = 0
  for cell in elements:
  # split into 5 tetrahedrons
    volumes[count] =  ComputeVolumeOfTetrahedron(nodes[cell[1]-1],nodes[cell[2]-1],
                                                nodes[cell[3]-1],nodes[cell[6]-1])
    volumes[count] += ComputeVolumeOfTetrahedron(nodes[cell[1]-1],nodes[cell[6]-1],
                                                nodes[cell[8]-1],nodes[cell[5]-1])
    volumes[count] += ComputeVolumeOfTetrahedron(nodes[cell[1]-1],nodes[cell[6]-1],
                                                nodes[cell[3]-1],nodes[cell[8]-1])
    volumes[count] += ComputeVolumeOfTetrahedron(nodes[cell[8]-1],nodes[cell[6]-1],
                                                nodes[cell[3]-1],nodes[cell[7]-1])
    volumes[count] += ComputeVolumeOfTetrahedron(nodes[cell[1]-1],nodes[cell[3]-1],
                                                nodes[cell[4]-1],nodes[cell[8]-1])
    count += 1

  h5_file.create_dataset('Domain/Volume',data=volumes)
  #Done with -domain.h5
  h5_file.close()

  #write .xmf file so I can look at the domain
  print('Write .xmf file for -domain.h5')
  domainf = File(domainname,'r') #reopen for reading
  xmf = open(xmfname,'w')
  xmf.write(wxmf.header_template)
  xmf.write(wxmf.topo_template % (len(domainf['Domain/XC']),len(domainf['Domain/Cells']),
                                  domainname.split('.')[0]))
  xmf.write(wxmf.geo_template % (len(domainf['Domain/Vertices']),
                                 domainname.split('.')[0]))
  xmf.write(wxmf.XYZ_template % ('XC','Scalar','Cell',len(domainf['Domain/XC']),
                                 domainname.split('.')[0],'Domain','XC'))
  xmf.write(wxmf.XYZ_template % ('YC','Scalar','Cell',len(domainf['Domain/YC']),
                                 domainname.split('.')[0],'Domain','YC'))
  xmf.write(wxmf.XYZ_template % ('ZC','Scalar','Cell',len(domainf['Domain/ZC']),
                                 domainname.split('.')[0],'Domain','ZC'))
  xmf.write(wxmf.XYZ_template % ('Cell_Ids','Scalar','Cell',len(domainf['Domain/Cell_Ids']),
                                 domainname.split('.')[0],'Domain','Cell_Ids'))
  xmf.write(wxmf.XYZ_template % ('Node_Ids','Scalar','Node',len(domainf['Domain/Node_Ids']),
                                 domainname.split('.')[0],'Domain','Node_Ids'))
  xmf.write(wxmf.XYZ_template % ('Volume','Scalar','Cell',len(domainf['Domain/Volume']),
                                 domainname.split('.')[0],'Domain','Volume'))
  xmf.write(wxmf.tail_template)
  xmf.close()

  # find cell connections (face centroids), compute area of faces
  # Only good for hexes
  allfaces = []
  for cell in elements:
    #assign node ids to faces, which are 1-indexed because cell[0] is cell id
    #also tag face with position 1-6 and store assoc. cell id
    #look at all faces in direction of increasing coordinate value
    face1 = [1,cell[1],cell[2],cell[6],cell[5],cell[0]]
    face2 = [2,cell[3],cell[2],cell[6],cell[7],cell[0]]
    face3 = [3,cell[4],cell[3],cell[7],cell[8],cell[0]]
    face4 = [4,cell[4],cell[1],cell[5],cell[8],cell[0]]
    face5 = [5,cell[1],cell[2],cell[3],cell[4],cell[0]]
    face6 = [6,cell[5],cell[6],cell[7],cell[8],cell[0]]
    newface = True
    for face in allfaces:
      if face1[1:5] == face[1:5]:
        newface = False
        face.append(face1[5]) #second cell for that face is found!
        break
      else:
        newface = True
    if newface == True: allfaces.append(face1)
    for face in allfaces:
      if face2[1:5] == face[1:5]:
        newface = False
        face.append(face2[5]) #second cell for that face is found!
        break
      else:
        newface = True
    if newface == True: allfaces.append(face2)
    for face in allfaces:
      if face3[1:5] == face[1:5]:
        newface = False
        face.append(face3[5]) #second cell for that face is found!
        break
      else:
        newface = True
    if newface == True: allfaces.append(face3)
    for face in allfaces:
      if face4[1:5] == face[1:5]:
        newface = False
        face.append(face4[5]) #second cell for that face is found!
        break
      else:
        newface = True
    if newface == True: allfaces.append(face4)
    for face in allfaces:
      if face5[1:5] == face[1:5]:
        newface = False
        face.append(face5[5]) #second cell for that face is found!
        break
      else:
        newface = True
    if newface == True: allfaces.append(face5)
    for face in allfaces:
      if face6[1:5] == face[1:5]:
        newface = False
        face.append(face6[5]) #second cell for that face is found!
        break
      else:
        newface = True
    if newface == True: allfaces.append(face6)
  print('Found %i unique faces' % len(allfaces))
  #find faces that are connections
  connections = []
  south = []
  east = []
  north = []
  west = []
  bottom = []
  top = []
  for face in allfaces:
    if len(face) == 7:
      connections.append(face)
    elif face[0] == 1:
      south.append(face)
    elif face[0] == 2:
      bottom.append(face)
    elif face[0] == 3:
      north.append(face)
    elif face[0] == 4:
      top.append(face)
    elif face[0] == 5:
      east.append(face)
    elif face[0] ==6:
      west.append(face)
  print('Found %i connections' % len(connections))
    
  #connections is a list holding [facetype,node1,node2,node3,node4,cell1,cell2]
  print('Calculate centroids and areas of connections')
  face_centroids = np.zeros((len(connections),3),'f8')
  intercepts = np.zeros((len(connections),3),'f8')
  areas = np.zeros((len(connections)),'f8')
  projareas = np.zeros((len(connections)),'f8')
  count = 0
  for face in connections:
    for i in range(1,5): #average coordinates of the face vertices
      face_centroids[count] += nodes[face[i]-1] #matrix math should work!
    face_centroids[count] /= 4. #quad face
    
    # Don't know whether I need the centroids or the intercepts
    # Do both for now
    # add two planes and average the intercepts for QUAD_FACE_TYPE
    plane = ComputePlane(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    intercept1 = GetPlaneIntercept(plane,centroids[face[5]-1],centroids[face[6]-1])
    plane = ComputePlane(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    intercepts[count] = 0.5*( intercept1 + GetPlaneIntercept(plane,centroids[face[5]-1],centroids[face[6]-1]))

    # Area = sum of two tri planes for QUAD_FACE_TYPE
    areas[count] = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    areas[count] += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    # Or Area projected to intercept might be necessary in some case?
    projareas[count] = ProjectArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1],centroids[face[5]-1],centroids[face[6]-1])
    projareas[count] += ProjectArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1],centroids[face[5]-1],centroids[face[6]-1])

    count += 1

  #Write .uge file
  print('Write .uge file')
  f = open(outfilename,'w')
  f.write('CELLS %i\n' % len(elements))
  count = 0
  cellid = 1
  for cell in elements:
    f.write('%i %f %f %f %f\n' % (cellid,centroids[count][0],centroids[count][1],
                                  centroids[count][2],volumes[count]))
    count += 1
    cellid += 1
  f.write('CONNECTIONS %i\n' % len(connections))
  count = 0
  for face in connections:
    #f.write('%i %i %f %f %f %f\n' % (face[5],face[6],face_centroids[count][0],
    #                                 face_centroids[count][1],face_centroids[count][2],
    #                                 areas[count]))
    #print intercepts and projareas, because these are probably more general case for slanted surfaces
    f.write('%i %i %f %f %f %f\n' % (face[5],face[6],intercepts[count][0],
                                     intercepts[count][1],intercepts[count][2],
                                     projareas[count]))
    count += 1
  f.close()

  #Write .ex files for boundary conditions
  print('Write boundaries to .ex files')
  boundary = np.zeros((3),'f8')
  f = open(outfilename.split('.')[0]+'-top.ex','w')
  f.write('CONNECTIONS %i\n' % len(top))
  for face in top:
    boundary = [0.,0.,0.]
    for i in range(1,5): #average coordinates of the face vertices
      boundary += nodes[face[i]-1]
    boundary /= 4. #quad face
    b_area = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    b_area += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    f.write('%i %f %f %f %f\n' % (face[5],boundary[0],boundary[1],boundary[2],b_area))
  f.close()
  f = open(outfilename.split('.')[0]+'-bottom.ex','w')
  f.write('CONNECTIONS %i\n' % len(bottom))
  for face in bottom:
    boundary = [0.,0.,0.]
    for i in range(1,5): #average coordinates of the face vertices
      boundary += nodes[face[i]-1]
    boundary /= 4. #quad face
    b_area = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    b_area += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    f.write('%i %f %f %f %f\n' % (face[5],boundary[0],boundary[1],boundary[2],b_area))
  f.close()
  f = open(outfilename.split('.')[0]+'-east.ex','w')
  f.write('CONNECTIONS %i\n' % len(east))
  for face in east:
    boundary = [0.,0.,0.]
    for i in range(1,5): #average coordinates of the face vertices
      boundary += nodes[face[i]-1]
    boundary /= 4. #quad face
    b_area = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    b_area += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    f.write('%i %f %f %f %f\n' % (face[5],boundary[0],boundary[1],boundary[2],b_area))
  f.close()
  f = open(outfilename.split('.')[0]+'-west.ex','w')
  f.write('CONNECTIONS %i\n' % len(west))
  for face in west:
    boundary = [0.,0.,0.]
    for i in range(1,5): #average coordinates of the face vertices
      boundary += nodes[face[i]-1]
    boundary /= 4. #quad face
    b_area = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    b_area += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    f.write('%i %f %f %f %f\n' % (face[5],boundary[0],boundary[1],boundary[2],b_area))
  f.close()
  f = open(outfilename.split('.')[0]+'-north.ex','w')
  f.write('CONNECTIONS %i\n' % len(north))
  for face in north:
    boundary = [0.,0.,0.]
    for i in range(1,5): #average coordinates of the face vertices
      boundary += nodes[face[i]-1]
    boundary /= 4. #quad face
    b_area = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    b_area += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    f.write('%i %f %f %f %f\n' % (face[5],boundary[0],boundary[1],boundary[2],b_area))
  f.close()
  f = open(outfilename.split('.')[0]+'-south.ex','w')
  f.write('CONNECTIONS %i\n' % len(south))
  for face in south:
    boundary = [0.,0.,0.]
    for i in range(1,5): #average coordinates of the face vertices
      boundary += nodes[face[i]-1]
    boundary /= 4. #quad face
    b_area = ComputeArea(nodes[face[1]-1],nodes[face[2]-1],nodes[face[3]-1])
    b_area += ComputeArea(nodes[face[3]-1],nodes[face[4]-1],nodes[face[1]-1])
    f.write('%i %f %f %f %f\n' % (face[5],boundary[0],boundary[1],boundary[2],b_area))
  f.close()

  print('done')
  
if __name__ == "__main__":
  abaqus_to_uge()
