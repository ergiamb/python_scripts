#make regions, strata, initial condition, and source sink cards to go with cubit grid
#~/modeling/2016drepo/grids/drepo_grid3_usg.h5
#not making boundary condition cards in here, or flow and transport conditions, or actual materials
#though I could
#emily 2.17.16
#3.16.16 add  a file for glass waste form blocks to this
#5.6.16 multiple dsnf waste forms
#5.17.16 21 dsnf drifts

import wfg_block as wfg

#hardwire some names
mat1 = 'granite'
mat2 = 'drz'
mat3 = 'buffer'
mat4 = 'cement'
mat5 = 'wp_hlw'
mat6 = 'wp_dsnf'

flow1 = 'initial'
#flow2 = 'wp_hlw_heat' #incorporated into glassbins
#flow3 = 'wp_dsnf_heat' #incorporated into dsnfbins
trans1 = 'initial_granite'
trans2 = 'initial_wp_hlw' #initial concentrations in waste package
trans3 = 'initial_wp_dsnf' #initial concentrations in waste package
trans4 = 'initial_buffer' #initial bentonite_mineral for sorption

#define multiple glass bins: name, num wps, glass volume per wp
glassbins = [('hanford_glass',1485,5.7),('savannah_river_glass',1014,3.5)]
#define multiple dsnf waste form bins: name, num wps
dsnfbins = [('dsnf_50',787),('dsnf_100',158),('dsnf_200',636),('dsnf_300',8),
            ('dsnf_500',28),('dsnf_1000',60),('dsnf_1500',3)]

#define dimensions (for the most part)
small = 5./3. #smallest division of material

x1 = 595. + 2.*small #west end of repository drz
x2 = x1 + small #start of buffer
y1 = 600. #south face of repository
y2 = y1 + small #start of buffer (inside hallway)
y3 = y1 + 825. #north face of repository
y4 = y3 - small #end of buffer (inside hallway)
z1 = 675. - small #base of repository DRIFT drz (not vertical emplacement borehole)
z2 = z1 + small #start of buffer
zve = z1 - 4.*small #base of vertical emplacement borehole

#x drift dimensions repeat
x_drz = 5.*small
x_buff = 3.*small
x_wp_hlw = small
x_driftspacing = 20.
#x hall dimensions happen once
x_drz_hall = 830. - small
x_buff_hall = x_drz_hall - 2.*small
#need more numbers for vertical emplacement of dsnf
xve_drz = 5.*small/3.
xve_buff = small
xve_cement = small
xve_dsnf = small/3.

#y drift dimensions
y_drz_drift = 825. - 12.*small #overlaps drz on inside of hallway
y_buff_drift = y_drz_drift   #needs to overwrite drz on inside of hallway
y_wp_hlw = 5. #repeats
y_wp_spacing_hlw = 4.*small
#y hall dimensions
y_drz_hall = 7.*small
y_buff_hall = y_drz_hall - 2.*small
#need more numbers for vertical emplacement of dsnf
yve_drz = xve_drz
yve_buff = xve_buff
yve_cement = xve_cement
yve_dsnf = xve_dsnf
y_wp_spacing_dsnf = 20. #10.

#z drz and buffer are the same in drifts and hall
z_drz = 5.*small
z_buff = 3.*small
z_wp = small
#need more numbers for vertical emplacement of dsnf
zve_drz = 4.*small
zve_cement = small/3.
zve_buff = 13.*small/3.
zve_dsnf = 8.*small/3.

num_drifts = 42
num_drifts_dsnf = 21
num_wp_hlw = 119 #per drift
num_pair_dsnf = 40#0 #per drift
#check that dsnfbins sum to the same number as dsnf wps
sumdsnf = 0
for ii in range(len(dsnfbins)):
  sumdsnf += dsnfbins[ii][1] 
if sumdsnf != num_drifts_dsnf*num_pair_dsnf*2.:
  print('DSNF bins sums to %i and DSNF wps sum to %i.  They must be equal.' % (sumdsnf,num_drifts_dsnf*num_pair_dsnf*2.))
  #return

#write region, strata, and condition coupler blocks
#model domain, then DRZ, then buffer, then wp
rfile = file('21dsnf_regions.txt', 'w')
sfile = file('21dsnf_strata.txt', 'w')
cfile = file('21dsnf_condition.txt','w')
ssfile = file('21dsnf_source_sink.txt','w')
wfgfile = file('21dsnf_wfg.txt','w') 
wfg.wfg_to_file(wfgfile)


#Model Domain
rfile.write('skip\nREGION all\n  COORDINATES\n'+
    '    -1.d20 -1.d20 -1.d20\n'+
    '     1.d20  1.d20  1.d20\n'+
    '  /\nEND\nnoskip\n\n')
sfile.write('skip\nSTRATA\n  REGION all\n  MATERIAL '+mat1+'\nEND\nnoskip\n\n')
cfile.write('skip\nINITIAL_CONDITION all\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans1+
    '\n  REGION all\nEND\nnoskip\n\n')

#DRZ, hallway then drift, then ve boreholes
rfile.write('REGION drz_southhall\n  COORDINATES\n'+
    '    '+str(x1)+' '+str(y1)+' '+str(z1)+'\n'+
    '    '+str(x1+x_drz_hall)+' '+str(y1+y_drz_hall)+' '+str(z1+z_drz)+'\n'+
    '  /\nEND\n\n')
sfile.write('STRATA\n  REGION drz_southhall\n  MATERIAL '+mat2+'\nEND\n\n')

rfile.write('REGION drz_northhall\n  COORDINATES\n'+
    '    '+str(x1)+' '+str(y3-y_drz_hall)+' '+str(z1)+'\n'+
    '    '+str(x1+x_drz_hall)+' '+str(y3)+' '+str(z1+z_drz)+'\n'+
    '  /\nEND\n\n')
sfile.write('STRATA\n  REGION drz_northhall\n  MATERIAL '+mat2+'\nEND\n\n')

ymin = y1 + 6.*small #overlaps drz on inside of hallway
zmin = z1
ymax = ymin + y_drz_drift
zmax = zmin + z_drz
for i in range(num_drifts):
    xmin = x1 + i*x_driftspacing 
    xmax = xmin + x_drz
    rfile.write('REGION drz_drift'+str(i)+'\n  COORDINATES\n'+
      '    '+str(xmin)+' '+str(ymin)+' '+str(zmin)+'\n'+
      '    '+str(xmax)+' '+str(ymax)+' '+str(zmax)+'\n'+
      '  /\nEND\n\n')
    sfile.write('STRATA\n  REGION drz_drift'+str(i)+'\n  MATERIAL '+mat2+'\nEND\n\n')

#ve boreholes DRZ, update this # when grid is done.
zmin = zve
zmax = zmin + zve_drz
for i in range(num_drifts_dsnf):
    xmin_r = x1 + small/3.*4. + i*x_driftspacing #I think this number is correct 
    xmax_r = xmin_r + xve_drz
    xmin_l = x1 + small/3.*6. + i*x_driftspacing #I think this number is correct 
    xmax_l = xmin_l + xve_drz
    for j in range(num_pair_dsnf): #two alternating sets
      ymin_r = y1 + 15.+small/3. + j*y_wp_spacing_dsnf #I think this number is correct
      ymax_r = ymin_r + yve_drz
      ymin_l = y1 + 25.+small/3. + j*y_wp_spacing_dsnf #I think this number is correct
      ymax_l = ymin_l + yve_drz
      rfile.write('REGION drz_ve_r'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
        '    '+str(xmin_r)+' '+str(ymin_r)+' '+str(zmin)+'\n'+
        '    '+str(xmax_r)+' '+str(ymax_r)+' '+str(zmax)+'\n'+
        '  /\nEND\n\n')
      sfile.write('STRATA\n  REGION drz_ve_r'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat2+'\nEND\n\n')
      rfile.write('REGION drz_ve_l'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
        '    '+str(xmin_l)+' '+str(ymin_l)+' '+str(zmin)+'\n'+
        '    '+str(xmax_l)+' '+str(ymax_l)+' '+str(zmax)+'\n'+
        '  /\nEND\n\n')
      sfile.write('STRATA\n  REGION drz_ve_l'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat2+'\nEND\n\n')

#Buffer, hallway then drift, then ve boreholes
rfile.write('REGION buff_southhall\n  COORDINATES\n'+
    '    '+str(x2)+' '+str(y2)+' '+str(z2)+'\n'+
    '    '+str(x2+x_buff_hall)+' '+str(y2+y_buff_hall)+' '+str(z2+z_buff)+'\n'+
    '  /\nEND\n\n')
sfile.write('STRATA\n  REGION buff_southhall\n  MATERIAL '+mat3+'\nEND\n\n')
cfile.write('INITIAL_CONDITION buff_southhall\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans4+
    '\n  REGION buff_southhall\nEND\n\n')

rfile.write('REGION buff_northhall\n  COORDINATES\n'+
    '    '+str(x2)+' '+str(y4-y_buff_hall)+' '+str(z2)+'\n'+
    '    '+str(x2+x_buff_hall)+' '+str(y4)+' '+str(z2+z_buff)+'\n'+
    '  /\nEND\n\n')
sfile.write('STRATA\n  REGION buff_northhall\n  MATERIAL '+mat3+'\nEND\n\n')
cfile.write('INITIAL_CONDITION buff_northhall\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans4+
    '\n  REGION buff_northhall\nEND\n\n')

ymin = y1+6.*small #same length as drift drz
zmin = z2
ymax = ymin + y_buff_drift
zmax = zmin + z_buff
for i in range(num_drifts):
    xmin = x2 + i*x_driftspacing 
    xmax = xmin + x_buff
    rfile.write('REGION buff_drift'+str(i)+'\n  COORDINATES\n'+
      '    '+str(xmin)+' '+str(ymin)+' '+str(zmin)+'\n'+
      '    '+str(xmax)+' '+str(ymax)+' '+str(zmax)+'\n'+
      '  /\nEND\n\n')
    sfile.write('STRATA\n  REGION buff_drift'+str(i)+'\n  MATERIAL '+mat3+'\nEND\n\n')
    cfile.write('INITIAL_CONDITION buff_drift'+str(i)+'\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans4+
        '\n  REGION buff_drift'+str(i)+'\nEND\n\n')

#Buffer ve boreholes, update this # when grid is done.
zmin = zve + 2.*small/3.
zmax = zmin + zve_buff
for i in range(num_drifts_dsnf):
    xmin_r = x1 + small/3.*5. + i*x_driftspacing  #I think this number is correct
    xmax_r = xmin_r + xve_buff
    xmin_l = x1 + small/3.*7. + i*x_driftspacing  #I think this number is correct
    xmax_l = xmin_l + xve_buff
    for j in range(num_pair_dsnf): #two alternating sets
      ymin_r = y1 + 15. + small/3.*2. + j*y_wp_spacing_dsnf #I think this number is correct
      ymax_r = ymin_r + yve_buff
      ymin_l = y1 + 25. + small/3.*2. + j*y_wp_spacing_dsnf #I think this number is correct
      ymax_l = ymin_l + yve_buff
      rfile.write('REGION buff_ve_r'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
        '    '+str(xmin_r)+' '+str(ymin_r)+' '+str(zmin)+'\n'+
        '    '+str(xmax_r)+' '+str(ymax_r)+' '+str(zmax)+'\n'+
        '  /\nEND\n\n')
      sfile.write('STRATA\n  REGION buff_ve_r'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat3+'\nEND\n\n')
      cfile.write('INITIAL_CONDITION buff_ve_r'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans4+
          '\n  REGION buff_ve_r'+str(i)+'_'+str(j)+'\nEND\n\n')
      rfile.write('REGION buff_ve_l'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
        '    '+str(xmin_l)+' '+str(ymin_l)+' '+str(zmin)+'\n'+
        '    '+str(xmax_l)+' '+str(ymax_l)+' '+str(zmax)+'\n'+
        '  /\nEND\n\n')
      sfile.write('STRATA\n  REGION buff_ve_l'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat3+'\nEND\n\n')
      cfile.write('INITIAL_CONDITION buff_ve_l'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans4+
          '\n  REGION buff_ve_l'+str(i)+'_'+str(j)+'\nEND\n\n')

#Cement at base of ve
zmin = zve + small/3.
zmax = zmin + zve_cement
for i in range(num_drifts_dsnf):
    xmin_r = x1 + small/3.*5. + i*x_driftspacing  #I think this number is correct
    xmax_r = xmin_r + xve_cement
    xmin_l = x1 + small/3.*7. + i*x_driftspacing  #I think this number is correct
    xmax_l = xmin_l + xve_cement
    for j in range(num_pair_dsnf): #two alternating sets
      ymin_r = y1 + 15. + small/3.*2. + j*y_wp_spacing_dsnf #I think this number is correct
      ymax_r = ymin_r + yve_cement
      ymin_l = y1 + 25. + small/3.*2. + j*y_wp_spacing_dsnf #I think this number is correct
      ymax_l = ymin_l + yve_cement
      rfile.write('REGION cement_r'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
        '    '+str(xmin_r)+' '+str(ymin_r)+' '+str(zmin)+'\n'+
        '    '+str(xmax_r)+' '+str(ymax_r)+' '+str(zmax)+'\n'+
        '  /\nEND\n\n')
      sfile.write('STRATA\n  REGION cement_r'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat4+'\nEND\n\n')
      rfile.write('REGION cement_l'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
        '    '+str(xmin_l)+' '+str(ymin_l)+' '+str(zmin)+'\n'+
        '    '+str(xmax_l)+' '+str(ymax_l)+' '+str(zmax)+'\n'+
        '  /\nEND\n\n')
      sfile.write('STRATA\n  REGION cement_l'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat4+'\nEND\n\n')

# HLW Waste Packages  
zmin = z2+small
zmax = zmin + z_wp
zcenter = (zmin+zmax)/2.
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = glassbins[0][1] #ditto
for i in range(num_drifts_dsnf, num_drifts):
    xmin = x2+small + i*x_driftspacing 
    xmax = xmin + x_wp_hlw
    xcenter = (xmin+xmax)/2.
    for j in range(num_wp_hlw):
        ymin = y1 + 10.*small + j*y_wp_spacing_hlw
        ymax = ymin + y_wp_hlw
        ycenter = (ymin+ymax)/2.
        #write things that are the same for all glass
        rfile.write('REGION wp_hlw'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
          '    '+str(xmin)+' '+str(ymin)+' '+str(zmin)+'\n'+
          '    '+str(xmax)+' '+str(ymax)+' '+str(zmax)+'\n'+
          '  /\nEND\n\n')
        sfile.write('STRATA\n  REGION wp_hlw'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat5+'\nEND\n\n')
        cfile.write('INITIAL_CONDITION wp_hlw'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans2+
            '\n  REGION wp_hlw'+str(i)+'_'+str(j)+'\nEND\n\n')
        #write things that are different depending on glass bin
        if counter >= wpnum and counter < cumulative:
          ssfile.write('SOURCE_SINK wp_hlw'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+glassbins[ii][0]+'\n  TRANSPORT_CONDITION '+trans2+
              '\n  REGION wp_hlw'+str(i)+'_'+str(j)+'\nEND\n\n')
          wfg.glass_to_file(wfgfile,xcenter,ycenter,zcenter,glassbins[ii][2],glassbins[ii][0])
          counter += 1
          if counter == wpnum + glassbins[ii][1]:
            wpnum += glassbins[ii][1] #before increment ii
            ii += 1
            if ii < len(glassbins): cumulative += glassbins[ii][1] #after increment ii
          print('glass %i, %i, %i' % (counter,wpnum,ii))

# DSNF Waste Packages  
zmin = zve + 2.*small/3.
zmax = zmin + zve_dsnf
zcenter = (zmin+zmax)/2.
counter = 0 #for assigning dsnf bins
wpnum = 0 #ditto
ii = 0    #ditto
cumulative = dsnfbins[0][1] #ditto
for i in range(num_drifts_dsnf):
    xmin_r = x1 + small/3.*6. + i*x_driftspacing  #I think this number is correct
    xmax_r = xmin_r + xve_dsnf
    xcenter_r = (xmin_r+xmax_r)/2.
    xmin_l = x1 + small/3.*8. + i*x_driftspacing  #I think this number is correct
    xmax_l = xmin_l + xve_dsnf
    xcenter_l = (xmin_l+xmax_l)/2.
    for j in range(num_pair_dsnf): #two alternating sets
        ymin_r = y1 + 15. + small/3.*3. + j*y_wp_spacing_dsnf #I think this number is correct
        ymax_r = ymin_r + yve_dsnf
        ycenter_r = (ymin_r+ymax_r)/2.
        ymin_l = y1 + 25. + small/3.*3. + j*y_wp_spacing_dsnf #I think this number is correct
        ymax_l = ymin_l + yve_dsnf
        ycenter_l = (ymin_l+ymax_l)/2.
        #write things that are the same for all dsnf
        rfile.write('REGION wp_dsnf_r'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
          '    '+str(xmin_r)+' '+str(ymin_r)+' '+str(zmin)+'\n'+
          '    '+str(xmax_r)+' '+str(ymax_r)+' '+str(zmax)+'\n'+
          '  /\nEND\n\n')
        sfile.write('STRATA\n  REGION wp_dsnf_r'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat6+'\nEND\n\n')
        cfile.write('INITIAL_CONDITION wp_dsnf_r'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans3+
            '\n  REGION wp_dsnf_r'+str(i)+'_'+str(j)+'\nEND\n\n')
        rfile.write('REGION wp_dsnf_l'+str(i)+'_'+str(j)+'\n  COORDINATES\n'+
          '    '+str(xmin_l)+' '+str(ymin_l)+' '+str(zmin)+'\n'+
          '    '+str(xmax_l)+' '+str(ymax_l)+' '+str(zmax)+'\n'+
          '  /\nEND\n\n')
        sfile.write('STRATA\n  REGION wp_dsnf_l'+str(i)+'_'+str(j)+'\n  MATERIAL '+mat6+'\nEND\n\n')
        cfile.write('INITIAL_CONDITION wp_dsnf_l'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+flow1+'\n  TRANSPORT_CONDITION '+trans3+
            '\n  REGION wp_dsnf_l'+str(i)+'_'+str(j)+'\nEND\n\n')
        #write things that are different depending on dsnf bin
        if counter >= wpnum and counter < cumulative:
          ssfile.write('SOURCE_SINK wp_dsnf_r'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+dsnfbins[ii][0]+'\n  TRANSPORT_CONDITION '+trans3+
              '\n  REGION wp_dsnf_r'+str(i)+'_'+str(j)+'\nEND\n\n')
          wfg.dsnf_to_file(wfgfile,xcenter_r,ycenter_r,zcenter,dsnfbins[ii][0])
          counter += 1
          if counter == wpnum + dsnfbins[ii][1]:
            wpnum += dsnfbins[ii][1] #before increment ii
            ii += 1
            if ii < len(dsnfbins): cumulative += dsnfbins[ii][1] #after increment ii
          #print('right side %i, %i, %i' % (counter,wpnum,ii))
        if counter >= wpnum and counter < cumulative:
          ssfile.write('SOURCE_SINK wp_dsnf_l'+str(i)+'_'+str(j)+'\n  FLOW_CONDITION '+dsnfbins[ii][0]+'\n  TRANSPORT_CONDITION '+trans3+
              '\n  REGION wp_dsnf_l'+str(i)+'_'+str(j)+'\nEND\n\n')
          wfg.dsnf_to_file(wfgfile,xcenter_l,ycenter_l,zcenter,dsnfbins[ii][0])
          counter += 1
          if counter == wpnum + dsnfbins[ii][1]:
            wpnum += dsnfbins[ii][1]
            ii += 1
            if ii < len(dsnfbins): cumulative += dsnfbins[ii][1]
          #print('left side %i, %i, %i' % (counter,wpnum,ii))


rfile.close
sfile.close
cfile.close
ssfile.close
wfgfile.close


