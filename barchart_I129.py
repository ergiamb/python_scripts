''' This script will read a dakota tabular output for Uncertainty analysis and make scatter plots of response vs. variable.  Written by 
Emily 1.1.16 2016drepo/prob
'''

import scipy.stats
import pandas as pd
from pylab import *
import matplotlib as mpl
import matplotlib.pyplot as plt

#mpl.rcParams['text.usetex']= True;
mpl.rcParams['font.size'] = 18;
#mpl.rcParams['font.weight'] = 'bold'
#mpl.rcParams['axes.labelsize'] = 14;
#mpl.rcParams['xtick.labelsize'] = 14;
#mpl.rcParams['ytick.labelsize'] = 14;
#mpl.rcParams['axes.titlesize']= 14;
majorFormatter = mpl.ticker.FormatStrFormatter('%3.1g');

#get the data
dd = pd.read_table('dakota_tabular.dat',delim_whitespace=True);
params = ['rateUNF','kGlacial','tWP','pDRZ','pBuffer','rateWP','gNpKd','bNpKd'];
labels = ['UNF Degrad Rate','Glacial Seds k','WP Tortuosity','DRZ Porosity','Buffer Porosity','WP Degrad Rate','Granite Np Kd','Buffer Np Kd']

def make_subplots(nrow,ncol,nplots,labels,sp_corr_I129,titles):
  for iplot in range(nplots):
    ax5 = plt.subplot(nrow,ncol,iplot+1)
    width = 0.8
    plt.title(titles[iplot])
    ax5.bar(range(len(labels)),sp_corr_I129[iplot], width=width);
    ax5.set_xticks(arange(len(labels))+width/2)
    ax5.set_xticklabels(labels,rotation=60);
    hlines(0,0,width*(len(labels)+2));
    ylim(ymax=1, ymin=-1)
    if iplot == 0:
      plt.ylabel('Spearman Rank Correlation for max [$^{129}$I]', fontsize=22);

#get the data
dd = pd.read_table('dakota_tabular.dat',delim_whitespace=True);

#Figure1--Glacial
fig1 = plt.figure(figsize=(24,8));
#fig1.suptitle('Spearman Rank Correlations of $^{129}$Iodine at Specified Observation Points', fontsize=28)
sp_corr_I129 = []
titles = []
for r in dd.columns:
    if r == 'response_fn_1': #glacial1
        titles.append('a.) Observation point "glacial1"')
        sp_corr_local = [];
        #calculate spearman rank correlations
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_local.append(spcc);
        sp_corr_I129.append(sp_corr_local)
    if r == 'response_fn_2': #glacial2
        titles.append('b.) Observation point "glacial2"')
        sp_corr_local = [];
        #calculate spearman rank correlations
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_local.append(spcc);
        sp_corr_I129.append(sp_corr_local)
    if r == 'response_fn_3': #glacial3
        titles.append('c.) Observation point "glacial3"')
        sp_corr_local = [];
        #calculate spearman rank correlations
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_local.append(spcc);
        sp_corr_I129.append(sp_corr_local)
    else:
        continue
make_subplots(1,3,3,labels,sp_corr_I129,titles)
#make_subplots(1,3,3,params,sp_corr_I129,titles)
#f.subplots_adjust(hspace=0.2,wspace=0.2,bottom=0.16,top=0.9,left=0.14,right=0.9)
plt.savefig('gdsa_I129_glacial_rank_corr.png',bbox_inches='tight')


#Figure2--dz
fig2 = plt.figure(figsize=(24,8));
#fig2.suptitle('Spearman Rank Correlations of $^{129}$Iodine at Specified Observation Points', fontsize=28)
sp_corr_I129 = []
titles = []
for r in dd.columns:
    if r == 'response_fn_4': #dz1
        titles.append('a.) Observation point "dz1"')
        sp_corr_local = [];
        #calculate spearman rank correlations
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_local.append(spcc);
        sp_corr_I129.append(sp_corr_local)
    if r == 'response_fn_5': #dz2
        titles.append('b.) Observation point "dz2"')
        sp_corr_local = [];
        #calculate spearman rank correlations
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_local.append(spcc);
        sp_corr_I129.append(sp_corr_local)
    if r == 'response_fn_6': #dz3
        titles.append('c.) Observation point "dz3"')
        sp_corr_local = [];
        #calculate spearman rank correlations
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_local.append(spcc);
        sp_corr_I129.append(sp_corr_local)
    else:
        continue
make_subplots(1,3,3,labels,sp_corr_I129,titles)
#make_subplots(1,3,3,params,sp_corr_I129,titles)
#f.subplots_adjust(hspace=0.2,wspace=0.2,bottom=0.16,top=0.9,left=0.14,right=0.9)
plt.savefig('gdsa_I129_dz_rank_corr.png',bbox_inches='tight')
plt.show()
