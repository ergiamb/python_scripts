'''10.28.10 Emily Stein <ergiamb@sandia.gov>
   timeseries_for_sa.py
   use Payton's pickled dfile.dat and nearest neighbor method to
   create dakota_tabular.dat style file of response functions (e.g.,
   concentration) versus time. 
'''

import numpy as np
import pandas as pd
import pickle as p
#from pylab import sort

def load_input_variables(niv,dakota_tab='dakota_tabular.dat',
                         interface=True,evalid=True):
  '''patterned on stepwise.py
     creates DataFrame from dakota_tabular.dat (text file)
     assumes first row is column headings
     returns df
  '''
  if evalid == True and interface == True:
    xy = pd.read_table(dakota_tab,delim_whitespace=True,header=0,
                       index_col=0,
                       usecols=[x for x in range(niv+2)])
  elif evalid == True:
    xy = pd.read_table(dakota_tab,delim_whitespace=True,header=0,
                       index_col=0,
                       usecols=[x for x in range(niv+1)])
  elif interface == True:
    xy = pd.read_table(dakota_tab,delim_whitespace=True,header=0,
                       index_col=False,
                       usecols=[x for x in range(niv+1)])
  else:
    xy = pd.read_table(dakota_tab,delim_whitespace=True,header=0,
                       index_col=False,
                       usecols=[x for x in range(niv)])
  print('DataFrame is loaded')
  return(xy)

def make_response_fns(niv,obs,response,start_time):
  dfile = file('dfile.dat','r')
  data = p.load(dfile)
  dfile.close()

  #read sampled inputs from dakota_tabular.dat and put in a pandas dataframe
  dfv = load_input_variables(niv)
  dfl = load_input_variables(niv)
  
  #loop through realizations and find nearest neighbors to collate
  #response function columns from time series output in dfile.dat
  k = np.sort(data.keys())[0] #keys are vector numbers (0-nsim)
  times = data[k][obs]['Time [y]'].copy()
  idx0 = np.where(times >= start_time)[0][0] #returns a tuple or something
  count = 0
  for t in times[idx0:]:
    print(count,t)
    values=[]
    logs=[]
    for k in np.sort(data.keys()):
      #find nearest neighbor in time; save value and log transform of value
      idx = np.argmin(abs(data[k][obs]['Time [y]'] - t))
      values.append(data[k][obs][response][idx])
      logs.append(np.log10(data[k][obs][response][idx]))
    dfv['{:16.10e}'.format(t)] = values #headers are times
    dfl['{:16.10e}'.format(t)] = logs #headers are times
    count += 1
  
  #write df to file
  filename = '{}_timeseries.dat'.format(obs)
  dfv.to_csv(filename,sep=' ',float_format='%16.10e',header=True,index=True)
  filename = '{}log_timeseries.dat'.format(obs)
  dfl.to_csv(filename,sep=' ',float_format='%16.10e',header=True,index=True)

if __name__ == '__main__':
  for obs in ['drz0',]: #'drz1','drz2','cem0','cem1','cem2',]:
    make_response_fns(niv=13,obs=obs,response='Total Cs135 [M]',start_time=1000.)

