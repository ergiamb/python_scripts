'''stepwise2df.py
   read stepwise.py output files stepwise_regression_response_fn_*.txt
   load SRCs into a dataframe with response functions as index and
   input params as columns.
   plan to call this from barcharts_sa.py

   Emily Stein (ergiamb@sandia.gov)
   8.9.18
'''

import pandas as pd
import sys,os
import argparse
import numpy as np

def stepwise2df(directory,response_fns,rank=False):
  '''read stepwise output, put in dataframe and save as text file
  '''
  print('in stepwise2df.py') #faulty logic in here, but will need to rediscover the error
  for i in range(len(response_fns)):
    if rank:
      #use bash script to rename rank regression outputs to this:
      filename = os.path.join(directory,'srrc_response_fn_{}.txt'.format(i+1))
    else:
      filename = os.path.join(directory,'stepwise_regression_response_fn_{}.txt'.format(i+1))
    try:
      with open(filename) as f:
        f.readline() #title
        f.readline() #headers
        params = []
        srcs = []
        r2s = []
        while True:
          try:
            line = f.readline()
            params.append(line.split()[0])
            srcs.append(line.split()[2])
            r2s.append(line.split()[1])
          except: #EOF
            r2 = r2s[-1]
            params.append('R2')
            srcs.append(r2)
            break
    except Exception:
      print('{} not found'.format(filename))
    mini_df = pd.DataFrame(data=np.array((srcs,),dtype=np.float),index=[response_fns[i]],
                           columns=params)
    if i == 0: #first one
      df = mini_df.copy()
    else:
      df = df.append(mini_df)

  #df = df.fillna(0.) #NaNs to zeros
  #print(df)
  #df.to_csv('test.txt',sep=' ')
  return(df)
     
#######################################################################
if __name__ == '__main__':

  response_fn_names = [ 
                'usand1',
                'usand2',
                'usand3',
                'lime1',
                'lime2',
                'lime3',
                'lsand1',
                'lsand2',
                'lsand3',
               ]   

  
  parser = argparse.ArgumentParser(description='put stepwise output for all response functions into one df')
  parser.add_argument('-d','--directory',
                      help='directory containing stepwise.py output',
                      required=True,)
  parser.add_argument('-r','--response_fns',nargs='+',
                      help='list of response_fn names, in correct order!',
                      default = response_fn_names,
                      required=False,)
  args = parser.parse_args()

  stepwise2df(args.directory,args.response_fns)
      

