#before running on skybridge (or redsky) do
#module load canopy/1.4.1

import sys
import os
try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
import pflotran as pft

mpl.rcParams['font.size']=12
#mpl.rcParams['font.weight']='bold'

def make_subplots(nrow, ncol, nplots, filenames, columns, name, titles):
  for iplot in range(nplots):
    plt.subplot(nrow,ncol,iplot+1)
    plt.title(titles[iplot])
    plt.xlabel('Time (years)', fontsize=14, fontweight='bold')
#   if iplot == 0:
    plt.ylabel('[I-129] M', fontsize=14, fontweight='bold')

    plt.xlim(1.e-0,1.e6)
    plt.ylim(1.e-21,1.e-6)
    plt.grid(True)

    for icol in range(iplot*len(columns)/nplots,iplot*len(columns)/nplots+len(columns)/nplots):
      ifile = icol
      data = pft.Dataset(filenames[ifile],1,columns[icol])
    #label with column header
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.semilogx(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  string = data.get_name('yname').split()[3].split('_')[1]
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=string)
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.axis([0.1, 1.e6, 200., 0.]) #why is this here?
    #or label with file name
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=filenames[ifile])
    #or label with name assigned above
      string = name[icol]
      if(icol == 5 or icol == 20 or icol == 35):
	plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],label=string,linewidth=3)
#      if icol%nplots == 0:
#        plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],label=string,linewidth=3)
      elif(icol == 12 or icol == 27 or icol == 42):
	plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle='--',label=string,linewidth=2)
      elif(icol == 7 or icol == 22 or icol == 37):
	plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle='--',label=string,linewidth=2)
      else:
        plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],label=string)
      #plt.semilogx(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)
      #plt.plot(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)

path = []
path.append('.')

titles = []
files = []
columns = []
name = []
linestyles = []
colors = []
basestyles = ['-','-','-','-','-','-','-','-','-','-','-','-','-','-','-'] #['-','--','-.',':']
basecolors = ['indigo','darkviolet','mediumvioletred','red','crimson','darkorange',
              'gold','yellowgreen','green','olivedrab','teal','cadetblue','darkturquoise',
              'deepskyblue','navy']

nsim = 15
#glacial1 (1012.5, 1012.5, 1252.5)
titles.append('a.) Observation point "glacial1"')
for i in range(nsim):
  files.append('./domain'+str(i+1)+'/pflotran-obs-16.tec')
  columns.append(23)
  name.append('domain'+str(i+1))
linestyles.extend(basestyles)
colors.extend(basecolors)

#glacial2 (1807.5,1012.5,1252.5)
titles.append('b.) Observation point "glacial2"')
for i in range(nsim):
  files.append('./domain'+str(i+1)+'/pflotran-obs-13.tec')
  columns.append(86)
  name.append('domain'+str(i+1))
linestyles.extend(basestyles)
colors.extend(basecolors)

#glacial3 (2917.5,1012.5,1252.5)
titles.append('c.) Observation point "glacial3"')
for i in range(nsim):
  files.append('./domain'+str(i+1)+'/pflotran-obs-25.tec')
  columns.append(86)
  name.append('domain'+str(i+1))
linestyles.extend(basestyles)
colors.extend(basecolors)

filenames = pft.get_full_paths(path,files)

f = plt.figure(figsize=(16,16)) #adjust size to fit plots (width, height)
#f.suptitle("Cement, line loads: Temperature (degrees C)",fontsize=16)

make_subplots(2,2,3,filenames,columns,name,titles)

#Choose a location for the legend. #This is associated with the last plot
#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
plt.legend(loc=(1.20,0.175),title='FRACTURE REALIZATION')
#plt.legend(loc=0,title='Fracture Realization')
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
#plt.setp(plt.gca().get_legend().get_texts(),fontsize='large')
#plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
#plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
#plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

#adjust blank space and show/save the plot
f.subplots_adjust(hspace=0.2,wspace=0.2, bottom=.12,top=.9, left=.14,right=.9)
plt.savefig('df_I129_glacial.png',bbox_inches='tight')
plt.show()
