'''
Write 1D tails for explicit unstructured grid (.uge)
Requires cell centers and volumes; face centers and areas (connections)
Also nice to have some way of visualizing, so write .h5 and .xmf for Paraview

Object-oriented version

@author Emily Stein
Sandia National Laboratories
6.16.17
'''

import numpy as np
import os
import sys
import h5py
import write_xmf as wxmf

class Tail(object):
  '''Parent class with no options. A tail in which entire volume of
     3D bulk cell is discretized into a tail with nx cells. Last shell
     of the tail is what remains of the bulk 3D cell.'''

  def __init__(self,coord=[17.5,12.5,37.5],
                    nx=8, #16,
                    shape='PRISM',
                    length=5.,
                    ugename='3dgrid.uge',
                    domainname='3dgrid-domain.h5'):
    '''Initialize variables.'''
    self.coord = coord           #3d center to attach it to
    self.nx = nx                 #number of intervals to discretize it
    self.shape = shape           #'PRISM','CYLINDER','SPHERE'
    self.length = length         #length (m) of the cylinder or prism
    self.ugename = ugename       #in file containing main grid in uge format
    self.domainname = domainname #in file containing main grid for Paraview
    self.basename = self.ugename.split('.')[0] 
    self.tag = ''     
    self.cells = []              #list of lists containing id,xc,yc,zc,volume
    self.connections = []        #list of lists: id1,id2,xc,yc,zc,area
    self.totvol = None           #total volume of 3D bulk cell
    self.bulk_id = None          #cell id of 3D bulk cell
    self.r = None                #radius of equivalent-volume shape
    self.nhalf = None            #number of half cell widths in tail, will be odd
    self.dhalf = None            #width of half cell
    self.dx = None               #width of whole cell
    self.firsttail = None        #id of first cell in tail (innermost)
    self.firstcon = None         #id of first connection in tail (innermost)

  def __str__(self):
    '''Print a description'''
    return '{} contains functions to embed a 1D continuum in a 3D cell'.format(
            self.__class__.__name__)

  def __repr__(self):
    '''This string recreates the object.'''
    return '{}(coord={},nx={},shape={}, \
               length={},ugename={}, \
               domainname={})'.format(self.__class__.__name__,
                                      self.coord,
                                      self.nx,
                                      self.shape,
                                      self.length,
                                      self.ugename,
                                      self.domainname)

  def read_input(self):
    #cells = [] #[id, x, y, z, volume]
    #connections = [] #[[id1, id2, x, y, z, area]]
    infile = open(self.ugename,'r')
    while(1):
      line = infile.readline()
      if line == '': #empty string is end of file
        print('At end of file, big loop')
        break
      if line.startswith('CELLS'):
        while(True):
          line = infile.readline()
          if line.startswith('CONNECTIONS'): #then we've read all the cells
            break
          w = line.split()
          self.cells.append([int(w[0]),float(w[1]),
                             float(w[2]),float(w[3]),
                             float(w[4])])
          if len(self.cells) % 100 == 0:
            print('Cell %d' % len(self.cells))
      else: #Already read the line that startswith('CONNECTIONS')
        while(True):
          line = infile.readline()
          if line == '': #empty string is end of file
            print('At end of file, small loop')
            break
          w = line.split()
          self.connections.append([int(w[0]),int(w[1]),
                                   float(w[2]),float(w[3]),
                                   float(w[4]),float(w[5])])
          if len(self.connections) % 100 == 0:
            print('Connections %d' % len(self.connections))
    infile.close()

  def get_bulk_cell(self):
    '''Find volume and id of 3D bulk cell''' 
    for cell in self.cells:
      if cell[1:4] == self.coord:
        self.totvol = cell[4]
        self.bulk_id = cell[0] #id number not list index!
        break

  def calc_dx(self):
    '''Find radius of equivalent shape and discretization length (dx).'''

    #Find radius and dx
    sa = self.totvol/self.length #S.A. of small face prism or cylinder
    if self.shape.upper() == 'CYLINDER':
      print('CYLINDER')
      self.r = np.sqrt(sa/np.pi)
    elif self.shape.upper() == 'PRISM':
      print('PRISM')
      self.r = np.sqrt(sa)/2.
    elif self.shape.upper() == 'SPHERE':
      print('SPHERE')
      self.r = np.cbrt(3./4.*self.totvol/np.pi)
    self.nhalf = 2*self.nx-1
    self.dhalf = self.r/float(self.nhalf)
    self.dx = 2.*self.dhalf
    print('radius = %f' % self.r)
    print('dx = %f' % self.dx)

  def discretize_tail(self):
    '''Find xc,yc,zc,volume,surface area
    append to self.cells and self.connections'''
    tailvol = 0.
    self.firsttail = len(self.cells) #zero-indexed, not id number
    self.firstcon = len(self.connections)
    cellid = self.firsttail + 1
    print('CellID XCenter Volume TailVol CellID XConnection Area')
    for i in range(self.nx-1):
      if i == 0:
        xc = self.cells[self.bulk_id-1][1] + self.r
      else:
        xc = self.cells[self.bulk_id-1][1] + self.r-(self.dhalf+self.dx*i)
      yc = self.cells[self.bulk_id-1][2]
      zc = self.cells[self.bulk_id-1][3]
      rcon = self.dx*(i+1)
      if self.shape.upper() == 'CYLINDER':
        vol = np.pi*(rcon**2.)*self.length - tailvol
      elif self.shape.upper() == 'PRISM':
        vol = ((2.*rcon)**2.*self.length) - tailvol
      elif self.shape.upper() == 'SPHERE':
        vol = 4./3.*np.pi*rcon**3.
      self.cells.append([cellid,xc,yc,zc,vol])
      xc = self.cells[self.bulk_id-1][1] + (self.r-rcon) #distance from body of tail
      yc = self.cells[self.bulk_id-1][2]
      zc = self.cells[self.bulk_id-1][3]
      if self.shape.upper() == 'CYLINDER':
        area = 2.*rcon*np.pi*self.length + np.pi*rcon**2.*2.
      elif self.shape.upper() == 'PRISM':
        area = 4.*self.length*(2.*rcon) + 2*(2.*rcon)**2.
      elif self.shape.upper() == 'SPHERE':
        area = 4.*np.pi*rcon**2.
      if i < self.nx-2: #connect to next tail cell
        self.connections.append([cellid,cellid+1,xc,yc,zc,area])
      else: #connect to parent
        self.connections.append([cellid,self.bulk_id,xc,yc,zc,area])
      cellid += 1
      tailvol += vol
#      print('%i %f %f %f %i %f %f' % (self.cells[cellid][0],self.cells[cellid][1],
#                                      self.cells[cellid][4],tailvol,
#                                      connections[self.firstcon+i][0],
#                                      connections[self.firstcon+i][2],
#                                      connections[self.firstcon+i][5]))
    #Modify volume of bulk 3D cell
    self.cells[self.bulk_id-1][4] -= tailvol

  def write_uge(self):
    '''Write explicit grid file (.uge) with tail.'''
    tailname = self.basename+'-%s%i-%s.uge' % (
               self.shape.lower(),self.nx,self.tag)
    print('Write %s' % (tailname))
    f = open(tailname,'w')
    f.write('CELLS %d\n' % len(self.cells))
    for cell in self.cells:
      f.write('%d %f %f %f %f\n' % (cell[0],cell[1],cell[2],cell[3],cell[4]))
    f.write('CONNECTIONS %d\n' % len(self.connections))
    for con in self.connections:
      f.write('%d %d %f %f %f %f\n' % (con[0],con[1],con[2],con[3],con[4],con[5]))
    f.close()

  def write_paraview(self):
    '''Write .h5 and .xmf for Paraview visualization.'''
    h5name = self.basename+'-%s%i-%s.h5' % (
             self.shape.lower(),self.nx,self.tag)
    xmfname = self.basename+'-%s%i-%s.xmf' % (
              self.shape.lower(),self.nx,self.tag)
    #Assign cell corners (nodes/vertices) and write .h5 and .xmf for Paraview
    tid = []
    txc = []
    tyc = []
    tzc = []
    tvol = []
    tvertices = []
    telements = []
    npoint = 0
    icon = self.firstcon
    for i in range(self.firsttail,len(self.cells)):
      #number of cons in tail is the same as number of cells, so:
      side = self.connections[icon][5]/self.dx
      y1 = self.connections[icon][3] - side/2.
      y2 = y1 + side
      #not showing volume only area
      p1 = [self.connections[icon][2],y1,self.connections[icon][4]]
      p2 = [self.connections[icon][2]+self.dx,y1,self.connections[icon][4]]
      p3 = [self.connections[icon][2]+self.dx,y2,self.connections[icon][4]]
      p4 = [self.connections[icon][2],y2,self.connections[icon][4]]
      tvertices.extend([p1,p2,p3,p4])
      telements.extend([5,npoint,npoint+1,npoint+2,npoint+3])
      txc.append(self.cells[i][1])
      tyc.append(self.cells[i][2])
      tzc.append(self.cells[i][3])
      tvol.append(self.cells[i][4])
      tid.append(i+1) #pretty sure this captures the correct order
      npoint += 4
      icon += 1
    #open original domain file,read to arrays
    h5in = h5py.File(self.domainname,'r')
    Vertices = np.array(h5in['Domain/Vertices'])
    Cells = np.array(h5in['Domain/Cells'])
    Cell_Ids = np.array(h5in['Domain/Cell_Ids'])
    XC = np.array(h5in['Domain/XC'])
    YC = np.array(h5in['Domain/YC'])
    ZC = np.array(h5in['Domain/ZC'])
    Volume = np.array(h5in['Domain/Volume'])
    h5in.close()
    #add tail values to the end of all the arrays
    for i in range(len(telements)): #vertex ids in tail need to be upped
      if(i%5 != 0):  #don't change the 5s!
        telements[i] += len(Vertices)
    Vertices2 = np.append(arr=Vertices,axis=0,values=tvertices)
    Cells2 = np.append(arr=Cells,axis=0,values=telements)
    Cell_Ids2 = np.append(arr=Cell_Ids,axis=0,values=tid)
    XC2 = np.append(arr=XC,axis=0,values=txc)
    YC2 = np.append(arr=YC,axis=0,values=tyc)
    ZC2 = np.append(arr=ZC,axis=0,values=tzc)
    Volume2 = np.append(arr=Volume,axis=0,values=tvol)
    #then write with tails h5 file
    print('Write %s' % h5name)
    h5f = h5py.File(h5name,'w')
    h5f.create_dataset('Domain/Vertices',data=Vertices2)
    h5f.create_dataset('Domain/Cells',data=Cells2)
    h5f.create_dataset('Domain/Cell_Ids',data=Cell_Ids2)
    h5f.create_dataset('Domain/XC',data=XC2)
    h5f.create_dataset('Domain/YC',data=YC2)
    h5f.create_dataset('Domain/ZC',data=ZC2)
    h5f.create_dataset('Domain/Volume',data=Volume2)
    h5f.close()
    #and new xmf file pointing to tails h5 file
    print('Write %s' % xmfname)
    xmf = open(xmfname,'w')
    xmf.write(wxmf.header_template)
    xmf.write(wxmf.topo_template % (len(self.cells),len(Cells2),h5name.split('.')[0]))
    xmf.write(wxmf.geo_template % (len(Vertices2),h5name.split('.')[0]))
    xmf.write(wxmf.XYZ_template % ('XC','Scalar','Cell',len(XC2),
                                   h5name.split('.')[0],'Domain','XC'))
    xmf.write(wxmf.XYZ_template % ('YC','Scalar','Cell',len(YC2),
                                   h5name.split('.')[0],'Domain','YC'))
    xmf.write(wxmf.XYZ_template % ('ZC','Scalar','Cell',len(ZC2),
                                   h5name.split('.')[0],'Domain','ZC'))
    xmf.write(wxmf.XYZ_template % ('Volume','Scalar','Cell',len(Volume2),
                                   h5name.split('.')[0],'Domain','Volume'))
    xmf.write(wxmf.XYZ_template % ('Cell_Ids','Scalar','Cell',len(Cell_Ids2),
                                   h5name.split('.')[0],'Domain','Cell_Ids'))
    xmf.write(wxmf.tail_template)
    xmf.close()

class HalfVolumeTail(Tail):
  '''A tail in which half the volume of the 3D bulk cell is replaced with a
     discretized 1D tail'''

  def __init__(self):
    super(HalfVolumeTail,self).__init__()
    self.tag = 'hv'

  def calc_dx(self):
    '''Find radius and dx, saving half the volume of the bulk cell for itself.'''

    halfvol = self.totvol/2.
    sa = halfvol/self.length #S.A. of small face prism or cylinder
    if self.shape.upper() == 'CYLINDER':
      print('CYLINDER')
      self.r = np.sqrt(sa/np.pi)
    elif self.shape.upper() == 'PRISM':
      print('PRISM')
      self.r = np.sqrt(sa)/2.
    elif self.shape.upper() == 'SPHERE':
      print('SPHERE')
      self.r = np.cbrt(3./4.*halfvol/np.pi)
    nx_tail = self.nx - 1
    self.nhalf = 2*nx_tail
    self.dhalf = self.r/float(self.nhalf)
    self.dx = 2.*self.dhalf
    print('radius = %f' % self.r)
    print('dx = %f' % self.dx)

class WPVolTail(Tail):
  '''A tail in which the waste package volume is specified.'''

  def __init__(self,wpvol=13.88):
    '''Initialize variables.'''
    super(WPVolTail,self).__init__()
    self.tag = 'wp'
    self.wpvol = (5./3.)**2.*self.length
    self.r_wp = None      #radius of waste package

  def __repr__(self):
    '''This string recreates the object.'''
    return '{}(coord={},nx={},shape={}, \
               length={},wpvol={}, \
               ugename={}, \
               domainname={})'.format(self.__class__.__name__,
                                      self.coord,
                                      self.nx,
                                      self.shape,
                                      self.length,
                                      self.wpvol,
                                      self.ugename,
                                      self.domainname)

  def calc_dx(self):
    '''Find radius and dx, given the waste package volume.'''

    sa_wp = self.wpvol/self.length
    print('wpvol = {}'.format(self.wpvol))
    print('sa_wp = {}'.format(sa_wp))
    sa = self.totvol/self.length #S.A. of small face prism or cylinder
    if self.shape.upper() == 'CYLINDER':
      print('CYLINDER')
      self.r = np.sqrt(sa/np.pi)
      self.r_wp = np.sqrt(sa_wp/np.pi)
    elif self.shape.upper() == 'PRISM':
      print('PRISM')
      self.r = np.sqrt(sa)/2.
      self.r_wp = np.sqrt(sa_wp)/2.
      print('r_wp = {}'.format(self.r_wp))
    elif self.shape.upper() == 'SPHERE':
      print('SPHERE')
      self.r = np.cbrt(3./4.*self.totvol/np.pi)
      self.r_wp = np.cbrt(3./4.*self.wpvol/np.pi)
    nx_tail = self.nx - 1 
    self.nhalf = 2*nx_tail-1
    self.dhalf = (self.r-self.r_wp)/float(self.nhalf)
    self.dx = 2.*self.dhalf
    print('radius = %f' % self.r)
    print('dx = %f' % self.dx)

  def discretize_tail(self):
    '''Find xc,yc,zc,volume,surface area
    append to self.cells and self.connections'''
    tailvol = 0.
    self.firsttail = len(self.cells) #zero-indexed, not id number
    self.firstcon = len(self.connections)
    cellid = self.firsttail + 1
    print('CellID XCenter Volume TailVol CellID XConnection Area')
    print('self.nx = {}'.format(self.nx))
    for i in range(self.nx-1):
      print('i = {}'.format(i))
      if i == 0:
        xc = self.cells[self.bulk_id-1][1] + self.r
        rcon = self.r_wp
        print('rcon = {}'.format(rcon))
      else:
        xc = self.cells[self.bulk_id-1][1] + self.r-(self.r_wp+self.dhalf+self.dx*(i-1))
        rcon = self.r_wp + self.dx*i
      yc = self.cells[self.bulk_id-1][2]
      zc = self.cells[self.bulk_id-1][3]
      if self.shape.upper() == 'CYLINDER':
        vol = np.pi*(rcon**2.)*self.length - tailvol
      elif self.shape.upper() == 'PRISM':
        vol = ((2.*rcon)**2.*self.length) - tailvol
      elif self.shape.upper() == 'SPHERE':
        vol = 4./3.*np.pi*rcon**3.
      self.cells.append([cellid,xc,yc,zc,vol])
      xc = self.cells[self.bulk_id-1][1] + (self.r-rcon) #distance from body of tail
      yc = self.cells[self.bulk_id-1][2]
      zc = self.cells[self.bulk_id-1][3]
      if self.shape.upper() == 'CYLINDER':
        area = 2.*rcon*np.pi*self.length + np.pi*rcon**2.*2.
      elif self.shape.upper() == 'PRISM':
        area = 4.*self.length*(2.*rcon) + 2*(2.*rcon)**2.
      elif self.shape.upper() == 'SPHERE':
        area = 4.*np.pi*rcon**2.
      if i < self.nx-2: #connect to next tail cell
        self.connections.append([cellid,cellid+1,xc,yc,zc,area])
      else: #connect to parent
        self.connections.append([cellid,self.bulk_id,xc,yc,zc,area])
      cellid += 1
      tailvol += vol
      #print('%i %f %f %f %i %f %f' % (self.cells[cellid][0],self.cells[cellid][1],
      #                                self.cells[cellid][4],tailvol,
      #                                connections[self.firstcon+i][0],
      #                                connections[self.firstcon+i][2],
      #                                connections[self.firstcon+i][5]))
    #Modify volume of bulk 3D cell
    self.cells[self.bulk_id-1][4] -= tailvol

class WPVolHalfVolTail(WPVolTail):
  '''Make a tail that has specified waste package volume and
     half the volume of bulk cell saved for itself.'''

  def __init__(self):
    super(WPVolHalfVolTail,self).__init__()
    self.tag = 'wphv'

  def get_bulk_cell(self):
    '''Find volume and id of 3D bulk cell''' 
    super(WPVolHalfVolTail,self).get_bulk_cell()
    if self.wpvol > self.totvol/2.:
      print('wpvol > half volume - exiting')
      return #or sys.exit?

  def calc_dx(self):
    '''Find radius and dx, given the waste package volume.
       And saving half the bulk cell volume for itself.'''

    halfvol = self.totvol/2.
    sa = halfvol/self.length #S.A. of small face prism or cylinder
    sa_wp = self.wpvol/self.length
    if self.shape.upper() == 'CYLINDER':
      print('CYLINDER')
      self.r = np.sqrt(sa/np.pi)
      self.r_wp = np.sqrt(sa_wp/np.pi)
    elif self.shape.upper() == 'PRISM':
      print('PRISM')
      self.r = np.sqrt(sa)/2.
      self.r_wp = np.sqrt(sa_wp)/2.
    elif self.shape.upper() == 'SPHERE':
      print('SPHERE')
      self.r = np.cbrt(3./4.*self.totvol/np.pi)
      self.r_wp = np.cbrt(3./4.*self.wpvol/np.pi)
    nx_tail = self.nx - 2 
    self.dx = (self.r-self.r_wp)/float(nx_tail)
    self.dhalf = self.dx/2.
    #self.nhalf, don't think it's needed, don't think it should be self.
    print('radius = %f' % self.r)
    print('dx = %f' % self.dx)

class LayeredTail(Tail):
  '''Make a tail with specified layer thicknesses.'''

  def __init__(self,layers=[0.25,0.25,0.25,0.25],nx=[2,2,2,2]):
    if sum(layers) != 1.:
      print('Layer fractions must add to 1.')
      return
    if len(layers) != len(nx):
      print('Must supply same number of layer fractions and layer discretizations.')
      return
    super(LayeredTail,self).__init__()
    self.layers = layers #list of fractional radii
    self.nx = nx         #list of nx for each layer
    self.tag = 'layers'

  def __repr__(self):
    '''This string recreates the object.'''
    return '{}(coord={},nx={},shape={}, \
               length={},ugename={}, \
               domainname={},layers={})'.format(self.__class__.__name__,
                                      self.coord,
                                      self.nx,
                                      self.shape,
                                      self.length,
                                      self.ugename,
                                      self.domainname,
                                      self.layers)
  def calc_dx(self):
    '''Find radius of equivalent shape and discretization length (dx)
       for each layer.'''

    sa = self.totvol/self.length #S.A. of small face prism or cylinder
    if self.shape.upper() == 'CYLINDER':
      print('CYLINDER')
      self.r = np.sqrt(sa/np.pi)
    elif self.shape.upper() == 'PRISM':
      print('PRISM')
      self.r = np.sqrt(sa)/2.
    elif self.shape.upper() == 'SPHERE':
      print('SPHERE')
      self.r = np.cbrt(3./4.*self.totvol/np.pi)
    thickness = []
    self.dx = []
    for i in range(len(self.layers)):
      thickness.append(self.r*self.layers[i])
      self.dx.append(thickness[i]/self.nx[i])
    #these apply only to innermost layer
    self.nhalf = 2*self.nx[0]-1
    self.dhalf = thickness[0]/float(self.nhalf)
    self.dx[0] = 2.*self.dhalf
    print('radius = %f' % self.r)
    self.dx_expanded = []
    for i in range(len(self.dx)):
      for j in range(self.nx[i]):
        self.dx_expanded.append(self.dx[i])
    self.dx_expanded[0] = self.dhalf #? how is this used?
    print('dx_expanded = ',self.dx_expanded)

  def discretize_tail(self):
    '''Find xc,yc,zc,volume,surface area
    append to self.cells and self.connections'''
    tailvol = 0.
    self.firsttail = len(self.cells) #zero-indexed, not id number
    self.firstcon = len(self.connections)
    cellid = self.firsttail + 1
    print('CellID XCenter Volume TailVol CellID XConnection Area')
    #This doesn't really work, will fix later
    for j in range(len(self.nx)):
      for i in range(self.nx[j]):
        if j == 0:
          if i == 0:
            xc = self.cells[self.bulk_id-1][1] + self.r
            rcon = self.dhalf #?
          else:
            xc = self.cells[self.bulk_id-1][1] + self.r-(self.dx[j]*i)
            rcon += self.dx[j]
        else:
          if i == 0:
            xc -= (self.dx[j]+self.dx[j-1])/2.
          else:
            xc -= self.dx[j]
          rcon += self.dx[j]
        yc = self.cells[self.bulk_id-1][2]
        zc = self.cells[self.bulk_id-1][3]
        if self.shape.upper() == 'CYLINDER':
            vol = np.pi*(rcon**2.)*self.length - tailvol
        elif self.shape.upper() == 'PRISM':
          vol = ((2.*rcon)**2.*self.length) - tailvol
        elif self.shape.upper() == 'SPHERE':
          vol = 4./3.*np.pi*rcon**3.
        self.cells.append([cellid,xc,yc,zc,vol])
        xc = self.cells[self.bulk_id-1][1] + (self.r-rcon) #distance from body of tail
        yc = self.cells[self.bulk_id-1][2]
        zc = self.cells[self.bulk_id-1][3]
        if self.shape.upper() == 'CYLINDER':
          area = 2.*rcon*np.pi*self.length + np.pi*rcon**2.*2.
        elif self.shape.upper() == 'PRISM':
          area = 4.*self.length*(2.*rcon) + 2*(2.*rcon)**2.
        elif self.shape.upper() == 'SPHERE':
          area = 4.*np.pi*rcon**2.
        if j*i < sum(self.nx)-2: #connect to next tail cell
          self.connections.append([cellid,cellid+1,xc,yc,zc,area])
        else: #connect to parent
          self.connections.append([cellid,self.bulk_id,xc,yc,zc,area])
        cellid += 1
        tailvol += vol
      #Modify volume of bulk 3D cell
      self.cells[self.bulk_id-1][4] -= tailvol

  def write_uge(self):
    '''Write explicit grid file (.uge) with tail.'''
    tailname = self.basename+'-%s-%s.uge' % (self.shape.lower(),self.tag)
    print('Write %s' % (tailname))
    f = open(tailname,'w')
    f.write('CELLS %d\n' % len(self.cells))
    for cell in self.cells:
      f.write('%d %f %f %f %f\n' % (cell[0],cell[1],cell[2],cell[3],cell[4]))
    f.write('CONNECTIONS %d\n' % len(self.connections))
    for con in self.connections:
      f.write('%d %d %f %f %f %f\n' % (con[0],con[1],con[2],con[3],con[4],con[5]))
    f.close()

  def write_paraview(self):
    '''Write .h5 and .xmf for Paraview visualization.'''
    h5name = self.basename+'-%s-%s.h5' % (self.shape.lower(),self.tag)
    xmfname = self.basename+'-%s-%s.xmf' % (self.shape.lower(),self.tag)
    #Assign cell corners (nodes/vertices) and write .h5 and .xmf for Paraview
    tid = []
    txc = []
    tyc = []
    tzc = []
    tvol = []
    tvertices = []
    telements = []
    npoint = 0
    icon = self.firstcon
    counter = 0
    for i in range(self.firsttail,len(self.cells)):
      #number of cons in tail is the same as number of cells, so:
      side = self.connections[icon][5]/self.dx
      y1 = self.connections[icon][3] - side/2.
      y2 = y1 + side
      #not showing volume only area
      p1 = [self.connections[icon][2],y1,self.connections[icon][4]]
      p2 = [self.connections[icon][2]+self.dx_for_viz[counter],y1,self.connections[icon][4]]
      p3 = [self.connections[icon][2]+self.dx_for_viz[counter],y2,self.connections[icon][4]]
      p4 = [self.connections[icon][2],y2,self.connections[icon][4]]
      tvertices.extend([p1,p2,p3,p4])
      telements.extend([5,npoint,npoint+1,npoint+2,npoint+3])
      txc.append(self.cells[i][1])
      tyc.append(self.cells[i][2])
      tzc.append(self.cells[i][3])
      tvol.append(self.cells[i][4])
      tid.append(i+1) #pretty sure this captures the correct order
      npoint += 4
      icon += 1
      counter += 1
    #open original domain file,read to arrays
    h5in = h5py.File(self.domainname,'r')
    Vertices = np.array(h5in['Domain/Vertices'])
    Cells = np.array(h5in['Domain/Cells'])
    Cell_Ids = np.array(h5in['Domain/Cell_Ids'])
    XC = np.array(h5in['Domain/XC'])
    YC = np.array(h5in['Domain/YC'])
    ZC = np.array(h5in['Domain/ZC'])
    Volume = np.array(h5in['Domain/Volume'])
    h5in.close()
    #add tail values to the end of all the arrays
    for i in range(len(telements)): #vertex ids in tail need to be upped
      if(i%5 != 0):  #don't change the 5s!
        telements[i] += len(Vertices)
    Vertices2 = np.append(arr=Vertices,axis=0,values=tvertices)
    Cells2 = np.append(arr=Cells,axis=0,values=telements)
    Cell_Ids2 = np.append(arr=Cell_Ids,axis=0,values=tid)
    XC2 = np.append(arr=XC,axis=0,values=txc)
    YC2 = np.append(arr=YC,axis=0,values=tyc)
    ZC2 = np.append(arr=ZC,axis=0,values=tzc)
    Volume2 = np.append(arr=Volume,axis=0,values=tvol)
    #then write with tails h5 file
    print('Write %s' % h5name)
    h5f = h5py.File(h5name,'w')
    h5f.create_dataset('Domain/Vertices',data=Vertices2)
    h5f.create_dataset('Domain/Cells',data=Cells2)
    h5f.create_dataset('Domain/Cell_Ids',data=Cell_Ids2)
    h5f.create_dataset('Domain/XC',data=XC2)
    h5f.create_dataset('Domain/YC',data=YC2)
    h5f.create_dataset('Domain/ZC',data=ZC2)
    h5f.create_dataset('Domain/Volume',data=Volume2)
    h5f.close()
    #and new xmf file pointing to tails h5 file
    print('Write %s' % xmfname)
    xmf = open(xmfname,'w')
    xmf.write(wxmf.header_template)
    xmf.write(wxmf.topo_template % (len(self.cells),len(Cells2),h5name.split('.')[0]))
    xmf.write(wxmf.geo_template % (len(Vertices2),h5name.split('.')[0]))
    xmf.write(wxmf.XYZ_template % ('XC','Scalar','Cell',len(XC2),
                                   h5name.split('.')[0],'Domain','XC'))
    xmf.write(wxmf.XYZ_template % ('YC','Scalar','Cell',len(YC2),
                                   h5name.split('.')[0],'Domain','YC'))
    xmf.write(wxmf.XYZ_template % ('ZC','Scalar','Cell',len(ZC2),
                                   h5name.split('.')[0],'Domain','ZC'))
    xmf.write(wxmf.XYZ_template % ('Volume','Scalar','Cell',len(Volume2),
                                   h5name.split('.')[0],'Domain','Volume'))
    xmf.write(wxmf.XYZ_template % ('Cell_Ids','Scalar','Cell',len(Cell_Ids2),
                                   h5name.split('.')[0],'Domain','Cell_Ids'))
    xmf.write(wxmf.tail_template)
    xmf.close()

  
if __name__ == '__main__':
#  tail = Tail() #with default inputs for coord, nx, etc.
  tail = LayeredTail() #with default inputs for coord, nx, etc.
  tail.__str__()
  tail.__repr__()
  tail.read_input()
  tail.get_bulk_cell()
  tail.calc_dx()
  tail.discretize_tail()
  tail.write_uge()
  tail.write_paraview()


