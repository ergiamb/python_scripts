'''
modify checkpoint file produced by t = -5 to 0 run for restart at 0 y

Emily 3.3.17
'''

import numpy as np
from h5py import *
import wipp_uge as wipp

fnchpt = 'pflotran-restart0y.h5' #copy (and rename) this from -5 to 0 directory
#regions to reset
fnwas_area = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rWAS_AREA.txt'
fnreposit = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rREPOSIT.txt'
fnops_area = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rOPS_AREA.txt'
fnexp_area = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rEXP_AREA.txt'
fnconc_mon = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rCONC_MON.txt'
fnshftl = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rSHFTL.txt'
fnshftu = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rSHFTU.txt'
#need to reset permeability and porosity for all the regions in which
#material is going to change at t = 0. In addition to the above:
fnculebra = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rCULEBRA.txt'
fnfortynin = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rFORTYNIN.txt'
fndewylake = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rDEWYLAKE.txt'
fnsantaros = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rSANTAROS.txt'
fntamarisk = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rTAMARISK.txt'
fnmagenta = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rMAGENTA.txt'
fndrz = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rDRZ.txt'
fndrz_oe = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rDRZ_OE.txt'
fndrz_pcs = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rDRZ_PCS.txt'
fnpcs = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rPCS.txt'

wa_pres = 128039.
#these are all gas saturations
wa_sg = 1. - 0.015 #WAS_AREA, REPOSIT 0 y saturation
zero_sg = 1. - 0.001 #OPS_AREA, EXP_AREA 0 y are supposed to have Sw = 0
pcs_sg = 1. - 0.566419 #PCS sampled residual Sw

wa_por = 8.48e-1
wa_k = np.power(10.,-1.26198e1)
wa_sat = 0.015
rep_por = 8.48e-1
rep_k = np.power(10.,-1.26198e1)
ops_por = 0.18
ops_k = np.power(10.,-1.1e1)
exp_por = 0.18
exp_k = np.power(10.,-1.1e1)
cm_por = 5.e-2
cm_k = np.power(10.,-1.4e1)
sl_por = 1.13e-1
sl_k = np.power(10.,-18.806)
su_por = 2.91e-1
su_k = np.power(10.,-18.3667)
cul_por = 1.51e-1
cul_k = np.power(10.,-1.4018e1)
for_por = 8.2e-2
for_k = np.power(10.,-3.5e1)
dew_por = 1.43e-1
dew_k = np.power(10.,-1.63e1)
san_por = 1.75e-1
san_k = np.power(10.,-1.e1)
tam_por = 6.4e-2
tam_k = np.power(10.,-3.5e1)
mag_por = 1.38e-1
mag_k = np.power(10.,-1.4678e1)
drz_por = 7.3824e-3 #applies to DRZ_0, DRZ_OE_0, DRZ_PC_0
drz_k = np.power(10.,-1.7e1)
pcs_por = 0.1338
pcs_k = np.power(10.,-1.72537e1) #PCS_T1
 

#etc

h5 = File(fnchpt,'r+')
#Reset initial pressure and saturation, porosity and permeability 
icp_chpt = np.array(h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:],'=f8')
state = np.array(h5['Checkpoint/PMCSubsurface/flow/State'][:],'=f8')
por = np.array(h5['Checkpoint/PMCSubsurface/flow/Porosity'][:],'=f8')
permx = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_X'][:],'=f8')
permy = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_Y'][:],'=f8')
permz = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_Z'][:],'=f8')
f = open(fnwas_area,'r') #nonzero initial brine saturation
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wa_pres
  icp_chpt[index+1] = wa_sg
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = wa_por
  permx[int(line)-1] = wa_k
  permy[int(line)-1] = wa_k
  permz[int(line)-1] = wa_k
f.close()
f = open(fnreposit,'r') #nonzero initial brine saturation
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wa_pres
  icp_chpt[index+1] = wa_sg
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = rep_por
  permx[int(line)-1] = rep_k
  permy[int(line)-1] = rep_k
  permz[int(line)-1] = rep_k
f.close()
f = open(fnops_area,'r') #"zero" initial brine saturation
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wipp.REF_PRES
  icp_chpt[index+1] = wipp.gas_sat1
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = ops_por
  permx[int(line)-1] = ops_k
  permy[int(line)-1] = ops_k
  permz[int(line)-1] = ops_k
f.close()
f = open(fnexp_area,'r') #"zero" initial brine saturation
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wipp.REF_PRES
  icp_chpt[index+1] = wipp.gas_sat1
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = exp_por
  permx[int(line)-1] = exp_k
  permy[int(line)-1] = exp_k
  permz[int(line)-1] = exp_k
f.close()
f = open(fnconc_mon,'r') #initial brine saturation of 1, with atm P
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wipp.REF_PRES
  icp_chpt[index+1] = wipp.molefrac
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 1
  por[int(line)-1] = cm_por
  permx[int(line)-1] = cm_k
  permy[int(line)-1] = cm_k
  permz[int(line)-1] = cm_k
f.close()
f = open(fnshftl,'r') #initial brine saturation of 1, with atm P
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wipp.REF_PRES
  icp_chpt[index+1] = wipp.molefrac
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 1
  por[int(line)-1] = sl_por
  permx[int(line)-1] = sl_k
  permy[int(line)-1] = sl_k
  permz[int(line)-1] = sl_k
f.close()
f = open(fnshftu,'r') #initial brine saturation of 1, with atm P
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wipp.REF_PRES
  icp_chpt[index+1] = wipp.molefrac
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 1
  por[int(line)-1] = su_por
  permx[int(line)-1] = su_k
  permy[int(line)-1] = su_k
  permz[int(line)-1] = su_k
f.close()
#modify only porosity and permeability
f = open(fnculebra,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = wa_por
  permx[int(line)-1] = cul_k
  permy[int(line)-1] = cul_k
  permz[int(line)-1] = cul_k
f.close()
f = open(fnfortynin,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = for_por
  permx[int(line)-1] = for_k
  permy[int(line)-1] = for_k
  permz[int(line)-1] = for_k
f.close()
f = open(fndewylake,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = dew_por
  permx[int(line)-1] = dew_k
  permy[int(line)-1] = dew_k
  permz[int(line)-1] = dew_k
f.close()
f = open(fnsantaros,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = san_por
  permx[int(line)-1] = san_k
  permy[int(line)-1] = san_k
  permz[int(line)-1] = san_k
f.close()
f = open(fntamarisk,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = tam_por
  permx[int(line)-1] = tam_k
  permy[int(line)-1] = tam_k
  permz[int(line)-1] = tam_k
f.close()
f = open(fnmagenta,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = mag_por
  permx[int(line)-1] = mag_k
  permy[int(line)-1] = mag_k
  permz[int(line)-1] = mag_k
f.close()
f = open(fndrz,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = drz_por
  permx[int(line)-1] = drz_k
  permy[int(line)-1] = drz_k
  permz[int(line)-1] = drz_k
f.close()
f = open(fndrz_oe,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = drz_por
  permx[int(line)-1] = drz_k
  permy[int(line)-1] = drz_k
  permz[int(line)-1] = drz_k
f.close()
f = open(fndrz_pcs,'r')
for line in f:
  index = (int(line)-1)*3
  por[int(line)-1] = drz_por
  permx[int(line)-1] = drz_k
  permy[int(line)-1] = drz_k
  permz[int(line)-1] = drz_k
f.close()
f = open(fnpcs,'r') #non-zero initial brine saturation
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = wipp.REF_PRES
  icp_chpt[index+1] = pcs_sg
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = pcs_por
  permx[int(line)-1] = pcs_k
  permy[int(line)-1] = pcs_k
  permz[int(line)-1] = pcs_k
f.close()

h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:] = icp_chpt
h5['Checkpoint/PMCSubsurface/flow/State'][:] = state
h5['Checkpoint/PMCSubsurface/flow/Porosity'][:] = por
h5['Checkpoint/PMCSubsurface/flow/Permeability_X'][:] = permx
h5['Checkpoint/PMCSubsurface/flow/Permeability_Y'][:] = permy
h5['Checkpoint/PMCSubsurface/flow/Permeability_Z'][:] = permz
h5.close()



