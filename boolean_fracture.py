#read in FCM permeability file and assign 0 or 1 to each cell based on permX
# 0 if permX = background k; 1 if permX > background k

nx = 201
ny = 135
nz = 84
background = '0'

nval = nx*ny*nz
perm_X = []
counter = 0

fin = file('mapELLIPSES.txt',mode='r')
fin.readline() #throw away the first line, it only says "perm"

filename = 'mapELLIPSES_bool.inp' #because connect looks for .inp
fout = file(filename,mode='w')

for i in range(nval):
  line = fin.readline()
  #print line.split()[3]
  if line.split()[3] != background:
    perm_X.append(1)
    counter += 1
  else:
    perm_X.append(0)
  fout.write('%i\n' %(perm_X[i]))

print counter
fin.close()
fout.close()


