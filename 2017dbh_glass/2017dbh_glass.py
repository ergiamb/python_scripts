#started with /2016dbh_safetycase/ave_heat/inst_split.py
#eliminate heat stacking features. See /2016dbh_safetycase/ave_heat/hvt_split.py if want them back
#add write functions, as in other recent domain set-up scripts
#make regions and obs points for 2017dbh_glass
#Emily 12.2.16

import numpy as np
import wfg_block as wfg 

#some names:
mat = ['wp_hlw','steel','cement','bentonite','ballast','brine','granite','granite_drz']
flow = ['initial','savannah_river_glass']
trans = ['initial']
glassbins = [('savannah_river_glass',200,0.7)] #keep structure from DWR, add another glass if want

#some numbers:
xwest = 0.
xeast = 9.23827e2
ysouth = 0.
ynorth = 1.
zbase = 0.
ztop = 2990.

n_wp = 10 #number of wp in an interval
n_int = 20 #number of intervals
z1 = zbase + 1000.0 #base of disposal zone
z_il = 0.7 #ht of impact limiter
z_wp = 3.0 #ht of waste package
half = z_wp/2. #to find center of waste package
z_st = 1.0 #ht of impact limiter + fishing neck
z_fn = 0.3 #ht of fishing neck
z_cement = 10. #length of cement plug
z_scement = 100.
z_bentonite = 50.
z_ballast = 50.
r_wp = 0.37
r_bh = 0.46
r_drz = 0.956 #I should make this script write the grid block, too
#z increments for writing obs points in seal zone
seal_obs = [2.5, 12.5, 27.5, 52.5, 102.5, 127.5, 152.5, 202.5, 252.5]

rfile = file('region.txt','w')
ssfile = file('ss.txt', 'w')
sfile = file('strata.txt','w')
obsfile = file('obs_points.txt','w')
obsregf = file('obs_region.txt','w')
wfgfile = file('wfg.txt','w')
wfg.drepo_to_file(wfgfile) #this could be changed!

#files need to be open already, and closed afterward!
def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

def write_obs(obsname,x,y,z,obsfile=obsfile,obsregf=obsregf):
  obsfile.write('OBSERVATION\n  REGION %s\n  VELOCITY\nEND\n\n'
                % (obsname))
  obsregf.write('REGION %s\n  COORDINATE %f %f %f\nEND\n\n'
                % (obsname,x,y,z))
  return

#write non-looping regions
write_reg('all',xwest,ysouth,zbase,xeast,ynorth,ztop)
write_strata('all',mat[6])
write_reg_face('top',xwest,ysouth,ztop,xeast,ynorth,ztop)
write_reg_face('bottom',xwest,ysouth,zbase,xeast,ynorth,zbase)
write_reg_face('east',xeast,ysouth,zbase,xeast,ynorth,ztop)
write_reg('drz',xwest,ysouth,z1,r_drz,ynorth,ztop)
write_strata('drz',mat[7])
write_reg('annulus',xwest,ysouth,z1,r_bh,ynorth,ztop)
write_strata('annulus',mat[5])
write_reg('impact_limiter',xwest,ysouth,z1,r_wp,ynorth,z1+z_il)
write_strata('impact_limiter',mat[1])
z = z1 + z_il

#write waste package regions alternating with impact limiter/fishing neck regions
for i in range(int(n_wp*n_int)):
#write regions, etc. which depend on position in space
  if (i != 0 and i%n_wp == 0 and i != n_wp*n_int): #i will never equal n_wp*n_int!
    write_reg('cement_plug'+str(i),xwest,ysouth,z,r_bh,ynorth,z+z_cement)
    write_strata('cement_plug'+str(i),mat[2])
    write_reg('impact_limiter'+str(i),xwest,ysouth,z+z_cement,r_wp,ynorth,z+z_cement+z_il)
    write_strata('impact_limiter'+str(i),mat[1])
    z += (z_cement+z_il)
  #write waste package region
  write_reg('wp'+str(i),xwest,ysouth,z,r_wp,ynorth,z+z_wp)
  write_strata('wp'+str(i),mat[0])
  write_ss('wp'+str(i),flow[1],trans[0],'wp'+str(i))
  wfg.homog_glass_to_file(wfgfile,r_wp/2.,ynorth/2.,z+half,glassbins[0][2],glassbins[0][0])
  #then decide whether to write a fishing neck or a complete steel region
  if ((i+1)%n_wp == 0): #if we're just below a cement plug or it's the last wp
    write_reg('fishing_neck'+str(i),xwest,ysouth,z+z_wp,r_wp,ynorth,z+z_wp+z_fn)
    write_strata('fishing_neck'+str(i),mat[1])
    delta_z = z_wp+z_fn
  else: #a waste package somewhere in the middle of an interval
    write_reg('steel'+str(i),xwest,ysouth,z+z_wp,r_wp,ynorth,z+z_wp+z_st)
    write_strata('steel'+str(i),mat[1])
    delta_z = z_wp+z_st
  zcenter = z + half
  if (i == 0 or i == n_wp*n_int/2 or i == n_wp*n_int-1):
    write_obs('wp_obs'+str(i),r_wp/2.,ynorth/2.,zcenter)
    write_obs('annulus'+str(i),(r_wp+r_bh)/2.,ynorth/2.,zcenter)
    write_obs('drz'+str(i),r_bh+0.025,ynorth/2.,zcenter)
    write_obs('1m'+str(i),1.0,ynorth/2.,zcenter)
  z += delta_z

#write seal regions
# z = top of last waste package, so we are good to go.
zseal = z
write_reg('scement1',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement1',mat[2])
z += z_scement
write_reg('bentonite1',xwest,ysouth,z,r_bh,ynorth,z+z_bentonite)
write_strata('bentonite1',mat[3])
z += z_bentonite
write_reg('scement2',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement2',mat[2])
z += z_scement
write_reg('ballast1',xwest,ysouth,z,r_bh,ynorth,z+z_ballast)
write_strata('ballast1',mat[4])
z += z_ballast
write_reg('scement3',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement3',mat[2])
z += z_scement
write_reg('bentonite2',xwest,ysouth,z,r_bh,ynorth,z+z_bentonite)
write_strata('bentonite2',mat[3])
z += z_bentonite
write_reg('scement4',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement4',mat[2])
z += z_scement
write_reg('ballast2',xwest,ysouth,z,r_bh,ynorth,z+z_ballast)
write_strata('ballast2',mat[4])
z += z_ballast
write_reg('scement5',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement5',mat[2])
z += z_scement
write_reg('bentonite3',xwest,ysouth,z,r_bh,ynorth,z+z_bentonite)
write_strata('bentonite3',mat[3])
z += z_bentonite
write_reg('scement6',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement6',mat[2])
z += z_scement
write_reg('ballast3',xwest,ysouth,z,r_bh,ynorth,z+z_ballast)
write_strata('ballast3',mat[4])
z += z_ballast
write_reg('scement7',xwest,ysouth,z,r_bh,ynorth,z+z_scement)
write_strata('scement7',mat[2])
z += z_scement

#add an obs point above at base of seal region
for i in range(len(seal_obs)):
  zcenter = zseal + seal_obs[i]
  write_obs('wp_seal'+str(i),r_wp/2.,ynorth/2.,zcenter)
  write_obs('annulus_seal'+str(i),(r_wp+r_bh)/2.,ynorth/2.,zcenter)
  write_obs('drz_seal'+str(i),r_bh+0.025,ynorth/2.,zcenter)
  write_obs('1m_seal'+str(i),1.0,ynorth/2.,zcenter)

rfile.close()
ssfile.close()
sfile.close()
obsfile.close()
obsregf.close()
wfgfile.close()

