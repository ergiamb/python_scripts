import numpy as np
import matplotlib.pyplot as plt

r_l = 20. # lower cut off
print 'Lower Cut Off:', r_l
r_u = 500. # upper cut off
print 'Uppper Cut Off:', r_u
alpha = 2.5 # exponent
print 'Exponent:', alpha 

N = 6400 # number of samples
print 'Number of Samples:', N
u = np.random.uniform(0,1,N) # N samples from uniform distribution on (0,1)

# use inverse of powerlaw cdf to get truncated power law samples
#def f (x): return r_u*(1 - x + x*(r_u/r_l)**alpha)**(-1.0/alpha) 
def f (x): return r_l*(1 - x + x*(r_l/r_u)**alpha)**(-1.0/alpha) 
r = f(u)

#find fracture area
atot = sum(np.square(r)*np.pi)
print 'Total fracture area:', atot
print 'P32 if 1 km3:', atot/1.e9

#Make a histogram!
plt.hist(r, bins= 100)
plt.show()


