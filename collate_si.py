'''
Read dakota stdout from gaussian process calculation of sobol indices,
or from pce calculation of sobol indices,
or from postrun analysis of correlation coefficients
and create pandas DataFrame for each Main effects and Total effects,
or Simple, Partial, Simple Rank, and Partial Rank correlation coefficients.
DataFrame is a table with rows labeled by "index" (time or response_fn, etc), and
"columns" labeled by input parameters. Each column is a "series" - assuming
that response_fns create a times series

range() in python3 = xrange() in python2

Emily
01.20.18
'''

import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import argparse
import sys

print(mpl.get_configdir())
mpl.style.use('presentation')

def postrun_collate(infilename,times):
  '''Pull correlation coefficients from standard Dakota post run processing:
     Simple, Partial, Simple Rank, Partial Rank
  '''
  sfilename = infilename.split('.')[0] + '_simple.txt'
  pfilename = infilename.split('.')[0] + '_partial.txt'
  srfilename = infilename.split('.')[0] + '_simple_rank.txt'
  prfilename = infilename.split('.')[0] + '_partial_rank.txt'
  print("Input file: {}\nOutput:\n{}\n{}\n{}\n{}".format(infilename,
        sfilename,pfilename,srfilename,prfilename))

  params = [] #hold input parameter names
  with open(infilename,'r') as f:
    while(True):
      line = f.readline()
      if 'descriptors' in line:
        params.extend([w.strip("'") for w in line.split()[1:]])
      elif 'response_functions' in line:
        nparams = len(params)
        print('{} parameters are {}'.format(len(params),' '.join(x for x in params)))
        n_fn = int(line.split()[2])
        print('Number of response functions is {}'.format(n_fn))
        if times == False:
          times = [x+1 for x in range(n_fn)]
        elif len(times) != n_fn:
          print("ERROR: Number of times {} != number of response functions.".format(
                len(times)))
          sys.exit()
  #"Sample moment statistics" - skip for now because uq not sa
  #"95% confidence intervals" - skip for now because uq not sa
      elif line.startswith('Simple Correlation Matrix'):
        #setup pandas DataFrames
        simple = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        simple = pd.DataFrame(data=simple,
                              index = times,
                              columns = params)
        #get column headers
        header = f.readline().split() #first nparams are input parameter names
        #throw away rows holding correlation coeff for input params
        for i in range(nparams):
          f.readline()
        #read simple correlation coeff for response fns
        print('Reading Simple Correlation Coefficients')
        for i in range(n_fn):
          line = f.readline()
          if 'response_fn' in line: #should be true
            w = line.split()
            for j in range(nparams):
              simple.loc[times[i], header[j]] = float(w[j+1])
      elif line.startswith('Partial Correlation Matrix'):
        #setup pandas DataFrames
        partial = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        partial = pd.DataFrame(data=partial,
                              index = times,
                              columns = params)
        #throw away column headers - which are the response functions
        f.readline()
        #read partial correlation coeff
        for j in range(nparams):
          line = f.readline()
          w = line.split()
          print('Reading {} Partial Correlation Coefficients'.format(w[0]))
          for i in range(n_fn):
            partial.loc[times[i],w[0]] = float(w[i+1])
      elif line.startswith('Simple Rank Correlation Matrix'):
        simple_rank = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        simple_rank = pd.DataFrame(data=simple_rank,
                              index = times,
                              columns = params)
        #get column headers
        header = f.readline().split() #first nparams are input parameter names
        #throw away rows holding correlation coeff for input params
        for i in range(nparams):
          f.readline()
        #read simple rank correlation coeff for response fns
        print('Reading Simple Rank Correlation Coefficients')
        for i in range(n_fn):
          line = f.readline()
          if 'response_fn' in line: #should be true
            w = line.split() #w[0] = 'response_fn_#'
            for j in range(nparams):
              simple_rank.loc[times[i], header[j]] = float(w[j+1])
      elif line.startswith('Partial Rank Correlation Matrix'):
        #setup pandas DataFrames
        partial_rank = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        partial_rank = pd.DataFrame(data=partial_rank,
                              index = times,
                              columns = params)
        #throw away column headers - which are the response functions
        f.readline()
        #read partial correlation coeff
        for j in range(nparams):
          line = f.readline()
          w = line.split()
          print('Reading {} Partial Rank Correlation Matrix'.format(w[0]))
          for i in range(n_fn):
            partial_rank.loc[times[i],w[0]] = float(w[i+1])
      elif line == '':
        print('File finished')
        break 

  #R^2 is not exactly a concept for partial, but sum squares anyway
  simple['R2'] = simple.pow(2.).sum(axis=1)
  partial['R2'] = partial.pow(2.).sum(axis=1)
  simple_rank['R2'] = simple_rank.pow(2.).sum(axis=1)
  partial_rank['R2'] = partial_rank.pow(2.).sum(axis=1)
  simple.to_csv(sfilename,sep=' ',index_label='Time_[y]')
  partial.to_csv(pfilename,sep=' ',index_label='Time_[y]')
  simple_rank.to_csv(srfilename,sep=' ',index_label='Time_[y]')
  partial_rank.to_csv(prfilename,sep=' ',index_label='Time_[y]')
     
  return(simple,partial,simple_rank,partial_rank)
       

def gp_collate(infilename,times):
  '''Pull SI from dakota gaussian process stdout
  '''
  mainfilename = infilename.split('.')[0] + '_main.txt'
  totalfilename = infilename.split('.')[0] + '_total.txt'
  print("Infile: {} Output Main SI: {} Output Total SI".format(
                           infilename,mainfilename,totalfilename))

  params = [] #hold input parameter names
  with open(infilename,'r') as f:
    while(True):
      line = f.readline()
      #print(line)
      if 'descriptors' in line:
        params.extend([w.strip("'") for w in line.split()[1:]])
      elif 'response_function'in line:
        print('{} parameters are {}'.format(len(params),' '.join(x for x in params)))
        n_fn = int(line.split()[2])
        print('Number of response functions is {}'.format(n_fn))
        if times == False:
          times = [x+1 for x in range(n_fn)]
        elif len(times) != n_fn:
          print("ERROR: Number of times {} != number of response functions.".format(
                len(times)))
          sys.exit()
      elif line.startswith('Global'): #found the Sobol' indices
        #setup pandas DataFrames
        main = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        main = pd.DataFrame(data=main,
                            index=times,
                            columns = params)
        total = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        total = pd.DataFrame(data=total,
                            index=times,
                            columns = params)
        for i in range(n_fn):
          print(i)
          line = f.readline()
          if 'response_fn_' in line: #should be true
            index = times[i] #int(line.split()[0].split('_')[2])
            print('Reading Response Function {}'.format(index))
            line = f.readline() #throw away line Main Total
          try: #see if there are Si values
            line = f.readline()
            w = line.split()
            main.loc[times[i],w[2]] = float(w[0])
            total.loc[times[i],w[2]] = float(w[1])
            for p in range(len(params)-1):
              w = f.readline().split()
              main.loc[times[i],w[2]] = float(w[0])
              total.loc[times[i],w[2]] = float(w[1])
          except ValueError: #found next response function
            continue #this works by sheer luck not logic
          except:
            print('Unexpected error - Stopping.')
            return
      elif line == '':
        print('File finished')
        break 
  
  main['R2'] = main.sum(axis=1) #this may not really be R2 but I need to find it by that name
  total['R2'] = total.sum(axis=1)
  main.to_csv(mainfilename,sep=' ',index_label='Time_[y]')
  total.to_csv(totalfilename,sep=' ',index_label='Time_[y]')
     
  return(main,total)

def pce_collate(infilename,times,interactions=None):
  '''Pull SI from dakota pce standard out. Save them to pandas DataFrames
     Option to save desired interaction terms, specified w/ list "interactions".
  '''
  mainfilename = infilename.split('.')[0] + '_main.txt'
  totalfilename = infilename.split('.')[0] + '_total.txt'
  print("Infile: {} Output Main SI: {} Output Total SI".format(
                           infilename,mainfilename,totalfilename))
  if interactions:
    interfilename = infilename.split('.')[0] + '_inter.txt'
    print("Output Interaction SI: {}".format(interfilename))

  exception = False #haven't run into exception yet
  params = [] #hold input parameter names
  with open(infilename,'r') as f:
    while(True):
      line = f.readline()
      #print(line)
      if 'descriptors' in line:
        params.extend([w.strip("'") for w in line.split()[1:]])
      elif 'response_function'in line:
        print('{} parameters are {}'.format(len(params),' '.join(x for x in params)))
        n_fn = int(line.split()[2])
        print('Number of response functions is {}'.format(n_fn))
        if times == False:
          times = [x+1 for x in range(n_fn)]
        elif len(times) != n_fn:
          print("ERROR: Number of times {} != number of response functions.".format(
                len(times)))
          sys.exit()
      elif line.startswith('Global'): #found the Sobol' indices
        main = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        main = pd.DataFrame(data=main,
                            index = times,
                            columns = params
                            )
        total = np.full((n_fn,len(params)),np.nan,dtype=np.float64)
        total = pd.DataFrame(data=total,
                            index = times,
                            columns = params
                            )
        if interactions:
          inter = np.full((n_fn,len(interactions)),np.nan,dtype=np.float64)
          inter = pd.DataFrame(data=inter,
                              index = times,
                              columns = interactions
                              )
        for i in range(n_fn):
          print(i)
          if not exception: line = f.readline()
          if 'not available' in line:
            print('No SI for {}, t = {}'.format(line.split()[0],times[i]))
          elif 'indices:' in line:
            print('Reading {}, t = {}'.format(line.split()[0],times[i]))
            line = f.readline() #throw away line Main Total
            for p in range(len(params)):
              w = f.readline().split()
              main.loc[times[i],w[2]] = float(w[0])
              total.loc[times[i],w[2]] = float(w[1])
            line = f.readline() #throw away line Interaction
            while(True):
              line = f.readline()
              w = line.split()
              try:
                interaction_si = float(w[0])
                if interactions: #requires knowledge of interaction list in stdout
                  this_interaction = '_'.join(w[1:])
                  if this_interaction in interactions:
                    inter.loc[times[i],this_interaction] = interaction_si
              except ValueError:
                print('found ValueError')
                exception = True
                break
              except:
                print('Unexpected error - Stopping.')
                return
      elif line == '':
        print('File finished')
        break 
  
  main['R2'] = main.sum(axis=1) #need to find this later by name R2
  total['R2'] = total.sum(axis=1)
  #print(main)
  #print(total)
  main.to_csv(mainfilename,sep=' ',index_label='Time_[y]')
  total.to_csv(totalfilename,sep=' ',index_label='Time_[y]')
  if interactions:
    inter['R2'] = inter.sum(axis=1) #I guess this has some kind of validity
    print(inter)
    inter.to_csv(interfilename,sep=' ',index_label='Time_[y]')
    return(main,total,inter)
  else:
    return(main,total,)
     
def plot_sa(sa_df,sa_type,infilename,logtime,nskip):
  '''Plot sensitivity indices  or correlation coefficients 
     for all params vs. response_fns in a pd.DataFrame
     sa_df = pandas dataframe
     sa_type = string to be used in plot title, and to set axis label. Choose:
        'Main', 'Total' for axis label 'Sensitivity Index' 
        'Simple', 'Partial', 'Simple Rank', Partial Rank' for axis 'Correlation Coefficient' 
     infilename = name of original input file (e.g., 'mc1024_gp1.stdout')
  '''
  outfilename = '{}_{}.png'.format(infilename.split('.')[0],sa_type.replace(' ','_'))
  if nskip > 10:
    colors = ['C0','C1','C2','C3','C4','C5','C6','C7','C8','C9',
              'indigo','purple','blue','cornflowerblue','limegreen',
              'gold','darkorange','chocolate','mediumvioletred','hotpink',
              'slategrey','orchid','olivedrab',]
  else:
    colors = ['C0','C1','C2','C3','C4','C5','C6','C7','C8','C9',]
  if logtime == True: 
    sa_df.drop('R2',axis=1).plot.line(logx=True,color=colors,figsize=(12,10))
    plt.xticks = [1,10,100,1000,10000,1.e5,1.e6,1.e7]
  elif logtime == False:
    sa_df.drop('R2',axis=1).plot.line(color=colors,figsize=(12,10))
    #sa_df.plot(xticks=[x for x in range(1,len(sa_df.index)+1)])
  if(sa_type.lower() == 'main' or sa_type.lower() == 'total' or sa_type.lower() == 'interaction'):
    plt.title('{} SI, {}'.format(sa_type,infilename.split('.')[0]))
    plt.xlabel('Time [y]')
    plt.ylabel('{} Sensitivity Index'.format(sa_type))
    plt.ylim(-.05,1.05)
  elif(sa_type.lower() == 'simple' or sa_type.lower() == 'partial' or
       sa_type.lower() == 'simple rank' or sa_type.lower() == 'partial rank'):
    plt.title('{} CC, {}'.format(sa_type,infilename.split('.')[0]))
    plt.xlabel('Time [y]')
    plt.ylabel('{} Correlation Coefficient'.format(sa_type))
    plt.ylim(-1.05,1.05)
  else:
    plt.title('{}, {}'.format(sa_type,infilename.split('.')[0]))
    plt.xlabel('Time [y]')
    plt.ylabel('{}'.format(sa_type))
  plt.setp(plt.gca().get_legend().get_texts(),fontsize='x-large')
  plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
  plt.savefig(outfilename)
  plt.show()
  
  return

def read_times(infilename,nskip):
  '''Possibly fragile function to read response function labels from
     dakota_tabular.dat
  '''
  try:
    with open(infilename,'r') as f:
      times = f.readline().split()[nskip:]
  except IOError:
    print("ERROR: Can't find {}".format(infilename))
    sys.exit()
  for i in range(len(times)):
    times[i] = float(times[i].strip())

  return (times)

def horsetails(infilename,nskip):
  '''Plot response functions as horsetails from dakota_tabular.dat
  '''
  df = pd.read_table(infilename,delim_whitespace=True,index_col=0,)
  df = df.T
  df.plot()
  

#############################################
if __name__ == "__main__":

  parser = argparse.ArgumentParser(
    description='Pull corelation coefficients or sensitivity indices from ' +
                'Dakota screen output and tabulate in pandas DataFrame. ' +
                'Dump to text file.')
  parser.add_argument('-t','--analysis_type',
                      help='type of analysis (gp, pce, or postrun)',
                      required=True,)
  parser.add_argument('-f','--infilename',
                      help='name of the file to be read',
                      required=True,)
  parser.add_argument('-i','--indexfile',
                      help='optional -  name of dakota_tabular.dat type file ' +
                           'containing index labels (such as times) for dataframe. ' +
                           '[default = None]',
                      default=None,
                      required=False,)
  parser.add_argument('-s','--nskip',
                      help='optional - number of column headings to skip in ' +
                           'indexfile when reading index labels. ' +
                           '[default = 0, which is guaranteed to be wrong]',
                      default = 0,
                      required=False,)
  parser.add_argument('-l','--log_time',
                      help='True/False use log scale on the x (time?) axis ' +
                           '[default = True]',
                      default = True,
                      required = False,)
  parser.add_argument('-a','--interactions',
                      help='Name of text file containing list of parameter ' +
                           'interactions to process. List names as appear ' +
                           'in Dakota screen output. Only works for pce. ' +
                           '[default = None]',
                      default = None,
                      required = False,)
  args = parser.parse_args()

  analysis_type = args.analysis_type.lower()
  infilename = args.infilename
  nskip = int(args.nskip)
  if args.log_time == 'False':
    log_time = False
  else:
    log_time = True
  if args.indexfile != None:
    index_list = read_times(args.indexfile,nskip)
  else:
    index_list = False
  if args.interactions != None:
    if analysis_type != 'pce':
      print('WARNING: Analysis type is not pce; interactions will not be used.')
    else:
      with open(args.interactions,'r') as f:
        interactions = f.read().split()
        print('Will plot higher order SI for these interactions: \n{}'.format(interactions))
  else:
    interactions = args.interactions #(= None)
 
  #print(infilename)
  print(log_time)

  if analysis_type == 'pce' and interactions == None:
    main,total = pce_collate(infilename,index_list,interactions)
    plot_sa(main,'Main',infilename,log_time,nskip)
    plot_sa(total,'Total',infilename,log_time,nskip)
  elif analysis_type == 'pce' and interactions != None:
    main,total,inter = pce_collate(infilename,index_list,interactions)
    plot_sa(main,'Main',infilename,log_time,nskip)
    plot_sa(total,'Total',infilename,log_time,nskip)
    plot_sa(inter,'Interaction',infilename,log_time,nskip)
  elif analysis_type == 'gp':
    main,total = gp_collate(infilename,index_list)
    plot_sa(main,'Main',infilename,log_time,nskip)
    plot_sa(total,'Total',infilename,log_time,nskip)
  elif analysis_type == 'postrun':
    simple,partial,simple_rank,partial_rank = postrun_collate(infilename,index_list)
    plot_sa(simple,'Simple',infilename,log_time,nskip)
    plot_sa(partial,'Partial',infilename,log_time,nskip)
    plot_sa(simple_rank,'Simple Rank',infilename,log_time,nskip)
    plot_sa(partial_rank,'Partial Rank',infilename,log_time,nskip)
  else:
    print("ERROR: unrecognized analysis type")
    print("Exiting.")
    sys.exit()

  
    
