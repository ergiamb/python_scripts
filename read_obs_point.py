# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 15:06:16 2012
@author: wpgardn
modified by Emily 7.14.16
"""

import string
from numpy import loadtxt

def SplitPflotranObsPtMultipt(obsptfile='observation-4.tec',numfields=12):
    '''
    Reads in pflotran-obs-*.tec with multiple observation points and writes one
    obs file for each observation point.  
    obsptfile = string, name of the file to split
    numfields = int, number of data fields per observation point
    numfields is used to calculate numpoint = number of obs pts in the file
    '''
    ################################################################################
    # read in the output observation point
    ################################################################################    
    f = file(obsptfile,'r');
    ll = f.readlines();
    f.close();
    #first we need to deal with that nast ass header
    header = ll[0];
    llh = header.split(',');
    numpoint = (len(llh)-1)/numfields;
    llh_dict = {}
    lldict = {}
    for i in xrange(numpoint):
        istart = 1+i*numfields 
        iend = 1+(i+1)*numfields
        llh_dict[i]=[llh[0]] + llh[istart:iend]
        ll_i = []
        ll_i.append(string.join(llh_dict[i],sep=',')+'\n')
        lldict[i] = ll_i
    for line in ll[1::]:
        lli=line.split();
        for i in xrange(numpoint):
            istart = 1+i*numfields 
            iend = 1+(i+1)*numfields
            lldict[i].append(string.join([lli[0]]+lli[istart:iend])+'\n')
    for i in xrange(numpoint):
        hh = obsptfile.split('.')[0]+'_'+str(i)+'.tec';
        f = file(hh,'w')
        f.writelines(lldict[i])
        f.close()
    return numpoint    

def ReadPflotranObsPt(obsptfile='observation-4.tec'):
    '''reads in the observation point file from plotran and returns a numpy rec array.  
   With names given by the header in observation point file.'''
    ################################################################################
    # read in the output observation point
    ################################################################################    
    f = file(obsptfile,'r');
    ll = f.readlines();
    f.close();
    #first we need to deal with that nast ass header
    header = ll[0];
    llh = header.split(',');
    fm = [];
    ll2 = [];
    ll2.append(ll[0]);
    
    for line in ll[1::]:
        j=0;
        li = line.split(); 
        for field in li:
            if len(field.split('E'))==1:
                #set_trace();
                if len(field.split('-'))==2:
                    field = field.split('-')[0][0:-1] + 'E-' + field.split('-')[1];
                if len(field.split('+'))==2:
                    field = field.split('+')[0][0:-1] +'E+' + field.split('+')[1];    
                li[j]=field;
            j=j+1;
        line = string.join(li);
        line = line+'\n'
        ll2.append(line);
    manglefile = 'observation_pg.tec'
    f = file(manglefile,'w');
    f.writelines(ll2);
    f.close();
    #make the names and formats to define the rec array
    #'Annual' and 'Model' checks are specific to .bio files
    for i in range(len(llh)):
        lh_i = llh[i].strip();    
        lh_i = lh_i.strip('"');
        if any(word == 'Annual' for word in lh_i.split(' ')):
          llh[i] = string.join(lh_i.split(' ')[0:4])
        else:
          llh[i]= string.join(lh_i.split(' ')[0:3]);  #just keep the first part of the original
        #lh[i]=lh_i.split(' ')[0];
        if any(word == 'Model' for word in lh_i.split(' ')):
          fm.append('U25')
        else:
          fm.append('f4');
    
    dd = loadtxt(manglefile,skiprows=1,dtype={'names':llh,'formats':fm});
    return dd
