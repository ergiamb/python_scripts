
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.lines as mlines

mpl.rc('lines',linewidth=2)
mpl.rc('font',size=16)

basestyles = ['-','-','-','-','-','-','-','-','-','-','-','-','-','-','-'] #['-','--','-.',':']
basecolors = ['indigo','darkviolet','mediumvioletred','red','crimson','darkorange',
              'gold','yellowgreen','green','olivedrab','teal','cadetblue','darkturquoise',
              'deepskyblue','navy']

sim = 'run5b_th'
nbins = 1000 
nwp = 2565 #breached
twp = 2575 #total wp in simulation
time = np.zeros((nwp),'=f8')
f = file(sim+'/wp_breach.txt','r')
wp = 0
for line in f:
  try:
    time[wp] = (float(line.split()[0]))
    wp += 1
  except ValueError:
    print('Skip the first line.')
f.close()
print(time)
#set up figure
fig = plt.figure(figsize=(8,8)) #adjust size to fit plots (width, height)
ax = fig.add_subplot(111)
#find percentiles
per01 = int(twp/100)
per10 = int(twp/10)
per50 = int(twp/2)
per99 = nwp
chronology = np.sort(time)
time01per = chronology[per01]
time10per = chronology[per10]
time50per = chronology[per50]
time99per = chronology[-1]
ax.text(155000,per99+20,'99% ({}) by {} y'.format(per99,int(time99per)))
ax.text(155000,per50+20,'50% ({}) by {} y'.format(per50,int(time50per)))
ax.text(155000,per10+20,'10% ({}) by {} y'.format(per10,int(time10per)))
ax.text(155000,per01+20,'1% ({}) by {} y'.format(per01,int(time01per)))
x,y = np.array([[0,1e6],[per01,per01]])
x0,y0 = np.array([[0,1e6],[per10,per10]])
x1,y1 = np.array([[0,1e6],[per50,per50]])
x2,y2 = np.array([[0,1e6],[per99,per99]])
line = mlines.Line2D(x,y,lw=3.,color='darkturquoise',alpha=0.3)
line0 = mlines.Line2D(x0,y0,lw=3.,color='darkturquoise',alpha=0.5)
line1 = mlines.Line2D(x1,y1,lw=3.,color='darkturquoise',alpha=0.7)
line2 = mlines.Line2D(x2,y2,lw=3.,color='darkturquoise',alpha=0.9)
ax.add_line(line)
ax.add_line(line0)
ax.add_line(line1)
ax.add_line(line2)
#plot the histogram
n,bins,patch = plt.hist(time,nbins,range=(0.,max(time)),histtype='step',cumulative='True',linewidth=3,color='indigo')
plt.xlim(0.,np.max(time))
plt.ylim(0,twp+200)
plt.xlabel('Time (y)')
plt.ylabel('Number of Waste Packages Breached') 
plt.subplots_adjust(left=0.15)
plt.savefig('wp_breach_dpc.png')
print('Bins are %f years long.' %(np.max(time)/nbins))
for i in range(len(n)):
  print('%i, %f' %(i,n[i]))
plt.show()

#could if I wante to plot multiple CDFs on one plot!
#which would be cool for the probabilistic set of simulations

