import sys
from h5py import *
import numpy
import math

def write_dataset(h5file,datasetname,nx,ny,nz,data):
  # 3d uniform grid
  h5grp = h5file.create_group(datasetname)
  # 3D will always be XYZ where as 2D can be XY, XZ, etc. and 1D can be X, Y or Z
  h5grp.attrs['Dimension'] = numpy.string_('XYZ')
  # based on Dimension, specify the uniform grid spacing
  h5grp.attrs['Discretization'] = [15.,15.,15.]
  # again, depends on Dimension
  h5grp.attrs['Origin'] = [0.,0.,0.]
  # leave this line out if not cell centered.  It set to False, it will still
  # be true (issue with HDF5 and Fortran)
  h5grp.attrs['Cell Centered'] = [True]
  h5grp.attrs['Interpolation Method'] = numpy.string_('Step')
  
  rarray = numpy.zeros((nx,ny,nz),'=f8')

  for k in range(nz):
    for j in range(ny):
      for i in range(nx):
        index = i + j*nx + k*nx*ny
        rarray[i][j][k] = data[index]
  h5grp.create_dataset('Data', data=rarray) #does this matter that it is also called data?

def write_dataset2(h5file,datasetname,nx,ny,nz,data):
  # 3d uniform grid
  h5grp = h5file.create_group(datasetname)
  # 3D will always be XYZ where as 2D can be XY, XZ, etc. and 1D can be X, Y or Z
  h5grp.attrs['Dimension'] = numpy.string_('XYZ')
  # based on Dimension, specify the uniform grid spacing
  h5grp.attrs['Discretization'] = [15.,15.,15.]
  # again, depends on Dimension
  h5grp.attrs['Origin'] = [0.,0.,0.]
  # leave this line out if not cell centered.  It set to False, it will still
  # be true (issue with HDF5 and Fortran)
  h5grp.attrs['Cell Centered'] = [True]
  h5grp.attrs['Interpolation Method'] = numpy.string_('Step')
  
  rarray = numpy.zeros((nx,ny,nz),'=f8')

  h5grp.create_dataset('Data', data=data) #does this matter that it is also called data?

def geometric_mean(data_array):
  #calculate geometric mean of everything in the nd_array
  mean = 0.
  for i in range(len(data_array)):
    mean += math.log10(data_array[i])
#    print data_array[i], mean
#  print mean, len(data_array)
  mean /= len(data_array)
  return pow(10.,mean)

def k_of_z(zbase, dz, nx, ny, nz):
  #Calculate k=f(z) and return array of cell-centered permeability values to use in write_dataset()
  #very similar to make_dataset.permeability(), which is for cell indexed datasets
  logk = numpy.zeros(nz)
  zgrid = zbase
  for i in range(nz):
    zcell = zgrid - dz/2. #dz is positive so subtract it to move upward
    logk[i] = -1.38 * math.log10(zcell/1000.) - 16.4 #Stober and Bucher, 2007. Black Forest gneisses
    zgrid -= dz
    print zgrid, zcell, logk[i]
  #allocate array to hold permeability for each point in gridded dataset
  k_m2 = numpy.zeros(nx*ny*nz)
  #and assign values to each layer
  for k in range(nz):
    for j in range(ny):
      for i in range(nx):
        index = i + j*nx + k*nx*ny
        k_m2[index] = math.pow(10.,logk[k])
  #return permeability
  return k_m2


'''
#Try out k_of_z
nx = 132
ny = 66
nz = 400
dz = 15.
zbase = 6000. #depth below surface from the bottom up
h5file = File('testk_of_z.h5',mode='w')
write_dataset(h5file,'Permeability',nx,ny,nz,k_of_z(zbase,dz,nx,ny,nz))
h5file.close()
'''
'''


#Now use the functions
nx = 201 #90 #110
ny = 135 #90 #87
nz = 84 #180 #80

nval = nx*ny*nz
data_array_X = numpy.zeros((nval), '=f8')
data_array_Y = numpy.zeros((nval), '=f8')
data_array_Z = numpy.zeros((nval), '=f8')

#txtfile = file('k_big.txt',mode='r')
#txtfile = file('k_dispersion.txt',mode='r')
txtfile = file('k_fullsize.txt',mode='r')
txtfile.readline() #throw away the first line, it only says "perm"
for i in range(nval):
  line = txtfile.readline()
  data_array_X[i] = line.split()[3]
  data_array_Y[i] = line.split()[4]
  data_array_Z[i] = line.split()[5]
txtfile.close()

filename = 'k_fullsize_iso.h5'
#filename = 'k_dispersion.h5'
#filename = 'big_kiso.h5'
h5file = File(filename,mode='w')

write_dataset(h5file,'Permeability',nx,ny,nz,data_array_X) #make isotropic k instead
#write_dataset(h5file,'PermeabilityX',nx,ny,nz,data_array_X)
#write_dataset(h5file,'PermeabilityY',nx,ny,nz,data_array_Y)
#write_dataset(h5file,'PermeabilityZ',nx,ny,nz,data_array_Z)

h5file.close()

print('Mean PermeabilityX = ',(geometric_mean(data_array_X)))
#print('Mean PermeabilityY = '+str(geometric_mean(data_array_Y)))
#print('Mean PermeabilityZ = '+str(geometric_mean(data_array_Z)))
'''
#print('done')
