import sys
from h5py import *
import numpy

#filename = sys.argv[1]
filename = 'boolean_fracture_colored.inp'
outfilename = filename.split('.')[0]+'.h5'
h5file = File(outfilename,mode='w')

f = open(filename,'r')
prev_line2 = ''
coordinates = []
line1 = ''
line2 = ''
iline = 0
#for line in f:
while True:
  line = f.readline()
  prev_line2 = line2
  line2 = line1
  line1 = line
  if line.startswith('color'):
    break
  if iline > 0:
    coordinates.append(line)
  iline += 1

nx = 0
ny = 0
nz = 0
for icoord in range(len(coordinates)-1):
  w = coordinates[icoord].split()
  nx = max(int(w[1])+1,nx)
  ny = max(int(w[2])+1,ny)
  nz = max(int(w[3])+1,nz)
print(nx,ny,nz)

# size of domain
n = nx*ny*nz

#length of cell side
d = 15.

# coordinates
a = numpy.zeros(nx+1,'=f8')
for i in range(nx+1):
  a[i] = float(i)*d
dataset_name = 'Coordinates/X [m]'
h5dset = h5file.create_dataset(dataset_name, data=a)
a = numpy.zeros(ny+1,'=f8')
for i in range(ny+1):
  a[i] = float(i)*d
dataset_name = 'Coordinates/Y [m]'
h5dset = h5file.create_dataset(dataset_name, data=a)
a = numpy.zeros(nz+1,'=f8')
for i in range(nz+1):
  a[i] = float(i)*d
dataset_name = 'Coordinates/Z [m]'
h5dset = h5file.create_dataset(dataset_name, data=a)

a = numpy.zeros((nx,ny,nz),'=f8')
for icoord in range(len(coordinates)-1):
  line = f.readline()
  if len(line) == 0:
    break
  w = line.split()
  value = float(w[1])
  w = coordinates[icoord].split()
#  print(icoord,w,value)
  i = int(w[1])
  j = int(w[2])
  k = int(w[3])
  a[i][j][k] = value

dataset_name = 'Time:  0.00000E+00 y/value'
h5dset = h5file.create_dataset(dataset_name, data=a)

h5file.close()
f.close()
print('done with everything')
  
