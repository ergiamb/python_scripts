#make regions, strata, initial condition, and source sink cards to go with 2017dwr/claydwr_withbuffer_usg.h5
#not making boundary condition cards in here, or flow and transport conditions, or actual materials
#DSNF drift has double wide refinement along horizontal boreholes
#But this layout has no buffer in DSNF hbh
#emily 4.20.17

import wfg_block as wfg

#hardwire some names
mat = ['shale','drz','siltstone','silt_drz','sandstone','sand_drz','limestone','lower_shale','buffer',
       'calcine','naval','overburden','overburden_drz']
flow = ['initial']
trans = ['initial']
#define multiple glass bins: name, num wps, glass volume per wp
glassbins = [('hanford_glass',5540,1.14),('savannah_river_glass',3781,0.7),
             ('cssr_glass',170,1.14)]
hotglass = [('frg_glass',8,0.121)] #2 frg per wp
#define multiple dsnf waste form bins: name, num wps
dsnfbins = [('dsnf_50',582),('dsnf_100',117),('dsnf_200',470),('dsnf_300',6),
            ('dsnf_500',21),('dsnf_1000',44)]
hotdsnf = [('dsnf_1500',2)]
#calcine will use wfg.glass_to_file_reg, here is the volume
calcine_vol = 9.5 #0.95 m^3/can * 10 cans per wp

#define dimensions (for the most part)
small = 5./3. #small division of the grid
smallest = 5./9. #smallest division of the grid

num_drift_hlw = 8 #number of hlw/hbh drifts
num_drift_dsnf = 1 #number of dsnf/hbh drifts
num_drift = num_drift_hlw + num_drift_dsnf #hbh drifts
num_drift_calcine = 2 #number of in-drift calcine drifts
num_drift_naval = 1 #number of in-drift naval drifts
num_hbh = 35 #number of hbh pairs
num_hbh_lastglass = 20 #number of hbh pairs in last glass drift
num_wp = 18 #number of wp/hbh (single)
num_hot = 9 #number of hot wp/hbh (single)
num_calcine = 80 #number of calcine wps per drift
num_naval = 7 #number of Naval wps per drift

x0 = 460. #start of longhall, no drz, room for 5 m of drz behind it
x_shaft = 10.
x_shaft_drz = 5.
x_between = 40. #distance from shaft to repository
x_shorthall = 5. #width of short hall connecting long hall to panel
x1 = 510. #start of first hbh w/o drz
x_hbh = 100.
x_wp = 5.
x_bf = 0. #0 meters of backfill at deep end of hbh
x_hot = 5. #5 meters of backfill between hot wps (FRG and DSNF1500)
x_seal = 10. #10 m of backfill/seal at near end of hbh
x_drift = 5.
x_drz = smallest #hbh
x_drift_drz = small #calcine and naval drifts
x_hall_drz = 5.
x_pillar = 30. #distance between hbh
x_repeat = 2.*x_hbh + x_drift + x_pillar
x_saccess = x_repeat*num_drift - x_pillar - x_hbh + 3.*(x_pillar+x_drift) #no drz
x_naccess = x_repeat*num_drift - x_pillar - 2.*x_hbh #no drz
x_longhall = 2.*x_shaft + 2.*x_between + x_hbh + x_saccess #no drz
x2 = x0 + x_longhall - x_shaft #start of east shaft, no drz
xwest = 0.
xeast = 7800.

#I moved the hbh in y, so something in here needs to change.
y1 = 20. #south face of repository (long hall), no drz
y_drz = smallest #hbh
y_hall_drz = 5. #halls and hbh drifts
y_shaft = 5. #width of shaft (w/o drz)
y_longhall = 5. #width w/o drz
y_shorthall = 40. #distance between longhall and panel (of num_drift)
y_access = 5. #width of hall connecting panels
y2 = y1 + y_longhall + y_shorthall #south face of south access hall
y3 = y2 + y_access #start of drift including backfill/seal
y_drift = 1065. #including 2 20-m lengths of backfill
y_drift_calcine = 820 #including 1 20-m length of backfill
y_drift_naval = 90 #including 1 20-m length of backfill
y_drift_drz = small #calcine and naval drifts
y_wp = smallest #in hbh
y_bf_drift = 20. #each end of drift w/o hbh
y_bf_hbh = 0. #within the hbh
y4 = y3 + y_bf_drift + small + smallest #south face of first hbh, no drz
y_hbh = smallest
y_repeat = 30. #30 m centers
y_bigwp = 5.
y_repeat_bigwp = y_bigwp + 5. #10 m centers for calcine and naval wps
ysouth = 0.
ynorth = 1605.

z1 = 685. #base of repository, no drz
z2 = 685. + small + smallest #base of hbh, no drz
#z2 = 685. + small #base of hbh, no drz
z_hall = 5.
z_hall_drz = 5. #halls and hbh drifts
z_hbh = smallest #?
#z_hbh = small
z_drz = smallest #hbh
z_wp = smallest
z_drift_drz = small #calcine and naval drifts
ztop = 1200. #top of domain which is siltstone
ztop_aq3 = ztop - 30. #top of sandstone aquifer
ztop_shale3 = ztop_aq3 - 60.
ztop_aq2 = ztop_shale3 - 195. #top of Red Bird Siltstone
ztop_shale2 = ztop_aq2 - 90. #top of repository horizon
ztop_aq1 = ztop_shale2 - 300. #top of Niobrara
ztop_shale1 = ztop_aq1 - 75.  #top of Greenhorn/Carlile shale
ztop_aq0 = ztop_shale1 - 150. #top of Dakota
ztop_shale0 = ztop_aq0 - 30. #top of basal confining unit

zbase = 0.

#write region, strata, source sink, and waste form blocks
#model domain, then DRZ, then buffer, then wp
rfile = file('regions.txt', 'w')
sfile = file('strata.txt', 'w')
ssfile = file('source_sink.txt','w')
obsfile = file('obs_points.txt','w')
obsregf = file('obs_regions.txt','w')
wfgfile = file('wfg.txt','w')
wfg.drepo_to_file(wfgfile) #right now this is inventory for 12-PWR 100 y OoR

#files need to be open already, and closed afterward!
def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

def write_obs(obsname,x,y,z,obsfile=obsfile,obsregf=obsregf):
  obsfile.write('OBSERVATION\n  REGION %s\n  VELOCITY\nEND\n\n'
                % (obsname))
  obsregf.write('REGION %s\n  COORDINATE %f %f %f\nEND\n\n'
                % (obsname,x,y,z))
  return

#Model Domain - cut and paste this first region into the main input deck
write_reg('all',xwest,ysouth,zbase,xeast,ynorth,ztop)
write_strata('all',mat[0])
write_reg_face('top',xwest,ysouth,ztop,xeast,ynorth,ztop)
write_reg_face('bottom',xwest,ysouth,zbase,xeast,ynorth,zbase)
write_reg_face('west',xwest,ysouth,zbase,xwest,ynorth,ztop)
write_reg_face('east',xeast,ysouth,zbase,xeast,ynorth,ztop)
write_reg_face('south',xwest,ysouth,zbase,xeast,ysouth,ztop)
write_reg_face('north',xwest,ynorth,zbase,xeast,ynorth,ztop)
#obs points in shale (repository horizon)
y = 622.5 #as close to wp obs y as I can get
z = 682.5 #repository horizon, middle of a 15-m cell
x = 2782.5 #first 15-m cell east of repository
write_obs('shale_obs1',x,y,z)
x += 2490.
write_obs('shale_obs2',x,y,z)
x += 2505.
write_obs('shale_obs3',x,y,z)

#Other sedimentary strata above and below the repository horizon 
write_reg('overburden',xwest,ysouth,ztop_aq3,xeast,ynorth,ztop)
write_strata('overburden',mat[11])
write_reg('sand1',xwest,ysouth,ztop_shale3,xeast,ynorth,ztop_aq3)
write_strata('sand1',mat[4])
write_reg('silt',xwest,ysouth,ztop_shale2,xeast,ynorth,ztop_aq2)
write_strata('silt',mat[2])
write_reg('lime',xwest,ysouth,ztop_shale1,xeast,ynorth,ztop_aq1)
write_strata('lime',mat[6])
write_reg('shale1',xwest,ysouth,ztop_aq0,xeast,ynorth,ztop_shale1)
write_strata('shale1',mat[7])
write_reg('sand0',xwest,ysouth,ztop_shale0,xeast,ynorth,ztop_aq0)
write_strata('sand0',mat[4])
write_reg('shale0',xwest,ysouth,zbase,xeast,ynorth,ztop_shale0)
write_strata('shale0',mat[7])
#obs points in upper aquifer (aq3)
y = 622.5 #mid-drift
z = (ztop_shale3 + ztop_aq3)/2.
x = 2782.5 #<~30 m east of repository
write_obs('sand_obs1',x,y,z)
x += 2490.
write_obs('sand_obs2',x,y,z)
x += 2505.
write_obs('sand_obs3',x,y,z)
#obs points in silty layer (aq2)
y = 622.5 #mid-drift
z = (ztop_shale2 + ztop_aq2)/2.
x = 2782.5 #<~30 m east of repository
write_obs('silt_obs1',x,y,z)
x += 2490.
write_obs('silt_obs2',x,y,z)
x += 2505.
write_obs('silt_obs3',x,y,z)
#obs points in limestone aquifer (aq1)
y = 622.5 #mid-drift
z = (ztop_shale1 + ztop_aq1)/2.
x = 2782.5 #<~30 m east of repository
write_obs('lime_obs1',x,y,z)
x += 2490.
write_obs('lime_obs2',x,y,z)
x += 2505.
write_obs('lime_obs3',x,y,z)
#obs points in lower sand aquifer (aq0)
y = 622.5 #mid-drift
z = (ztop_shale0 + ztop_aq0)/2.
x = 2782.5 #<~30 m east of repository
write_obs('lsand_obs1',x,y,z)
x += 2490.
write_obs('lsand_obs2',x,y,z)
x += 2505.
write_obs('lsand_obs3',x,y,z)

#DRZ, longhall
xmin = x0-x_shaft_drz
ymin = y1-y_hall_drz
zmin = z1-z_hall_drz
xmax = xmin + x_longhall + x_shaft_drz*2.
ymax = ymin + y_longhall + y_hall_drz*2.
zmax = zmin + z_hall + z_hall_drz*2.
write_reg('drz_longhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_longhall',mat[1])
#DRZ west shaft, drz=shale;adrz=aquifer;sdrz=siltstone;odrz=overburden
xmin = x0-x_shaft_drz
ymin = y1-y_hall_drz
zmin = z1-z_hall_drz
xmax = xmin + x_shaft + x_shaft_drz*2.
ymax = ymin + y_shaft + y_hall_drz*2.
zmax = ztop_shale3
write_reg('drz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_wshaft',mat[1])
zmin = ztop_shale2
zmax = ztop_aq2
write_reg('sdrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('sdrz_wshaft',mat[3])
zmin = ztop_shale3
zmax = ztop_aq3
write_reg('adrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('adrz_wshaft',mat[5])
zmin = ztop_aq3
zmax = ztop
write_reg('odrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('odrz_wshaft',mat[12])
#DRZ east shaft, drz=shale;adrz=aquifer;sdrz=siltstone;odrz=overburden
xmin = x2 - x_shaft_drz
zmin = z1 - z_drz
xmax = xmin + x_shaft + x_shaft_drz*2.
zmax = ztop_shale3
write_reg('drz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_eshaft',mat[1])
zmin = ztop_shale2
zmax = ztop_aq2
write_reg('sdrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('sdrz_eshaft',mat[3])
zmin = ztop_shale3
zmax = ztop_aq3
write_reg('adrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('adrz_eshaft',mat[5])
zmin = ztop_aq3
zmax = ztop
write_reg('odrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('odrz_eshaft',mat[12])
#DRZ short hall, access halls
zmin = z1 - z_hall_drz
zmax = z1 + z_hall + z_hall_drz
#short hall connecting long hall to panel
ymin = 0.
ymax = y1 + y_longhall + y_shorthall
xmin = x1 + x_hbh - x_hall_drz
xmax = xmin + x_shorthall + 2.*x_hall_drz
write_reg('drz_shorthall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_shorthall',mat[1])
#south access hall of panel
ymin = y2 - y_hall_drz
ymax = ymin + y_access + y_hall_drz*2.
xmin = x1 + x_hbh - x_hall_drz
xmax = xmin + x_saccess + 2.*x_hall_drz
write_reg('drz_saccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_saccess',mat[1])
#north access hall of panel
ymin = y3 + y_drift - y_hall_drz
ymax = ymin + y_access + y_hall_drz*2.
xmin = x1 + x_hbh - x_hall_drz
xmax = xmin + x_naccess + 2.*x_hall_drz
write_reg('drz_naccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_naccess',mat[1])
#hbh drifts
zmin = z1 - z_hall_drz
zmax = z1 + z_hall + z_hall_drz
ymin = y3 - y_hall_drz
ymax = y3 + y_drift + y_hall_drz*2.
for i in range(num_drift):
  xmin = x1 + x_hbh - x_hall_drz + i*x_repeat
  xmax = xmin + x_drift + x_hall_drz*2.
  write_reg('drz_drift'+str(i),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_drift'+str(i),mat[1])
#horizontal boreholes, glass
zmin = z2 - z_drz
zmax = zmin + z_hbh + 2.*z_drz
for i in range(num_drift_hlw):
  xmin = x1 - x_drz + i*x_repeat
  xmax = xmin + x_drift + 2.*x_hbh + 2.*x_drz
  if i != num_drift_hlw-1: #which is the last glass drift
    nj = num_hbh
  else:
    nj = num_hbh_lastglass
  for j in range(nj):
    if i == num_drift_hlw-1 and j == num_hbh_lastglass-1:
      xleft = x1 + x_bf + i*x_repeat
      xmin = xleft + x_hbh + x_drift
      xmax = xmin + x_seal + hotglass[0][1]*(x_wp+x_hot) + x_drz #need a portion of last hbh
    ymin = y4 - y_drz + j*y_repeat
    ymax = ymin + y_hbh + 2.*y_drz
    write_reg('drz_hbh'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('drz_hbh'+str(i)+'_'+str(j),mat[1])
#horizontal boreholes, dsnf with NO buffer, dimensions are the same as for glass
zmin = z2 - z_drz
zmax = zmin + z_hbh + 2.*z_drz
for i in range(num_drift_hlw,num_drift):
  xmin = x1 - x_drz + i*x_repeat
  xmax = xmin + x_drift + 2.*x_hbh + 2.*x_drz
  for j in range(num_hbh):
    if i == num_drift-1 and j == num_hbh-1:
      xmax = xmin + x_drift + x_hbh + x_seal + hotdsnf[0][1]*(x_wp+x_hot) +x_drz #need a portion of last hbh
    ymin = y4 - y_drz + j*y_repeat
    ymax = ymin + y_hbh + 2.*y_drz
    write_reg('drz_hbh_dsnf'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('drz_hbh_dsnf'+str(i)+'_'+str(j),mat[1])
#calcine drifts
zmin = z1 - z_drift_drz
zmax = zmin + z_hall + 2.*z_drift_drz
ymin = y3 - y_drift_drz
ymax = ymin + y_drift_calcine + 2.*y_drift_drz
for i in range(num_drift_calcine):
  xmin = x1 + x_repeat*num_drift - x_drift_drz + i*(x_pillar+x_drift)
  xmax = xmin + x_drift + 2.*x_drift_drz
  write_reg('drz_cal'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_cal'+str(i)+'_'+str(j),mat[1])
#naval drift
zmin = z1 - z_drift_drz
zmax = zmin + z_hall + 2.*z_drift_drz
ymin = y3 - y_drift_drz
ymax = ymin + y_drift_naval + 2.*y_drift_drz
for i in range(num_drift_naval):
  xmin += x_pillar + x_drift
  xmax = xmin + x_drift + 2.*x_drift_drz
  write_reg('drz_nav'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_nav'+str(i)+'_'+str(j),mat[1])

#Buffer/Backfill
#buffer, longhall
xmin = x0
ymin = y1
zmin = z1
xmax = xmin + x_longhall
ymax = ymin + y_longhall
zmax = zmin + z_hall
write_reg('bf_longhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_longhall',mat[8])
#buffer west shaft
xmin = x0
ymin = y1
zmin = z1
xmax = xmin + x_shaft
ymax = ymin + y_shaft
zmax = ztop
write_reg('bf_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_wshaft',mat[8])
#buffer east shaft
xmin = x2
xmax = xmin + x_shaft
write_reg('bf_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_eshaft',mat[8])
#buffer short hall, access halls, and disposal drifts
zmin = z1
zmax = z1 + z_hall
#short hall connecting long hall to panel
ymin = 0.
ymax = y1 + + y_longhall + y_shorthall
xmin = x1 + x_hbh
xmax = xmin + x_shorthall
write_reg('bf_shorthall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_shorthall',mat[8])
#south access hall of panel
ymin = y2
ymax = ymin + y_access
xmin = x1 + x_hbh
xmax = xmin + x_saccess
write_reg('bf_saccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_saccess',mat[8])
#north access hall of panel
ymin = y2 + y_access + y_drift
ymax = ymin + y_access
xmin = x1 +x_hbh
xmax = xmin + x_naccess
write_reg('bf_naccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_naccess',mat[8])
#hbh drifts
ymin = y3
ymax = y3 + y_drift
for i in range(num_drift):
  xmin = x1 + x_hbh + i*x_repeat
  xmax = xmin + x_drift
  write_reg('bf_drift'+str(i),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('bf_drift'+str(i),mat[8])
#horizontal boreholes,glass - this will fill the hbh w/ buffer, all but seal will get overwritten with wp
zmin = z2
zmax = zmin + z_hbh
for i in range(num_drift_hlw):
  xmin = x1 + i*x_repeat
  xmax = xmin + x_drift + 2.*x_hbh
  if i != num_drift_hlw-1:
    nj = num_hbh
  else:
    nj = num_hbh_lastglass
  for j in range(nj):
    if i == num_drift_hlw-1 and j == num_hbh_lastglass-1:
      xmin = xleft + x_hbh + x_drift
      xmax = xmin + x_seal + hotglass[0][1]*(x_wp+x_hot) #need a portion of last hbh
    ymin = y4 + j*y_repeat
    ymax = ymin + y_hbh
    write_reg('bf_hbh'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('bf_hbh'+str(i)+'_'+str(j),mat[8])
#horizontal boreholes, dsnf with NO buffer, dimensions are the same as for glass
zmin = z2 
zmax = zmin + z_hbh
for i in range(num_drift_hlw,num_drift):
  xmin = x1 + i*x_repeat
  xmax = xmin + x_drift + 2.*x_hbh
  for j in range(num_hbh):
    if i == num_drift-1 and j == num_hbh-1:
      xmax = xmin + x_drift + x_hbh + x_seal + hotdsnf[0][1]*(x_wp+x_hot) #need a portion of last hbh
    ymin = y4 + j*y_repeat
    ymax = ymin + y_hbh
    write_reg('bf_hbh_dsnf'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('bf_hbh_dsnf'+str(i)+'_'+str(j),mat[8])
#calcine drifts
zmin = z1
zmax = zmin + z_hall
ymin = y3
ymax = ymin + y_drift_calcine
for i in range(num_drift_calcine):
  xmin = x1 + x_repeat*num_drift + i*(x_pillar+x_drift)
  xmax = xmin + x_drift
  write_reg('bf_cal'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('bf_cal'+str(i)+'_'+str(j),mat[8])
#naval drift
zmin = z1
zmax = zmin + z_hall
ymin = y3
ymax = ymin + y_drift_naval
for i in range(num_drift_naval):
  xmin += x_pillar + x_drift
  xmax = xmin + x_drift
  write_reg('bf_nav'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('bf_nav'+str(i)+'_'+str(j),mat[8])
  
#HLW Waste Packages (cool ones)
zmin = z2
zmax = zmin + z_wp
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = glassbins[0][1] #ditto
for i in range(num_drift_hlw):
  xleft = x1 + x_bf + i*x_repeat
  xright = xleft + (x_hbh-x_bf) + x_drift + x_seal
  for j in range(num_hbh):
    ymin = y4 + y_bf_hbh + j*y_repeat
    ymax = ymin + y_wp
    for k in range(num_wp*2):
      if k < num_wp:
        xmin = xleft + k*(x_wp+x_bf)
      else:
        xmin = xright + (k-num_wp)*(x_wp+x_bf)
      xmax = xmin + x_wp
      #write only if counter < total number of packages (0 indexed)
      if counter >= wpnum and counter < cumulative:
        savej = j
        name = glassbins[ii][0].split('_')[0]+str(i)+'_'+str(j)+'_'+str(k)
        write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
        write_strata(name,glassbins[ii][0])
        write_ss(name,glassbins[ii][0],trans[0],name)
        wfg.glass_to_file_reg(wfgfile,name,glassbins[ii][2],glassbins[ii][0]) #write heterog. hlw to REGION
        counter += 1
        if counter == wpnum + glassbins[ii][1]:
          #write an obs point
          zobs = (zmin+zmax)/2.
          yobs = (ymin+ymax)/2.
          xobs = (xmin+xmax)/2.
          write_obs(name+'obs',xobs,yobs,zobs)
          yobs += smallest
          write_obs(name+'drz',xobs,yobs,zobs)
          yobs += smallest
          write_obs(name+'uhr',xobs,yobs,zobs)
          #increment for next bin
          wpnum += glassbins[ii][1] #before increment ii
          ii += 1
          if ii < len(glassbins): cumulative += glassbins[ii][1] #after increment ii
        print('glass %i %i %i' % (counter, wpnum, ii))

#HLW Waste Packages (FRG glass - hot has 5 m between)
#spatially, pick up where we left off - next hbh, same drift
#this piece is very specific to inventory
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = hotglass[0][1] #ditto
#i unchanged
#xleft unchanged
#xright unchanged
j = savej+1 #place these up one hbh
ymin = y4 + (y_bf_hbh) + j*y_repeat
ymax = ymin + y_wp
for k in range(num_hot):
  xmin = xright + k*(x_wp+x_hot)
  xmax = xmin + x_wp
  #write only if counter < total number of packages (0 indexed)
  if counter >= wpnum and counter < cumulative:
    name = hotglass[ii][0].split('_')[0]+str(i)+'_'+str(j)+'_'+str(k)
    write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata(name,hotglass[ii][0])
    write_ss(name,hotglass[ii][0],trans[0],name)
    wfg.glass_to_file_reg(wfgfile,name,hotglass[ii][2],hotglass[ii][0]) #write heterog. hlw to REGION
    counter += 1
    if counter == wpnum + hotglass[ii][1]:
      #write an obs point
      zobs = (zmin+zmax)/2.
      yobs = (ymin+ymax)/2.
      xobs = (xmin+xmax)/2.
      write_obs(name+'obs',xobs,yobs,zobs)
      yobs += smallest
      write_obs(name+'drz',xobs,yobs,zobs)
      yobs += smallest
      write_obs(name+'uhr',xobs,yobs,zobs)
      #increment for next bin
      wpnum += hotglass[ii][1] #before increment ii
      ii += 1
      if ii < len(hotglass): cumulative += hotglass[ii][1] #after increment ii
    print('glass %i %i %i' % (counter, wpnum, ii))

#DSNF Waste Packages with no buffer!
zmin = z2
zmax = zmin + z_wp
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = dsnfbins[0][1] #ditto
for i in range(num_drift_hlw,num_drift):
  xleft = x1 + x_bf + i*x_repeat
  xright = xleft + (x_hbh-x_bf) + x_drift + x_seal
  for j in range(num_hbh):
    ymin = y4 + y_bf_hbh + j*y_repeat
    ymax = ymin + y_wp
    for k in range(num_wp*2):
      if k < num_wp:
        xmin = xleft + k*(x_wp+x_bf) 
      else:
        xmin = xright + (k-num_wp)*(x_wp+x_bf)
      xmax = xmin + x_wp
      #write only if counter < total number of packages (0 indexed)
      if counter >= wpnum and counter < cumulative:
        savej = j
        name = dsnfbins[ii][0].split('_')[0]+str(i)+'_'+str(j)+'_'+str(k)
        write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
        write_strata(name,dsnfbins[ii][0])
        write_ss(name,dsnfbins[ii][0],trans[0],name)
        wfg.dsnf_to_file_reg(wfgfile,name,dsnfbins[ii][0]) #write dsnf to REGION
        counter += 1
        if counter == wpnum + dsnfbins[ii][1]:
          #write an obs point
          zobs = (zmin+zmax)/2.
          yobs = (ymin+ymax)/2.
          xobs = (xmin+xmax)/2.
          write_obs(name+'obs',xobs,yobs,zobs)
          yobs += smallest
          write_obs(name+'buf',xobs,yobs,zobs)
          yobs += smallest
          write_obs(name+'drz',xobs,yobs,zobs)
          #increment for next bin
          wpnum += dsnfbins[ii][1] #before increment ii
          ii += 1
          if ii < len(dsnfbins): cumulative += dsnfbins[ii][1] #after increment ii
        print('dsnf %i %i %i' % (counter, wpnum, ii))

#DSNF Waste Packages (1500 W - hot has 5 m between)
#spatially, pick up where we left off - next hbh, same drift
#this piece is very specific to inventory
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = hotdsnf[0][1] #ditto
#i unchanged
#xleft unchanged
#xright unchanged
j = savej #place these across the hall
ymin = y4 + (y_bf_hbh) + j*y_repeat
ymax = ymin + y_wp
for k in range(num_wp,num_wp+num_hot):
  xmin = xright + (k-num_wp)*(x_wp+x_hot)
  xmax = xmin + x_wp
  #write only if counter < total number of packages (0 indexed)
  if counter >= wpnum and counter < cumulative:
    name = hotdsnf[ii][0].split('_')[0]+str(i)+'_'+str(j)+'_'+str(k)
    write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata(name,hotdsnf[ii][0])
    write_ss(name,hotdsnf[ii][0],trans[0],name)
    wfg.dsnf_to_file_reg(wfgfile,name,hotdsnf[ii][0]) #write dsnf to REGION
    counter += 1
    if counter == wpnum + hotdsnf[ii][1]:
      #write an obs point
      zobs = (zmin+zmax)/2.
      yobs = (ymin+ymax)/2.
      xobs = (xmin+xmax)/2.
      write_obs(name+'obs',xobs,yobs,zobs)
      yobs += smallest
      write_obs(name+'drz',xobs,yobs,zobs)
      yobs += smallest
      write_obs(name+'uhr',xobs,yobs,zobs)
      #increment for next bin
      wpnum += hotdsnf[ii][1] #before increment ii
      ii += 1
      if ii < len(hotdsnf): cumulative += hotdsnf[ii][1] #after increment ii
    print('dsnf %i %i %i' % (counter, wpnum, ii))

#calcine Waste Packages
zmin = z1 + small
zmax = zmin + small
for i in range(num_drift_calcine):
  xmin = x1 + x_repeat*num_drift + small + i*(x_pillar+x_drift)
  xmax = xmin + small
  for j in range(num_calcine):
    ymin = y3 + y_bf_drift + j*(y_repeat_bigwp)
    ymax = ymin + y_bigwp
    name = 'calcine'+str(i)+'_'+str(j)
    write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata(name,mat[9])
    write_ss(name,'calcine',trans[0],name)
    wfg.glass_to_file_reg(wfgfile,name,calcine_vol,'calcine')
    if j == num_calcine/2:
      zobs = (zmin+zmax)/2.
      yobs = (ymin+ymax)/2.
      xobs = (xmin+xmax)/2.
      write_obs('calobs'+str(i)+'_'+str(j),xobs,yobs,zobs)
      xobs += small
      write_obs('calbuf'+str(i)+'_'+str(j),xobs,yobs,zobs)
      xobs += small
      write_obs('caldrz'+str(i)+'_'+str(j),xobs,yobs,zobs)

#naval Waste Packages
zmin = z1 + small
zmax = zmin + small
for i in range(num_drift_naval):
  xmin += x_pillar + x_drift
  xmax = xmin + small
  for j in range(num_naval):
    ymin = y3 + y_bf_drift + j*(y_repeat_bigwp)
    ymax = ymin + y_bigwp
    name = 'naval'+str(i)+'_'+str(j)
    write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata(name,mat[10])
    write_ss(name,'naval',trans[0],name)
    wfg.naval_to_file_reg(wfgfile,name)
    if j == num_naval/2:
      zobs = (zmin+zmax)/2.
      yobs = (ymin+ymax)/2.
      xobs = (xmin+xmax)/2.
      write_obs('navobs'+str(i)+'_'+str(j),xobs,yobs,zobs)
      xobs += small
      write_obs('navbuf'+str(i)+'_'+str(j),xobs,yobs,zobs)
      xobs += small
      write_obs('navdrz'+str(i)+'_'+str(j),xobs,yobs,zobs)

rfile.close
sfile.close
ssfile.close
obsfile.close
obsregf.close
wfgfile.close


