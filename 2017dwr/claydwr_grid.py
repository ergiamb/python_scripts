#make regions, strata, initial condition, and source sink cards to go with 2017dwr/claydwr_grid
#not making boundary condition cards in here, or flow and transport conditions, or actual materials
#emily 2.7.17

import wfg_block as wfg

#hardwire some names
mat = ['shale','drz','siltstone','silt_drz','sandstone','sand_drz','limestone','lime_drz','buffer']
flow = ['initial']
trans = ['initial']
#define multiple glass bins: name, num wps, glass volume per wp
glassbins = [('hanford_glass',5540,1.14),('savannah_river_glass',3781,0.7)]
#define multiple dsnf waste form bins: name, num wps
dsnfbins = [('dsnf_50',582),('dsnf_100',117),('dsnf_200',470),('dsnf_300',6),
            ('dsnf_500',21),('dsnf_1000',44),('dsnf_1500',2)]

#define dimensions (for the most part)
small = 5./3. #small division of the grid
smallest = 5./9. #smallest division of the grid

num_drift_hlw = 7 #number of hlw/hbh drifts
num_drift_dsnf = 1 #number of dsnf/hbh drifts
num_drift = num_drift_hlw + num_drift_dsnf #hbh drifts
num_drift_calcine = 2 #number of in-drift calcine drifts
num_drift_naval = 1 #number of in-drift naval drifts
num_hbh = 35 #number of hbh pairs
num_wp = 18 #number of wp/hbh (single)
num_calcine = 80 #number of calcine wps per drift
num_naval = 7 #number of Naval wps per drift

x0 = 460. #start of longhall, no drz, room for 5 m of drz behind it
x_shaft = 10.
x_shaft_drz = 5.
x_between = 140. #distance from shaft to first access hall
x_shorthall = 5. #width of short hall connecting long hall to panel
x1 = 510. #start of first hbh w/o drz
x_hbh = 100.
x_wp = 5.
x_bf = 0. #0 meters of backfill at deep end of hbh
#x_bf = 5. #5 meters of backfill at deep end of hbh
x_seal = 10. #10 m of backfill/seal at near end of hbh
x_drift = 5.
x_drz = smallest #hbh
x_drift_drz = small #calcine and naval drifts
x_hall_drz = 5.
x_pillar = 30. #distance between hbh
x_repeat = 2.*x_hbh + x_drift + x_pillar
x_access = x_repeat*num_drift - x_pillar - x_hbh + 3.*(x_pillar+x_drift) #no drz, this is longer than in grid1
#x_access = x_repeat*num_drift - x_pillar - 2.*x_hbh #no drz, this is longer than in grid1
x_longhall = 2.*x_shaft + 2.*x_between + x_access #no drz in here 
x2 = x0 + x_longhall - x_shaft #start of east shaft, no drz
xwest = 0.
xeast = 7695.

#I moved the hbh in y, so something in here needs to change.
y1 = 20. #south face of repository (long hall), no drz
y_drz = smallest #hbh
y_hall_drz = 5. #halls and hbh drifts
y_shaft = 5. #width of shaft (w/o drz)
y_longhall = 5. #width w/o drz
y_shorthall = 40. #distance between longhall and panel (of num_drift)
y_access = 5. #width of hall connecting panels
y2 = y1 + y_longhall + y_shorthall #south face of south access hall
y3 = y2 + y_access #start of drift including backfill/seal
y_drift = 1060. #including 2 20-m lengths of backfill
y_drift_calcine = 820 #including 1 20-m length of backfill
y_drift_naval = 90 #including 1 20-m length of backfill
y_drift_drz = small #calcine and naval drifts
y_wp = smallest
y_bf_drift = 20. #each end of drift w/o hbh
y_bf_hbh = 0. #within the hbh
#y_bf_hbh = smallest #within the hbh
y4 = y3 + y_bf_drift #south face of first hbh, no drz
#y4 = y3 + y_bf_drift - smallest #south face of first hbh, no drz
y_hbh = smallest #?
#y_hbh = small
y_repeat = 30. #30 m centers
ysouth = 0.
ynorth = 1605.

z1 = 685. #base of repository, no drz
z2 = 685. + small + smallest #base of hbh, no drz
#z2 = 685. + small #base of hbh, no drz
z_hall = 5.
z_hall_drz = 5. #halls and hbh drifts
z_hbh = smallest #?
#z_hbh = small
z_drz = smallest #hbh
z_wp = smallest
z_drift_drz = small #calcine and naval drifts
ztop = 1200. #top of domain which is siltstone
ztop_aq3 = ztop-105. #top of sandstone aquifer
ztop_shale3 = ztop_aq3 - 45. #top of shale/host rock
ztop_aq2 = ztop_shale3 - 600. #top of carbonate/chalk aquifer
ztop_shale2 = ztop_aq2 - 45. #top of second thinner shale
ztop_aq1 = ztop_shale2 - 90. #top of deep sandstone aquifer
ztop_shale1 = ztop_aq1 - 45. #top of basal confining unit
zbase = 0.

#write region, strata, source sink, and waste form blocks
#model domain, then DRZ, then buffer, then wp
rfile = file('regions.txt', 'w')
sfile = file('strata.txt', 'w')
ssfile = file('source_sink.txt','w')
obsfile = file('obs_points.txt','w')
obsregf = file('obs_regions.txt','w')
wfgfile = file('wfg.txt','w')
wfg.gdsa_to_file(wfgfile) #right now this is inventory for 12-PWR 100 y OoR
#will need to create 4pwr_to_file

#files need to be open already, and closed afterward!
def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

def write_obs(obsname,x,y,z,obsfile=obsfile,obsregf=obsregf):
  obsfile.write('OBSERVATION\n  REGION %s\n  VELOCITY\nEND\n\n'
                % (obsname))
  obsregf.write('REGION %s\n  COORDINATE %f %f %f\nEND\n\n'
                % (obsname,x,y,z))
  return

#Model Domain - cut and paste this first region into the main input deck
write_reg('all',xwest,ysouth,zbase,xeast,ynorth,ztop)
write_strata('all',mat[0])
write_reg_face('top',xwest,ysouth,ztop,xeast,ynorth,ztop)
write_reg_face('bottom',xwest,ysouth,zbase,xeast,ynorth,zbase)
write_reg_face('west',xwest,ysouth,zbase,xwest,ynorth,ztop)
write_reg_face('east',xeast,ysouth,zbase,xeast,ynorth,ztop)
write_reg_face('south',xwest,ysouth,zbase,xeast,ysouth,ztop)
write_reg_face('north',xwest,ynorth,zbase,xeast,ynorth,ztop)
#obs points in shale (repository horizon)
y = 592.5
z = 682.5 #repository horizon, middle of a 15-m cell
x = 2617.5 #<~30 m east of repository
write_obs('shale1',x,y,z)
x += 2490.
write_obs('shale2',x,y,z)
x += 2505.
write_obs('shale3',x,y,z)

#Other sedimentary strata above and below the repository horizon 
write_reg('siltstone',xwest,ysouth,ztop_aq3,xeast,ynorth,ztop)
write_strata('siltstone',mat[2])
write_reg('aq3',xwest,ysouth,ztop_shale3,xeast,ynorth,ztop_aq3)
write_strata('aq3',mat[4])
write_reg('aq2',xwest,ysouth,ztop_shale2,xeast,ynorth,ztop_aq2)
write_strata('aq2',mat[6])
write_reg('aq1',xwest,ysouth,ztop_shale1,xeast,ynorth,ztop_aq1)
write_strata('aq1',mat[4])
#obs points in upper aquifer (aq3)
y = 592.5 #mid-drift
z = (ztop_shale3 + ztop_aq3)/2.
x = 2617.5 #<~30 m east of repository
write_obs('aqup1',x,y,z)
x += 2490.
write_obs('aqup2',x,y,z)
x += 2505.
write_obs('aqup3',x,y,z)
#obs points in lower aquifer (aq2)
y = 592.5 #mid-drift
z = (ztop_shale2 + ztop_aq2)/2.
x = 2617.5 #<~30 m east of repository
write_obs('aqlow1',x,y,z)
x += 2490.
write_obs('aqlow2',x,y,z)
x += 2505.
write_obs('aqlow3',x,y,z)

#DRZ, longhall
xmin = x0-x_shaft_drz
ymin = y1-y_hall_drz
zmin = z1-z_hall_drz
xmax = xmin + x_longhall + x_shaft_drz*2.
ymax = ymin + y_longhall + y_hall_drz*2.
zmax = zmin + z_hall + z_hall_drz*2.
write_reg('drz_longhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_longhall',mat[1])
#DRZ west shaft, drz=halite;adrz=aquifer;ssdrz=siltstone
xmin = x0-x_shaft_drz
ymin = y1-y_hall_drz
zmin = z1-z_hall_drz
xmax = xmin + x_shaft + x_shaft_drz*2.
ymax = ymin + y_shaft + y_hall_drz*2.
zmax = ztop_shale3
write_reg('drz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_wshaft',mat[1])
zmin = ztop_shale3
zmax = ztop_aq3
write_reg('adrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('adrz_wshaft',mat[5])
zmin = ztop_aq3
zmax = ztop
write_reg('sdrz_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('sdrz_wshaft',mat[3])
#DRZ east shaft, drz=halite;adrz=aquifer;ssdrz=siltstone
xmin = x2 - x_shaft_drz
zmin = z1 - z_drz
xmax = xmin + x_shaft + x_shaft_drz*2.
zmax = ztop_shale3
write_reg('drz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_eshaft',mat[1])
zmin = ztop_shale3
zmax = ztop_aq3
write_reg('adrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('adrz_eshaft',mat[5])
zmin = ztop_aq3
zmax = ztop
write_reg('sdrz_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('sdrz_eshaft',mat[3])
#DRZ short hall, access halls
zmin = z1 - z_hall_drz
zmax = z1 + z_hall + z_hall_drz
#short hall connecting long hall to panel
ymin = 0.
ymax = y1 + y_longhall + y_shorthall
xmin = x1 + x_hbh - x_hall_drz
xmax = xmin + x_shorthall + 2.*x_hall_drz
write_reg('drz_shorthall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_shorthall',mat[1])
#south access hall of panel
ymin = y2 - y_hall_drz
ymax = ymin + y_access + y_hall_drz*2.
xmin = x1 + x_hbh - x_hall_drz
xmax = xmin + x_access + 2.*x_hall_drz
write_reg('drz_saccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_saccess',mat[1])
#north access hall of panel
ymin = y3 + y_drift - y_hall_drz
ymax = ymin + y_access + y_hall_drz*2.
xmin = x1 + x_hbh - x_hall_drz
xmax = xmin + x_access + 2.*x_hall_drz
write_reg('drz_naccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_naccess',mat[1])
#hbh drifts
zmin = z1 - z_hall_drz
zmax = z1 + z_hall + z_hall_drz
ymin = y3 - y_hall_drz
ymax = y3 + y_drift + y_hall_drz*2.
for i in range(num_drift):
  xmin = x1 + x_hbh - x_hall_drz + i*x_repeat
  xmax = xmin + x_drift + x_hall_drz*2.
  write_reg('drz_drift'+str(i),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_drift'+str(i),mat[1])
#horizontal boreholes
zmin = z2 - z_drz
zmax = zmin + z_hbh + 2.*z_drz
for i in range(num_drift):
  xmin = x1 - x_drz + i*x_repeat
  xmax = xmin + x_drift + 2.*x_hbh + 2.*x_drz
  for j in range(num_hbh):
    ymin = y4 - y_drz + j*y_repeat
    ymax = ymin + y_hbh + 2.*y_drz
    write_reg('drz_hbh'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('drz_hbh'+str(i)+'_'+str(j),mat[1])
#calcine drifts
zmin = z1 - z_drift_drz
zmax = zmin + z_hall + 2.*z_drift_drz
ymin = y3 - y_drift_drz
ymax = ymin + y_drift_calcine + 2.*y_drift_drz
for i in range(num_drift_calcine):
  xmin = x1 + x_repeat*num_drift - x_drift_drz + i*(x_pillar+x_drift)
  xmax = xmin + x_drift + 2.*x_drift_drz
  write_reg('drz_cal'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_cal'+str(i)+'_'+str(j),mat[1])
#naval drift
zmin = z1 - z_drift_drz
zmax = zmin + z_hall + 2.*z_drift_drz
ymin = y3 - y_drift_drz
ymax = ymin + y_drift_naval + 2.*y_drift_drz
for i in range(num_drift_naval):
  xmin += x_pillar + x_drift
  xmax = xmin + x_drift + 2.*x_drift_drz
  write_reg('drz_nav'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('drz_nav'+str(i)+'_'+str(j),mat[1])
#PICK UP HERE! I THINK DRZ IS IN THE RIGHT PLACE, NEXT IS BUFFER,
#THEN CALCINE AND NAVAL WPS.
#NOT SURE HOW FRG AND CS/SR GLASS IS GOING TO FIT IN, BECAUSE 
#HBH LOOK FULL WITH JUST HANFORD AND SAVANNAH RIVER.
#BETTER FIGURE THAT OUT BEFORE PROCEEDING.

#Buffer/Backfill
#buffer, longhall
xmin = x0
ymin = y1
zmin = z1
xmax = xmin + x_longhall
ymax = ymin + y_longhall
zmax = zmin + z_hall
write_reg('bf_longhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_longhall',mat[8])
#buffer west shaft
xmin = x0
ymin = y1
zmin = z1
xmax = xmin + x_shaft
ymax = ymin + y_shaft
zmax = ztop
write_reg('bf_wshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_wshaft',mat[8])
#buffer east shaft
xmin = x2
xmax = xmin + x_shaft
write_reg('bf_eshaft',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_eshaft',mat[8])
#buffer short hall, access halls, and disposal drifts
zmin = z1
zmax = z1 + z_hall
#short hall connecting long hall to panel
ymin = 0.
ymax = y1 + + y_longhall + y_shorthall
xmin = x1 + x_hbh
xmax = xmin + x_shorthall
write_reg('bf_shorthall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_shorthall',mat[8])
#south access hall of panel
ymin = y2
ymax = ymin + y_access
xmin = x1 + x_hbh
xmax = xmin + x_access
write_reg('bf_saccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_saccess',mat[8])
#north access hall of panel
ymin = y2 + y_access + y_drift
ymax = ymin + y_access
xmin = x1 +x_hbh
xmax = xmin + x_access
write_reg('bf_naccess',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('bf_naccess',mat[8])
#hbh drifts
ymin = y3
ymax = y3 + y_drift
for i in range(num_drift):
  xmin = x1 + x_hbh + i*x_repeat
  xmax = xmin + x_drift
  write_reg('bf_drift'+str(i),xmin,ymin,zmin,xmax,ymax,zmax)
  write_strata('bf_drift'+str(i),mat[8])
#horizontal boreholes - this will fill the hbh w/ buffer, all but seal will get overwritten with wp
zmin = z2
zmax = zmin + z_hbh
for i in range(num_drift):
  xmin = x1 + i*x_repeat
  xmax = xmin + x_drift + 2.*x_hbh
  for j in range(num_hbh):
    ymin = y4 + j*y_repeat
    ymax = ymin + y_hbh
    write_reg('bf_hbh'+str(i)+'_'+str(j),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('bf_hbh'+str(i)+'_'+str(j),mat[8])
  
#HLW Waste Packages
zmin = z2 #+ smallest #middle of drift
#zmin = z2 + smallest #middle of drift
zmax = zmin + z_wp
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = glassbins[0][1] #ditto
for i in range(num_drift_hlw):
  xleft = x1 + x_bf + i*x_repeat
  xright = xleft + (x_hbh-x_bf) + x_drift + x_seal
  for j in range(num_hbh):
    ymin = y4 + y_bf_hbh + j*y_repeat
    ymax = ymin + y_wp
    for k in range(num_wp*2):
      if k < num_wp:
        xmin = xleft + k*(x_wp+x_bf)
      else:
        xmin = xright + (k-num_wp)*(x_wp+x_bf)
      xmax = xmin + x_wp
      #write only if counter < total number of packages (0 indexed)
      if counter >= wpnum and counter < cumulative:
        name = 'hlw'+str(i)+'_'+str(j)+'_'+str(k)
        write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
        write_strata(name,glassbins[ii][0])
        write_ss(name,glassbins[ii][0],trans[0],name)
        wfg.glass_to_file_reg(wfgfile,name,glassbins[ii][2],glassbins[ii][0]) #write heterog. hlw to REGION
        counter += 1
        if counter == wpnum + glassbins[ii][1]:
          wpnum += glassbins[ii][1] #before increment ii
          ii += 1
          if ii < len(glassbins): cumulative += glassbins[ii][1] #after increment ii
        print('glass %i %i %i' % (counter, wpnum, ii))

#DSNF Waste Packages
zmin = z2 #+ smallest #middle of drift
#zmin = z2 + smallest #middle of drift
zmax = zmin + z_wp
counter = 0 #for assigning glass bins
wpnum = 0 #ditto
ii = 0 #ditto
cumulative = dsnfbins[0][1] #ditto
for i in range(num_drift_hlw,num_drift):
  xleft = x1 + x_bf + i*x_repeat
  xright = xleft + (x_hbh-x_bf) + x_drift + x_seal
  for j in range(num_hbh):
    ymin = y4 + y_bf_hbh + j*y_repeat
    ymax = ymin + y_wp
    for k in range(num_wp*2):
      if k < num_wp:
        xmin = xleft + k*(x_wp+x_bf)
      else:
        xmin = xright + (k-num_wp)*(x_wp+x_bf)
      xmax = xmin + x_wp
      #write only if counter < total number of packages (0 indexed)
      if counter >= wpnum and counter < cumulative:
        name = 'dsnf'+str(i)+'_'+str(j)+'_'+str(k)
        write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
        write_strata(name,dsnfbins[ii][0])
        write_ss(name,dsnfbins[ii][0],trans[0],name)
        wfg.dsnf_to_file_reg(wfgfile,name,dsnfbins[ii][0]) #write dsnf to REGION
        counter += 1
        if counter == wpnum + dsnfbins[ii][1]:
          wpnum += dsnfbins[ii][1] #before increment ii
          ii += 1
          if ii < len(dsnfbins): cumulative += dsnfbins[ii][1] #after increment ii
        print('dsnf %i %i %i' % (counter, wpnum, ii))

#obs points in repository - THESE NEED TO BE ADJUSTED FOR DWR
x = 1372.5 # mid repository (right hbh, middle waste package)
z = z1 + 2.5 #mid-drift, mid-wp
for i in range(3):
  y = 600.277777 - i*30. #mid-wp
  write_obs('wp'+str(i),x,y,z)
  y += smallest
  write_obs('bf'+str(i),x,y,z)
  y += smallest
  write_obs('drz'+str(i),x,y,z)

rfile.close
sfile.close
ssfile.close
obsfile.close
obsregf.close
wfgfile.close


