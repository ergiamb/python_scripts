''' This script will read a dakota tabular output for Uncertainty analysis and make scatter plots of response vs. variable.  Written by 
W. Payton Gardner, Sandia National Laboratories.
Modified 8.18.15 by ers to plot /gscratch/ergiamb/clay/richards/probabilistic'''


import scipy.stats
import pandas as pd
from pylab import *
import matplotlib as mpl
import pdb
import numpy as np

#mpl.rcParams['text.usetex']= True;
mpl.rcParams['font.size'] = 18;
mpl.rcParams['axes.labelsize'] = 18;
mpl.rcParams['xtick.labelsize'] = 14;
mpl.rcParams['ytick.labelsize'] = 14;
mpl.rcParams['axes.titlesize']= 14;
mpl.rcParams['figure.figsize']= [15.,10.];
majorFormatter = mpl.ticker.FormatStrFormatter('%3.1g');

#get the data
dd = pd.read_table('dakota_tabular.dat',delim_whitespace=True);
#here might be a good place to map dakot variable names to something sensable but I'm not quite sure how to do that yet

r_map = {}
r_map['response_fn_1']='glacial1'
r_map['response_fn_2']='glacial2'
r_map['response_fn_3']='glacial3'
r_map['response_fn_4']='dz1'
r_map['response_fn_5']='dz2'
r_map['response_fn_6']='dz3'
r_map['response_fn_7']='glacial1'
r_map['response_fn_8']='glacial2'
r_map['response_fn_9']='glacial3'
r_map['response_fn_10']='dz1'
r_map['response_fn_11']='dz2'
r_map['response_fn_12']='dz3'
#r_map['response_fn_7']='to_glacial'
#r_map['response_fn_8']='to_east'

sol_dens = 2700. 
wat_dens = 1000.
poro_gran = 0.005
poro_buff = 0.43

params = ['rateWP','rateUNF','tWP','pBuffer','pDRZ','bNpKd','gNpKd','kGlacial'];
labels = ['rateWP','rateUNF','tWP','pBuffer','pDRZ','bNpKd','gNpKd','kGlacial'];

#params = ['rateUNF','kGlacial','tWP','pDRZ','pBuffer','rateWP'];
#labels = ['rateUNF','kGlacial','tWP','pDRZ','pBuffer','rateWP'];

figcount=0

for r in dd.columns[8:15]:
    if r.split('_')[0] == 'response':
        sp_corr_I129 = [];
        #calculate spearman rank correlations
        print r
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_I129.append(spcc);
            print params[i],spcc

        #figure(1)--I-129;
        fig1,ax1 = subplots(2,2)
        subplot(2,2,1);
        semilogy(dd['rateWP'],dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Waste Package Degradation Rate (log(yr$^{-1}$))');

        subplot(2,2,2);
        semilogy(np.log10(dd['rateUNF']),dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('UNF Degradation Rate (log(yr$^{-1}$))');

        subplot(2,2,3);
        loglog(dd['tWP'],dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Waste Package Tortuosity');

        subplot(2,2,4);
        semilogy(dd['pBuffer'],dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Buffer Porosity');
        tight_layout()
        savefig(r_map[r]+'_scatter1_I129.png')

        #figure(2)--I-129;
        fig2, ax2 = subplots(2,2)
        subplot(2,2,1);
        semilogy(dd['pDRZ'],dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('DRZ Porosity');

        subplot(2,2,2);
        loglog((dd['bNpKd']/(sol_dens*wat_dens*(1.- poro_buff))),dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Buffer Neptunium Kd (m$^3$/kg)');

        subplot(2,2,3) 
        loglog((dd['gNpKd']/(sol_dens*wat_dens*(1.- poro_gran))),dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Granite Neptunium Kd (m$^3$/kg)');
	
        subplot(2,2,4) 
        loglog(dd['kGlacial'],dd[r],'ko');
        ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Glacial Seds Permeability (m$^2$)');
        tight_layout()
        savefig(r_map[r]+'_scatter2_I129.png')
	
	#subplot(2,2,3);
        #loglog(dd['kGlacial'],dd[r],'ko');
        #ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
        #xlabel('Glacial Seds Permeability');

       # subplot(2,2,4);
       # semilogy(dd['p4'],dd[r],'ko');
       # ylabel('[$^{129}$I] at ' +r_map[r]+ ' (mol/L)');
       # xlabel('Shaft Porosity');
       # tight_layout()
       # savefig(r_map[r]+'_scatter2.png')

for r in dd.columns[15:21]:
    if r.split('_')[0] == 'response':
        sp_corr_I129 = [];
        #calculate spearman rank correlations
        print r
        for i in xrange(len(params)):
            spcc = scipy.stats.spearmanr(dd[params[i]],dd[r])[0];
            sp_corr_I129.append(spcc);
            print params[i],spcc

	#figure(3)--Np-237;
        fig1,ax1 = subplots(2,2)
        subplot(2,2,1);
        semilogy(dd['rateWP'],dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Waste Package Degradation Rate (log(yr$^{-1}$))');

        subplot(2,2,2);
        semilogy(log10(dd['rateUNF']),dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('UNF Degradation Rate (log(yr$^{-1}$))');

        subplot(2,2,3);
        loglog(dd['tWP'],dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Waste Package Tortuosity');

        subplot(2,2,4);
        semilogy(dd['pBuffer'],dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Buffer Porosity');
        tight_layout()
        savefig(r_map[r]+'_scatter1_Np237.png')

	#figure(4)--Np-237;
        fig2, ax2 = subplots(2,2)
        subplot(2,2,1);
        semilogy(dd['pDRZ'],dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('DRZ Porosity');

        subplot(2,2,2);
        loglog((dd['bNpKd']/(sol_dens*wat_dens*(1.- poro_buff))),dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Buffer Neptunium Kd (m$^3$/kg)');

        subplot(2,2,3) 
        loglog((dd['gNpKd']/(sol_dens*wat_dens*(1.- poro_gran))),dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Granite Neptunium Kd (m$^3$/kg)');
	
        subplot(2,2,4) 
        loglog(dd['kGlacial'],dd[r],'ko');
        ylabel('[$^{237}$Np] at ' +r_map[r]+ ' (mol/L)');
        xlabel('Glacial Seds Permeability (m$^2$)');
        tight_layout()
        savefig(r_map[r]+'_scatter2_Np237.png')







        #bar chart 1
        #pdb.set_trace();
       # mpl.rcParams['figure.figsize']= [10.,10.];
       # mpl.rcParams['text.usetex']= False;
       # fig5 = figure();
       # ax5 = subplot(1,1,1);
       # width=0.8;
       # ax5.bar(range(len(labels)),sp_corr_I129, width=width);
       # ax5.set_xticks(arange(len(labels))+width/2)
       # ax5.set_xticklabels(labels,rotation=60);
       # hlines(0,0,width*(len(labels)+2));
       # xlabel('Parameter');
       # ylabel('Spearman Rank Correlation for max[$^{129}$I] at '+ r_map[r]);
       # ylim(ymax=1, ymin=-1)
       # savefig(r_map[r]+'_rank_corr.png')
        #close('a6ll')
    else:
        continue
