# function for calculating and assigning permeability as a function of depth
# ES 6/16/15
# log k = -3.2log(z)-14 
 
import numpy as N
import h5py as h5

def permeability(zbase, dz, ny, nx):
    '''Calculates k=f(z) (log k = -3.2*log(depth in km)-15), and
    returns an array of permeability values corresponding to cell centers for all
    or a portion of a 3d structured grid.
    
    NOTE: original reference used -14 rather than -15, but Arnold and Hagdu use -15.
    original reference = Ingebritsen and Manning 1999

    zbase = depth of base of region of interest in m below earth surface
    dz = numpy.ndarray of cell heights in meters for region of interest
    ny = number of cells in y direction
    nx = number of cells in x direction
    (nz is calculated)
    All of these values need to be positive numbers!
    
    Could use this fn to define permeability for the entire model domain (REGION all) and then
    overwrite materials for regions that have other permeability value when you couple
    material properties to regions using STRATA cards. Actually, for now that is the only way to 
    align it with create_cell_ids().
    
    ES 6/17/15'''
    
    #create and fill logk array from base of region upward
    logk = N.zeros(len(dz))      #make an ndarray the same size as dz
    zgrid = zbase
    for i in range(len(dz)):
        zcell = zgrid - dz[i]/2. #dz is positive, so subtract it to move upward
        logk[i] = -3.2*N.log10( zcell/1000. ) - 15.
        zgrid -= dz[i]           #and subtract again to move upward
        print zgrid, zcell, logk[i]
        
    #allocate array to hold permeability (m2) for each grid cell in 3d domain
    nz = len(logk)
    k_m2 = N.zeros(nz*ny*nx)
    
    #loop through to assign k_m2 to each cell
    #loop through x for each y, and thru xy for each z
    #assigned permeability depends on where we are in z (index k)
    for k in range(nz):
      for j in range(ny):
        for i in range(nx):
          index = i + j*nx + k*nx*ny
          k_m2[index] = N.power(10.,logk[k])
   
    
    #finally return k (a 1d ndarray, holding values for the 3d domain)
    return k_m2

def drz_perm(k_m2,index1,nx,ny,numz):
    '''overwrite portion of permeability array with 10x higher values. 
       assumes drz is only one cell wide and it's vertical.
       not sure overwriting works. maybe return a whole different array?
       k_m2 = nd.array of permeability values for the entire 3d grid
       index1 = the index of the bottommost drz cell (=cell_id -1)
       nx = number of cells in x dimension
       ny = number of cells in y dimension
       numz = number of z layers to fill with new permeability values

       ES 6/23/15'''

    k_new = N.zeros(len(k_m2))
    for i in xrange(len(k_m2)):
        k_new[i] = k_m2[i]  #I am doing this because I think it's necessary to not end up with a pointer
    index = index1
    for i in xrange(numz):
        k_new[index] = k_m2[index]*10
        index += ny*nx
    return k_new
#but maybe I could have passed in k_m2 and directly altered it's values without returning anything new???

def create_cell_ids(nz,ny,nx):
    '''Create and return array of cell id numbers for a 3d structured grid
       
       ES 6/17/15'''
    
    cell_id = N.zeros(nz*ny*nx,'i4') #integers
    for i in range(len(cell_id)):
        cell_id[i] = i+1 #cells are indexed from 1
    return cell_id
    
def write_mat_data(filename, cell_id, k_m2):
    '''write hdf5 dataset for use in PFLOTRAN input deck.
       filename = filename to write output to (string ending in .h5)
       cell_id = numpy.ndarray of cell id numbers (integers)
       k = numpy.ndarray of permeability (m2) corresponding to cell centers
       Will add other optional arguments as add thermal conductivity and other variations
       
       ES 6/15/17'''
    
    if len(cell_id) != len(k_m2):
        print "Arrays are not of equal length."
        return #I am sure there is a better error way to do this.   
        
    h5file = h5.File(filename,mode='w')
    dataset_name = 'Cell Ids'      #PFLOTRAN looks for 'Cell_Ids' exactly
    h5dset = h5file.create_dataset(dataset_name, data=cell_id)
    dataset_name2 = 'Permeability' #PFLOTRAN looks for 'Permeability', 
                                   #tho if you were doing realization dependent you could have 'Permeability1' etc.
    h5dset2 = h5file.create_dataset(dataset_name2, data=k_m2)
    h5file.close()
       
'''
#now try it out:
zbase = 2200.
dz = N.array([100.,200.,300.,200.,400.,1000])
ny = 2
nx =3

permeability = permeability(zbase, dz, ny, nx)
cell_ids = create_cell_ids(len(dz), ny, nx)
write_mat_data('testperm.h5', cell_ids, permeability)
'''
