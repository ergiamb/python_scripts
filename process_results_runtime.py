# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 15:06:16 2012
@author: wpgardn
modified by Emily 7.14.16
"""

import read_obs_point as rop

#split glacial1, granite3
rop.SplitPflotranObsPtMultipt(obsptfile='pflotran-obs-16.tec',numfields=63)
#split dz4, glacial2, granite6
rop.SplitPflotranObsPtMultipt(obsptfile='pflotran-obs-13.tec',numfields=63)
#split dz2, glacial3, granite7, granite8
rop.SplitPflotranObsPtMultipt(obsptfile='pflotran-obs-25.tec',numfields=63)
#split to_glacial, to_east
rop.SplitPflotranObsPtMultipt(obsptfile='pflotran-int.dat',numfields=63)

#glacial1
dd1 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-16_0.tec');
#glacial2
dd2 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-13_1.tec');
#glacial3
dd3 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-25_1.tec');
#dz1
dd4 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-117.tec');
#dz2
dd5 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-25_0.tec');
#dz3
dd6 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-5.tec');
#integral_flux to_glacial
dd7 = rop.ReadPflotranObsPt(obsptfile='pflotran-int_0.tec');
#integral_flux to_east
dd8 = rop.ReadPflotranObsPt(obsptfile='pflotran-int_1.tec');

# calculate senstivities only on the observation for now
###I129
#glacial1
print max(dd1['Total I129 [M]']);
#glacial2
print max(dd2['Total I129 [M]']);
#glacial3
print max(dd3['Total I129 [M]']);
#dz1
print max(dd4['Total I129 [M]']);
#dz2
print max(dd5['Total I129 [M]']);
#dz3
print max(dd6['Total I129 [M]']);
###Np237
#glacial1
print max(dd1['Total Np237 [M]']);
#glacial2
print max(dd2['Total Np237 [M]']);
#glacial3
print max(dd3['Total Np237 [M]']);
#dz1
print max(dd4['Total Np237 [M]']);
#dz2
print max(dd5['Total Np237 [M]']);
#dz3
print max(dd6['Total Np237 [M]']);
### integral fluxes I129 and then Np237
#to_glacial
print max(dd7['to_glacial I129 [mol]']);
#to_east
print max(dd8['to_east I129 [mol]']);
#to_glacial
print max(dd7['to_glacial Np237 [mol]']);
#to_east
print max(dd8['to_east Np237 [mol]']);
