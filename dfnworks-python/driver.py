#!/usr/bin/env python

import os,sys
from dfnworks import *

dfn = dfnworks(inp_file='full_mesh.inp',perm_file='perm.dat',aper_file='aperture.dat')
dfn.lagrit2pflotran()
#dfn.zone2ex(zone_file='pboundary_back_n.zone',face='north')
#dfn.zone2ex(zone_file='pboundary_front_s.zone',face='south')
dfn.zone2ex(zone_file='pboundary_left_w.zone',face='west')
dfn.zone2ex(zone_file='pboundary_right_e.zone',face='east')
#os.system('mpirun -np 1 $PFLOTRAN_DIR/src/pflotran/pflotran -pflotranin dfn_st.in')
dfn.parse_pflotran_vtk()
