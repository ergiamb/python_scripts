#make regions, strata, initial condition, and source sink cards to go with 2017wipp/ 3d grid
#Simple 3D grid for comparison with BRAGFLO 2D flaring grid as implemented in PFLOTRAN
#? making boundary condition cards in here, and flow and transport conditions, but not materials
#emily 1.23.17

import wfg_block as wfg
import math as math

#hardwire some names, copied from Heeho's input deck
mat = ['INACTIVE','S_HALITE','DRZ_0','S_MB139','S_ANH_AB', #0-4
       'S_MB138','CAVITY_1','CAVITY_2','CAVITY_3','CAVITY_4', #5-9
       'IMPERM_Z','CASTILER','OPS_AREA','EXP_AREA','CULEBRA', #10-14
       'MAGENTA','DEWYLAKE','SANTAROS','WAS_AREA','DRZ_1', #15-19
       'DRZ_PCS','PCS_T1','CONC_PLG','BH_OPEN','BH_SAND', #20-24
       'BH_CREEP','UNNAMED','TAMARISK','FORTYNIN','BOREHOLE', #25-29
       'PCS_T2','REPOSIT','CONC_MON','SHFTU','SHFTL_T1', #30-34
       'SHFTL_T2','PCS_T3']                              #35-36
flow = ['FC_ALL','FC_R_DRZ_1','FC_R_WAS_AREA']
trans = ['initial','contaminant']

#define dimensions (for the most part)
small = 5. #smallest z discretization

zbase = 0. #base of domain
z1 = 125.83 #top of brine pocket
z2 = z1 + 52.27 #top of Castile, base of Salado
z3 = z2 + 66.72+66.72+65.72+1.00 #base of MB139 and lower DRZ
z4 = z3 + 0.85 #top of MB139
z5 = z4 + 0.69*2. #top of lower DRZ, base of waste panels
z6 = z5 + 1.32*3. #top of waste panels, base of upper DRZ
z7 = z6 + 2.62    #base of anhydrite AB
z8 = z7 + 0.27    #top of anhydrite AB
z9 = z8 + 4.53*2. #top of upper DRZ, base of MB138
z10 = z9 + 0.18   #top of MB138
z11 = z10 + 54.73*7 #top of Salado
z12 = z11 + 36.   #top of Unnamed (Los Medanos)
z13 = z12 + 7.7   #top of Culebra
z14 = z13 + 24.8  #top of Tamarisk
z15 = z14 + 8.5   #top of Magenta
z16 = z15 + 17.3  #top of Fortyniner
z17 = z16 + 106. + 43.30  #top of Dewey Lake
z18 = z17 + 15.66 + 0.1   #top of Santa Rosa, top of domain
ztop = z18
z_lbh = z5 #height of lower borehole
z_ubh = z18 - z5 #the rest of the borehole
z_lplug = 36. #It's in the Los Medanos/Unnamed
z_uplug = 15.66+0.10 #It's in the Santa Rosa
z_monolith = z7 - z4
z_lshft = z11 - z8 #assign materials to shaft before assigning interbeds
z_ushft = z18 - z11 #from Unnamed upward

xleft = 0. #this is really south
xright = 4500. #this is really north
x1 = 1860. #I think
x_panel = 110.
x_pillar = 60.
x_pcs1 = 10
x_pcs2 = 40.
x_pcs3 = 40.
x_sror = 300.
x_nror = 280.
x_ops = 100.
x_exp = 100.
x_repo = x_panel*4. + x_pillar*4. + x_pcs3 + x_ops + x_exp

yfront = 0.
yback = 4500.
y1 = 2070. #start of exp
y2 = 2120. #start of waste panel
y3 = 2150. #start of RoR panels
y_panel = 100.
y_pcs = 30.
y_sror = 30.
y_nror = 30.
y_ops = 30.
y_exp = 360.
y_repo = y_exp

#write region, strata, and waste form blocks
#order matters
rfile = file('regions.txt', 'w')
sfile = file('strata.txt', 'w')
cfile = file('condition.txt','w')
#ssfile = file('source_sink.txt','w') #no heat in wipp
wfgfile = file('wfg.txt','w')
wfg.dbh_to_file(wfgfile) #inventory for Cs and Sr capsules

#files need to be open already, and closed afterward!
#someday I should put these functions in a separate file
def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_polygon(rname,x1,y1,zmin,zmax,radius,angle,pt2,pt1=0,closeloop=0): #this is totally not general
  rfile.write('REGION %s\n  POLYGON\n    TYPE CELL_CENTERS_IN_VOLUME\n    XY\n'
              % (rname))
  for i in range(pt1,pt2):
    x = x1 + radius*(math.cos(math.radians(angle*i)))
    y = y1 + radius*(math.sin(math.radians(angle*i))) 
    rfile.write('    %f %f 0.0\n' % (x,y))
  if (closeloop != 0):
    rfile.write('    %f %f 0.0\n' % (x1,y1)) #hopefully this order works for both left and right halves
  rfile.write('    /\n    XZ\n'+
              '    -1.d20 0.0 %f\n     1.d20 0.0 %f\n     1.d20 0.0 %f\n    -1.d20 0.0 %f\n    /\n  /\nEND\n\n'
              % (zmin,zmin,zmax,zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ic(flow,trans,rname,cfile=cfile):
  cfile.write('INITIAL_CONDITION\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
              % (flow, trans, rname))
  return

def write_bc(flow,trans,rname,cfile=cfile):
  cfile.write('BOUNDARY_CONDITION %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
              % (rname,flow,trans,rname))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

#Model Domain - cut and paste this first region into the main input deck
write_reg('all',xleft,yfront,zbase,xright,yback,ztop)
write_ic(flow[0],trans[0],'all')
write_strata('all',mat[1]) #begin with everything is Salado and then overwrite
#not sure about BC, probably should be no flow left, right, front, back
write_reg_face('top',xleft,yfront,ztop,xright,yback,ztop)
write_bc(flow[0],trans[0],'top')
write_reg_face('bottom',xleft,yfront,zbase,xright,yback,zbase)
write_bc(flow[0],trans[0],'bottom')
write_reg_face('west',xleft,yfront,zbase,xleft,yback,ztop)
write_bc(flow[0],trans[0],'west') #these are pflotran reference directions, not wipp directions
write_reg_face('east',xright,yfront,zbase,xright,yback,ztop)
write_bc(flow[0],trans[0],'east')
write_reg_face('south',xleft,yfront,zbase,xright,yfront,ztop)
write_bc(flow[0],trans[0],'south')
write_reg_face('north',xleft,yback,zbase,xright,yback,ztop)
write_bc(flow[0],trans[0],'north')

#sedimentary strata,everything but marker beds
write_reg('castile',xleft,yfront,zbase,xright,yback,z2)
write_strata('castile',mat[10]) #this should be inactive
write_reg('brinepocket',x1,y1,zbase,x1+x_repo,y1+y_repo,z1)
write_strata('brinepocket',mat[11]) #this is inactive in undisturbed scenario
write_reg('unnamed',xleft,yfront,z11,xright,yback,z12)
write_strata('unnamed',mat[26])
write_reg('culebra',xleft,yfront,z12,xright,yback,z13)
write_strata('culebra',mat[14])
write_reg('tamarisk',xleft,yfront,z13,xright,yback,z14)
write_strata('tamarisk',mat[27])
write_reg('magenta',xleft,yfront,z14,xright,yback,z15)
write_strata('magenta',mat[15])
write_reg('49er',xleft,yfront,z15,xright,yback,z16)
write_strata('49er',mat[28])
write_reg('deweylake',xleft,yfront,z16,xright,yback,z17)
write_strata('deweylake',mat[16])
write_reg('santarosa',xleft,yfront,z17,xright,yback,ztop)
write_strata('santarosa',mat[17])

#in and around repository
write_reg('wastepanel',x1,y2,z5,x1+x_panel,y1+y_panel,z6)
write_strata('wastepanel',mat[18])
write_reg('pcs1',x1,y2,z5,x1+x_panel,y1+y_panel,z6) #these numbers all need fixing
write_strata('pcs1',mat[21])
write_reg('pcs2',x1,y2,z5,x1+x_panel,y1+y_panel,z6) #these numbers all need fixing
write_strata('pcs2',mat[21])
write_reg('pcs2',x1,y2,z5,x1+x_panel,y1+y_panel,z6) #these numbers all need fixing
write_strata('pcs2',mat[21])

#marker beds

#upper and lower DRZ

#borehole

#DRZ granite, needs to be in 4 pieces in order to skip the fracture
#2 left/west pieces, above and below fracture
xmin = (xleft+xright)/2.# - r_drz
ymin = yfront
zmin = z1 #base of bh
zmax = zfwb #base of left fracture at drz
radius = r_drz
write_polygon('drz_granite_wb',xmin,ymin,zmin,zmax,radius,angle,9,pt1=4,closeloop=1)
write_strata('drz_granite_wb',mat[1])
zmin = zfwt
zmax = zgranite
write_polygon('drz_granite_wt',xmin,ymin,zmin,zmax,radius,angle,9,pt1=4,closeloop=1)
write_strata('drz_granite_wt',mat[1])
#2 right/east pieces, above and below fracture
zmin = z1
zmax = zfeb
write_polygon('drz_granite_eb',xmin,ymin,zmin,zmax,radius,angle,5,pt1=0,closeloop=1)
write_strata('drz_granite_eb',mat[1])
zmin = zfet
zmax = zgranite
write_polygon('drz_granite_et',xmin,ymin,zmin,zmax,radius,angle,5,pt1=0,closeloop=1)
write_strata('drz_granite_et',mat[1])

#DRZ seds
zmin = zgranite
zmax = ztop
write_polygon('drz_sed',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('drz_sed',mat[10])

#Borehole radius
#Annulus (brine)
xmin = (xleft+xright)/2. #- r_annulus
ymin = yfront
zmin = z1
zmax = zstuck + z_wp #open borehole to stuck waste package
radius = r_annulus
write_polygon('brine',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('brine',mat[5])
write_ic(flow[0],trans[3],'brine')
#Cement plug (lower)
zmin = z2
zmax = zmin + z_cement_ez
write_polygon('cement1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('cement1',mat[6])
write_ic(flow[0],trans[3],'cement1')
#Cement plug (upper)
zmin = z3
zmax = zmin + z_cement_ez
write_polygon('cement2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('cement2',mat[6])
write_ic(flow[0],trans[3],'cement2')
#Cement seal 1 (lowest), above the stuck waste package
zmin = zstuck + z_wp
zmax = zmin + small*20. #100 m
write_polygon('scement1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement1',mat[6])
write_ic(flow[0],trans[3],'scement1')
#Bentonite seal 1
zmin = zmax
zmax = zmin + small*10. #50 m
write_polygon('bentonite1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('bentonite1',mat[7])
write_ic(flow[0],trans[2],'bentonite1')
#Cement seal 2
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement2',mat[6])
write_ic(flow[0],trans[3],'scement2')
#Ballast 1
zmin = zmax
zmax = zmin + small*10. #50 m
write_polygon('ballast1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast1',mat[8])
write_ic(flow[0],trans[3],'ballast1')
#Cement seal 3
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement3',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement3',mat[6])
write_ic(flow[0],trans[3],'scement3')
#Bentonite seal 2
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('bentonite2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('bentonite2',mat[7])
write_ic(flow[0],trans[2],'bentonite2')
#Cement seal 4
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement4',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement4',mat[6])
write_ic(flow[0],trans[3],'scement4')
#Ballast 2
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('ballast2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast2',mat[8])
write_ic(flow[0],trans[3],'ballast2')
#Cement seal 5
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement5',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement5',mat[6])
write_ic(flow[0],trans[3],'scement5')
#Bentonite seal 3
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('bentonite3',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('bentonite3',mat[7])
write_ic(flow[0],trans[2],'bentonite3')
#Cement seal 6
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement6',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement6',mat[6])
write_ic(flow[0],trans[3],'scement6')
#Ballast 3
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('ballast3',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast3',mat[8])
write_ic(flow[0],trans[3],'ballast3')
#Cement seal 7 (top)
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement7',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement7',mat[6])
write_ic(flow[0],trans[3],'scement7')
#Additional seal we'll do entirely with ballast for now
zmin = zmax
zmax = ztop
write_polygon('ballast4',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast4',mat[8])
write_ic(flow[0],trans[3],'ballast4')

#Waste Package - Cs
xmin = (xleft+xright)/2. #- r_wp
ymin = yfront
radius = r_wp
for k in range(num_cs):
  if (k <= 39): #below cement plug
    zmin = z1 + z_wp*k
  else: #above cement plug
    zmin = z1 + z_wp*k + z_cement_ez
  zmax = zmin + z_wp
  name = 'cs_wp'+str(k)
  write_polygon(name,xmin,ymin,zmin,zmax,radius,angle,9)
  write_strata(name,mat[2])
  write_ss(name,flow[1],trans[3],name)
  wfg.cs_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

zsr = zmax
#Waste Package - Sr
for k in range(num_sr-1): #very last Sr wp is the one that gets stuck, subtract it
  if (k <= 5): #below cement plug, totally not automated
    zmin = zsr + z_wp*k
  else: #above cement plug
    zmin = zsr + z_wp*k + z_cement_ez
  zmax = zmin + z_wp
  name = 'sr_wp'+str(k)
  write_polygon(name,xmin,ymin,zmin,zmax,radius,angle,9)
  write_strata(name,mat[3])
  write_ss(name,flow[2],trans[3],name)
  wfg.sr_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

#Waste Package - Stuck (Sr)
zmin = zstuck
zmax = zmin + z_wp
name = 'sr_wp_stuck'
write_polygon(name,xmin,ymin,zmin,zmax,radius,angle,9)
write_strata(name,mat[3])
write_ss(name,flow[2],trans[3],name)
wfg.sr_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

rfile.close
sfile.close
ssfile.close
wfgfile.close


