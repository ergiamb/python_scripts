# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 15:06:16 2012

@author: wpgardn

modified 12.4.16 Emily
"""

import cPickle as pickle
import read_obs_point as rop

dd_obs_dict = {}; #this dictionary will include all data at each of the following points:
                  #so we can get I129, Np237 horsetails and also means for other radionuclides
                  #out of dfile.dat
#glacial1
dd1 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-16_0.tec');
dd_obs_dict['glacial1']=dd1
#glacial2
dd2 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-13_1.tec');
dd_obs_dict['glacial2']=dd2
#glacial3
dd3 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-25_1.tec');
dd_obs_dict['glacial3']=dd3
#dz1
dd4 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-117.tec');
dd_obs_dict['dz1']=dd4
#dz2
dd5 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-25_0.tec');
dd_obs_dict['dz2']=dd5
#dz3
dd6 = rop.ReadPflotranObsPt(obsptfile='pflotran-obs-5.tec');
dd_obs_dict['dz3']=dd6

#make data_dictionary
try:
    f = file('../dfile.dat','r');
except IOError:
    f = file('../dfile.dat','w');
    ddict={0:dd_obs_dict};
    pickle.dump(ddict,f);
    f.close();
else:
# avoid trampling...
    try:
		ddict=pickle.load(f);
    except EOFError:
		time.sleep(10);
		ddict=pickle.load(f);
		f.close();
    f=file('../dfile.dat','w');   
    ddict[max(ddict.keys())+1]=dd_obs_dict;
    pickle.dump(ddict,f);
    f.close();

