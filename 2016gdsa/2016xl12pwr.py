#make regions, strata, initial condition, source sink and waste form cards to go with cubit grid (/biggrid/grid2)
#not making boundary condition cards in here, or flow and transport conditions, or actual materials
#11.16.16 Use WF regions instead of point sources (single coordinates)

import wfg_block as wfg

#hardwire some names
mat = ['granite','drz','buffer','wp']
flow = ['initial','wp_heatsource']
trans = ['initial']

#define dimensions (for the most part)
small = 5./3. #smallest division of material

x1 = 595. + 2.*small #west end of repository drz
x2 = x1 + small #start of buffer
y1 = 600. #south face of repository
y2 = y1 + small #start of buffer (inside hallway)
y3 = y1 + 825. #north face of repository
y4 = y3 - small #end of buffer (inside hallway)
z1 = 670. #base of repository #this might be only change i need to make
z2 = z1 + small #start of buffer

xwest = 0.
xeast = 3015.
ysouth = 0.
ynorth = 2025.
zbase = 0.
ztop = 1260.

#x drift dimensions repeat
x_drz = 5.*small
x_buff = 3.*small
x_wp = small
x_driftspacing = 20.
#x hall dimensions happen once
x_drz_hall = 830. - small
x_buff_hall = x_drz_hall - 2.*small

#y drift dimensions
y_drz_drift = 825. - 12.*small #overlaps drz on inside of hallway
y_buff_drift = y_drz_drift   #needs to overwrite drz on inside of hallway
y_wp = 5. #repeats
y_wpspacing = 10.
#y hall dimensions
y_drz_hall = 7.*small
y_buff_hall = y_drz_hall - 2.*small

#z drz and buffer are the same in drifts and hall
z_drz = 5.*small
z_buff = 3.*small
z_wp = small

num_drifts = 42
num_wp = 80 #per drift

#write region, strata, and condition coupler blocks
#model domain, then DRZ, then buffer, then wp
rfile = file('regions.txt', 'w')
sfile = file('strata.txt', 'w')
ssfile = file('source_sink.txt','w')
wfgfile = file('wfg.txt','w')
wfg.gdsa_to_file(wfgfile)

def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

#Model Domain
#write_reg('all',xwest,ysouth,zbase,xeast,ynorth,ztop)
#write_strata('all',mat[0])
write_reg_face('top',xwest,ysouth,ztop,xeast,ynorth,ztop)
write_reg_face('bottom',xwest,ysouth,zbase,xeast,ynorth,zbase)
write_reg_face('west',xwest,ysouth,zbase,xwest,ynorth,ztop)
write_reg_face('east',xeast,ysouth,zbase,xeast,ynorth,ztop)
write_reg_face('south',xwest,ysouth,zbase,xeast,ysouth,ztop)
write_reg_face('north',xwest,ynorth,zbase,xeast,ynorth,ztop)
#'all',repository region and glacial seds are defined in input deck

#DRZ, hallway then drift
xmin = x1
ymin = y1
zmin = z1
xmax = xmin+x_drz_hall
ymax = ymin+y_drz_hall
zmax = zmin+z_drz
write_reg('drz_southhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_southhall',mat[1])
ymin = y3-y_drz_hall
ymax = y3
write_reg('drz_northhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('drz_northhall',mat[1])

ymin = y1 + 6.*small #overlaps drz on inside of hallway
zmin = z1
ymax = ymin + y_drz_drift
zmax = zmin + z_drz
for i in range(num_drifts):
    xmin = x1 + i*x_driftspacing 
    xmax = xmin + x_drz
    write_reg('drz_drift'+str(i),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('drz_drift'+str(i),mat[1])

#Buffer, hallway then drift
xmin = x2
ymin = y2
zmin = z2
xmax = xmin+x_buff_hall
ymax = ymin+y_buff_hall
zmax = zmin+z_buff
write_reg('buff_southhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('buff_southhall',mat[2])
ymin = y4-y_buff_hall
ymax = y4
write_reg('buff_northhall',xmin,ymin,zmin,xmax,ymax,zmax)
write_strata('buff_northhall',mat[2])

ymin = y1+6.*small #same length as drift drz
zmin = z2
ymax = ymin + y_buff_drift
zmax = zmin + z_buff
for i in range(num_drifts):
    xmin = x2 + i*x_driftspacing 
    xmax = xmin + x_buff
    write_reg('buff_drift'+str(i),xmin,ymin,zmin,xmax,ymax,zmax)
    write_strata('buff_drift'+str(i),mat[2])

#Waste Packages  WOW THAT'S A LOT! 42*80
zmin = z2+small
zmax = zmin + z_wp
for i in range(num_drifts):
    xmin = x2+small + i*x_driftspacing 
    xmax = xmin + x_wp
    for j in range(num_wp):
        ymin = y1+9.*small + j*y_wpspacing
        ymax = ymin + y_wp
        name = 'wp'+str(i)+'_'+str(j)
        write_reg(name,xmin,ymin,zmin,xmax,ymax,zmax)
        write_strata(name,mat[3])
        write_ss(name,flow[1],trans[0],name)
        wfg.csnf_to_file_reg(wfgfile,name)

rfile.close
sfile.close
ssfile.close
wfgfile.close


