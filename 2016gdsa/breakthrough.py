
'''
Plot DFN and CPM normalized cumulative breakthrough curves.
11.22.16
'''

import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pflotran as pft

mpl.rcParams['font.size']=12

def make_subplots(nrow, ncol, nplots, filenames, columns, name, titles):
  for iplot in range(nplots):
    plt.subplot(nrow,ncol,iplot+1)
    plt.title(titles[iplot])
    plt.xlabel('Time (years)', fontsize=14, fontweight='bold')
#   if iplot == 0:
    plt.ylabel('Tracer (fraction of total)', fontsize=14, fontweight='bold')

    plt.ylim(0.,1.1)
    plt.xlim(1.e-0,1.e6)
    plt.grid(True)

    for icol in range(iplot*len(columns)/nplots,iplot*len(columns)/nplots+len(columns)/nplots):
      ifile = icol
      data = pft.Dataset(filenames[ifile],1,columns[icol])
    #label with column header or shortened column header
      #string=data.get_name('yname')
      #string = data.get_name('yname').split()[3].split('_')[1]
    #or label with file name
      #string = filenames[ifile]
    #or label with name assigned above
      string = name[icol]
    #plot it (need -data.get_array('y') for cpm if want to plot on loglog scale)
      plt.semilogx(data.get_array('x'),data.get_array('y')/data.get_array('y')[-1],color=colors[icol],linestyle=linestyles[icol],label=string)
      #plt.loglog(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)
      #plt.plot(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol], label=string)
  return

path = []
path.append('.')

nsim = 10

titles = []
files = []
columns = []
names = []
linestyles = []
colors = []
basestyles = ['-' for x in range(nsim)] #['-','--','-.',':']
basecolors = ['indigo','mediumvioletred','crimson','darkorange',
              'gold','yellowgreen','green','teal','cadetblue',
              'deepskyblue']

#CPM first
titles.append('a. ECPM normalized cumulative breakthrough')
for i in range(nsim):
  files.append('./small'+str(i+1)+'/cpm/pflotran-mas.dat')
  columns.append(12) #east Tracer [mol]
  names.append('small'+str(i+1))
linestyles.extend(basestyles)
colors.extend(basecolors)

#DFN next
titles.append('b. DFN normalized cumulative breakthrough')
for i in range(nsim):
  files.append('./small'+str(i+1)+'/dfnpflo/pflotran-mas.dat')
  columns.append(17) #Region outflow Total Mass [kg], which is really moles! mislabeled
  names.append('small'+str(i+1))
linestyles.extend(basestyles)
colors.extend(basecolors)

filenames = pft.get_full_paths(path,files)

f = plt.figure(figsize=(16,8)) #adjust size to fit plots (width, height)
#f.suptitle('Cumulative Breakthrough Curves',fontsize=16)

make_subplots(1,2,2,filenames,columns,names,titles)

#ALL THIS STUFF CAN GO ONCE EVERYTHING IS GOOD. IT'S ALREADY A MESS.
#Extra - plot corrected small6/cpm_dzg
#plt.subplot(1,2,1)
#data = pft.Dataset(filenames[6],1,columns[6])
#ydata = -data.get_array('y')-8.51913792E-07*data.get_array('x')
#plt.semilogx(data.get_array('x'),ydata,color=colors[6],linestyle=':',label='corrected',linewidth=3)
#And corrected small6/dfn_nodiffusion
#data = pft.Dataset(filenames[13],1,columns[13])
#slope = (data.get_array('y')[1:]-data.get_array('y')[:-1])/data.get_array('x')[1:]
#for i in range(len(slope)):
#  print('%e %e' % (data.get_array('x')[1:][i],slope[i]))
plt.subplot(1,2,2)
data = pft.Dataset('./small6/dfn_nodiffusion/pflotran-mas.dat',1,17)
plt.semilogx(data.get_array('x'),data.get_array('y')/data.get_array('y')[-1],color='darkorange',linestyle='--',label='dfn_nodiffusion',linewidth=3)
#ydata = data.get_array('y')-1.66098582E-04 #subtract bulk sum before break in slope
#for i in range(len(ydata)):                #value is amazingly close to 8.5e-7 mol/y * 200 y (1.7e-4)
#  if (ydata[i] <= 0.):
#    ydata[i] = 1.e-20
#plt.semilogx(data.get_array('x'),ydata,color=colors[13],linestyle=':',label='corrected',linewidth=3)

#Choose a location for the legend. #This is associated with the last plot
#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
#plt.legend(loc=(1.20,0.175),title='FRACTURE REALIZATION')
plt.legend(loc=0,title='FRACTURE REALIZATION')
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
#plt.setp(plt.gca().get_legend().get_texts(),fontsize='large')
#plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
#plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
#plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

#adjust blank space and show/save the plot
f.subplots_adjust(hspace=0.2,wspace=0.2, bottom=.12,top=.9, left=.14,right=.9)
plt.savefig('small_breakthrough.png',bbox_inches='tight')
plt.show()
                                                                                               
