#cannibalizing Glenn's script observation_processor.py to make one that just lists what all the obs files contain. emily 2.1.16
import sys
import math

#run this from outside the simulation directory
#and only use one path at a time, or disaster will occur
paths = []
paths.append('domain1')
#or else use this:
#paths.append('.')

names = []
names.append('pflotran-obs-10.tec')
names.append('pflotran-obs-237.tec')
names.append('pflotran-obs-27.tec')
names.append('pflotran-obs-32.tec')
names.append('pflotran-obs-340.tec')
names.append('pflotran-obs-347.tec')
names.append('pflotran-obs-424.tec')
names.append('pflotran-obs-508.tec')
names.append('pflotran-obs-68.tec')
names.append('pflotran-obs-70.tec')
names.append('pflotran-obs-7.tec')
names.append('pflotran-obs-8.tec')

filenames = []
for ipath in range(len(paths)):
  for iname in range(len(names)):
    filename = []
    filename.append(paths[ipath])
    filename.append(names[iname])
    filename = '/'.join(filename)
    filenames.append(filename)

fileout = open(paths[0]+'_obs_list.txt', 'w')
files = []
# open files and determine column ids
for ifilename in range(len(filenames)):
  fileout.write(filenames[ifilename]+'\n')
  files.append(open(filenames[ifilename],'r'))
  header = files[ifilename].readline()
  headings = header.split(',')
  for icol in range(len(headings)):
    fileout.write('%d: %s\n' % (icol+1, headings[icol]))
fileout.close()
for ifile in range(len(files)):
  files[ifile].close()
print 'done'






#Don't need the rest of this:
'''
  if ifilename == 0: # only read header from first file
    while(1):
      headings = header.split(',')
      for icol in range(len(headings)):
        headings[icol] = headings[icol].replace('"','')
        print '%d: %s' % (icol+1,headings[icol])
      print 'Enter the column ids for the desired data, delimiting with spaces:'
      s = raw_input('-> ')
      w = s.split()
      columns = []
      columns.append(int(0))
      for i in range(len(w)):
        if not w[i].isdigit():
          print 'Entry %s not a recognized integer' % w[i]
        else:
          icol = int(w[i])-1
          if icol > 0:
            columns.append(int(w[i])-1)
      print 'Desired columns include:'
      for i in range(len(columns)):
        print '%d: %s' % (i+1,headings[columns[i]])
      yes = 0
      while(1):
        s = raw_input('Are these correct [y/n]?: ').lstrip().lower()
        if (s.startswith('y') and len(s) == 1) or \
           (s.startswith('yes') and len(s) == 3):
          yes = 1
          break
      if yes == 1:
        break
    
filename = 'processor_output.txt'
fout = open(filename,'w')

# write out filename header
for ifile in range(len(files)):
  for icol in range(len(columns)):
    if icol == 0:
      fout.write(filenames[ifile].strip())
    fout.write('\t')
  fout.write('\t')
fout.write('\n')
# write out variable header
for ifile in range(len(files)):
  for icol in range(len(columns)):
    fout.write(headings[columns[icol]])
    fout.write('\t')
  fout.write('\t')
fout.write('\n')
while(1):
  end_of_all_files_found = 1
  for ifile in range(len(files)):
    s = files[ifile].readline()
    w = s.split()
    if len(w) >= len(columns): # ensure that a valid line was read
      end_of_all_files_found = 0
      for icol in range(len(columns)): # skip time (only printed once)
        fout.write(w[columns[icol]])
        fout.write('\t')
    else:
      for icol in range(len(columns)):
        fout.write(' \t')
    fout.write('\t')
  fout.write('\n')
  if end_of_all_files_found == 1:
    break
      
for ifile in range(len(files)):
  files[ifile].close()
fout.close()
'''

  
