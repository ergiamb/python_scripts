import random
import math

def triangular_distribution(a,b,c,rand):
  if rand < (c-a)/(b-a):
    return math.sqrt(rand*(b-a)*(c-a))+a
  else:
    return b-math.sqrt((1.-rand)*(b-a)*(b-c))

wfg_simulation_template = \
'''#Place this section in the SIMULATION block. w/o the skip/noskip   

skip
    WASTE_FORM wf_general
      TYPE GENERAL
    / 
noskip

'''

pwr_60GWdMTU_150yOoR_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE
  MECHANISM CUSTOM
    NAME pwr_60GWdMTU_150yOoR
    FRACTIONAL_DISSOLUTION_RATE 1.d-6 1/day #deterministic rate Mariner et al 2015
    MATRIX_DENSITY 10970. #also really dense
    SPECIES
    !mass fractions are g/g fuel for CSNF, 60GWd/MTHM burnup, 150 yrs OoR
    !decayed from Carter et al 2013 Table C-1 using Christine Stockman's spreadsheet
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  9.42d-4  0.0d0  Np237
      Am243  243.06d0  2.98d-12  1.86d-4  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.33d-4  0.0d0   U234
      Pu239  239.05d0  9.01d-13  5.14d-3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  2.84d-3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  5.67d-4  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.05d-3  0.0d0   U233
       U233  233.04d0  1.38d-13  4.61d-8  0.0d0  Th229
       U234  234.04d0  8.90d-14  4.18d-4  0.0d0  Th230
       U236  236.05d0  9.20d-16  4.37d-3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  6.32d-1  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  1.84d-11 0.0d0
      Th230  230.03d0  2.75d-13  1.26d-7  0.0d0  Ra226 
      Ra226  226.03d0  1.37d-11  7.04d-11 0.0d0
       Tc99   98.91d0  1.04d-13  8.89d-4  0.07d0 #Johnson et al 2005 IRF
      I129   128.90d0  1.29d-15  2.17d-4  0.10d0 #Johnson et al 2005 IRF
      Cs135  134.91d0  9.550d-15 5.36d-4  0.10d0 #Johnson et al 2005 IRF
       Cl36   35.97d0  7.297d-14 3.48d-7  0.16d0 #Johnson et al 2005 IRF
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM CUSTOM

END_WASTE_FORM_GENERAL
noskip

'''

#2019 note: gdsa_template dates from 2016 and 2017 xline and shale ref cases, 
#which considered one version of CSNF only.
#it is equivalent to pwr_60GWdMTU_100yOoR in updated naming convention
gdsa_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE
  MECHANISM CUSTOM
    NAME csnf
    FRACTIONAL_DISSOLUTION_RATE 1.d-6 1/day #deterministic rate Mariner et al 2015
    MATRIX_DENSITY 10970. #also really dense
    SPECIES
    !mass fractions are g/g fuel for CSNF, 60GWd/MTHM burnup, 100 yrs OoR
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.01d-3  0.0d0  Np237
      Am243  243.06d0  2.98d-12  1.87d-4  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.97d-4  0.0d0   U234
      Pu239  239.05d0  9.01d-13  5.14d-3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  2.85d-3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  5.67d-4  0.0d0   U238
      Np237  237.05d0  1.03d-14  9.72d-4  0.0d0   U233
       U233  233.04d0  1.38d-13  3.01d-8  0.0d0  Th229
       U234  234.04d0  8.90d-14  3.55d-4  0.0d0  Th230
       U236  236.05d0  9.20d-16  4.35d-3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  6.32d-1  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  1.03d-11 0.0d0
      Th230  230.03d0  2.75d-13  7.22d-8  0.0d0  Ra226 
      Ra226  226.03d0  1.37d-11  2.77d-11 0.0d0
       Tc99   98.91d0  1.04d-13  8.89d-4  0.07d0 #Johnson et al 2005 IRF
      I129   128.90d0  1.29d-15  2.17d-4  0.10d0 #Johnson et al 2005 IRF
      Cs135  134.91d0  9.550d-15 5.36d-4  0.10d0 #Johnson et al 2005 IRF
       Cl36   35.97d0  7.297d-14 3.48d-7  0.16d0 #Johnson et al 2005 IRF
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM CUSTOM

END_WASTE_FORM_GENERAL
noskip

'''

pwr_40GWdMTU_100yOoR_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE
  MECHANISM CUSTOM
    NAME pwr_40GWdMTU_100yOoR
    FRACTIONAL_DISSOLUTION_RATE 1.d-6 1/day #deterministic rate Mariner et al 2015
    MATRIX_DENSITY 10970. #also really dense
    SPECIES
    !mass fractions are g/g fuel for CSNF, 40GWd/MTHM burnup, 100 yrs OoR
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  9.45d-4  0.0d0  Np237
      Am243  243.06d0  2.98d-12  9.57d-5  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  8.40d-5  0.0d0   U234
      Pu239  239.05d0  9.01d-13  4.43d-3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.78d-3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  3.92d-4  0.0d0   U238
      Np237  237.05d0  1.03d-14  6.03d-4  0.0d0   U233
       U233  233.04d0  1.38d-13  1.82d-8  0.0d0  Th229
       U234  234.04d0  8.90d-14  2.25d-4  0.0d0  Th230
       U236  236.05d0  9.20d-16  3.29d-3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  6.48d-1  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  4.19d-12 0.0d0
      Th230  230.03d0  2.75d-13  5.19d-8  0.0d0  Ra226 
      Ra226  226.03d0  1.37d-11  2.12d-11 0.0d0
       Tc99   98.91d0  1.04d-13  6.36d-4  0.07d0 #Johnson et al 2005 IRF
      I129   128.90d0  1.29d-15  1.50d-4  0.10d0 #Johnson et al 2005 IRF
      Cs135  134.91d0  9.550d-15 3.37d-4  0.10d0 #Johnson et al 2005 IRF
       Cl36   35.97d0  7.297d-14 2.44d-7  0.16d0 #Johnson et al 2005 IRF
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM CUSTOM

END_WASTE_FORM_GENERAL
noskip

'''

pwr_40GWdMTU_50yOoR_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE
  MECHANISM CUSTOM
    NAME pwr_40GWdMTU_100yOoR
    FRACTIONAL_DISSOLUTION_RATE 1.d-6 1/day #deterministic rate Mariner et al 2015
    MATRIX_DENSITY 10970. #also really dense
    SPECIES
    !mass fractions are g/g fuel for CSNF, 40GWd/MTHM burnup, 50 yrs OoR
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  9.36d-4  0.0d0  Np237
      Am243  243.06d0  2.98d-12  9.61d-5  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.24d-4  0.0d0   U234
      Pu239  239.05d0  9.01d-13  4.43d-3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.79d-3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  3.92d-4  0.0d0   U238
      Np237  237.05d0  1.03d-14  5.28d-4  0.0d0   U233
       U233  233.04d0  1.38d-13  9.18d-9  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.85d-4  0.0d0  Th230
       U236  236.05d0  9.20d-16  3.28d-3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  6.48d-1  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  2.01d-12 0.0d0
      Th230  230.03d0  2.75d-13  2.32d-8  0.0d0  Ra226 
      Ra226  226.03d0  1.37d-11  5.10d-12 0.0d0
       Tc99   98.91d0  1.04d-13  6.36d-4  0.07d0 #Johnson et al 2005 IRF
      I129   128.90d0  1.29d-15  1.50d-4  0.10d0 #Johnson et al 2005 IRF
      Cs135  134.91d0  9.550d-15 3.37d-4  0.10d0 #Johnson et al 2005 IRF
       Cl36   35.97d0  7.297d-14 2.44d-7  0.16d0 #Johnson et al 2005 IRF
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM CUSTOM

END_WASTE_FORM_GENERAL
noskip

'''

drepo_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME hanford_glass
    SPECIFIC_SURFACE_AREA 2.6d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for HS HLW in 2038 (HS HLW total decay heat by ers_cts.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.162d-6  0.0d0 Np237
      Am243  243.06d0  2.98d-12  2.162d-9  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  6.141d-9  0.0d0   U234
      Pu239  239.05d0  9.01d-13  3.199d-5  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.551d-6  0.0d0   U236
      Pu242  242.06d0  5.80d-14  7.540d-9  0.0d0   U238
      Np237  237.05d0  1.03d-14  5.800d-6  0.0d0 U233
       U233  233.04d0  1.38d-13  1.517d-6  0.d0 Th229
       U234  234.04d0  8.90d-14  1.015d-6  0.0d0  Th230
       U236  236.05d0  9.20d-16  2.673d-6  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.704d-2  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  4.488d-10 0.0d0 
      Th230  230.03d0  2.75d-13  7.960d-11 0.0d0 #Ra226 
       Tc99   98.91d0  1.04d-13  5.044d-5  0.0d0 
       I129  128.90d0  1.29d-15  7.829d-6  0.0d0
      Cs135  134.91d0  9.55d-15  3.651d-5  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME savannah_river_glass
    SPECIFIC_SURFACE_AREA 2.8d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for SRS HLW in 2038 (SRS HLW total decay heat by ers_cts.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  4.610d-5  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.399d-6  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  2.233d-5  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.394d-4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  2.009d-5  0.0d0   U236
      Pu242  242.06d0  5.80d-14  2.771d-6  0.0d0   U238
      Np237  237.05d0  1.03d-14  2.251d-5  0.0d0   U233
       U233  233.04d0  1.38d-13  2.876d-6  0.0d0  Th229
       U234  234.04d0  8.90d-14  9.710d-6  0.0d0  Th230
       U236  236.05d0  9.20d-16  2.820d-5  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  7.037d-2  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  5.823d-10 0.0d0 
      Th230  230.03d0  2.75d-13  7.874d-10 0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  2.683d-4  0.0d0 
       I129  128.90d0  1.29d-15  9.073d-7  0.0d0
      Cs135  134.91d0  9.55d-15  1.027d-5  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME calcine
    SPECIFIC_SURFACE_AREA 2.8d-3 m^2/kg #Strachan 2004 - Good enough, I guess
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for HIP'd Calcine in 2038 (Calcine total decay heat.xlsm)
    !based on volumes in SNL 2014, which are messy and not correct
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  3.179d-7  0.0d0  Np237
      Am243  243.06d0  2.98d-12  6.348d-10 0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  4.628d-7  0.0d0   U234
      Pu239  239.05d0  9.01d-13  4.615d-6  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  8.484d-7  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.219d-7  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.234d-5  0.0d0   U233
       U233  233.04d0  1.38d-13  2.214d-10 0.0d0  Th229
       U234  234.04d0  8.90d-14  5.362d-7  0.0d0  Th230
       U236  236.05d0  9.20d-16  1.252d-6  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  4.234d-5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  0.d-10    0.0d0 
      Th230  230.03d0  2.75d-13  1.992d-9  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  2.498d-5  0.0d0 
       I129  128.90d0  1.29d-15  3.944d-8  0.0d0
      Cs135  134.91d0  9.55d-15  1.517d-5  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME cssr_glass
    SPECIFIC_SURFACE_AREA 2.6d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for vitrified Cs/Sr in 2038 (2017DWR/CsSr ave decay heat.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Sr90   90.0d0   7.542d-10  5.701d-5 0.0d0
      Cs135 134.91d0  9.55d-15   3.067d-4 0.0d0
      Cs137 136.907d0 7.322d-10  2.141d-4 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME frg_glass
    SPECIFIC_SURFACE_AREA 2.6d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2610. kg/m^3 #SNL 2014
    SPECIES
    !mass fractions are g/g glass for FRG glass in 2038 (2017DWR/FGR ave decay heat.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Sr90   90.0d0   7.542d-10  1.723d-3 0.0d0
      Cs135 134.91d0  9.55d-15   8.089d-3 0.0d0
      Cs137 136.907d0 7.322d-10  3.725d-3 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM CUSTOM
    NAME naval !cool
    FRACTIONAL_DISSOLUTION_RATE 1.d-6 1/day #deterministic rate Mariner et al 2015
    MATRIX_DENSITY 10970. #also really dense
    SPECIES
    !mass fractions are g/g fuel as calc'd in Naval representative wp decay heat.xlsm
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.676d-4  0.0d0  Np237
      Am243  243.06d0  2.98d-12  1.256d-5  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.226d-3  0.0d0   U234
      Pu239  239.05d0  9.01d-13  8.579d-4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  2.477d-4  0.0d0   U236
      Pu242  242.06d0  5.80d-14  8.015d-5  0.0d0   U238
      Np237  237.05d0  1.03d-14  9.014d-3  0.0d0   U233
       U233  233.04d0  1.38d-13  3.671d-5  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.734d-2  0.0d0  Th230
       U236  236.05d0  9.20d-16  1.540d-1  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.482d-2  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  1.441d-8  0.0d0
      Th230  230.03d0  2.75d-13  5.008d-6  0.0d0  Ra226 
      Ra226  226.03d0  1.37d-11  2.326d-9  0.0d0
       Tc99   98.91d0  1.04d-13  1.632d-2  0.07d0 #Johnson et al 2005 IRF
      I129   128.90d0  1.29d-15  2.464d-3  0.10d0 #Johnson et al 2005 IRF
      Cs135  134.91d0  9.550d-15 1.731d-2  0.10d0 #Johnson et al 2005 IRF
      !Cl36   35.97d0  7.297d-14 0.000d-7  0.16d0 #Johnson et al 2005 IRF
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM CUSTOM

  MECHANISM DSNF
    NAME dsnf_50
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  2.59d0   0.0d0 Np237
      Am243  243.06d0  2.98d-12  1.67d-1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  7.00d-1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  5.17d+1  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.32d+1  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.66d0   0.0d0   U238
      Np237  237.05d0  1.03d-14  3.35d0   0.0d0 U233
       U233  233.04d0  1.38d-13  4.10d-1  0.d0 Th229
       U234  234.04d0  8.90d-14  9.16d0   0.0d0  Th230
       U236  236.05d0  9.20d-16  1.61d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.06d+4  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  9.67d-5  0.d0 
      Th230  230.03d0  2.75d-13  9.63d-4  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  1.33d+1  0.0d0 
       I129  128.90d0  1.29d-15  2.40d0   0.0d0
      Cs135  134.91d0  9.550d-15 9.15d0   0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_100
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  3.40d+1  0.0d0  Np237
      Am243  243.06d0  2.98d-12  4.29d-1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.60d-0  0.0d0   U234
      Pu239  239.05d0  9.01d-13  8.09d+2  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.78d+2  0.0d0   U236
      Pu242  242.06d0  5.80d-14  3.48d+0  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.81d+1  0.0d0   U233
       U233  233.04d0  1.38d-13  2.35d-3  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.20d+1  0.0d0  Th230
       U236  236.05d0  9.20d-16  4.70d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.05d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  3.69d-7  0.0d0 
      Th230  230.03d0  2.75d-13  1.02d-3  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  5.90d+1  0.0d0 
       I129  128.90d0  1.29d-15  1.08d+1  0.0d0
      Cs135  134.91d0  9.550d-15 3.01d+1  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_200
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.08d+2  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.30d+0  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  6.65d-0  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.82d+3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  4.55d+2  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.99d+1  0.0d0   U238
      Np237  237.05d0  1.03d-14  5.19d+1  0.0d0   U233
       U233  233.04d0  1.38d-13  2.17d-1  0.0d0  Th229
       U234  234.04d0  8.90d-14  2.05d+1  0.0d0  Th230
       U236  236.05d0  9.20d-16  9.01d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.83d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  5.14d-5  0.0d0 
      Th230  230.03d0  2.75d-13  1.94d-3  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  1.09d+2  0.0d0 
       I129  128.90d0  1.29d-15  2.08d+1  0.0d0
      Cs135  134.91d0  9.55d-15  4.12d+1  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_300
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  3.65d+2  0.0d0  Np237
      Am243  243.06d0  2.98d-12  1.21d+1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.86d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  6.57d+3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.75d+3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  6.94d+1  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.21d+2  0.0d0   U233
       U233  233.04d0  1.38d-13  1.09d-2  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.08d+1  0.0d0  Th230
       U236  236.05d0  9.20d-16  1.02d+3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.68d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  2.52d-6  0.0d0 
      Th230  230.03d0  2.75d-13  9.54d-4  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  2.34d+2  0.0d0 
       I129  128.90d0  1.29d-15  5.34d+1  0.0d0
      Cs135  134.91d0  9.55d-15  1.74d+2  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_500
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  6.70d+2  0.0d0  Np237
      Am243  243.06d0  2.98d-12  2.01d+1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  2.98d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.29d+4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  3.33d+3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.01d+2  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.89d+2  0.0d0   U233
      U233   233.04d0  1.38d-13  1.88d-2  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.70d+1  0.0d0  Th230
       U236  236.05d0  9.20d-16  1.69d+3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  2.26d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  4.04d-6  0.d0 
      Th230  230.03d0  2.75d-13  1.49d-3  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  2.80d+2  0.0d0 
       I129  128.90d0  1.29d-15  6.06d+1  0.0d0
      Cs135  134.91d0  9.55d-15  1.16d+2  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_1000
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.14d+3  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.98d+1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  5.58d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.98d+4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  5.03d+3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.33d+2  0.0d0   U238
      Np237  237.05d0  1.03d-14  3.51d+2  0.0d0   U233
       U233  233.04d0  1.38d-13  3.02d-2  0.0d0  Th229
       U234  234.04d0  8.90d-14  5.19d+2  0.0d0  Th230
       U236  236.05d0  9.20d-16  5.53d+3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  3.03d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  6.01d-6  0.0d0 
      Th230  230.03d0  2.75d-13  4.79d-2  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  4.84d+2  0.0d0 
       I129  128.90d0  1.29d-15  1.09d+2  0.0d0
      Cs135  134.91d0  9.55d-15  1.93d+2 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_1500
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.90d+3  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.55d+0  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  4.22d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  6.01d+4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.42d+4  0.0d0   U236
      Pu242  242.06d0  5.80d-14  2.05d+1  0.0d0   U238
      Np237  237.05d0  1.03d-14  3.79d+2  0.0d0   U233
       U233  233.04d0  1.38d-13  3.74d-3  0.0d0  Th229
       U234  234.04d0  8.90d-14  2.27d+1  0.0d0  Th230
       U236  236.05d0  9.20d-16  5.39d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  7.23d+4  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  3.23d-7  0.0d0 
      Th230  230.03d0  2.75d-13  1.95d-3  0.0d0  #Ra226 
       Tc99   98.91d0  1.04d-13  1.34d+3  0.0d0 
       I129  128.90d0  1.29d-15  3.29d+2  0.0d0
      Cs135  134.91d0  9.55d-15  1.95d+3  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

END_WASTE_FORM_GENERAL
noskip

'''

#for OWL 2018 work package, isotope ratios project with Rob Rechard
owl_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME hanford_glass
    SPECIFIC_SURFACE_AREA 2.6d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for HS HLW in 2038 (HS HLW total decay heat by ers_cts.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.162d-6  0.0d0 Np237
      Am243  243.06d0  2.98d-12  2.162d-9  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  6.141d-9  0.0d0   U234
      Pu239  239.05d0  9.01d-13  3.199d-5  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.551d-6  0.0d0   U236
      Pu242  242.06d0  5.80d-14  7.540d-9  0.0d0   U238
      Np237  237.05d0  1.03d-14  5.800d-6  0.0d0 U233
       U233  233.04d0  1.38d-13  1.517d-6  0.d0 Th229
       U234  234.04d0  8.90d-14  1.015d-6  0.0d0  Th230
       U235  235.00d0  3.12d-17  1.199d-4  0.0d0  #Th231
       U236  236.05d0  9.20d-16  2.673d-6  0.0d0  Th232
       U238  238.05d0  4.87d-18  1.704d-2  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  4.488d-10 0.0d0 
      Th230  230.03d0  2.75d-13  7.960d-11 0.0d0 #Ra226 
      Th232  232.00d0  1.56d-18  2.100d-3  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  5.042d-8  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  5.044d-5  0.0d0 
       I129  128.90d0  1.29d-15  7.829d-6  0.0d0
       Ihfd  128.90d0  1.29d-15  7.829d-6  0.0d0
       Isrs  128.90d0  1.29d-15  2.349d-5  0.0d0
       Ical  128.90d0  1.29d-15  7.829d-5  0.0d0
       I50d  128.90d0  1.29d-15  2.349d-4  0.0d0
      I200d  128.90d0  1.29d-15  7.829d-4  0.0d0
      Cs135  134.91d0  9.55d-15  3.651d-5  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME savannah_river_glass
    SPECIFIC_SURFACE_AREA 2.8d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for SRS HLW in 2038 (SRS HLW total decay heat by ers_cts.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  4.610d-5  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.399d-6  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  2.233d-5  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.394d-4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  2.009d-5  0.0d0   U236
      Pu242  242.06d0  5.80d-14  2.771d-6  0.0d0   U238
      Np237  237.05d0  1.03d-14  2.251d-5  0.0d0   U233
       U233  233.04d0  1.38d-13  2.876d-6  0.0d0  Th229
       U234  234.04d0  8.90d-14  9.710d-6  0.0d0  Th230
       U235  235.00d0  3.12d-17  1.527d-4  0.0d0  #Th231
       U236  236.05d0  9.20d-16  2.820d-5  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  7.037d-2  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  5.823d-10 0.0d0 
      Th230  230.03d0  2.75d-13  7.874d-10 0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  6.358d-3  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  3.797d-6  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  2.683d-4  0.0d0 
       I129  128.90d0  1.29d-15  9.073d-7  0.0d0
       Ihfd  128.90d0  1.29d-15  2.722d-7  0.0d0
       Isrs  128.90d0  1.29d-15  9.073d-7  0.0d0
       Ical  128.90d0  1.29d-15  2.722d-6  0.0d0
       I50d  128.90d0  1.29d-15  9.073d-6  0.0d0
      I200d  128.90d0  1.29d-15  2.722d-5  0.0d0
      Cs135  134.91d0  9.55d-15  1.027d-5  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME calcine
    SPECIFIC_SURFACE_AREA 2.8d-3 m^2/kg #Strachan 2004 - Good enough, I guess
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for HIP'd Calcine in 2038 (Calcine total decay heat.xlsm)
    !based on volumes in SNL 2014, which are messy and not correct
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  3.179d-7  0.0d0  Np237
      Am243  243.06d0  2.98d-12  6.348d-10 0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  4.628d-7  0.0d0   U234
      Pu239  239.05d0  9.01d-13  4.615d-6  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  8.484d-7  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.219d-7  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.234d-5  0.0d0   U233
       U233  233.04d0  1.38d-13  2.214d-10 0.0d0  Th229
       U234  234.04d0  8.90d-14  5.362d-7  0.0d0  Th230
       U235  235.00d0  3.12d-17  1.417d-5  0.0d0  #Th231
       U236  236.05d0  9.20d-16  1.252d-6  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  4.234d-5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  0.d-10    0.0d0 
      Th230  230.03d0  2.75d-13  1.992d-9  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  8.008d-13 0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  4.052d-8  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  2.498d-5  0.0d0 
       I129  128.90d0  1.29d-15  3.944d-8  0.0d0
       Ihfd  128.90d0  1.29d-15  3.944d-9  0.0d0
       Isrs  128.90d0  1.29d-15  1.183d-8  0.0d0
       Ical  128.90d0  1.29d-15  3.944d-8  0.0d0
       I50d  128.90d0  1.29d-15  1.183d-7  0.0d0
      I200d  128.90d0  1.29d-15  3.944d-7  0.0d0
      Cs135  134.91d0  9.55d-15  1.517d-5  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME cssr_glass
    SPECIFIC_SURFACE_AREA 2.6d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    !mass fractions are g/g glass for vitrified Cs/Sr in 2038 (2017DWR/CsSr ave decay heat.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Sr90   90.0d0   7.542d-10  5.701d-5 0.0d0
      Cs135 134.91d0  9.55d-15   3.067d-4 0.0d0
      Cs137 136.907d0 7.322d-10  2.141d-4 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM GLASS
    KIENZLER_DISSOLUTION
    NAME frg_glass
    SPECIFIC_SURFACE_AREA 2.6d-3 m^2/kg #Strachan 2004
    MATRIX_DENSITY 2610. kg/m^3 #SNL 2014
    SPECIES
    !mass fractions are g/g glass for FRG glass in 2038 (2017DWR/FGR ave decay heat.xlsm)
    !isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, daughter
      Sr90   90.0d0   7.542d-10  1.723d-3 0.0d0
      Cs135 134.91d0  9.55d-15   8.089d-3 0.0d0
      Cs137 136.907d0 7.322d-10  3.725d-3 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM GLASS

  MECHANISM CUSTOM
    NAME naval !cool
    FRACTIONAL_DISSOLUTION_RATE 1.d-6 1/day #deterministic rate Mariner et al 2015
    MATRIX_DENSITY 10970. #also really dense
    SPECIES
    !mass fractions are g/g fuel as calc'd in Naval representative wp decay heat.xlsm
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.676d-4  0.0d0  Np237
      Am243  243.06d0  2.98d-12  1.256d-5  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.226d-3  0.0d0   U234
      Pu239  239.05d0  9.01d-13  8.579d-4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  2.477d-4  0.0d0   U236
      Pu242  242.06d0  5.80d-14  8.015d-5  0.0d0   U238
      Np237  237.05d0  1.03d-14  9.014d-3  0.0d0   U233
       U233  233.04d0  1.38d-13  3.671d-5  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.734d-2  0.0d0  Th230
       U235  235.00d0  3.12d-17  6.640d-1  0.0d0  #Th231
       U236  236.05d0  9.20d-16  1.540d-1  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.482d-2  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  1.441d-8  0.0d0
      Th230  230.03d0  2.75d-13  5.008d-6  0.0d0  Ra226 
      Th232  232.00d0  1.56d-18  5.882d-4 0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  2.076d-5  0.0d0 #Br79, t1/2 = 326000 y
      Ra226  226.03d0  1.37d-11  2.326d-9  0.0d0
       Tc99   98.91d0  1.04d-13  1.632d-2  0.07d0 #Johnson et al 2005 IRF
      I129   128.90d0  1.29d-15  2.464d-3  0.10d0 #Johnson et al 2005 IRF
      Cs135  134.91d0  9.550d-15 1.731d-2  0.10d0 #Johnson et al 2005 IRF
      !Cl36   35.97d0  7.297d-14 0.000d-7  0.16d0 #Johnson et al 2005 IRF
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM CUSTOM

  MECHANISM DSNF
    NAME dsnf_50
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  2.59d0   0.0d0 Np237
      Am243  243.06d0  2.98d-12  1.67d-1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  7.00d-1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  5.17d+1  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.32d+1  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.66d0   0.0d0   U238
      Np237  237.05d0  1.03d-14  3.35d0   0.0d0 U233
       U233  233.04d0  1.38d-13  4.10d-1  0.d0 Th229
       U234  234.04d0  8.90d-14  9.16d0   0.0d0  Th230
       U235  235.00d0  3.12d-17  2.34d+3  0.0d0  #Th231
       U236  236.05d0  9.20d-16  1.61d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.06d+4  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  9.67d-5  0.d0 
      Th230  230.03d0  2.75d-13  9.63d-4  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  3.32d+1  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  1.28d-1  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  1.33d+1  0.0d0 
       I129  128.90d0  1.29d-15  2.40d0   0.0d0
       Ihfd  128.90d0  1.29d-15  7.2d-2   0.0d0
       Isrs  128.90d0  1.29d-15  2.4d-1   0.0d0
       Ical  128.90d0  1.29d-15  7.2d-1   0.0d0
       I50d  128.90d0  1.29d-15  2.40d0   0.0d0
      I200d  128.90d0  1.29d-15  7.20d0   0.0d0
      Cs135  134.91d0  9.550d-15 9.15d0   0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_100
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  3.40d+1  0.0d0  Np237
      Am243  243.06d0  2.98d-12  4.29d-1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.60d-0  0.0d0   U234
      Pu239  239.05d0  9.01d-13  8.09d+2  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.78d+2  0.0d0   U236
      Pu242  242.06d0  5.80d-14  3.48d+0  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.81d+1  0.0d0   U233
       U233  233.04d0  1.38d-13  2.35d-3  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.20d+1  0.0d0  Th230
       U235  235.00d0  3.12d-17  5.88d+3  0.0d0  #Th231
       U236  236.05d0  9.20d-16  4.70d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.05d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  3.69d-7  0.0d0 
      Th230  230.03d0  2.75d-13  1.02d-3  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  2.17d-1  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  5.90d-1  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  5.90d+1  0.0d0 
       I129  128.90d0  1.29d-15  1.08d+1  0.0d0
      Cs135  134.91d0  9.550d-15 3.01d+1  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_200
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.08d+2  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.30d+0  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  6.65d-0  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.82d+3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  4.55d+2  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.99d+1  0.0d0   U238
      Np237  237.05d0  1.03d-14  5.19d+1  0.0d0   U233
       U233  233.04d0  1.38d-13  2.17d-1  0.0d0  Th229
       U234  234.04d0  8.90d-14  2.05d+1  0.0d0  Th230
       U235  235.00d0  3.12d-17  8.70d+3  0.0d0  #Th231
       U236  236.05d0  9.20d-16  9.01d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.83d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  5.14d-5  0.0d0 
      Th230  230.03d0  2.75d-13  1.94d-3  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  1.75d+1  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  1.04d00  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  1.09d+2  0.0d0 
       I129  128.90d0  1.29d-15  2.08d+1  0.0d0
       Ihfd  128.90d0  1.29d-15  2.08d-1  0.0d0
       Isrs  128.90d0  1.29d-15  6.24d-1  0.0d0
       Ical  128.90d0  1.29d-15  2.08d+0  0.0d0
       I50d  128.90d0  1.29d-15  6.24d+0  0.0d0
      I200d  128.90d0  1.29d-15  2.08d+1  0.0d0
      Cs135  134.91d0  9.55d-15  4.12d+1  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_300
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  3.65d+2  0.0d0  Np237
      Am243  243.06d0  2.98d-12  1.21d+1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  1.86d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  6.57d+3  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.75d+3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  6.94d+1  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.21d+2  0.0d0   U233
       U233  233.04d0  1.38d-13  1.09d-2  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.08d+1  0.0d0  Th230
       U235  235.00d0  3.12d-17  9.58d+3  0.0d0  #Th231
       U236  236.05d0  9.20d-16  1.02d+3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  1.68d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  2.52d-6  0.0d0 
      Th230  230.03d0  2.75d-13  9.54d-4  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  2.40d-1  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  2.02d00  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  2.34d+2  0.0d0 
       I129  128.90d0  1.29d-15  5.34d+1  0.0d0
      Cs135  134.91d0  9.55d-15  1.74d+2  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_500
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  6.70d+2  0.0d0  Np237
      Am243  243.06d0  2.98d-12  2.01d+1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  2.98d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.29d+4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  3.33d+3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.01d+2  0.0d0   U238
      Np237  237.05d0  1.03d-14  1.89d+2  0.0d0   U233
      U233   233.04d0  1.38d-13  1.88d-2  0.0d0  Th229
       U234  234.04d0  8.90d-14  1.70d+1  0.0d0  Th230
       U235  235.00d0  3.12d-17  1.13d+4  0.0d0  #Th231
       U236  236.05d0  9.20d-16  1.69d+3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  2.26d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  4.04d-6  0.d0 
      Th230  230.03d0  2.75d-13  1.49d-3  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  4.67d-1  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  2.38d00  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  2.80d+2  0.0d0 
       I129  128.90d0  1.29d-15  6.06d+1  0.0d0
      Cs135  134.91d0  9.55d-15  1.16d+2  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_1000
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.14d+3  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.98d+1  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  5.58d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  1.98d+4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  5.03d+3  0.0d0   U236
      Pu242  242.06d0  5.80d-14  1.33d+2  0.0d0   U238
      Np237  237.05d0  1.03d-14  3.51d+2  0.0d0   U233
       U233  233.04d0  1.38d-13  3.02d-2  0.0d0  Th229
       U234  234.04d0  8.90d-14  5.19d+2  0.0d0  Th230
       U235  235.00d0  3.12d-17  3.18d+4  0.0d0  #Th231
       U236  236.05d0  9.20d-16  5.53d+3  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  3.03d+5  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  6.01d-6  0.0d0 
      Th230  230.03d0  2.75d-13  4.79d-2  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  7.43d-1  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  3.67d00  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  4.84d+2  0.0d0 
       I129  128.90d0  1.29d-15  1.09d+2  0.0d0
      Cs135  134.91d0  9.55d-15  1.93d+2 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME dsnf_1500
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Am241  241.06d0  5.08d-11  1.90d+3  0.0d0  Np237
      Am243  243.06d0  2.98d-12  3.55d+0  0.0d0  Pu239
      Pu238  238.05d0  2.56d-10  4.22d+1  0.0d0   U234
      Pu239  239.05d0  9.01d-13  6.01d+4  0.0d0  #U235 (not tracking)
      Pu240  240.05d0  3.34d-12  1.42d+4  0.0d0   U236
      Pu242  242.06d0  5.80d-14  2.05d+1  0.0d0   U238
      Np237  237.05d0  1.03d-14  3.79d+2  0.0d0   U233
       U233  233.04d0  1.38d-13  3.74d-3  0.0d0  Th229
       U234  234.04d0  8.90d-14  2.27d+1  0.0d0  Th230
       U235  235.00d0  3.12d-17  1.41d+4  0.0d0  #Th231
       U236  236.05d0  9.20d-16  5.39d+2  0.0d0  #Th232 (not tracking)
       U238  238.05d0  4.87d-18  7.23d+4  0.0d0  #Th234 (not tracking)
      Th229  229.03d0  2.78d-12  3.23d-7  0.0d0 
      Th230  230.03d0  2.75d-13  1.95d-3  0.0d0  #Ra226 
      Th232  232.00d0  1.56d-18  4.21d-2  0.0d0 #Ra228
       Se79   79.00d0  6.74d-14  1.19d+1  0.0d0 #Br79, t1/2 = 326000 y
       Tc99   98.91d0  1.04d-13  1.34d+3  0.0d0 
       I129  128.90d0  1.29d-15  3.29d+2  0.0d0
      Cs135  134.91d0  9.55d-15  1.95d+3  0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

END_WASTE_FORM_GENERAL
noskip

'''

dbh_template = \
'''
#Place relevant pieces of this section at the bottom of pflotran.in outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  EXTERNAL_FILE ../grid/wfg.txt
  PRINT_MASS_BALANCE

  MECHANISM DSNF
    NAME cs_capsule
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per CAPSULE! (18 capsules/waste package)
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Cs135 134.91d0 9.550d-15 2.449d2 0.0d0
      Cs137 136.907d0 7.322d-10 1.292d2 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      #VITALITY_LOG10_MEAN -2.4
      #VITALITY_LOG10_STDEV 0.25
      #VITALITY_UPPER_TRUNCATION -2.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

  MECHANISM DSNF
    NAME sr_capsule
    MATRIX_DENSITY 1.d0 g/cm^3 #virtual number
    SPECIES
    !mass fractions are actually total g per canister
    !isotope, atomic wt (g/mol), 1/s, g/g waste, instant release fraction, daughter
      Sr90 90.0d0 7.542d-10 7.612d1 0.0d0
    /
    CANISTER_DEGRADATION_MODEL
      #VITALITY_LOG10_MEAN -2.4
      #VITALITY_LOG10_STDEV 0.25
      #VITALITY_UPPER_TRUNCATION -2.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /
  END #MECHANISM DSNF

END_WASTE_FORM_GENERAL
noskip

'''

glass_template = \
'''  
  WASTE_FORM
     COORDINATE %f %f %f
     EXPOSURE_FACTOR %f
     VOLUME %f
     MECHANISM_NAME %s
  /
'''

glass_template_region = \
'''  
  WASTE_FORM
     REGION %s
     EXPOSURE_FACTOR %f
     VOLUME %f
     MECHANISM_NAME %s
  /
'''

glass_template_breachtime = \
'''  
  WASTE_FORM
     COORDINATE %f %f %f
     EXPOSURE_FACTOR %f
     VOLUME %f
     MECHANISM_NAME %s
     CANISTER_BREACH_TIME %f y
  /
'''

dsnf_template = \
'''
  WASTE_FORM
    COORDINATE %f %f %f
    VOLUME 1 cm^3 #not a real number
    MECHANISM_NAME %s
  /
'''

dsnf_template_region = \
'''
  WASTE_FORM
    REGION %s
    VOLUME 1 cm^3 #not a real number
    MECHANISM_NAME %s
  /
'''

custom_template = \
'''
  WASTE_FORM
    COORDINATE %f %f %f
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 0.685d0 m^3 #5.22MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), 12 PWRs, Freeze et al 2013
    MECHANISM_NAME csnf
  /
'''

custom_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 0.685d0 m^3 #5.22MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), 12 PWRs, Freeze et al 2013
    MECHANISM_NAME csnf
  /
'''

custom_template_breach = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 0.685d0 m^3 #5.22MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), 12 PWRs, Freeze et al 2013
    MECHANISM_NAME csnf
    CANISTER_BREACH_TIME 0. yr
  /
'''

cust4pwr_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 0.228d0 m^3 #5.22MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), 12 PWRs, Freeze et al 2013
    MECHANISM_NAME csnf #DIVIDED BY 3 TO MAKE 4PWR!
  /
'''

cust37pwr_150yOoR_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 2.113d0 m^3 #37*.435MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), Freeze et al 2013
    MECHANISM_NAME pwr_60GWdMTU_150yOoR
  /
'''

cust37pwr_100yOoR_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 2.113d0 m^3 #37*.435MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), Freeze et al 2013
    MECHANISM_NAME pwr_60GWdMTU_100yOoR
  /
'''

cust24pwr_100yOoR_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 1.370d0 m^3 #24*.435MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), Freeze et al 2013
    MECHANISM_NAME pwr_40GWdMTU_100yOoR
  /
'''

cust24pwr_50yOoR_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 1.370d0 m^3 #24*.435MTHM*1.44e3kgwaste/MTHM*(m3/10970kgUO2), Freeze et al 2013
    MECHANISM_NAME pwr_40GWdMTU_50yOoR
  /
'''

naval_template_region = \
'''
  WASTE_FORM
    REGION %s
    EXPOSURE_FACTOR 1.d0 #for now
    VOLUME 1.68d-3 m^3 #see Naval representative wp decay heat.xlsm
    MECHANISM_NAME naval
  /
'''

cs_capsule_template = \
'''
  WASTE_FORM
    COORDINATE %f %f %f
    VOLUME 18 cm^3 #18 capsules per waste package
    MECHANISM_NAME cs_capsule
    #CANISTER_VITALITY_RATE 0.004 1/day
    CANISTER_BREACH_TIME 1. yr
  /
'''

cs_template_region = \
'''
  WASTE_FORM
    REGION %s
    VOLUME 18 cm^3 #18 capsules per waste package
    MECHANISM_NAME cs_capsule
    #CANISTER_VITALITY_RATE 0.004 1/day
    CANISTER_BREACH_TIME 1. yr
  /
'''

sr_capsule_template = \
'''
  WASTE_FORM
    COORDINATE %f %f %f
    VOLUME 18 cm^3 #18 capsules per waste package
    MECHANISM_NAME sr_capsule
    #CANISTER_VITALITY_RATE 0.004 1/day
    CANISTER_BREACH_TIME 1. yr
  /
'''

sr_template_region = \
'''
  WASTE_FORM
    REGION %s
    VOLUME 18 cm^3 #18 capsules per waste package
    MECHANISM_NAME cs_capsule
    #CANISTER_VITALITY_RATE 0.004 1/day
    CANISTER_BREACH_TIME 1. yr
  /
'''

def get_exposure_factor():
  return random.triangular(4.,17.,4.)

exposure_factor = 4.

def shale_dpc_to_file(fp,):
  fp.write(wfg_simulation_template)
  fp.write(pwr_40GWdMTU_100yOoR_template)
  fp.write(pwr_60GWdMTU_150yOoR_template)
  return

def gdsa_to_file(fp):
  fp.write(wfg_simulation_template)
  fp.write(gdsa_template)
  return

def drepo_to_file(fp):
  fp.write(wfg_simulation_template)
  fp.write(drepo_template)
  return

def dbh_to_file(fp):
  fp.write(wfg_simulation_template)
  fp.write(dbh_template)
  return

def glass_to_file(fp,xcoord,ycoord,zcoord,volume,name):
  fp.write(glass_template % (xcoord,ycoord,zcoord,get_exposure_factor(),volume,name))
  return

def glass_to_file_reg(fp,rname,volume,name):
  fp.write(glass_template_region % (rname,get_exposure_factor(),volume,name))
  return

def homog_glass_to_file(fp,xcoord,ycoord,zcoord,volume,name):
  fp.write(glass_template % (xcoord,ycoord,zcoord,exposure_factor,volume,name))
  return

def homog_glass_to_file_bt (fp,xcoord,ycoord,zcoord,volume,name,time):
  fp.write(glass_template_breachtime % (xcoord,ycoord,zcoord,exposure_factor,volume,name,time))
  return

def dsnf_to_file(fp,xcoord,ycoord,zcoord,name):
  fp.write(dsnf_template % (xcoord,ycoord,zcoord,name))
  return

def dsnf_to_file_reg(fp,rname,name):
  fp.write(dsnf_template_region % (rname,name))
  return

def csnf_to_file(fp,xcoord,ycoord,zcoord):
  fp.write(custom_template % (xcoord,ycoord,zcoord))
  return

def csnf_to_file_reg(fp,rname):
  fp.write(custom_template_region % (rname))
  return

def csnf_to_file_breach(fp,rname):
  fp.write(custom_template_breach % (rname))
  return

def csnf4pwr_to_file_reg(fp,rname):
  fp.write(cust4pwr_template_region % (rname))
  return

def csnf24pwr_to_file_reg(fp,rname):
  fp.write(cust24pwr_100yOoR_template_region % (rname))
  return

def csnf37pwr_to_file_reg(fp,rname):
  fp.write(cust37pwr_150yOoR_template_region % (rname))
  return

def naval_to_file_reg(fp,rname):
  fp.write(naval_template_region % (rname))
  return

def cs_to_file(fp,xcoord,ycoord,zcoord):
  fp.write(cs_capsule_template % (xcoord,ycoord,zcoord))
  return

def cs_to_file_reg(fp,rname):
  fp.write(cs_template_region % (rname))
  return

def sr_to_file(fp,xcoord,ycoord,zcoord):
  fp.write(sr_capsule_template % (xcoord,ycoord,zcoord))
  return

def sr_to_file_reg(fp,rname):
  fp.write(sr_template_region % (rname))
  return

