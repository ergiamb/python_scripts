import random
import math

def triangular_distribution(a,b,c,rand):
  if rand < (c-a)/(b-a):
    return math.sqrt(rand*(b-a)*(c-a))+a
  else:
    return b-math.sqrt((1.-rand)*(b-a)*(b-c))

glass_simulation_template = \
'''#Place this section in the SIMULATION block. w/o the skip/noskip   

skip
    WASTE_FORM wf_general
      TYPE GENERAL
    / 
noskip

'''

glass_template = \
'''
#Place this section at the bottom of the file outside the SUBSURFACE block.  
#without the skip, noskip!

skip
WASTE_FORM_GENERAL
  PRINT_MASS_BALANCE
  MECHANISM GLASS
    NAME hanford_glass
    SPECIFIC_SURFACE_AREA 2.78d-3 m^2/m^3 #for the glass waste form
    MATRIX_DENSITY 2750. kg/m^3 #Kienzler et al 2012
    SPECIES
    # mass fractions are g/g glass for HW HLW in 2037 (HS HLW total decay heat by ers_cts.xlsm)
      #isotope, atomic wt (g/mol), 1/s, g/g glass, instant release fraction, parent
      I129 128.90d0 1.29d-15 8.12d-6 0.0d0
      Cs135 134.91d0 9.550d-15 6.84d-6 0.0d0
      Am241 241.06d0 5.08d-11 1.21d-6 0.0d0
      Np237 237.05d0 1.03d-14 6.02d-6 0.0d0 Am241
      U233 233.04d0 1.38d-13 1.57d-6 0.d0 Np237
      Th229 229.03d0 2.78d-12 4.59d-10 0.d0 U233
    /
    CANISTER_DEGRADATION_MODEL
      VITALITY_LOG10_MEAN -4.5
      VITALITY_LOG10_STDEV 0.5
      VITALITY_UPPER_TRUNCATION -3.0
      CANISTER_MATERIAL_CONSTANT 1500.
    /

  EXTERNAL_FILE ../grids/glass_grid3.txt
END_WASTE_FORM_GENERAL
noskip

'''

waste_form_template = \
'''  
  WASTE_FORM
     COORDINATE %f %f %f
     EXPOSURE_FACTOR %f
     VOLUME 1.9d0 #hardwired volume of glass (5 logs)
     MECHANISM_NAME hanford_glass
  /
'''

def get_exposure_factor():
  return random.triangular(4.,17.,4.)

def glass_to_file(filename,xcoord,ycoord,zcoord):
  '''filename = string, name of txt file to write
     xcoord = [float], list of xcoords at center of wp
     ycoord = [float], list of ycoords at center of wp
     zcoord = [float], list of zcoords at center of wp
  '''
  f = open(filename,'w')

  f.write(glass_simulation_template)
  f.write(glass_template)
#  for line in open('coordinates.txt','r'):
#    w = line.split()
#    x = float(w[0])
#    y = float(w[1])
#    z = float(w[2])
  for i in range(len(xcoord)):
    f.write(waste_form_template % (xcoord[i],ycoord[i],zcoord[i],get_exposure_factor()))
  f.write('/\n\n')

  f.close()
#print('done')
