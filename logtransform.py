'''logtransform.py
   take the log of the response functions and create a new dakota_tabular.dat
   #also unlog the rateWP 
   #log transform log uniform uncertain inputs
   Emily Stein <ergiamb@sandia.gov>
   11.01.19
'''

from stepwise import load_data
import pandas as pd
import numpy as np
import argparse

#log transform is so tiny, it didn't get its own function definition
 
if __name__ == '__main__':
  #file I/O is copied from stepwise.py and same as in calc_pcc.py.
  parser = argparse.ArgumentParser(description=
                    'Iterate to find Partial Correlation Coefficients')
  parser.add_argument('-n','--num_ind_var',
                      help = 'number of independent (input) variables',
                      required = True)
  parser.add_argument('-d','--num_dep_var',
                      help = 'number of dependent (output) variables to process'+
                             '[default = all]',
                      default = 'all',
                      required = False)
  parser.add_argument('-f','--dakota_tab',
                      help = 'dakota tabular file containing input and output values'+
                             '[default = dakota_tabular.dat]',
                      default = 'dakota_tabular.dat',
                      required = False)
  parser.add_argument('-r','--rank',
                      help = 'set to True for rank transform'+
                             '[default = False]',
                      default = False,
                      required = False)
  args = parser.parse_args()

  niv = int(args.num_ind_var)
  if args.num_dep_var != 'all':
    try:
      ndv = int(args.num_dep_var)
    except:
      print('If used, --num_dep_var must be an integer or the word "all". Exiting')
      sys.exit()

  with open(args.dakota_tab,'r') as f:
    header = f.readline().split()
  if '%eval_id' in header:
    evalid = True
    xydata = load_data(args.dakota_tab,evalid,args.rank)
    if 'interface' in header: #assumes first column is %eval_id and second is interface
      if args.num_dep_var == 'all':
        ndv = len(header)-niv-2
      iv_list = header[2:niv+2]
      dv_list = header[niv+2:niv+2+ndv]
    else: #assumes first column is %eval_id
      if args.num_dep_var == 'all':
        ndv = len(header)-niv-1
      iv_list = header[1:niv+1]
      dv_list = header[niv+1:niv+1+ndv]
  else:
    evalid = False
    xydata = load_data(args.dakota_tab,evalid,args.rank)
    if 'interface' in header: #assumes first column is interface
      if args.num_dep_var == 'all':
        ndv = len(header)-niv-1
      iv_list = header[1:niv+1]
      dv_list = header[niv+1:niv+1+ndv]
    else: #assumes first column is first iv
      if args.num_dep_var == 'all':
        ndv = len(header)-niv
      iv_list = header[:niv]
      dv_list = header[niv:niv+ndv]
  print('independent variables {}'.format(iv_list))
  print('dependent variables {}'.format(dv_list))
  #for dv in dv_list:
  #  Stepwise(xydata,iv_list,dv)
  print('done with stuff that is identical to stepwise.py')

  xydata[dv_list] = xydata[dv_list].transform(lambda x: np.log10(x))
  outfile = args.dakota_tab.split('.')[0]+'_log.dat'
  xydata.to_csv(outfile,sep=' ',float_format='%16.10e',header=True,index=True)
  print('Done!')

#it's possible that inputs should be log transformed, too, but let's
#start with outputs. 
#NOPE! inputs should not be transformed because type of distribution
#is part of the input. at least this is true for PCE. Leave them alone.
#Not sure what GP does with input distribution info, but leave this alone, too.
