'''
#take output from DFNworks and map it into a regular grid for continuous porous medium representation
Emily 3.7.16
#Add function to read normal vectors, fracture translations and radii from output of 
dfnWorks-Version2.0: normal_vectors.dat, translations.dat, and radii_Final.dat
Emily 6.14.17
'''


import time
import numpy as np
import transformations as tr
from h5py import File
#from mapdfcy import centercy, cornercy

def readEllipse(filename, nfrac):
  '''filename = string
   nfrac = int number of fractures
  '''
  
  fin = file(filename,'r')
  ellipses = []
  for f in range(nfrac):
    line = fin.readline()
    ellipses.append( {'normal':np.zeros((3),dtype=np.float),'translation':np.zeros((3),dtype=np.float),'xrad':0.0,'yrad':0.0} )
    #this version reads List(...,...,...)
    ellipses[f]['normal'][0] = float(line[5:-1].split(',')[0])
    ellipses[f]['normal'][1] = float(line[5:-1].split(',')[1])
    ellipses[f]['normal'][2] = float(line[5:-2].split(',')[2])
    line = fin.readline()
    ellipses[f]['translation'][0] = float(line[5:-1].split(',')[0])
    ellipses[f]['translation'][1] = float(line[5:-1].split(',')[1])
    ellipses[f]['translation'][2] = float(line[5:-2].split(',')[2])
    ellipses[f]['xrad'] = float(fin.readline())
    ellipses[f]['yrad'] = float(fin.readline())
  fin.close()

  return ellipses

def readEllipse2(radiifile='radii_Final.dat',normalfile='normal_vectors.dat',transfile='translations.dat'):
  '''read fracture radius, normal vector, and translation from dfnWorks-Version2.0 output files
  '''

  ellipses = []
  with open(radiifile,'r') as f:
    radii = f.readlines() #two lines to get rid of at top of file
  radii.pop(0)
  radii.pop(0)
  with open(normalfile, 'r') as f:
    normals = f.readlines() #no lines to get rid of at top of file
  with open(transfile, 'r') as f:
    temp = f.readlines() #one line to get rid of at top of file
  temp.pop(0)
  translations = []
  for line in temp:
    if line.split()[-1] != 'R':
      translations.append(line)
  print(len(radii))
  print(len(normals))
  print(len(translations))
  for i in range(len(radii)):
    ellipses.append( {'normal':np.zeros((3),dtype=np.float),'translation':np.zeros((3),dtype=np.float),'xrad':0.0,'yrad':0.0} )
    ellipses[i]['normal'][0] = float(normals[i].split()[0])
    ellipses[i]['normal'][1] = float(normals[i].split()[1])
    ellipses[i]['normal'][2] = float(normals[i].split()[2])
    ellipses[i]['translation'][0] = float(translations[i].split()[0])
    ellipses[i]['translation'][1] = float(translations[i].split()[1])
    ellipses[i]['translation'][2] = float(translations[i].split()[2])
    ellipses[i]['xrad'] = float(radii[i].split()[0])
    ellipses[i]['yrad'] = float(radii[i].split()[1])
  
  return ellipses

def findT(aperturefile, permfile):
  '''read aperture and permeability output and calculate fracture transmissivity
  '''
  
  with open(aperturefile,'r') as f:
    apertures = f.readlines()
  apertures.pop(0)
  with open(permfile,'r') as f:
    perms = f.readlines()
  perms.pop(0)
  T = []
  for i in range(len(perms)):
    aperture = float(apertures[i].split()[3])
    perm = float(perms[i].split()[3])
    T.append(aperture*perm)

  return T

def findT_old(apertureFile, permFile, nfrac):
  '''apertureFile = string
     permFile = string
     nfrac = int number of fractures
  '''
  
  fa = file(apertureFile,'r')
  fp = file(permFile,'r')
  fa.readline() #throw away the first line
  fp.readline()

  T = [] #intrinsic transmissivity m^3
  for f in range(nfrac):
    aperture = (float(fa.readline().split()[3]))
    perm = (float(fp.readline().split()[3]))  #there are three values in here, but they're all the same
    T.append(aperture*perm)
  
  return T
 
def map_dfn(ellipses, origin, nx, ny, nz, d):
  '''ellipses = list of dictionaries containing normal, translation, xrad, and yrad for each fracture
     origin = [x,y,z] float coordinates of lower left front corner
     nx = int number of cells in x
     ny = int number of cells in y
     nz = int number of cells in z
     d = float discretization length
  '''

  ncell = nx*ny*nz
  nfrac = len(ellipses)
  fracture = np.zeros((ncell,10), dtype=np.int)
  #fracture=fracture.tolist()
  #fracture = [[0,0]]*ncell

  t0 = time.time()
 # mod = nfrac/10
  for f in range(nfrac): 
 #   if f%mod == 0:
    print('Mapping ellipse %i of %i.' %(f,nfrac))
    tnow = time.time() - t0
    print('Time elapsed in map_dfn() = %f.' %(tnow))
    normal = ellipses[f]['normal']
    translation = ellipses[f]['translation']
    xrad = ellipses[f]['xrad']
    yrad = ellipses[f]['yrad']
    #calculate rotation matrix for use later in rotating coordinates of nearby cells
    direction = np.cross([0,0,1],normal)
#    cosa = np.dot([0,0,1],normal)/(np.linalg.norm([0,0,1])*np.linalg.norm(normal)) #frobenius norm = length
    #above evaluates to normal[2], so:
    angle = np.arccos(normal[2]) # everything is in radians
    M = tr.rotation_matrix(angle,direction)
    #find fracture in domain coordinates so can look for nearby cells
    if xrad > yrad:
      x1 = translation[0]-xrad
      x2 = translation[0]+xrad
      y1 = translation[1]-xrad
      y2 = translation[1]+xrad
      z1 = translation[2]-xrad
      z2 = translation[2]+xrad
    else: #this else is misleading because script only works on circles
      x1 = translation[0]-yrad
      x2 = translation[0]+yrad
      y1 = translation[1]-yrad
      y2 = translation[1]+yrad
      z1 = translation[2]-yrad
      z2 = translation[2]+yrad
    #find indices of nearby cells so don't have to check all of them!
    i1 = max(0,int((x1-origin[0])/d)) #round down?
    i2 = min(nx,int((x2-origin[0])/d+1)) #round up?
    j1 = max(0,int((y1-origin[1])/d))
    j2 = min(ny,int((y2-origin[1])/d+1))
    k1 = max(0,int((z1-origin[2])/d))
    k2 = min(nz,int((z2-origin[2])/d+1))
    for k in range(k1,k2): #for cells close to fracture
      for j in range(j1,j2):
        for i in range(i1,i2):
          #check if cell center is inside radius of fracture
          center = [origin[0]+i*d+d/2.,origin[1]+j*d+d/2.,origin[2]+k*d+d/2.] 
          #center = centercy(origin[0],origin[1],origin[2],i,j,k,d) 
          if center[0]>x1 and center[0]<x2 and center[1]>y1 and center[1]<y2 and center[2]>z1 and center[2]<z2:
            local_center = center - translation
            #rotate cell center coordinates to xyz of fracture
            rotate_center = np.dot(local_center, M[:3,:3]) 
            #calculate r^2 of cell center in xy of fracture (fracture lies in z=0 plane)
            rsq_cell = np.square(rotate_center[0]) + np.square(rotate_center[1])
            if rsq_cell < np.square(xrad): 
              #center is in radius, so check if fracture intersects cell
              #find z of cell corners in xyz of fracture
              corner = [[origin[0]+i*d, origin[1]+j*d, origin[2]+k*d],
                       [origin[0]+(i+1)*d, origin[1]+j*d, origin[2]+k*d],
                       [origin[0]+(i+1)*d, origin[1]+(j+1)*d, origin[2]+k*d],
                       [origin[0]+i*d, origin[1]+(j+1)*d, origin[2]+k*d],
                       [origin[0]+i*d, origin[1]+j*d, origin[2]+(k+1)*d],
                       [origin[0]+(i+1)*d, origin[1]+j*d, origin[2]+(k+1)*d],
                       [origin[0]+(i+1)*d, origin[1]+(j+1)*d, origin[2]+(k+1)*d],
                       [origin[0]+i*d, origin[1]+(j+1)*d, origin[2]+(k+1)*d]]
              #corner = cornercy(origin[0],origin[1],origin[2],i,j,k,d)
              maxz = 0.
              minz = 0.
              for c in range(len(corner)):
                rotate_corner = np.dot(corner[c]-translation, M[:3,:3]) 
                if rotate_corner[2] > maxz:
                  maxz = rotate_corner[2]
                elif rotate_corner[2] < minz:
                  minz = rotate_corner[2]
              #and store min and max values of z at corners
              if minz < 0. and maxz > 0.: #fracture lies in z=0 plane
                #fracture intersects that cell
                index = i+nx*j+nx*ny*k
                fracture[index][0] +=1 #store number of fractures that intersect cell
                if fracture[index][0] > 9:
                  print('Number of fractures in cell %d exceeds allotted memory.' %index)
                  return
                fracture[index][fracture[index][0]] = f+1
  return fracture
    
def porosity(aperturefile,fracture,d,bulk_por):
  '''apertureFile = string, somehow I am reading this again!
     fracture = numpy array containing number of fractures in each cell, list of fracture numbers in each cell
     d = float length of cell side
     bulk_por = float bulk porosity (which would normally be larger than fracture porosity)
     This assigns bulk porosity to any cells that don't contain a fracture, and assigns fracture porosity else.
  '''
  with open(aperturefile,'r') as f:
    temp = f.readlines()
  temp.pop(0)
  apertures = []
  for i in range(len(temp)):
    apertures.append(float(temp[i].split()[3]))
  
#  fa = file(apertureFile,'r')
#  fa.readline() #throw away the first line
#
#  aperture = [] #aperture m
#  for f in range(nfrac):
#    aperture.append(float(fa.readline().split()[3]))
#  fa.close()

  t0 = time.time()
  ncell = fracture.shape[0]
  porosity = np.zeros((ncell),'=f8')
  for i in range(ncell):
    if fracture[i][0] == 0:
      porosity[i] = bulk_por
    else: #there are fractures in this cell
      for j in range(1,fracture[i][0]+1):  
        fracnum = fracture[i][j]
        porosity[i] += apertures[fracnum-1]/d #aperture is 0 indexed, fracture numbers are 1 indexed

  t1 = time.time()
  print('Time spent in porosity() = %f.' %(t1-t0))
  return porosity
  
def permIso(fracture,T,d,k_background):
  '''fracture = numpy array containing number of fractures in each cell, list of fracture numbers in each cell
     T = [] containing intrinsic transmissivity for each fracture
     d = length of cell sides
     k_background = float background permeability for cells with no fractures in them
  '''
  t0 = time.time()
  ncell = fracture.shape[0]
  k_iso = np.full((ncell),k_background,'=f8')
  for i in range(ncell):
    if fracture[i][0] != 0:
      for j in range(1,fracture[i][0]+1):
        fracnum = fracture[i][j]
        k_iso[i] += T[fracnum-1]/d #T is 0 indexed, fracture numbers are 1 indexed
  t1 = time.time()
  print('Time spent in permIso() = %f.' %(t1-t0))
  return k_iso
      
def permAniso(fracture,ellipses,T,d,k_background,LUMP=0):
  '''fracture = numpy array containing number of fractures in each cell, list of fracture numbers in each cell
     ellipses = [{}] containing normal and translation vectors for each fracture
     T = [] containing intrinsic transmissivity for each fracture
     d = length of cell sides
     k_background = float background permeability for cells with no fractures in them
  '''
  #quick error check
  nfrac = len(ellipses)
  if nfrac != len(T):
    print ('ellipses and transmissivity contain different numbers of fractures')
    return  #I guess this works

  ellipseT = np.zeros((nfrac,3),'=f8')
  fullTensor = []
  T_local = np.zeros((3,3),dtype=np.float)
  t0 = time.time()
  #calculate transmissivity tensor in domain coordinates for each ellipse
  for f in range(nfrac):
    normal = ellipses[f]['normal']
    #direction = np.cross([0,0,1],normal)
    #cosa = np.dot([0,0,1],normal)/(np.linalg.norm([0,0,1])*np.linalg.norm(normal)) #frobenius norm = length
    #angle = -np.arccos(cosa) #rotating the opposite direction from rotation in map()
    #opposite to the direction and angle in map() because doing the opposite thing
    #returns the same result as above which is commented out
    direction = np.cross(normal,[0,0,1])
  #  cosa = np.dot(normal,[0,0,1])/(np.linalg.norm(normal)*np.linalg.norm([0,0,1])) #frobenius norm = length
  #ok - so np.dot returns normal[2] and if normals are unit vectors then denominator evaluates to 1, so:
  # angle = np.arccos(cosa) 
    angle = np.arccos(normal[2]) 
    M = tr.rotation_matrix(angle, direction)
    Transpose = np.transpose(M[:3,:3])
    T_local[0,0] = T[f]
    T_local[1,1] = T[f]
  #  T_local[2,2] = 0.0 #no permeability in local z direction of fracture
    T_domain = np.dot(np.dot(M[:3,:3],T_local),Transpose)
    ellipseT[f][0:3]=[T_domain[0,0],T_domain[1,1],T_domain[2,2]]
    fullTensor.append(T_domain)
  t1 = time.time()
  print('time spent calculating fracture transmissivity %f' %(t1-t0))

#  print (fullTensor)
  t0 = time.time()
  fout=file('Ttensor.txt','w')
  for f in range(len(fullTensor)):
    fout.write(str(fullTensor[f]))
    fout.write('\n\n')
  fout.close()
  t1 = time.time()
  print('time spent writing fracture transmissivity %f' %(t1-t0))

  #calculate cell effective permeability
  #add fracture k to background k (good enough?)
  t0 = time.time()
  ncells = fracture.shape[0]
  maxfrac = fracture.shape[1]
  k_aniso = np.full((ncells,3),k_background,'=f8')
  for i in range(ncells):
    if fracture[i][0] != 0:
      for j in range(1,fracture[i][0]+1):
        fracnum = fracture[i][j]
        if LUMP: #lump off diagonal terms
          #because symmetrical doesn't matter if direction of summing is correct, phew!
          k_aniso[i][0] += np.sum(fullTensor[fracnum-1][0,:3])/d
          k_aniso[i][1] += np.sum(fullTensor[fracnum-1][1,:3])/d
          k_aniso[i][2] += np.sum(fullTensor[fracnum-1][2,:3])/d
        else: #discard off diagonal terms (default)
          k_aniso[i][0] += ellipseT[fracnum-1][0]/d #ellipseT is 0 indexed, fracture numbers are 1 indexed
          k_aniso[i][1] += ellipseT[fracnum-1][1]/d
          k_aniso[i][2] += ellipseT[fracnum-1][2]/d
  t1 = time.time()
  print('time spent summing cell permeabilities %f' %(t1-t0))

  return k_aniso

#def permCorrected(k_aniso):
#someday calculate permeabilities corrected for stair stepping of grid
  
def count_stuff(filename):
  '''filename = string which is most likely mapELLIPSES.txt
     look for cells with 3 or more fractures in them and count them as possible
     places of false connectivity
  '''

  fin = file(filename,'r')
  cellcount = 0
  count0 = 0
  count1 = 0
  count2 = 0
  count3 = 0
  morethan4 = 0
  for line in fin:
    if line.startswith('#'):
      continue
    elif int(line.split()[3]) == 0:
      cellcount += 1
      count0 += 1
    elif int(line.split()[3]) == 1:
      cellcount += 1
      count1 += 1
    elif int(line.split()[3]) == 2:
      cellcount += 1
      count2 += 1
    elif int(line.split()[3]) == 3:
      cellcount += 1
      count3 += 1
    elif int(line.split()[3]) >= 4:
      cellcount += 1
      morethan4 += 1
  print('\n')
  print('Information for %s ' % filename)
  print('Total number of cells in grid %i' % cellcount)
  print('Number of cells containing fractures %i' % (cellcount-count0))
  print('Percent active cells %.1f' % (100.*(float(cellcount)-float(count0))/float(cellcount)))
  print('Number of cells containing 1 fracture %i' %(count1))
  print('Number of cells containing 2 fractures %i' %(count2))
  print('Number of cells containing 3 fractures %i' %(count3))
  print('Number of cells containing 4 or more fractures %i' %(morethan4))
  print('Possible false connections %i (cells containing >= 3 fractures)' % (count3+morethan4))
  fin.close()
  count = {'cells':cellcount,'active':(cellcount-count0),'false':(count3+morethan4)}
  return count

def p32(filename,nfrac,nx,ny,nz,d):
  '''filename = string which ought to be 'length.dat'
     nx, ny, nz = integers, number of cells in each direction
     d = float, length of cell side
     calculate total surface area of fractures and divide by volume of domain
     return P32, fracture density in m2/m3
  '''

  fin = file(filename,'r')
  r = np.zeros((nfrac),'=f8')
  fin.readline() #first line is a header
  for i in range(nfrac):
    line = fin.readline()
    r[i] = (line.split()[3])

  fin.close()
  area = np.square(r)*np.pi
  vol = (nx*d)*(ny*d)*(nz*d)
  print(vol)
  P32 = np.sum(area[5:])/vol #this is an approximation because not all fracs are completely within the domain
                             #so I've skipped the largest ones
  return P32


#Neither of these write functions works
def write_isok_h5(filename,khdf5,d):
  h5file=File(filename,'w')
  # 3d uniform grid
  h5grp = h5file.create_group('Permeability')
  # 3D will always be XYZ where as 2D can be XY, XZ, etc. and 1D can be X, Y or Z
  h5grp.attrs['Dimension'] = numpy.string_('XYZ')
  # based on Dimension, specify the uniform grid spacing
  h5grp.attrs['Discretization'] = [d,d,d]
  # again, depends on Dimension
  h5grp.attrs['Origin'] = [0.,0.,0.]#this will move the whole thing to 0,0,0
  # leave this line out if not cell centered.  If set to False, it will still
  # be true (issue with HDF5 and Fortran)
  h5grp.attrs['Cell Centered'] = [True]
  h5grp.attrs['Interpolation Method'] = numpy.string_('Step')
  h5grp.create_dataset('Data', data=khdf5) #does this matter that it is also called data?
  h5file.close()

  return

def write_anisok_h5(h5file,kx,ky,kz,d):
  # 3d uniform grid
  h5grp = h5file.create_group('PermeabilityX')
  # 3D will always be XYZ where as 2D can be XY, XZ, etc. and 1D can be X, Y or Z
  h5grp.attrs['Dimension'] = numpy.string_('XYZ')
  # based on Dimension, specify the uniform grid spacing
  h5grp.attrs['Discretization'] = [d,d,d]
  # again, depends on Dimension
  h5grp.attrs['Origin'] = [0.,0.,0.]#this will move the whole thing to 0,0,0
  # leave this line out if not cell centered.  If set to False, it will still
  # be true (issue with HDF5 and Fortran)
  h5grp.attrs['Cell Centered'] = [True]
  h5grp.attrs['Interpolation Method'] = numpy.string_('Step')
  h5grp.create_dataset('Data', data=kx) #does this matter that it is also called data?

  # 3d uniform grid
  h5grp = h5file.create_group('PermeabilityY')
  # 3D will always be XYZ where as 2D can be XY, XZ, etc. and 1D can be X, Y or Z
  h5grp.attrs['Dimension'] = numpy.string_('XYZ')
  # based on Dimension, specify the uniform grid spacing
  h5grp.attrs['Discretization'] = [d,d,d]
  # again, depends on Dimension
  h5grp.attrs['Origin'] = [0.,0.,0.]#this will move the whole thing to 0,0,0
  # leave this line out if not cell centered.  If set to False, it will still
  # be true (issue with HDF5 and Fortran)
  h5grp.attrs['Cell Centered'] = [True]
  h5grp.attrs['Interpolation Method'] = numpy.string_('Step')
  h5grp.create_dataset('Data', data=ky) #does this matter that it is also called data?

  # 3d uniform grid
  h5grp = h5file.create_group('PermeabilityZ')
  # 3D will always be XYZ where as 2D can be XY, XZ, etc. and 1D can be X, Y or Z
  h5grp.attrs['Dimension'] = numpy.string_('XYZ')
  # based on Dimension, specify the uniform grid spacing
  h5grp.attrs['Discretization'] = [d,d,d]
  # again, depends on Dimension
  h5grp.attrs['Origin'] = [0.,0.,0.]#this will move the whole thing to 0,0,0
  # leave this line out if not cell centered.  If set to False, it will still
  # be true (issue with HDF5 and Fortran)
  h5grp.attrs['Cell Centered'] = [True]
  h5grp.attrs['Interpolation Method'] = numpy.string_('Step')
  h5grp.create_dataset('Data', data=kz) #does this matter that it is also called data?

  return

'''    
##########Try it out#############
nfrac = 9
origin = [-5.,-5.,-5.]
nx = 50
ny = 50
nz = 50
d = 0.2
ellipses = readEllipse('ELLIPSES.OUT',nfrac)
fracture = map(ellipses, origin, nx, ny, nz, d)
fout = file('mapELLIPSES.txt','w')
fout.write('#x, y, z, number of fractures, fracture numbers\n')
h5file = File('mapELLIPSES.h5','w')

#arrays for making PFLOTRAN hf file
x = np.zeros(nx,'=f8')
y = np.zeros(ny,'=f8')
z = np.zeros(ny,'=f8')
a = np.zeros((nx,ny,nz),'=f8')

#write text file and fill arrays
for k in range(nz):
  z[k] = origin[2]+k*d+d/2
  for j in range(ny):
    y[j] = origin[1]+j*d+d/2
    for i in range(nx):
      x[i] = origin[0]+i*d+d/2
      fout.write('%e  %e  %e  %i ' %(origin[0]+i*d+d/2.,origin[1]+j*d+d/2.,origin[2]+k*d+d/2.,fracture[i+nx*j+nx*ny*k][0] ))
      if type(fracture[i+nx*j+nx*ny*k][1]) == list:
        a[i][j][k] = fracture[i+nx*j+nx*ny*k][1][0] #color by the first fracture number in the list
        for c in range(len(fracture[i+nx*j+nx*ny*k][1])): 
          fout.write(' '+str(fracture[i+nx*j+nx*ny*k][1][c]))
      else:
        a[i][j][k] = 0 #color it zero
        fout.write(' '+str(fracture[i+nx*j+nx*ny*k][1])) #?
      fout.write('\n')

fout.close()

#create PFLOTRAN h5 file for 3D viewing
dataset_name = 'Coordinates/X [m]'
h5dset = h5file.create_dataset(dataset_name, data=x)
dataset_name = 'Coordinates/Y [m]'
h5dset = h5file.create_dataset(dataset_name, data=y)
dataset_name = 'Coordinates/Z [m]'
h5dset = h5file.create_dataset(dataset_name, data=z)

dataset_name = 'Time:  0.00000E+00 y/value'
h5dset = h5file.create_dataset(dataset_name, data=a)
h5file.close()

print 'Done!'
'''
