#!/usr/bin/env python

import os,sys
from dfnworks import *

dfn = dfnworks(inp_file='../full_mesh.inp',perm_file='../perm.dat',aper_file='../aperture.dat')

#need some kind of loop in here, to do each vtk file?
dfn.parse_pflotran_vtk()
