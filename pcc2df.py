'''pcc2df.py
   read calc_pcc.py output files pcc.txt or prcc.txt
   load PCCs into a dataframe with response functions as index and
   input params as columns.
   plan to call this from barcharts_sa.py

   Emily Stein (ergiamb@sandia.gov)
   9.8.18
'''

import pandas as pd
import sys,os
import argparse
import numpy as np

def pcc2df(directory,response_fns,transform):
  '''read calc_pcc output to dataframe,
     user input (response_fns) names as index labels
     transform = 'raw' or 'rank'
  '''
  print('in pcc2df.py')
  if transform == 'raw':
    filename = os.path.join(directory,'pcc.txt')
  elif transform == 'rank':
    filename = os.path.join(directory,'prcc.txt')
  else:
    print('unrecognized transform type. choose raw or rank.')
    return
  df = pd.read_csv(filename,delim_whitespace=True,header=0,index_col=0)
  return(df)
     
#######################################################################
if __name__ == '__main__':

  response_fn_names = [ 
                'usand1',
                'usand2',
                'usand3',
                'lime1',
                'lime2',
                'lime3',
                'lsand1',
                'lsand2',
                'lsand3',
               ]   

  
  parser = argparse.ArgumentParser(description='read calc_pcc.py output into df')
  parser.add_argument('-d','--directory',
                      help='directory containing calc_pcc.py output',
                      required=True,)
  parser.add_argument('-r','--response_fns',nargs='+',
                      help='list of response_fn names, in correct order!',
                      default = response_fn_names,
                      required=False,)
  parser.add_argument('-t','--transform',
                      help='raw or rank' +
                           '[default = raw]',
                      default='raw',
                      required=False,)
  args = parser.parse_args()

  df = pcc2df(args.directory,args.response_fns,args.transform)
  print(df)
      

