'''
Write 1D tails for explicit unstructured grid (.uge)
Requires cell centers and volumes; face centers and areas (connections)
Also nice to have some way of visualizing, so write .h5 and .xmf for Paraview

Adding a 6-way split in bulk 3D cell for improved discretization. 4.7.17
Use half of bulk/parent cell volume for 6-way split
Specify waste package volume
Include ends of prism/cylinder in connection areas
Connection area between tail and split has same shape (eg PRISM) as volumes in tail
Eliminate lateral_connection option.

@author Emily Stein
Sandia National Laboratories
1.13.17
'''

#I should separate reading input from the calculation of the tail (into 2 f'ns)
#so that I can call the tail fn from abaqus2explicit.py
#without ever writing the original explicit grid file.
#perhaps. need to think about the organizational effort involved.

import numpy as np
import os
import sys
import h5py
import write_xmf as wxmf

def make_tail():
  coord = [17.5,12.5,37.5] #3d center to attach it to
  nx = 16               #number of intervals to discretize it
  shape = 'PRISM' #'PRISM','CYLINDER','SPHERE'
  length = 5.           #length (m) of the cylinder or prism
  wpvol = (5./3.)**2.*length #size of 12 pwr in typical pa discretization

  ugename = '3dgrid.uge'   #in file containing main grid in uge format
  domainname = '3dgrid-domain.h5' #in file containing main grid for Paraview
  tailname = ugename.split('.')[0]+'-split-%s%i.uge' % (shape.lower(),nx)
  h5name = ugename.split('.')[0]+'-split-%s%i.h5' % (shape.lower(),nx)
  xmfname = ugename.split('.')[0]+'-split-%s%i.xmf' % (shape.lower(),nx)

  cells = [] #[id, x, y, z, volume]
  connections = [] #[[id1, id2, x, y, z, area]]
  infile = open(ugename,'r')
  while(1):
    line = infile.readline()
    if line == '': #empty string is end of file
      print('At end of file, big loop')
      break
    if line.startswith('CELLS'):
      while(True):
        line = infile.readline()
        if line.startswith('CONNECTIONS'): #then we've read all the cells
          break
        w = line.split()
        cells.append([int(w[0]),float(w[1]),float(w[2]),float(w[3]),float(w[4])])
        if len(cells) % 100 == 0:
          print('Cell %d' % len(cells))
    else: #Already read the line that startswith('CONNECTIONS')
      while(True):
        line = infile.readline()
        if line == '': #empty string is end of file
          print('At end of file, small loop')
          break
        w = line.split()
        connections.append([int(w[0]),int(w[1]),float(w[2]),float(w[3]),float(w[4]),float(w[5])])
        if len(connections) % 100 == 0:
          print('Connections %d' % len(connections))
  infile.close()

  print('Find volume and id of parent cell')
  for cell in cells:
    if cell[1:4] == coord:
      totvol = cell[4]
      parent = cell[0] #id number not list index!
      break
  print('Find adjacent cells')
  eastcoord = [coord[0]-5.,coord[1],coord[2]]
  westcoord = [coord[0]+5.,coord[1],coord[2]]
  southcoord = [coord[0],coord[1]-5.,coord[2]]
  northcoord = [coord[0],coord[1]+5.,coord[2]]
  botcoord = [coord[0],coord[1],coord[2]-5.]
  topcoord = [coord[0],coord[1],coord[2]+5.]
  for cell in cells:
    if cell[1:4] == eastcoord: east = cell[0]
    elif cell[1:4] == westcoord: west = cell[0]
    elif cell[1:4] == southcoord: south = cell[0]
    elif cell[1:4] == northcoord: north = cell[0]
    elif cell[1:4] == botcoord: bot = cell[0]
    elif cell[1:4] == topcoord: top = cell[0]
  #Find radius of equivalent shape, discretize, and find volumes, etc.
  print('Discretize tail')
  firsttail = len(cells) #zero-indexed, not id number
  firstcon = len(connections)
  cellid = firsttail + 1
  sa = totvol/length
  print('sa = %f' % sa)
  sa_wp = wpvol/length
  print('sa_wp = %f' % sa_wp)
  halfvol = totvol/2.
  sa_half = halfvol/length
  print('sa_half = %f' % sa_half)
  if wpvol > halfvol:
    print('wpvol > half volume - exiting')
    return
  #find r and r_wp
  if shape.upper() == 'CYLINDER': #volume is the same whether you choose prism or cylinder!
    print('CYLINDER')             #but the surface area is smaller and the radius is greater
    r_half = np.sqrt(sa/np.pi)
    print('radius = %f' % r)
    r_half = np.sqrt(sa_half/np.pi)
    r_wp = np.sqrt(sa_wp/np.pi)
    print('wp radius = %f' % r_wp)
  elif shape.upper() == 'PRISM':
    print('PRISM')
    r_half = np.sqrt(sa)/2.
    r_half = np.sqrt(sa_half)/2.
    r_wp = np.sqrt(sa_wp)/2.
    print('wp radius = %f' % r_wp)
  elif shape.upper() == 'SPHERE':
    print('SPHERE')
    r_half = np.cbrt(3./4.*totvol/np.pi)
    r_half = np.cbrt(3./4.*totvol/2./np.pi)
    r_wp = np.cbrt(3./4.*wpvol/np.pi)
    print('wp radius = %f' % r_wp)
  #calc dx
  nx_tail = nx - 2
  dx = (r_half-r_wp)/float(nx_tail)
  dhalf = dx/2.
  r = r_half - dhalf #one ring smaller for 6-way split
  #find cell centers
  tailvol = 0.
  for i in range(nx_tail):
    if i == 0:
      xc = cells[parent-1][1] + r
    else:
      xc = cells[parent-1][1] + r-(r_wp+dhalf+dx*(i-1))
    yc = cells[parent-1][2]
    zc = cells[parent-1][3]
    if i == 0: rcon = r_wp
    else: rcon = r_wp + dx*i
    if shape.upper() == 'CYLINDER':
      vol = np.pi*(rcon**2.)*length - tailvol
    elif shape.upper() == 'PRISM':
      vol = ((2.*rcon)**2.*length) - tailvol
    elif shape.upper() == 'SPHERE':
      vol = 4./3.*np.pi*rcon**3.
    cells.append([cellid,xc,yc,zc,vol])
    xc = cells[parent-1][1] + (r-rcon) #distance from body of tail
    yc = cells[parent-1][2]
    zc = cells[parent-1][3]
    if shape.upper() == 'CYLINDER':
      area = 2.*rcon*np.pi*length + np.pi*rcon**2.*2
    elif shape.upper() == 'PRISM':
      area = 4.*length*(2.*rcon) + 2*(2.*rcon)**2.
    elif shape.upper() == 'SPHERE':
      area = 4.*np.pi*rcon**2.
    if i < nx_tail-1: #connect to next tail cell
      connections.append([cellid,cellid+1,xc,yc,zc,area])
    else: #connect to parent
      connections.append([cellid,parent,xc,yc,zc,area])
    cellid += 1
    tailvol += vol
  #modify volume of body cell
  cells[parent-1][4] -= (tailvol + halfvol)
  #create 6 new cells with quick and dirty addition of 2.
  #cells[parent-1][4] /= 7.
  #vol = cells[parent-1][4] 
  vol = halfvol/6.
  #area = np.cbrt(halfvol)**2.
  if shape.upper() == 'CYLINDER':
    area = 2.*r_half*np.pi*length + np.pi*r_half**2.*2
    area /= 6.
  elif shape.upper() == 'PRISM':
    area = 4.*length*(2.*r_half) + 2*(2.*r_half)**2.
    area /= 6.
  elif shape.upper() == 'SPHERE':
    area = 4.*np.pi*r_half**2.
    area /= 6.
  #cell east
  westid = cellid
  xc = cells[parent-1][1] + 2.25
  yc = cells[parent-1][2]
  zc = cells[parent-1][3]
  cells.append([cellid,xc,yc,zc,vol])
  connections.append([cellid,parent,xc-2.,yc,zc,area])
  cellid +=1 #cell north
  northid = cellid
  xc = cells[parent-1][1]
  yc = cells[parent-1][2] + 2.25
  zc = cells[parent-1][3]
  cells.append([cellid,xc,yc,zc,vol])
  connections.append([cellid,parent,xc,yc-2.,zc,area])
  cellid +=1 #cell top
  topid = cellid
  xc = cells[parent-1][1]
  yc = cells[parent-1][2]
  zc = cells[parent-1][3] + 2.25
  cells.append([cellid,xc,yc,zc,vol])
  connections.append([cellid,parent,xc,yc,zc-2.,area])
  cellid +=1 #cell west
  eastid = cellid
  xc = cells[parent-1][1] - 2.25
  yc = cells[parent-1][2]
  zc = cells[parent-1][3]
  cells.append([cellid,xc,yc,zc,vol])
  connections.append([cellid,parent,xc+2.,yc,zc,area])
  cellid +=1 #cell south
  southid = cellid
  xc = cells[parent-1][1]
  yc = cells[parent-1][2] - 2.25
  zc = cells[parent-1][3]
  cells.append([cellid,xc,yc,zc,vol])
  connections.append([cellid,parent,xc,yc+2.,zc,area])
  cellid +=1 #cell bot
  botid = cellid 
  xc = cells[parent-1][1]
  yc = cells[parent-1][2]
  zc = cells[parent-1][3] - 2.25
  cells.append([cellid,xc,yc,zc,vol])
  connections.append([cellid,parent,xc,yc,zc+2.,area])
  #overwrite parent cell connections to adjacent cells with these new cells (same areas and locations)
  for connect in connections:
    if connect[0:2] == [parent, east]: connect[0] = eastid
    elif connect[0:2] == [east, parent]: connect[1] = eastid
    elif connect[0:2] == [parent, west]: connect[0] = westid
    elif connect[0:2] == [west, parent]: connect[1] = westid
    elif connect[0:2] == [parent, south]: connect[0] = southid
    elif connect[0:2] == [south, parent]: connect[1] = southid
    elif connect[0:2] == [parent, north]: connect[0] = northid
    elif connect[0:2] == [north, parent]: connect[1] = northid
    elif connect[0:2] == [parent, top]: connect[0] = topid
    elif connect[0:2] == [top, parent]: connect[1] = topid
    elif connect[0:2] == [parent, bot]: connect[0] = botid
    elif connect[0:2] == [bot, parent]: connect[1] = botid

  #Write .uge file, original plus tail
  print('Write %s' % (tailname))
  f = open(tailname,'w')
  f.write('CELLS %d\n' % len(cells))
  for cell in cells:
    f.write('%d %f %f %f %f\n' % (cell[0],cell[1],cell[2],cell[3],cell[4]))
  f.write('CONNECTIONS %d\n' % len(connections))
  for con in connections:
    f.write('%d %d %f %f %f %f\n' % (con[0],con[1],con[2],con[3],con[4],con[5]))
  f.close()
  #Print tail values to screen
  print('CellID XCenter Volume TailVol CellID XConnection Area')
  icon = firstcon #zero-indexed 
  tailvol = 0. #over again, totally inefficient
  for i in range(firsttail,len(cells)):
    tailvol += cells[i][4]
    print('%i %f %f %f %i %f %f' % (cells[i][0],cells[i][1],cells[i][4],tailvol,
                                 connections[icon][0],connections[icon][2],connections[icon][5]))
    icon += 1
  
  #Assign cell corners (nodes/vertices) and write .h5 and .xmf for Paraview
  #Inspired by WIPP 2D flaring grid
  tid = []
  txc = []
  tyc = []
  tzc = []
  tvol = []
  tvertices = []
  telements = []
  npoint = 0
  icon = firstcon
  for i in range(firsttail,len(cells)):
    #number of cons in tail is the same as number of cells, so:
    side = connections[icon][5]/dx
    y1 = connections[icon][3] - side/2.
    y2 = y1 + side
    p1 = [connections[icon][2],y1,connections[icon][4]]    #because neglecting the length
    p2 = [connections[icon][2]+dx,y1,connections[icon][4]] #not showing volume only area
    p3 = [connections[icon][2]+dx,y2,connections[icon][4]] #not showing volume only area
    p4 = [connections[icon][2],y2,connections[icon][4]]    #because neglecting the length
    tvertices.extend([p1,p2,p3,p4])
    telements.extend([5,npoint,npoint+1,npoint+2,npoint+3])
    txc.append(cells[i][1])
    tyc.append(cells[i][2])
    tzc.append(cells[i][3])
    tvol.append(cells[i][4])
    tid.append(i+1) #pretty sure this captures the correct order, because why wouldn't they be in order?
    npoint += 4
    icon += 1
  #write h5 file with all cells (bodies and tails) in it
  #open original domain file,read to arrays
  h5in = h5py.File(domainname,'r')
  Vertices = np.array(h5in['Domain/Vertices'])
  Cells = np.array(h5in['Domain/Cells'])
  Cell_Ids = np.array(h5in['Domain/Cell_Ids'])
  XC = np.array(h5in['Domain/XC'])
  YC = np.array(h5in['Domain/YC'])
  ZC = np.array(h5in['Domain/ZC'])
  Volume = np.array(h5in['Domain/Volume'])
  h5in.close()
  #add tail values to the end of all the arrays
  for i in range(len(telements)): #vertex ids in tail need to be upped
    if(i%5 != 0):  #don't change the 5s!
      telements[i] += len(Vertices)
  Vertices2 = np.append(arr=Vertices,axis=0,values=tvertices)
  Cells2 = np.append(arr=Cells,axis=0,values=telements)
  Cell_Ids2 = np.append(arr=Cell_Ids,axis=0,values=tid)
  XC2 = np.append(arr=XC,axis=0,values=txc)
  YC2 = np.append(arr=YC,axis=0,values=tyc)
  ZC2 = np.append(arr=ZC,axis=0,values=tzc)
  Volume2 = np.append(arr=Volume,axis=0,values=tvol)
  #then write with tails h5 file
  print('Write %s' % h5name)
  h5f = h5py.File(h5name,'w')
  h5f.create_dataset('Domain/Vertices',data=Vertices2)
  h5f.create_dataset('Domain/Cells',data=Cells2)
  h5f.create_dataset('Domain/Cell_Ids',data=Cell_Ids2)
  h5f.create_dataset('Domain/XC',data=XC2)
  h5f.create_dataset('Domain/YC',data=YC2)
  h5f.create_dataset('Domain/ZC',data=ZC2)
  h5f.create_dataset('Domain/Volume',data=Volume2)
  h5f.close()   
  #and new xmf file pointing to tails h5 file
  print('Write %s' % xmfname)
  xmf = open(xmfname,'w')
  xmf.write(wxmf.header_template)
  xmf.write(wxmf.topo_template % (len(cells),len(Cells2),h5name.split('.')[0]))
  xmf.write(wxmf.geo_template % (len(Vertices2),h5name.split('.')[0]))
  xmf.write(wxmf.XYZ_template % ('XC','Scalar','Cell',len(XC2),h5name.split('.')[0],'Domain','XC'))
  xmf.write(wxmf.XYZ_template % ('YC','Scalar','Cell',len(YC2),h5name.split('.')[0],'Domain','YC'))
  xmf.write(wxmf.XYZ_template % ('ZC','Scalar','Cell',len(ZC2),h5name.split('.')[0],'Domain','ZC'))
  xmf.write(wxmf.XYZ_template % ('Volume','Scalar','Cell',len(Volume2),h5name.split('.')[0],'Domain','Volume'))
  xmf.write(wxmf.XYZ_template % ('Cell_Ids','Scalar','Cell',len(Cell_Ids2),h5name.split('.')[0],'Domain','Cell_Ids'))
  xmf.write(wxmf.tail_template)
  xmf.close()

  print('done')
  return

if __name__ == "__main__":
  make_tail()

