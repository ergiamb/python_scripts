'''calc_pcc.py
   Dakota is returning nan for some partial correlation coefficients, so I am
   wondering what  some other thing returns. pandas and statsmodels and scipy do
   not seem to have partial correlation coeff methods.
   Emily Stein <ergiamb@sandia.gov>
   08.17.18 (generalized 11/1/19)

   the method partial_corr() is from https://gist.github.com/fabianp/9396204419c7b638d38f
   the file io is mine
'''
from stepwise import load_data
import pandas as pd
import argparse

"""
Partial Correlation in Python (clone of Matlab's partialcorr)

This uses the linear regression approach to compute the partial 
correlation (might be slow for a huge number of variables). The 
algorithm is detailed here:
    http://en.wikipedia.org/wiki/Partial_correlation#Using_linear_regression
Taking X and Y two variables of interest and Z the matrix with all the variable minus {X, Y},
the algorithm can be summarized as
    1) perform a normal linear least-squares regression with X as the target and Z as the predictor
    2) calculate the residuals in Step #1
    3) perform a normal linear least-squares regression with Y as the target and Z as the predictor
    4) calculate the residuals in Step #3
    5) calculate the correlation coefficient between the residuals from Steps #2 and #4; 
    The result is the partial correlation between X and Y while controlling for the effect of Z

Date: Nov 2014
Author: Fabian Pedregosa-Izquierdo, f@bianp.net
Testing: Valentina Borghesani, valentinaborghesani@gmail.com
"""

import numpy as np
from scipy import stats, linalg

def partial_corr(C):
    """
    Returns the sample linear partial correlation coefficients between pairs of variables in C, controlling 
    for the remaining variables in C.

    Parameters
    ----------
    C : array-like, shape (n, p)
        Array with the different variables. Each column of C is taken as a variable

    Returns
    -------
    P : array-like, shape (p, p)
        P[i, j] contains the partial correlation of C[:, i] and C[:, j] controlling
        for the remaining variables in C.
    """
    C = np.asarray(C)
    p = C.shape[1]
    P_corr = np.zeros((p, p), dtype=np.float)
    P_pval = np.zeros((p, p), dtype=np.float)
    for i in range(p):
        P_corr[i, i] = 1
        P_pval[i, i] = 1
        for j in range(i+1, p):
            idx = np.ones(p, dtype=np.bool)
            idx[i] = False
            idx[j] = False
            beta_i = linalg.lstsq(C[:, idx], C[:, j])[0]
            beta_j = linalg.lstsq(C[:, idx], C[:, i])[0]

            res_j = C[:, j] - C[:, idx].dot( beta_i)
            res_i = C[:, i] - C[:, idx].dot(beta_j)
            
            #pearsonr() returns (CC,p-value)
            corr,pvalue = stats.pearsonr(res_i, res_j)#[0]
            P_corr[i, j] = corr
            P_corr[j, i] = corr
            P_pval[i, j] = pvalue
            P_pval[j, i] = pvalue
        
    return P_corr,P_pval

if __name__ == '__main__':
  #file I/O is copied from stepwise.py.
  parser = argparse.ArgumentParser(description=
                    'Iterate to find Partial Correlation Coefficients')
  parser.add_argument('-n','--num_ind_var',
                      help = 'number of independent (input) variables',
                      required = True)
  parser.add_argument('-d','--num_dep_var',
                      help = 'number of dependent (output) variables to process'+
                             '[default = all]',
                      default = 'all',
                      required = False)
  parser.add_argument('-f','--dakota_tab',
                      help = 'dakota tabular file containing input and output values'+
                             '[default = dakota_tabular.dat]',
                      default = 'dakota_tabular.dat',
                      required = False)
  parser.add_argument('-r','--rank',
                      help = 'set to True for rank transform'+
                             '[default = False]',
                      default = False,
                      required = False)
  args = parser.parse_args()

  niv = int(args.num_ind_var)
  if args.num_dep_var != 'all':
    try:
      ndv = int(args.num_dep_var)
    except:
      print('If used, --num_dep_var must be an integer or the word "all". Exiting')
      sys.exit()

  with open(args.dakota_tab,'r') as f:
    header = f.readline().split()
  if '%eval_id' in header:
    evalid = True
    xydata = load_data(args.dakota_tab,evalid,args.rank)
    if 'interface' in header: #assumes first column is %eval_id and second is interface
      if args.num_dep_var == 'all':
        ndv = len(header)-niv-2
      iv_list = header[2:niv+2]
      dv_list = header[niv+2:niv+2+ndv]
    else: #assumes first column is %eval_id
      if args.num_dep_var == 'all':
        ndv = len(header)-niv-1
      iv_list = header[1:niv+1]
      dv_list = header[niv+1:niv+1+ndv]
  else:
    evalid = False
    xydata = load_data(args.dakota_tab,evalid,args.rank)
    if 'interface' in header: #assumes first column is interface
      if args.num_dep_var == 'all':
        ndv = len(header)-niv-1
      iv_list = header[1:niv+1]
      dv_list = header[niv+1:niv+1+ndv]
    else: #assumes first column is first iv
      if args.num_dep_var == 'all':
        ndv = len(header)-niv
      iv_list = header[:niv]
      dv_list = header[niv:niv+ndv]
  print('independent variables {}'.format(iv_list))
  print('dependent variables {}'.format(dv_list))
  #for dv in dv_list:
  #  Stepwise(xydata,iv_list,dv)
  print('done with stuff that is identical to stepwise.py')

  if args.rank == True:
    pcc_outfile = 'prcc.txt'
    pvalue_out = 'prcc_pvalue.txt'
  else:
    pcc_outfile = 'pcc.txt'
    pvalue_out = 'pcc_pvalue.txt'
  with open(pcc_outfile,'w') as f, open(pvalue_out,'w') as g:
    f.write('                ')
    f.write(''.join('{:13s}'.format(xydata.columns[j]) for j in range(niv)))
    f.write('\n')
    g.write('                ')
    g.write(''.join('{:13s}'.format(xydata.columns[j]) for j in range(niv)))
    g.write('\n')
    for i in range(ndv):
      input_list = iv_list
      input_list.append('response_fn_{}'.format(i+1))
      pcc = pd.DataFrame(data=partial_corr(xydata[input_list].values)[0],index=input_list,columns=input_list)
      pval = pd.DataFrame(data=partial_corr(xydata[input_list].values)[1],index=input_list,columns=input_list)
      f.write('{} '.format(dv_list[i]))
      f.write(''.join('{:13.5e}'.format(pcc.iloc[-1,j]) for j in range(niv)))
      f.write('\n')
      g.write('{} '.format(dv_list[i]))
      g.write(''.join('{:13.5e}'.format(pval.iloc[-1,j]) for j in range(niv)))
      g.write('\n')
