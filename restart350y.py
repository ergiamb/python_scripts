'''
modify checkpoint file produced by t = 0 to 350 run for restart at 350 y

Emily 3.3.17
'''

import numpy as np
from h5py import *
import wipp_uge as wipp

fnchpt = 'pflotran-restart350y.h5' #copy (and rename) this from 0 to 350 directory
#reset permeability, porosity, pressure, and saturation in the borehole intrusion
fnbh_open = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rBH_OPEN.txt'
fnbh_creep = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rBH_CREEP.txt'
fnconc_plg = '/home/ergiamb/modeling/2017wipp/run_control/general_pfd/grid/rCONC_PLG.txt'

bh_pres = wipp.REF_PRES
zero_sg = 1. - 0.001 

bh_por = 3.2e-1
bh_k = np.power(10.,-9.0)
cp_por = 3.2e-1
cp_k = np.power(10.,-17.8022) 

h5 = File(fnchpt,'r+')
#Reset initial pressure and saturation, porosity and permeability 
icp_chpt = np.array(h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:],'=f8')
state = np.array(h5['Checkpoint/PMCSubsurface/flow/State'][:],'=f8')
por = np.array(h5['Checkpoint/PMCSubsurface/flow/Porosity'][:],'=f8')
permx = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_X'][:],'=f8')
permy = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_Y'][:],'=f8')
permz = np.array(h5['Checkpoint/PMCSubsurface/flow/Permeability_Z'][:],'=f8')
f = open(fnbh_open,'r') #saturation and pressure?
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = bh_pres
  icp_chpt[index+1] = zero_sg
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = bh_por
  permx[int(line)-1] = bh_k
  permy[int(line)-1] = bh_k
  permz[int(line)-1] = bh_k
f.close()
f = open(fnbh_creep,'r') #saturation and pressure?
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = bh_pres
  icp_chpt[index+1] = zero_sg
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = bh_por
  permx[int(line)-1] = bh_k
  permy[int(line)-1] = bh_k
  permz[int(line)-1] = bh_k
f.close()
f = open(fnconc_plg,'r') #saturation and pressure?
for line in f:
  index = (int(line)-1)*3
  icp_chpt[index] = bh_pres #?
  icp_chpt[index+1] = zero_sg #?
  icp_chpt[index+2] = wipp.temperature
  state[int(line)-1] = 3
  por[int(line)-1] = cp_por
  permx[int(line)-1] = cp_k
  permy[int(line)-1] = cp_k
  permz[int(line)-1] = cp_k
f.close()

h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:] = icp_chpt
h5['Checkpoint/PMCSubsurface/flow/State'][:] = state
h5['Checkpoint/PMCSubsurface/flow/Porosity'][:] = por
h5['Checkpoint/PMCSubsurface/flow/Permeability_X'][:] = permx
h5['Checkpoint/PMCSubsurface/flow/Permeability_Y'][:] = permy
h5['Checkpoint/PMCSubsurface/flow/Permeability_Z'][:] = permz
h5.close()



