def boolean_fractures(filename='mapELLIPSES.txt'):
  '''filename = string which is most likely mapELLIPSES.txt
     look for cells with fractures in them and assign 0 (no fractures) or 1

     also return count of likely false connectivity.
     this script was originally count_stuff() in mapdfn.py

     Emily 8.2.17
  '''

  fin = file(filename,'r')
  bool_list = []
  cellcount = 0
  count0 = 0
  count1 = 0
  count2 = 0
  count3 = 0
  morethan4 = 0
  print('Counting...')
  for line in fin:
    if line.startswith('#'):
      continue
    elif int(line.split()[3]) == 0:
      cellcount += 1
      count0 += 1
      bool_list.append(0)
    elif int(line.split()[3]) == 1:
      cellcount += 1
      count1 += 1
      bool_list.append(1)
    elif int(line.split()[3]) == 2:
      cellcount += 1
      count2 += 1
      bool_list.append(1)
    elif int(line.split()[3]) == 3:
      cellcount += 1
      count3 += 1
      bool_list.append(1)
    elif int(line.split()[3]) >= 4:
      cellcount += 1
      morethan4 += 1
      bool_list.append(1)
  print('\n')
  fout = file('falseconnections.txt','w')
  fout.write('Information for %s\n ' % filename)
  fout.write('Total number of cells in grid %i\n' % cellcount)
  fout.write('Number of cells containing fractures %i\n' % (cellcount-count0))
  fout.write('Percent active cells %.1f\n' % (100.*(float(cellcount)-float(count0))/float(cellcount)))
  fout.write('Number of cells containing 1 fracture %i\n' %(count1))
  fout.write('Number of cells containing 2 fractures %i\n' %(count2))
  fout.write('Number of cells containing 3 fractures %i\n' %(count3))
  fout.write('Number of cells containing 4 or more fractures %i\n' %(morethan4))
  fout.write('Possible false connections %i (cells containing >= 3 fractures)\n' % (count3+morethan4))
  fout.close()
  fin.close()
  fout = file('boolean_fracture.inp','w')
  for x in bool_list:
    fout.write('{}\n'.format(x))
  fout.close()
  count = {'cells':cellcount,'active':(cellcount-count0),'false':(count3+morethan4)}
  return count


if __name__ == '__main__':
  boolean_fractures()
  print('done')

