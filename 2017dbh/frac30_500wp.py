#make regions, strata, initial condition, and source sink cards to go with 2017dbh/grid500wp/
#Fracture with 30 degree dip intersecting the borehole. WP stuck at intersection, z=3458.7
#Keep cement plugs in EZ. Open borehole to stuck package. Seal above that.
#half symmetry domain.
#yes making boundary condition cards in here, and flow and transport conditions, but not materials
#emily 11.17.16

#Need polygon regions, which is a new complication!

import wfg_block as wfg
import math as math

#hardwire some names
mat = ['granite','granite_drz','Cs_waste_package','Sr_waste_package',
       'steel','brine','cement','bentonite','ballast','sediment','sed_drz']
flow = ['initial','heat_cs','heat_sr','bottom']
trans = ['initial_granite','initial_sediment','initial_bentonite','initial_nosorption']

#define dimensions (for the most part)
small = 5. #smallest z discretization

num_cs = 74 #number of cs waste packages
num_sr = 34 #number of sr waste packages

zbase = 0. #base of domain
z1 = 1000. #base of emplacement zone (ez)
z_wp = small
z_cement_ez = small*2.
z2 = z1 + z_wp*40. #base of first cement plug
z3 = z2 + z_cement_ez + z_wp*40. #base of second cement plug
z4 = z3 + z_cement_ez + z_wp*(num_cs+num_sr-80.) #base of seal zone
zgranite = 4005.
ztop = 6000.
zstuck = 3455. #+5m length will cross fracture intersection
zfwb = 3450. #base of west fracture at drz
zfwt = 3465. #top of west fracture at drz
zfeb = 3450. #base of east fracture at drz
zfet = 3480 #top of east fracture at drz

xwest = 0.
xeast = 2010
ysouth = 0.
ynorth = 2010./2.

r_wp = 0.11
r_annulus = 0.16
r_drz = 0.32
angle = 360./16. #22.5 degrees

#write region, strata, source sink, and waste form blocks
#model domain, then DRZ, then buffer, then wp
rfile = file('regions.txt', 'w')
sfile = file('strata.txt', 'w')
cfile = file('condition.txt','w')
ssfile = file('source_sink.txt','w')
wfgfile = file('wfg.txt','w')
wfg.dbh_to_file(wfgfile) #inventory for Cs and Sr capsules

#files need to be open already, and closed afterward!
#someday I should put these functions in a separate file
def write_reg(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_reg_face(rname,xmin,ymin,zmin,xmax,ymax,zmax,rfile=rfile):
  rfile.write('REGION %s\n  FACE %s\n  COORDINATES\n    %f %f %f\n    %f %f %f\n  /\nEND\n\n'
              % (rname, rname, xmin, ymin, zmin, xmax, ymax, zmax))
  return

def write_polygon(rname,x1,y1,zmin,zmax,radius,angle,pt2,pt1=0,closeloop=0): #this is totally not general
  rfile.write('REGION %s\n  POLYGON\n    TYPE CELL_CENTERS_IN_VOLUME\n    XY\n'
              % (rname))
  for i in range(pt1,pt2):
    x = x1 + radius*(math.cos(math.radians(angle*i)))
    y = y1 + radius*(math.sin(math.radians(angle*i))) 
    rfile.write('    %f %f 0.0\n' % (x,y))
  if (closeloop != 0):
    rfile.write('    %f %f 0.0\n' % (x1,y1)) #hopefully this order works for both left and right halves
  rfile.write('    /\n    XZ\n'+
              '    -1.d20 0.0 %f\n     1.d20 0.0 %f\n     1.d20 0.0 %f\n    -1.d20 0.0 %f\n    /\n  /\nEND\n\n'
              % (zmin,zmin,zmax,zmax))
  return

def write_strata(rname,material,sfile=sfile):
  sfile.write('STRATA\n  REGION %s\n  MATERIAL %s\nEND\n\n'
              % (rname, material))
  return

def write_ic(flow,trans,rname,cfile=cfile):
  cfile.write('INITIAL_CONDITION\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
              % (flow, trans, rname))
  return

def write_bc(flow,trans,rname,cfile=cfile):
  cfile.write('BOUNDARY_CONDITION %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
              % (rname,flow,trans,rname))
  return

def write_ss(ssname,flow,trans,rname,ssfile=ssfile):
  ssfile.write('SOURCE_SINK %s\n  FLOW_CONDITION %s\n  TRANSPORT_CONDITION %s\n  REGION %s\nEND\n\n'
                % (ssname, flow, trans, rname))
  return

#Model Domain - cut and paste this first region into the main input deck
write_reg('all',xwest,ysouth,zbase,xeast,ynorth,ztop)
write_ic(flow[0],trans[0],'all')
write_strata('all',mat[0])
write_reg_face('top',xwest,ysouth,ztop,xeast,ynorth,ztop)
write_bc(flow[0],trans[1],'top')
write_reg_face('bottom',xwest,ysouth,zbase,xeast,ynorth,zbase)
write_bc(flow[0],trans[0],'bottom')
write_reg_face('west',xwest,ysouth,zbase,xwest,ynorth,ztop)
write_bc(flow[0],trans[0],'west')
write_reg_face('east',xeast,ysouth,zbase,xeast,ynorth,ztop)
write_bc(flow[0],trans[0],'east')
write_reg_face('south',xwest,ysouth,zbase,xeast,ysouth,ztop)
#noflow by default for south
write_reg_face('north',xwest,ynorth,zbase,xeast,ynorth,ztop)
write_bc(flow[0],trans[0],'north')

#sedimentary strata (only one for now)
write_reg('sediment',xwest,ysouth,zgranite,xeast,ynorth,ztop)
write_strata('sediment',mat[9])
write_ic(flow[0],trans[1],'sediment')

#DRZ granite, needs to be in 4 pieces in order to skip the fracture
#2 left/west pieces, above and below fracture
xmin = (xwest+xeast)/2.# - r_drz
ymin = ysouth
zmin = z1 #base of bh
zmax = zfwb #base of left fracture at drz
radius = r_drz
write_polygon('drz_granite_wb',xmin,ymin,zmin,zmax,radius,angle,9,pt1=4,closeloop=1)
write_strata('drz_granite_wb',mat[1])
zmin = zfwt
zmax = zgranite
write_polygon('drz_granite_wt',xmin,ymin,zmin,zmax,radius,angle,9,pt1=4,closeloop=1)
write_strata('drz_granite_wt',mat[1])
#2 right/east pieces, above and below fracture
zmin = z1
zmax = zfeb
write_polygon('drz_granite_eb',xmin,ymin,zmin,zmax,radius,angle,5,pt1=0,closeloop=1)
write_strata('drz_granite_eb',mat[1])
zmin = zfet
zmax = zgranite
write_polygon('drz_granite_et',xmin,ymin,zmin,zmax,radius,angle,5,pt1=0,closeloop=1)
write_strata('drz_granite_et',mat[1])

#DRZ seds
zmin = zgranite
zmax = ztop
write_polygon('drz_sed',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('drz_sed',mat[10])

#Borehole radius
#Annulus (brine)
xmin = (xwest+xeast)/2. #- r_annulus
ymin = ysouth
zmin = z1
zmax = zstuck + z_wp #open borehole to stuck waste package
radius = r_annulus
write_polygon('brine',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('brine',mat[5])
write_ic(flow[0],trans[3],'brine')
#Cement plug (lower)
zmin = z2
zmax = zmin + z_cement_ez
write_polygon('cement1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('cement1',mat[6])
write_ic(flow[0],trans[3],'cement1')
#Cement plug (upper)
zmin = z3
zmax = zmin + z_cement_ez
write_polygon('cement2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('cement2',mat[6])
write_ic(flow[0],trans[3],'cement2')
#Cement seal 1 (lowest), above the stuck waste package
zmin = zstuck + z_wp
zmax = zmin + small*20. #100 m
write_polygon('scement1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement1',mat[6])
write_ic(flow[0],trans[3],'scement1')
#Bentonite seal 1
zmin = zmax
zmax = zmin + small*10. #50 m
write_polygon('bentonite1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('bentonite1',mat[7])
write_ic(flow[0],trans[2],'bentonite1')
#Cement seal 2
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement2',mat[6])
write_ic(flow[0],trans[3],'scement2')
#Ballast 1
zmin = zmax
zmax = zmin + small*10. #50 m
write_polygon('ballast1',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast1',mat[8])
write_ic(flow[0],trans[3],'ballast1')
#Cement seal 3
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement3',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement3',mat[6])
write_ic(flow[0],trans[3],'scement3')
#Bentonite seal 2
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('bentonite2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('bentonite2',mat[7])
write_ic(flow[0],trans[2],'bentonite2')
#Cement seal 4
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement4',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement4',mat[6])
write_ic(flow[0],trans[3],'scement4')
#Ballast 2
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('ballast2',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast2',mat[8])
write_ic(flow[0],trans[3],'ballast2')
#Cement seal 5
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement5',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement5',mat[6])
write_ic(flow[0],trans[3],'scement5')
#Bentonite seal 3
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('bentonite3',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('bentonite3',mat[7])
write_ic(flow[0],trans[2],'bentonite3')
#Cement seal 6
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement6',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement6',mat[6])
write_ic(flow[0],trans[3],'scement6')
#Ballast 3
zmin = zmax
zmax = zmin + small*10. #~50 m
write_polygon('ballast3',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast3',mat[8])
write_ic(flow[0],trans[3],'ballast3')
#Cement seal 7 (top)
zmin = zmax
zmax = zmin + small*20.
write_polygon('scement7',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('scement7',mat[6])
write_ic(flow[0],trans[3],'scement7')
#Additional seal we'll do entirely with ballast for now
zmin = zmax
zmax = ztop
write_polygon('ballast4',xmin,ymin,zmin,zmax,radius,angle,9)
write_strata('ballast4',mat[8])
write_ic(flow[0],trans[3],'ballast4')

#Waste Package - Cs
xmin = (xwest+xeast)/2. #- r_wp
ymin = ysouth
radius = r_wp
for k in range(num_cs):
  if (k <= 39): #below cement plug
    zmin = z1 + z_wp*k
  else: #above cement plug
    zmin = z1 + z_wp*k + z_cement_ez
  zmax = zmin + z_wp
  name = 'cs_wp'+str(k)
  write_polygon(name,xmin,ymin,zmin,zmax,radius,angle,9)
  write_strata(name,mat[2])
  write_ss(name,flow[1],trans[3],name)
  wfg.cs_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

zsr = zmax
#Waste Package - Sr
for k in range(num_sr-1): #very last Sr wp is the one that gets stuck, subtract it
  if (k <= 5): #below cement plug, totally not automated
    zmin = zsr + z_wp*k
  else: #above cement plug
    zmin = zsr + z_wp*k + z_cement_ez
  zmax = zmin + z_wp
  name = 'sr_wp'+str(k)
  write_polygon(name,xmin,ymin,zmin,zmax,radius,angle,9)
  write_strata(name,mat[3])
  write_ss(name,flow[2],trans[3],name)
  wfg.sr_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

#Waste Package - Stuck (Sr)
zmin = zstuck
zmax = zmin + z_wp
name = 'sr_wp_stuck'
write_polygon(name,xmin,ymin,zmin,zmax,radius,angle,9)
write_strata(name,mat[3])
write_ss(name,flow[2],trans[3],name)
wfg.sr_to_file_reg(wfgfile,name) #this writes a REGION instead of COORDINATE

rfile.close
sfile.close
ssfile.close
wfgfile.close


