# -*- coding: utf-8 -*-
"""
5.25.16 - Pull waste package breach times from pflotran-wf_mass-*.dat
Assume first time at which canister vitality = 0 is the breach time.
Plot histogram of breach times.

11.22.16 update to read new waste package name, so no repeats
When 2016drepo/prob4 is done running then get rid of fn defs in here
and import read_obs_points as rop (after hg pull -u)
or not because I need more then the first three words in column header

08.11.19 do not confuse this with standard split and read functions in
python-scripts repo - these are adapted to waste form files 9.wf)
@author: ergiamb (based on one of Payton's scripts)
"""

import string
import pickle
import pdb
#from pflotran_tools import ReadPflotranObsPt,SplitPflotranObsPtMultipt
import time
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

def SplitPflotranWF(obsptfile='pflotran-34.wf',num_fields=59):
    '''reads in a file (could be obs point file) with multiple observation points 
       and returns a numpy rec array with names given by header in original file.
       num_fields = number of fields associated with each point
       calculate numpoints from length of header and num_fields
    '''
    ################################################################################
    # read in the output observation point
    ################################################################################    
    #set_trace();
    f = file(obsptfile,'r');
    ll = f.readlines();
    f.close();
    #first we need to deal with that nast ass header
    header = ll[0];
    llh = header.split(',');
    numpoint = (len(llh)-1)/num_fields;
    llh_dict = {}
    lldict = {}
    for i in xrange(numpoint):
        istart = 1+i*num_fields 
        iend = 1+(i+1)*num_fields
        llh_dict[i]=[llh[0]] + llh[istart:iend]
        ll_i = []
        ll_i.append(string.join(llh_dict[i],sep=',')+'\n')
        lldict[i] = ll_i
    for line in ll[1::]:
        lli=line.split();
        for i in xrange(numpoint):
            istart = 1+i*num_fields 
            iend = 1+(i+1)*num_fields
            lldict[i].append(string.join([lli[0]]+lli[istart:iend])+'\n')
    for i in xrange(numpoint):
        hh = obsptfile.split('.')[0]+'_'+str(i)+'.wf'; #special version for wf
        f = file(hh,'w')
        f.writelines(lldict[i])
        f.close()
    return numpoint     

def ReadPflotranObsPt(obsptfile='pflotran-34.wf'):
    '''reads in the observation point file from plotran and returns a numpy rec array.  
   With names given by the header in observation point file.'''
    ################################################################################
    # read in the output observation point
    ################################################################################    
    #set_trace();
    f = file(obsptfile,'r');
    ll = f.readlines();
    f.close();
    #first we need to deal with that nast ass header
    header = ll[0];
    llh = header.split(',');
    fm = [];
    ll2 = [];
    ll2.append(ll[0]);
    
    for line in ll[1::]:
        j=0;
        li = line.split(); 
        for field in li:
            if len(field.split('E'))==1:
                #set_trace();
                if len(field.split('-'))==2:
                    field = field.split('-')[0][0:-1] + 'E-' + field.split('-')[1];
                if len(field.split('+'))==2:
                    field = field.split('+')[0][0:-1] +'E+' + field.split('+')[1];    
                li[j]=field;
            j=j+1;
        line = string.join(li);
        line = line+'\n'
        ll2.append(line);
    manglefile = 'observation_pg.tec'
    f = file(manglefile,'w');
    f.writelines(ll2);
    f.close();
    #make the names and formats to define the rec array
    for i in range(len(llh)):
        lh_i = llh[i].strip();    
        lh_i = lh_i.strip('"');
        llh[i]= string.join(lh_i.split(' ')[0:3]);  #just keep the first part of the original
        #lh[i]=lh_i.split(' ')[0];
        fm.append('f8');
    
    dd = np.loadtxt(manglefile,skiprows=1,dtype={'names':llh,'formats':fm});
    return dd

ncores = 1024
nfields = 59
wps = []
breachtime = []
for i in range(ncores):
  try:
    print('looking for pflotran-{}.wf'.format(i))
    npoint = SplitPflotranWF(obsptfile='pflotran-'+str(i)+'.wf',num_fields=nfields)
    for j in range(npoint):
      #look for duplicate waste packages
      f = file('pflotran-'+str(i)+'_'+str(j)+'.wf','r')
      wpname = f.readline().split()[-1] #wow! this works
      f.close()
      duplicate = False
      for k in range(len(wps)):
        if (wpname == wps[k]):
          duplicate = True
      if not duplicate:
        wps.append(wpname)
        dd = ReadPflotranObsPt(obsptfile='pflotran-'+str(i)+'_'+str(j)+'.wf')
        if (dd['WF Canister Vitality'][np.argmin(dd['WF Canister Vitality'])] <= 0.):
          breachtime.append(dd['Time [y]'][np.argmin(dd['WF Canister Vitality'])])
          #len(breachtime) < len(wps) because not all wps breach
  except IOError:
    print('That one does not exist.')

time = np.array(breachtime)
fig = plt.figure(figsize=(8,8)) #adjust size to fit plots (width, height)
#plt.subplot(1,2,1)
nbins = 100
plt.hist(time,nbins,facecolor='green', alpha=0.5)
plt.xlabel('Waste Package Breach Time (y)')
plt.ylabel('Number of Waste Packages')
plt.xlim(0.,np.max(time))
#plt.subplot(1,2,2)
plt.hist(time,nbins,histtype='step',cumulative='True',color='violet')
plt.subplots_adjust(left=0.15)
plt.savefig('wp_breach.pdf')
plt.show()

#save breach times to a file
f = file('wp_breach.txt','w')
f.write('#Waste Package Breach Times (y)\n')
for i in range(len(time)):
  f.write('%s\n' % str(time[i]))
f.close()
