'''
Write 1D tails for explicit unstructured grid (.uge)
Requires cell centers and volumes; face centers and areas (connections)
Also nice to have some way of visualizing, so write .h5 and .xmf for Paraview

@author Emily Stein
Sandia National Laboratories
1.13.17
'''

#I should separate reading input from the calculation of the tail (into 2 f'ns)
#so that I can call the tail fn from abaqus2explicit.py
#without ever writing the original explicit grid file.
#perhaps. need to think about the organizational effort involved.

import numpy as np
import os
import sys
import h5py
import write_xmf as wxmf

lateral_connections = False
#if True, connect tail to adjacent buffer cells as well
#assumes wp is lengthwise in y
half_volume = True 
#if True, save half the volume for the parent cell
wp_volume = True
#if True, use exact wp volume for tip of tail
whole_area = True
#if True, use whole surface area of cylinder/prism shells at connections

def make_tail():
  coord = [17.5,12.5,37.5] #3d center to attach it to
  nx = 16               #number of intervals to discretize it
  shape = 'PRISM' #'PRISM','CYLINDER','SPHERE'
  length = 5.           #length (m) of the cylinder or prism
  if wp_volume:
    wpvol = (5./3.)**2.*length #size of 12 pwr in typical pa discretization

  ugename = '3dgrid.uge'   #in file containing main grid in uge format
  domainname = '3dgrid-domain.h5' #in file containing main grid for Paraview
  if whole_area == True and lateral_connections == True:
    print('Cannot use whole area and lateral connections at same time.')
    return
  #warning! all possibilities are not covered
  if half_volume == True and wp_volume == True and whole_area == False:
    print('will use half_volume and wp_volume options at the same time.')
    tailname = ugename.split('.')[0]+'-%s%i-hv.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-hv.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-hv.xmf' % (shape.lower(),nx)
  elif half_volume == True and wp_volume == True and whole_area == True:
    print('will use half_volume and wp_volume options at the same time.')
    tailname = ugename.split('.')[0]+'-%s%i-hvw.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-hvw.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-hvw.xmf' % (shape.lower(),nx)
  elif lateral_connections == True and half_volume == False:
    print('lateral_connections == True, will write lateral connections')
    tailname = ugename.split('.')[0]+'-%s%i-lat.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-lat.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-lat.xmf' % (shape.lower(),nx)
  elif half_volume == True and lateral_connections == False:
    print('half_volume == True, will save half volume for parent cell')
    tailname = ugename.split('.')[0]+'-%s%i-half.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-half.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-half.xmf' % (shape.lower(),nx)
  elif half_volume ==True and lateral_connections == True:
    print('lateral_connections == True, will write lateral connections')
    print('half_volume == True, will save half volume for parent cell')
    tailname = ugename.split('.')[0]+'-%s%i-hl.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-hl.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-hl.xmf' % (shape.lower(),nx)
  elif wp_volume == True and lateral_connections == False:
    print('wp_volume == True, will make wp volume as specified')
    tailname = ugename.split('.')[0]+'-%s%i-wp.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-wp.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-wp.xmf' % (shape.lower(),nx)
  elif wp_volume == True and lateral_connections == True:
    print('wp_volume == True, will make wp volume as specified')
    print('lateral_connections == True, will write lateral connections')
    tailname = ugename.split('.')[0]+'-%s%i-wpl.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i-wpl.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i-wpl.xmf' % (shape.lower(),nx)
  else:
    tailname = ugename.split('.')[0]+'-%s%i.uge' % (shape.lower(),nx)
    h5name = ugename.split('.')[0]+'-%s%i.h5' % (shape.lower(),nx)
    xmfname = ugename.split('.')[0]+'-%s%i.xmf' % (shape.lower(),nx)

  cells = [] #[id, x, y, z, volume]
  connections = [] #[[id1, id2, x, y, z, area]]
  infile = open(ugename,'r')
  while(1):
    line = infile.readline()
    if line == '': #empty string is end of file
      print('At end of file, big loop')
      break
    if line.startswith('CELLS'):
      while(True):
        line = infile.readline()
        if line.startswith('CONNECTIONS'): #then we've read all the cells
          break
        w = line.split()
        cells.append([int(w[0]),float(w[1]),float(w[2]),float(w[3]),float(w[4])])
        if len(cells) % 100 == 0:
          print('Cell %d' % len(cells))
    else: #Already read the line that startswith('CONNECTIONS')
      while(True):
        line = infile.readline()
        if line == '': #empty string is end of file
          print('At end of file, small loop')
          break
        w = line.split()
        connections.append([int(w[0]),int(w[1]),float(w[2]),float(w[3]),float(w[4]),float(w[5])])
        if len(connections) % 100 == 0:
          print('Connections %d' % len(connections))
  infile.close()

  print('Find volume and id of parent cell')
  for cell in cells:
    if cell[1:4] == coord:
      totvol = cell[4]
      parent = cell[0] #id number not list index!
      #break #yuck - have to do full loop because of lateral_connections search
    if lateral_connections == True:
      adjcoord1 = [coord[0],coord[1]-5.,coord[2]]
      adjcoord2 = [coord[0],coord[1]+5.,coord[2]]
      if cell[1:4] == adjcoord1:
        adj1 = cell[0]
      if cell[1:4] == adjcoord2:
        adj2 = cell[0]
  #Find radius of equivalent shape, discretize, and find volumes, etc.
  print('Discretize tail')
  firsttail = len(cells) #zero-indexed, not id number
  firstcon = len(connections)
  cellid = firsttail + 1
  sa = totvol/length
  if wp_volume == True:
    sa_wp = wpvol/length
  if half_volume == True:
    sa_half = totvol/2./length
  if wp_volume and half_volume:
    if wpvol > totvol/2.:
      print('wpvol > half volume - exiting')
      return
  if shape.upper() == 'CYLINDER': #volume is the same whether you choose prism or cylinder!
    print('CYLINDER')             #but the surface area is smaller and the radius is greater
    r = np.sqrt(sa/np.pi)
    print('radius = %f' % r)
    if half_volume == True:
      r = np.sqrt(sa_half/np.pi)
    if wp_volume == True:
      r_wp = np.sqrt(sa_wp/np.pi)
      print('wp radius = %f' % r_wp)
  elif shape.upper() == 'PRISM':
    print('PRISM')
    r = np.sqrt(sa)/2.
    if half_volume == True:
      r = np.sqrt(sa_half)/2.
    if wp_volume == True:
      r_wp = np.sqrt(sa_wp)/2.
      print('wp radius = %f' % r_wp)
  elif shape.upper() == 'SPHERE':
    print('SPHERE')
    r = np.cbrt(3./4.*totvol/np.pi)
    if half_volume == True:
      r = np.cbrt(3./4.*totvol/2./np.pi)
    if wp_volume == True:
      r_wp = np.cbrt(3./4.*wpvol/np.pi)
      print('wp radius = %f' % r_wp)
  if half_volume == True and wp_volume == False:
    nx_tail = nx - 1
    nhalf = 2*nx_tail
    dhalf = r/float(nhalf)
    dx = 2.*dhalf
  elif wp_volume == True and half_volume == False:
    nx_tail = nx - 1
    nhalf = 2*nx_tail-1
    dhalf = (r-r_wp)/float(nhalf)
    dx = 2.*dhalf
  elif wp_volume and half_volume:
    nx_tail = nx - 2
    dx = (r-r_wp)/float(nx_tail)
    dhalf = dx/2.
  else:
    nhalf = 2*nx-1
    dhalf = r/float(nhalf)
    dx = 2.*dhalf
  print('radius = %f' % r)
  print('dx = %f' % dx)
  tailvol = 0.
  if lateral_connections == True:
    lateral_area = []
  for i in range(nx-1):
    if i == 0:
      xc = cells[parent-1][1] + r
    else:
      if wp_volume == True:
        xc = cells[parent-1][1] + r-(r_wp+dhalf+dx*(i-1))
      else:
        xc = cells[parent-1][1] + r-(dhalf+dx*i)
    yc = cells[parent-1][2]
    zc = cells[parent-1][3]
    if wp_volume == True:
      if i == 0: rcon = r_wp
      else: rcon = r_wp + dx*i
    else:
      rcon = dx*(i+1)
    if shape.upper() == 'CYLINDER':
      vol = np.pi*(rcon**2.)*length - tailvol
    elif shape.upper() == 'PRISM':
      vol = ((2.*rcon)**2.*length) - tailvol
    elif shape.upper() == 'SPHERE':
      vol = 4./3.*np.pi*rcon**3.
    cells.append([cellid,xc,yc,zc,vol])
    xc = cells[parent-1][1] + (r-rcon) #distance from body of tail
    yc = cells[parent-1][2]
    zc = cells[parent-1][3]
    if shape.upper() == 'CYLINDER':
      if whole_area:
        area = 2.*rcon*np.pi*length + np.pi*rcon**2.*2
      else:
        area = 2.*rcon*np.pi*length #connection area does not include ends
      if lateral_connections == True:
        if i == 0:
          lateral_area.append(rcon**2.*np.pi)
        else:
          lateral_area.append(rcon**2.*np.pi-sum(lateral_area[:-1]))
    elif shape.upper() == 'PRISM':
      if whole_area:
        area = 4.*length*(2.*rcon) + 2*(2.*rcon)**2.
      else:
        area = 4.*length*(2.*rcon) #connection area does not include ends
      if lateral_connections == True:
        if i == 0:
          lateral_area.append((rcon*2.)**2.)
        else:
          lateral_area.append((rcon*2.)**2.-sum(lateral_area[:-1]))
    elif shape.upper() == 'SPHERE':
      area = 4.*np.pi*rcon**2.
      if lateral_connections == True:
        print('lateral_connections not supported for SPHERE')
    if i < nx-2: #connect to next tail cell
      connections.append([cellid,cellid+1,xc,yc,zc,area])
    else: #connect to parent
      connections.append([cellid,parent,xc,yc,zc,area])
    cellid += 1
    tailvol += vol
    print('tailvol = %f at i = %i' %(tailvol,i))
  #modify volume of body cell
  cells[parent-1][4] -= tailvol
  if lateral_connections == True:
    if shape.upper() == 'PRISM' or shape.upper() == 'CYLINDER':
      print('Add lateral connections.')
      for i in range(firsttail,len(cells)):
        area = lateral_area[i-firsttail] 
        xc = (cells[i][1] + cells[adj1-1][1])/2. #this is probably terrible way to figure x
        yc = (cells[i][2] + cells[adj1-1][2])/2. #this should work fine for y
        zc = (cells[i][3] + cells[adj1-1][3])/2. #zc should be the same for both
        connections.append([i+1,adj1,xc,yc,zc,area])
        xc = (cells[i][1] + cells[adj2-1][1])/2. #this is probably terrible way to figure x
        yc = (cells[i][2] + cells[adj2-1][2])/2. #this should work fine for y
        zc = (cells[i][3] + cells[adj2-1][3])/2. #zc should be the same for both
        connections.append([i+1,adj2,xc,yc,zc,area])

  #Write .uge file, original plus tail
  print('Write %s' % (tailname))
  f = open(tailname,'w')
  f.write('CELLS %d\n' % len(cells))
  for cell in cells:
    f.write('%d %f %f %f %f\n' % (cell[0],cell[1],cell[2],cell[3],cell[4]))
  f.write('CONNECTIONS %d\n' % len(connections))
  for con in connections:
    f.write('%d %d %f %f %f %f\n' % (con[0],con[1],con[2],con[3],con[4],con[5]))
  f.close()
  #Print tail values to screen
  print('CellID XCenter Volume TailVol CellID XConnection Area')
  icon = firstcon #zero-indexed 
  tailvol = 0. #over again, totally inefficient
  for i in range(firsttail,len(cells)):
    tailvol += cells[i][4]
    print('%i %f %f %f %i %f %f' % (cells[i][0],cells[i][1],cells[i][4],tailvol,
                                 connections[icon][0],connections[icon][2],connections[icon][5]))
    icon += 1
  
  #Assign cell corners (nodes/vertices) and write .h5 and .xmf for Paraview
  #Inspired by WIPP 2D flaring grid
  tid = []
  txc = []
  tyc = []
  tzc = []
  tvol = []
  tvertices = []
  telements = []
  npoint = 0
  icon = firstcon
  for i in range(firsttail,len(cells)):
    #number of cons in tail is the same as number of cells, so:
    side = connections[icon][5]/dx
    y1 = connections[icon][3] - side/2.
    y2 = y1 + side
    p1 = [connections[icon][2],y1,connections[icon][4]]    #because neglecting the length
    p2 = [connections[icon][2]+dx,y1,connections[icon][4]] #not showing volume only area
    p3 = [connections[icon][2]+dx,y2,connections[icon][4]] #not showing volume only area
    p4 = [connections[icon][2],y2,connections[icon][4]]    #because neglecting the length
    tvertices.extend([p1,p2,p3,p4])
    telements.extend([5,npoint,npoint+1,npoint+2,npoint+3])
    txc.append(cells[i][1])
    tyc.append(cells[i][2])
    tzc.append(cells[i][3])
    tvol.append(cells[i][4])
    tid.append(i+1) #pretty sure this captures the correct order, because why wouldn't they be in order?
    npoint += 4
    icon += 1
  #write h5 file with all cells (bodies and tails) in it
  #open original domain file,read to arrays
  h5in = h5py.File(domainname,'r')
  Vertices = np.array(h5in['Domain/Vertices'])
  Cells = np.array(h5in['Domain/Cells'])
  Cell_Ids = np.array(h5in['Domain/Cell_Ids'])
  XC = np.array(h5in['Domain/XC'])
  YC = np.array(h5in['Domain/YC'])
  ZC = np.array(h5in['Domain/ZC'])
  Volume = np.array(h5in['Domain/Volume'])
  h5in.close()
  #add tail values to the end of all the arrays
  for i in range(len(telements)): #vertex ids in tail need to be upped
    if(i%5 != 0):  #don't change the 5s!
      telements[i] += len(Vertices)
  Vertices2 = np.append(arr=Vertices,axis=0,values=tvertices)
  Cells2 = np.append(arr=Cells,axis=0,values=telements)
  Cell_Ids2 = np.append(arr=Cell_Ids,axis=0,values=tid)
  XC2 = np.append(arr=XC,axis=0,values=txc)
  YC2 = np.append(arr=YC,axis=0,values=tyc)
  ZC2 = np.append(arr=ZC,axis=0,values=tzc)
  Volume2 = np.append(arr=Volume,axis=0,values=tvol)
  #then write with tails h5 file
  print('Write %s' % h5name)
  h5f = h5py.File(h5name,'w')
  h5f.create_dataset('Domain/Vertices',data=Vertices2)
  h5f.create_dataset('Domain/Cells',data=Cells2)
  h5f.create_dataset('Domain/Cell_Ids',data=Cell_Ids2)
  h5f.create_dataset('Domain/XC',data=XC2)
  h5f.create_dataset('Domain/YC',data=YC2)
  h5f.create_dataset('Domain/ZC',data=ZC2)
  h5f.create_dataset('Domain/Volume',data=Volume2)
  h5f.close()   
  #and new xmf file pointing to tails h5 file
  print('Write %s' % xmfname)
  xmf = open(xmfname,'w')
  xmf.write(wxmf.header_template)
  xmf.write(wxmf.topo_template % (len(cells),len(Cells2),h5name.split('.')[0]))
  xmf.write(wxmf.geo_template % (len(Vertices2),h5name.split('.')[0]))
  xmf.write(wxmf.XYZ_template % ('XC','Scalar','Cell',len(XC2),h5name.split('.')[0],'Domain','XC'))
  xmf.write(wxmf.XYZ_template % ('YC','Scalar','Cell',len(YC2),h5name.split('.')[0],'Domain','YC'))
  xmf.write(wxmf.XYZ_template % ('ZC','Scalar','Cell',len(ZC2),h5name.split('.')[0],'Domain','ZC'))
  xmf.write(wxmf.XYZ_template % ('Volume','Scalar','Cell',len(Volume2),h5name.split('.')[0],'Domain','Volume'))
  xmf.write(wxmf.XYZ_template % ('Cell_Ids','Scalar','Cell',len(Cell_Ids2),h5name.split('.')[0],'Domain','Cell_Ids'))
  xmf.write(wxmf.tail_template)
  xmf.close()

  print('done')
  return

if __name__ == "__main__":
  make_tail()

